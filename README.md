# Taskmatch.ie 
Made with ❤️ Laravel 5.

Website: https://taskmatch.ie

# What is TaskMatch? 
TaskMatch is Ireland's first community marketplace for people and businesses to outsource tasks, find local services or hire flexible staff in minutes


## Installation Steps

### 1. Require the Package

After taking pull run the following command: 

```bash
composer update
```

### 2. Add the DB Credentials & APP_URL

Next make sure to create a new database and add your database credentials to your .env file:

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

You will also want to update your website URL inside of the `APP_URL` variable inside the .env file:

```
APP_URL=http://localhost:8000
```

> Only if you are on Laravel 5.4 will you need to [Add the Service Provider.]

### 3. Run The Migration

Lastly, Run the php artisan migration

```bash
php artisan mograte
```
