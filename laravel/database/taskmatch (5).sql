-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2017 at 01:02 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmatch`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `type`, `type_id`, `description`, `deleted`, `status`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'task_create', 1, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'http://www.taskmatch.ie/uploads/users/profile/7121504108826.jpg\\\' alt = \\\'John Bynre\\\' title=\\\'John Bynre\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">John Bynre posted <a href=\\\'http://www.taskmatch.ie/task/t180E4523qI9g131\\\'>Assemble Ikea Furniture </a></div>', 0, 1, '2017-08-30 17:15:28', NULL, 2),
(2, 'task_create', 2, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'http://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Paul  Kennedy posted <a href=\\\'http://www.taskmatch.ie/task/203AW9oG15g487H1\\\'>Paint a two bed apartment</a></div>', 0, 1, '2017-08-31 11:17:19', NULL, 3),
(3, 'task_create', 3, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/6621504185045.jpg\\\' alt = \\\'Matthew Toman\\\' title=\\\'Matthew Toman\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Matthew Toman posted <a href=\\\'https://www.taskmatch.ie/task/511zLe55103gn4H8\\\'>Window cleaner</a></div>', 0, 1, '2017-08-31 13:12:15', NULL, 5),
(4, 'task_create', 4, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/6.png\\\' alt = \\\'Patrick  Fagan\\\' title=\\\'Patrick  Fagan\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Patrick  Fagan posted <a href=\\\'https://www.taskmatch.ie/task/329420m1A5b7a405\\\'>Gardening Needed</a></div>', 0, 1, '2017-09-01 08:24:30', NULL, 6),
(5, 'task_create', 5, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/7.png\\\' alt = \\\'Nicole Duffy\\\' title=\\\'Nicole Duffy\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Nicole Duffy posted <a href=\\\'https://www.taskmatch.ie/task/g02i502K514m8E4w\\\'>Promo work in Dublin</a></div>', 0, 1, '2017-09-01 08:33:22', NULL, 7),
(6, 'task_create', 6, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/8.png\\\' alt = \\\'Tom  Foley\\\' title=\\\'Tom  Foley\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Tom  Foley posted <a href=\\\'https://www.taskmatch.ie/task/j48185872n0952xh\\\'>Clean my Air bnb apartment </a></div>', 0, 1, '2017-09-01 09:41:12', NULL, 8),
(7, 'task_create', 7, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/9.png\\\' alt = \\\'Jen Walsh\\\' title=\\\'Jen Walsh\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Jen Walsh posted <a href=\\\'https://www.taskmatch.ie/task/i0962Yg3V014t45O\\\'>Moving apartment </a></div>', 0, 1, '2017-09-01 10:06:34', NULL, 9),
(8, 'task_create', 8, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/10.png\\\' alt = \\\'Brian Quinn\\\' title=\\\'Brian Quinn\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Brian Quinn posted <a href=\\\'https://www.taskmatch.ie/task/356THl2Q001G6452\\\'>Labourer needed</a></div>', 0, 1, '2017-09-01 10:42:43', NULL, 10),
(9, 'task_create', 9, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Paul  Kennedy posted <a href=\\\'https://www.taskmatch.ie/task/145z0eoB5636a369\\\'>Build a wordpress website</a></div>', 0, 1, '2017-09-02 12:46:36', NULL, 3),
(10, 'task_create', 10, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/6.png\\\' alt = \\\'Patrick  Fagan\\\' title=\\\'Patrick  Fagan\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Patrick  Fagan posted <a href=\\\'https://www.taskmatch.ie/task/01xS466215jI9Sc5\\\'>Electrician Needed</a></div>', 0, 1, '2017-09-04 09:11:36', NULL, 6),
(11, 'task_create', 11, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'http://www.taskmatch.ie/uploads/users/profile/6651504731289.jpg\\\' alt = \\\'Cecilia Boyadjian\\\' title=\\\'Cecilia Boyadjian\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Cecilia Boyadjian posted <a href=\\\'http://www.taskmatch.ie/task/8F173415l0f7E4d1\\\'>Cleaner needed</a></div>', 0, 1, '2017-09-06 21:01:54', NULL, 35),
(12, 'task_create', 12, '<div class=\\\'recent__usr__img\\\'>\r\n                      \r\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3631504732368.jpg\\\' alt = \\\'Keith Wallace\\\' title=\\\'Keith Wallace\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Keith Wallace posted <a href=\\\'https://www.taskmatch.ie/task/58w314n7WY48X02k\\\'>Light needs fixing</a></div>', 0, 1, '2017-09-06 21:14:48', NULL, 38),
(13, 'task_create', 13, '<div class=\\\'recent__usr__img\\\'>\r\n            <img class=\\\'img-circle\\\' src =\\"https://www.taskmatch.ie/front/images/default.png\\" alt = \\\'TaskMatch Admin\\\' title = \\\'TaskMatch Admin\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">TaskMatch Admin posted <a href=\\\'https://www.taskmatch.ie/task/2745189i8490ot4P\\\'>This is a testing task</a></div>', 0, 1, '2017-09-08 19:00:44', NULL, 1),
(14, 'task_create', 13, '<div class=\\\'recent__usr__img\\\'>\r\n            <img class=\\\'img-circle\\\' src =\\"http://localhost/projects/laravel/taskmatch/web/front/images/default.png\\" alt = \\\'Task Poster\\\' title = \\\'Task Poster\\\' />\r\n    </div>\r\n<div class=\\"recent__usr__cont\\">Task Poster posted <a href=\\\'http://localhost/projects/laravel/taskmatch/web/task/y10V8t45i55923h4\\\'>Golpik Create Marketplace</a></div>', 0, 1, '2017-09-15 14:04:20', NULL, 74);

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assigns`
--

CREATE TABLE `assigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_price_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `card_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assigns`
--

INSERT INTO `assigns` (`id`, `offer_price_id`, `deleted`, `status`, `created_at`, `updated_at`, `card_id`) VALUES
(1, 1, 0, 1, '2017-08-31 12:06:31', '2017-08-31 12:06:31', NULL),
(2, 2, 0, 1, '2017-09-01 09:23:10', '2017-09-01 09:23:10', NULL),
(3, 3, 0, 1, '2017-09-04 08:59:11', '2017-09-04 08:59:11', NULL),
(4, 4, 0, 1, '2017-09-09 13:45:32', '2017-09-09 13:45:32', NULL),
(5, 4, 0, 1, '2017-09-09 13:45:36', '2017-09-09 13:45:36', NULL),
(6, 6, 0, 1, '2017-09-15 16:26:40', '2017-09-15 16:26:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bank_details`
--

CREATE TABLE `bank_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `accountTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iban` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bankName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `url` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `description`, `url`, `created_at`, `updated_at`, `status`, `image`) VALUES
(1, 'Home & Cleaning ', '', 'https://www.taskmatch.ie/tasks?cat_id=2', '2017-08-31 13:30:49', '2017-08-31 13:31:46', 1, '7951504186301.png'),
(2, 'Delivery & Removals', '', '', '2017-08-31 13:32:14', '2017-08-31 13:32:14', 1, ''),
(3, 'Handyman & Trades', '', 'https://www.taskmatch.ie/tasks?cat_id=8', '2017-08-31 13:32:38', '2017-08-31 13:32:51', 1, '6201504186371.png'),
(4, 'Marketing & Design', '', '', '2017-08-31 13:33:12', '2017-08-31 13:33:12', 1, ''),
(5, 'Events & Photography', '', '', '2017-08-31 13:33:34', '2017-08-31 13:33:34', 1, ''),
(6, 'Computer & IT', '', '', '2017-08-31 13:33:59', '2017-08-31 13:33:59', 1, ''),
(7, 'Business & Admin', '', '', '2017-08-31 13:35:51', '2017-08-31 13:35:51', 1, ''),
(8, 'Fun & Quirky', '', '', '2017-08-31 13:36:10', '2017-08-31 13:36:10', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `tags` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `teaser` text NOT NULL,
  `comments` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `keywords` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `created_at`, `updated_at`, `status`, `tags`, `user_id`, `name`, `description`, `category_id`, `image`, `teaser`, `comments`, `likes`, `keywords`, `metaDescription`, `url`) VALUES
(1, '2017-08-31 14:01:13', '2017-09-02 18:03:28', 1, 'Renovation Checklist', 1, 'Renovation Checklist: Everything you Need to Know', '<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.taskmatch.ie/blog/post/1"><img alt="Share on Facebook" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/facebook-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="Facebook" /></a><a href="https://twitter.com/home?status=Renovation%20Checklist%3A%20Everything%20you%20Need%20to%20Know%0Ahttps%3A//www.taskmatch.ie/blog/post/1"><img alt="Tweet about this on Twitter" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/twitter-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="Twitter" /></a><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//www.taskmatch.ie/blog/post/1&amp;title=Renovation%20Checklist%3A%20Everything%20you%20Need%20to%20Know&amp;summary=&amp;source="><img alt="Share on LinkedIn" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/linkedin-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="LinkedIn" /></a></p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Deciding to renovate is a massive decision because there&rsquo;s so much to consider like planning, finding trades and costs. We all have lots screenshots of beautiful homes and a dream-like Pinterest board that we love, but let&rsquo;s be honest, most of us will also be working to a budget.&nbsp;So with all this in mind, it&rsquo;s important to create a renovation checklist to avoid becoming overwhelmed or going over budget.&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">So let&rsquo;s get started and go through what should be on your renovation checklist.</p>\r\n\r\n<h2 style="box-sizing: border-box; line-height: 32px; font-weight: 700; color: rgb(58, 61, 62); letter-spacing: 0.019999999552965164px; font-size: 30px; max-width: 620px; margin: 50px auto 17px; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;">Planning and researching your renovations</h2>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Narrow it down&nbsp;</span><br style="box-sizing: border-box;" />\r\nWhen you&rsquo;re looking to renovate your home, you need to think about whether you&rsquo;re going to be truly ok with all the noise, dust and not to mention being without a kitchen or bathroom for a while. You can either narrow it down to smaller areas so that you can move to different rooms as the renovation progresses, or if it&rsquo;s a knock down you&rsquo;ll have no choice to move in with family or into an Airbnb (which let&rsquo;s face it, you don&rsquo;t want to do that longer than you absolutely have to).&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">If you&rsquo;re budget renovating, the best way is to try to do as much of it yourself and conquer smaller areas so that you don&rsquo;t have to move out and can save costs.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Collect ideas&nbsp;</span><br style="box-sizing: border-box;" />\r\nGather all the ideas you love and order by room via a Pinterest board, on your phone or even a magazine clippings folder. This will help you think about what you want to do and also develop a consistent theme that will flow throughout your home.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">In your room-by-room considerations, collate a list of the specific issues (e.g. dampness), recommendations and options you have considered.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Once you have all of your inspo, pick up sample paint colours that you can try on the walls, carpet clippings and timbers to see how it works with the paint colours, and also look at the different permanent fixtures you want such as lights in person. You might decide to go with a different look as you&rsquo;re inspired by other ideas in store.&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Finalise renovation budget and priorities</span><br style="box-sizing: border-box;" />\r\nCalculate what your maximum spend is but also put an additional budget together for any extras that you initially forgot &ndash; you might think you&rsquo;re fine but all renovations tend to go over budget. Next, prioritise the areas most important to you (or that need the most attention) and that you use the most (such as the bathrooms and kitchen). If you can afford to renovate those rooms, they should be the first you look at.&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Decide who will complete your renovations</span><br style="box-sizing: border-box;" />\r\nFinding a tradesman who can do a great job and isn&rsquo;t going to hurt your wallet is tough but personal recommendations from family or friends are your best starting point.&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Arrange to meet with a few different builders to run them through your plans, so that afterwards they can give you a quote and also possible start dates as many of them are booked out months in advance.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Material quotes</span><br style="box-sizing: border-box;" />\r\nGet quotes for your all of your materials to know what the end cost will be. Don&rsquo;t forget to shop around and also haggle to get a better price. Your tradesman might also be able to help with discounts or industry connections.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2 style="box-sizing: border-box; line-height: 32px; font-weight: 700; color: rgb(58, 61, 62); letter-spacing: 0.019999999552965164px; font-size: 30px; max-width: 620px; margin: 50px auto 17px; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;">The renovation begins!</h2>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Read the fine print&nbsp;</span><br style="box-sizing: border-box;" />\r\nMake sure you carefully review the paperwork (i.e. contracts) supplied by your builder and other tradesmen. Iron out any ambiguities before you sign the dotted line.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Before you get too far into the renovation process, don&rsquo;t forget to also avoid these top renovation mistakes.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Get the home ready</span><br style="box-sizing: border-box;" />\r\nIf you&rsquo;re living at home during the renovations, you may need to re-jig your living arrangements during this time. Think about the best way to manage this. If you are having your bathrooms renovated, you may need to hire a portable shower and toilet unless you have some lovely neighbours!</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">If you&rsquo;re moving out during the renovation, think about your options &ndash; including living with family or friends, or short-term (hopefully) leasing.</p>\r\n\r\n<h2 style="box-sizing: border-box; line-height: 32px; font-weight: 700; color: rgb(58, 61, 62); letter-spacing: 0.019999999552965164px; font-size: 30px; max-width: 620px; margin: 50px auto 17px; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;">Renovation complete</h2>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">First, Sigh with relief! Second, go through your house and contract thoroughly to make sure that everything has been delivered as initially agreed.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">If anything is missing, simply make a list and go through it with the person you contracted. It&rsquo;s always better to be safe than sorry, so make sure you keep all your documents, agreements and contracts in a safe place just in case anything happens down the track.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Don&rsquo;t forget to celebrate your renovations and enjoy your amazing new space.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://www.taskmatch.ie">www.taskmatch.ie</a></p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt="" src="https://www.taskmatch.ie//front/images/logo.png" style="width: 111px; height: 25px;" /></p>\r\n', 1, '1801504274882.jpg', 'Before you pick up the tools - double check you\'ve got these things sorted.', 0, 0, 'renovate, trades, renovations, ', 'Renovation Checklist: Everything you Need to Know', 'https://www.taskmatch.ie/blog/post/1'),
(2, '2017-08-31 14:47:44', '2017-09-01 13:53:59', 1, 'Handyman', 1, 'Top 5 Handyman Tips', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a class="ssba_facebook_share" data-site="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.taskmatch.ie/blog/post/2" style="box-sizing: border-box; background-color: transparent; -webkit-text-decoration-skip: objects; text-decoration: none; color: rgb(0, 143, 180); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px; text-align: center;" target="_blank">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt="Share on Facebook" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/facebook-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="Facebook" /></a><a class="ssba_twitter_share" data-site="" href="https://twitter.com/home?status=Top%205%20Handyman%20Tips%0Ahttps%3A//www.taskmatch.ie/blog/post/2" style="box-sizing: border-box; background-color: transparent; -webkit-text-decoration-skip: objects; text-decoration: none; color: rgb(0, 143, 180); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px; text-align: center;" target="_blank"><img alt="Tweet about this on Twitter" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/twitter-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="Twitter" /></a><a class="ssba_linkedin_share ssba_share_link" data-site="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//www.taskmatch.ie/blog/post/2&amp;title=Renovation%20Checklist%3A%20Everything%20you%20Need%20to%20Know&amp;summary=&amp;source=" style="box-sizing: border-box; background-color: transparent; -webkit-text-decoration-skip: objects; text-decoration: none; color: rgb(0, 143, 180); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px; text-align: center;" target="_blank"><img alt="Share on LinkedIn" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/linkedin-blog.png" style="box-sizing: border-box; border-style: none; max-width: 100%; height: auto; margin-left: 5px; width: 34px;" title="LinkedIn" /></a></p>\r\n\r\n<hr />\r\n<div class="entry-content" style="box-sizing: border-box; max-width: 720px; margin: 0px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">Some people are born with handyman skills, others have great mates to help out and if you&rsquo;re like me I try to tackle things myself. But what seems like a simple fix often goes from bad to worse, sometimes much worse.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">It&rsquo;s no secret that <a href="https://www.taskmatch.ie/tasks?cat_id=8">handyman help</a>&nbsp;tasks are one of the most popular on Taskmatch and there have been quite a variety from assembling furniture and painting, to building an outdoors deck and mounting new T.V&rsquo;s.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">From the tasks posted, to research and a bit of experience, I&rsquo;ve managed to put together a couple of helpful tips and tricks to share with you. Here are my top 5 handyman tips:</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;"><span style="box-sizing: border-box;">Tip 1: Prepare for the job</span><br style="box-sizing: border-box;" />\r\nYou might think it&rsquo;s a simple half a days work, but soon enough your whole weekend has gone and you&rsquo;re still at square one. So just incase you do burst that pipe you didn&rsquo;t know was there, always plan for the worst and allow yourself some extra time.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;"><span style="box-sizing: border-box;">Tip 2: Quality tools</span><br style="box-sizing: border-box;" />\r\nDon&rsquo;t skimp on quality tools- buy, borrow or even rent from <a href="http://www.hirehere.ie">hirehere.ie</a>.&nbsp;Already got the tools? Well make sure you keep them in top-notch condition by giving them a quick wipe of an oiled rag (not soap and water) to remove dirt and rust.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;"><span style="box-sizing: border-box;">Tip 3: Make a list and beat the rush</span><br style="box-sizing: border-box;" />\r\nBefore you go buy everything you don&rsquo;t need, do your research, make a list and don&rsquo;t leave the list at home (better yet type it into your phone).</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">Also, if you know you&rsquo;re going to need in-store help visit the hardware store on a weekday rather than a busy weekend.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;"><span style="box-sizing: border-box;">Tip 4: DIY Tricks</span><br style="box-sizing: border-box;" />\r\nIt&rsquo;s often said that you can do it the easy way or the hard way, but here&rsquo;s some tricks to do it the smart way.<br style="box-sizing: border-box;" />\r\n&bull; Tie multiple extension cords in a knot to keep from separating.<br style="box-sizing: border-box;" />\r\n&bull; Pour soda water on a rusted bolt to loosen it.<br style="box-sizing: border-box;" />\r\n&bull; Use a Post-It note to catch dust when drilling.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;"><span style="box-sizing: border-box;">Tip 5: Get a professional</span><br style="box-sizing: border-box;" />\r\nIf you don&rsquo;t want to face the same problem in a few months make sure you&rsquo;re getting the best person for the job. It should be someone who has a good track record, whether it&rsquo;s a <a href="http://www.taskmatch.ie/tasks">t</a><a href="https://www.taskmatch.ie/tasks">askmatch worker</a> or a professional, check his or her certifications, references and reviews</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">Have a chat to them and see if you&rsquo;re comfortable about using them to help with the task. You should ask them if they&rsquo;ve done this type of work before and if they&rsquo;re able to complete it on time.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto;">So there you have our top 5 handyman tips, have you got a handyman tip? Let us know your best tip by commenting below.</p>\r\n</div>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://www.taskmatch.ie">www.taskmatch.ie</a></p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt="" src="https://www.taskmatch.ie//front/images/logo.png" style="width: 111px; height: 25px;" /></p>\r\n', 1, '8851504263283.jpg', 'Some people are born with handyman skills, others have great mates to help out and if you’re like me I', 0, 0, 'Handyman, TaskMatch', 'Top 5 Handyman Tips', 'https://www.taskmatch.ie/blog/post/2'),
(3, '2017-09-01 11:46:49', '2017-09-01 12:06:36', 1, 'Wall mount', 1, 'How To Wall-mount Your TV', '<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a class="ssba_facebook_share" data-site="" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.taskmatch.ie/blog/post/3" style="box-sizing: border-box; background-color: transparent; color: rgb(0, 143, 180); text-decoration: none; cursor: pointer; font-variant-ligatures: normal; orphans: 2; widows: 2; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.02px; text-align: center; outline: none !important; transition: all 0.3s ease 0s !important; background-position: 0px 0px;" target="_blank"><img alt="Share on Facebook" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/facebook-blog.png" style="box-sizing: border-box; border: 0px none; vertical-align: middle; max-width: 100%; height: auto; position: relative; margin-left: 5px; width: 34px; transition: all 0.3s ease 0s !important;" title="Facebook" /></a><a class="ssba_twitter_share" data-site="" href="https://twitter.com/home?status=Top%205%20Handyman%20Tips%0Ahttps%3A//www.taskmatch.ie/blog/post/3" style="box-sizing: border-box; background-color: transparent; color: rgb(0, 143, 180); text-decoration: none; cursor: pointer; font-variant-ligatures: normal; orphans: 2; widows: 2; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.02px; text-align: center; outline: none !important; transition: all 0.3s ease 0s !important; background-position: 0px 0px;" target="_blank"><img alt="Tweet about this on Twitter" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/twitter-blog.png" style="box-sizing: border-box; border: 0px none; vertical-align: middle; max-width: 100%; height: auto; position: relative; margin-left: 5px; width: 34px; transition: all 0.3s ease 0s !important;" title="Twitter" /></a><a class="ssba_linkedin_share ssba_share_link" data-site="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A//www.taskmatch.ie/blog/post/3&amp;title=Renovation%20Checklist%3A%20Everything%20you%20Need%20to%20Know&amp;summary=&amp;source=" style="box-sizing: border-box; background-color: transparent; color: rgb(0, 143, 180); text-decoration: none; cursor: pointer; font-variant-ligatures: normal; orphans: 2; widows: 2; font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.02px; text-align: center; outline: none !important; transition: all 0.3s ease 0s !important; background-position: 0px 0px;" target="_blank"><img alt="Share on LinkedIn" class="ssba ssba-img" src="https://s3-ap-southeast-2.amazonaws.com/airtasker-email-assets/partnership_program/linkedin-blog.png" style="box-sizing: border-box; border: 0px none; vertical-align: middle; max-width: 100%; height: auto; position: relative; margin-left: 5px; width: 34px; transition: all 0.3s ease 0s !important;" title="LinkedIn" /></a></p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">You&rsquo;ve purchased your new TV and bought the bracket but stuck wondering how to mount the TV on your wall. Well here&rsquo;s a handy guide of important steps not to skip when mounting it on your freshly painted wall.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Step 1: Bracket</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Having the correct bracket is crucial. Ask the shop assistant at the same time when you buy the TV, they know their technology and are trained to help.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Step 2: Cables</span><br style="box-sizing: border-box;" />\r\nDeciding what you&rsquo;re going to do with the cables is the tricky part and you have three options:</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">1. Run the cables in the wall &ndash; You will need to know what type of wall it is e.g. internal plaster, stud, brick or even the ceiling.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><br style="box-sizing: border-box;" />\r\n2. Use a cable concealer &ndash; easy and you can paint it the same colour as the wall.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><br style="box-sizing: border-box;" />\r\n3. Nothing, just leave them hanging &ndash; not the prettiest but definitely the easiest.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Which ever you choose make sure you buy longer cables than you think you&rsquo;ll need, you don&rsquo;t want to stretch them. Also if you&rsquo;re hooking up an entertainment center why not buy coloured cables so you can easily tell them apart.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Step 3: Locate the studs</span><br style="box-sizing: border-box;" />\r\nWhile using a stud finder is useful it&rsquo;s not 100%, so if you are getting inconsistent results look for existing power points &ndash; remove the wall plate and use a tool to find which side the stud is on.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">&ndash; Once you&rsquo;ve found the stud, mark it with pencil (or tape if you have it).<br style="box-sizing: border-box;" />\r\n&ndash; Measure the vertical distance of the screw holes.<br style="box-sizing: border-box;" />\r\n&ndash; Take a step back and look at how high you want the TV<br style="box-sizing: border-box;" />\r\n&ndash; Mark where each screw will go, double check that it&rsquo;s level and at the right height.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Step 4: Attach mount and hide cables</span><br style="box-sizing: border-box;" />\r\nScrew the mount to the wall and check again that it&rsquo;s level, trust us you don&rsquo;t want to discover that it&rsquo;s not later. This is also the perfect time to drill a hole to hide your cables &ndash; remember to look out for the studs and know what&rsquo;s behind your wall.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><span style="box-sizing: border-box;">Step 5: Hang TV</span><br style="box-sizing: border-box;" />\r\nNow it&rsquo;s the fun part, get someone to help you place the TV onto the mount carefully following manufacturer&rsquo;s instructions. Make sure you double check it&rsquo;s stability and that it&rsquo;s firm in place by giving it a few light tugs.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Then hook up the cables put some popcorn in the microwave and you&rsquo;re ready to watch the latest movie on your brand-new wall mounted TV &ndash; and doesn&rsquo;t it look awesome.</p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;"><i style="box-sizing: border-box;">P.S. Don&rsquo;t forget if your needing an extra hand you can always find&nbsp;a&nbsp;<a href="https://www.taskmatch.ie/tasks?cat_id=8" style="box-sizing: border-box; background-color: transparent; -webkit-text-decoration-skip: objects; text-decoration: none; color: rgb(0, 143, 180);">Local Handyman</a>&nbsp;on Taskmatch</i><i style="box-sizing: border-box;">.</i></p>\r\n\r\n<p style="box-sizing: border-box; max-width: 620px; margin: 20px auto; color: rgb(58, 61, 62); font-family: museo_sans, \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; font-size: 21px; letter-spacing: 0.019999999552965164px;">Got any other tips and tricks? Help out a mate and share them below.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://www.taskmatch.ie">www.taskmatch.ie</a></p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt="" src="https://www.taskmatch.ie//front/images/logo.png" style="width: 111px; height: 25px;" /></p>\r\n', 1, '5951504266409.jpg', 'You’ve purchased your new TV and bought the bracket but stuck wondering how to mount the TV on your wall.', 0, 0, 'TV, Wall Mounted, How to', 'How to Wall mount TV', 'https://www.taskmatch.ie/blog/post/3');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` text,
  `image` varchar(255) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `description` text,
  `teaser` varchar(255) DEFAULT NULL,
  `sortOrder` int(11) DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1',
  `keywords` varchar(255) NOT NULL,
  `metaTitle` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci';

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`, `url`, `image`, `class`, `description`, `teaser`, `sortOrder`, `status`, `keywords`, `metaTitle`) VALUES
(1, 'HOME', 0, '2017-03-06 15:33:16', '2017-04-04 12:22:15', NULL, '5911489074951.jpg', 'fa fa-home', NULL, 'See our Apparel products for your best Style.', NULL, 1, '', NULL),
(2, ' Home & Cleaning', 1, '2017-03-06 15:34:10', '2017-06-02 06:16:39', NULL, '1511494599780.jpg', 'fa fa-home', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Routine Clean</b></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;10/hour&nbsp;</span></strong></p>\r\n\r\n<p>Apartment or house</p>\r\n\r\n<p>Supplies provided</p>\r\n</blockquote>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Deeper Clean</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;12/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Windows or other items</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Most supplies provided</p>\r\n</blockquote>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Spring Clean</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;15/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Major clean up</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Specialist tools needed</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(4, 'Online', 0, '2017-03-06 15:37:06', '2017-04-04 12:31:22', NULL, '3191489072613.jpg', 'fa fa-internet-explorer', NULL, '', NULL, 1, '', NULL),
(5, ' Computer & IT', 4, '2017-03-27 14:32:50', '2017-06-02 06:17:37', NULL, '3961494599806.jpg', 'fa fa-laptop', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Basic panel example</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;10/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Simple tech issue or website tweak</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Basic skills required</p>\r\n</blockquote>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Basic panel example</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;20/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Software / website tech help</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Computer skills needed</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Basic panel example</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;25/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Programming, development etc</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Professional skills needed</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(6, 'MISC', 0, '2017-04-04 12:03:52', '2017-05-19 12:10:37', NULL, '4661495195837.png', 'fa fa-random', '', '', NULL, 1, '', NULL),
(7, ' Delivery & Removals', 1, '2017-04-04 12:06:52', '2017-06-02 06:54:22', NULL, '5761494599853.jpg', 'fa fa-truck', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="font-size: 13px; box-sizing: border-box;" text-align:="">Small Item</b><span font-size:="" josefin="" style="font-size: 13px;" text-align:="">&nbsp;</span></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;10/hour&nbsp;</span></strong></p>\r\n\r\n<p>Easy to carry</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">On foot/Public transport Ok</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Larger Item</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;20/hour</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Larger box or package</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Car required</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Moving/Removals</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><span font-size:="" josefin="" style="font-family: " text-align:=""><strong>&euro;35/hour</strong>&nbsp;</span></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Furniture or Heavy Items</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Truck or Van required</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(8, ' Handyman & Trades', 1, '2017-04-04 12:07:53', '2017-06-02 06:49:36', NULL, '5791494599864.jpg', 'fa fa-wrench', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Basic</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;10/hour</span></strong></p>\r\n\r\n<p>No specialist skills required</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Basic tools only</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Skilled</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;15/hour</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Some special skills required</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Some tools needed</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Professional</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;35/hour</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Professional skills required</p>\r\n\r\n<p>Specific tools needed</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(9, 'Marketing & Design', 4, '2017-04-04 12:24:44', '2017-06-02 06:50:42', NULL, '2471494599881.jpg', 'fa fa-pencil-square-o', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Basic Design</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;10/hour&nbsp;</span></strong></p>\r\n\r\n<p>Simple tweaks or photoshop</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Basic design knowledge</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Photography /Graphics</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;20/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Photoshoot or brand design</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Some equipment required</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Design Projects</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;25/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Major brand / photo project</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Specialist skills or equipment</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(10, 'Business & Admin', 4, '2017-04-04 12:29:49', '2017-06-02 06:51:39', NULL, '7711494599894.jpg', 'fa fa-bar-chart', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box;" text-align:="">Basic Tasks</b><span font-size:="" josefin="" text-align:="">&nbsp;</span></h3>\r\n\r\n<p><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;12/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Simple research or data entry</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">No specialist skills required</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Semi skilled tasks</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;16/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Spreadsheet or call centre work</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Basic admin skills</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Specialist Tasks</b><span font-size:="" josefin="" style="font-family: " text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong><span font-size:="" josefin="" style="font-family: " text-align:="">&euro;25/hour&nbsp;</span></strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Acconting or Marketing work</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Some specialist skills needed</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(11, 'Events &  Photography', 6, '2017-04-04 12:37:26', '2017-06-02 06:52:57', NULL, '1261494599913.jpg', 'fa fa-camera-retro', '<blockquote>\r\n<h3><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Leaflet Handouts</b></h3>\r\n\r\n<p><strong>&euro;10/hour&nbsp;</strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Letterbox drops, flyer handouts etc</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Simple, friendly staff</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Event / Sales</b></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong>&euro;15/hour&nbsp;</strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Promotional or event help</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Basic sales or catering skills</p>\r\n</blockquote>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">&nbsp;</p>\r\n\r\n<blockquote>\r\n<h3 style="box-sizing: border-box; cursor: default;"><b cursor:="" font-size:="" roboto="" style="box-sizing: border-box; font-family: " text-align:="">Specialist Staff</b><span font-size:="" josefin="" text-align:="">&nbsp;</span></h3>\r\n\r\n<p style="box-sizing: border-box; cursor: default;"><strong>&euro;20/hour&nbsp;</strong></p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Trades eg plumbing, electrical</p>\r\n\r\n<p style="box-sizing: border-box; cursor: default;">Special tools or licence needed</p>\r\n</blockquote>\r\n', '', NULL, 1, '', NULL),
(12, ' Fun & Quirky', 6, '2017-04-04 12:38:30', '2017-05-12 14:38:49', NULL, '8281494599929.jpg', 'fa fa-hand-peace-o', '', '', NULL, 1, '', NULL),
(13, ' Misc.', 6, '2017-04-04 12:39:25', '2017-06-02 06:11:49', NULL, '1991495195849.png', 'fa fa-cogs', '', '', NULL, 1, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `task_id`, `deleted`, `status`, `created_at`, `updated_at`, `file`) VALUES
(1, 'Hi I could do it on Saturday if interested ?', 1, 6, 1, 1, '2017-09-06 22:00:49', '2017-09-06 22:01:14', ''),
(2, 'What size is the apartment? do you want walls, ceilings and woodword painted?', 63, 2, 0, 1, '2017-09-08 14:16:23', '0000-00-00 00:00:00', ''),
(3, 'can you give more details please.  How many windows? How many floors do the properties have? Inside or outside or both?', 63, 3, 0, 1, '2017-09-08 14:46:08', '0000-00-00 00:00:00', ''),
(4, 'Testing Comment!!!', 75, 13, 0, 1, '2017-09-15 15:20:55', '0000-00-00 00:00:00', ''),
(5, 'Again Testing!!!', 75, 13, 0, 1, '2017-09-15 15:31:59', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `code` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `teaser` varchar(255) DEFAULT NULL,
  `body` text NOT NULL,
  `language_id` varchar(10) NOT NULL,
  `metaTitle` varchar(255) DEFAULT NULL,
  `metaDescription` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `deleted` int(2) DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `type`, `code`, `title`, `subject`, `teaser`, `body`, `language_id`, `metaTitle`, `metaDescription`, `keywords`, `created_at`, `updated_at`, `status`, `deleted`, `image`, `url`) VALUES
(1, 'page', 'paypal_success', 'Your Payment was successful', 'sad', 'dsa', '<p>\r\nThank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You will receive your first issue of the Eurofish magazine when the next issue is relased. As a paying subscriber you will also receive access to our online version for FREE. This will be sent to the email address you provided. We will contact you before your subscription expires to ensure that you receive the magazine without interruption.\r\nIf you have any comments or questions please do not hesitate to contact us:\r\n</p>\r\n \r\n<p>Tel.: +45 333 777 55</p>\r\n<p>Fax: +45 333 777 56</p>\r\n<p>info@eurofishmagazine.com</p>\r\n<p>Thanks again and have a great day.</p>', 'en_uk', 'asd', '', 'Valentine\'s Day Outfits', '2016-02-24 12:14:19', '2016-10-06 12:14:31', 1, 0, '6451456317215.jpg', 'paypal-success'),
(2, 'page', 'paypal_cancel', 'Failure ', '', '', 'Please Try Again', '', '', '', '', NULL, '2016-10-06 12:14:55', 1, 0, NULL, 'paypal-cancel'),
(3, 'email', 'order_confirmation', 'Your Order is being processed.', 'Your Order is being processed.', NULL, '<p>Dear %%NAME%%</p>\n<p>\nYour order number is "%%ID%%".\n<p>Thank you for online purchase.\n This the the confirmation and summary of you order, which is current being processed.</p>\n<p>For your convenience, you will able to track the status of you online orders and manage you account by visiting the "My Account" section of our website.</p>\n<p>Regards,</p>\n<p>newcenturylabs</p>', '', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL),
(11, 'page', 'home', 'Home', NULL, '', '<!-- end:fh5co-header -->\r\n<div class="fh5co-hero">\r\n<div class="fh5co-overlay">&nbsp;</div>\r\n\r\n<div class="fh5co-cover" data-stellar-background-ratio="0.5" style="background-image: url({{asset(\'\')}}front/images/home-image.jpg);">\r\n<div class="desc animate-box">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-7">\r\n<h2>Fitness &amp; Health<br />\r\nis a <b>Mentality</b></h2>\r\n\r\n<p><span>Created with by the fine folks at <a class="fh5co-site-name" href="http://freehtml5.co">FreeHTML5.co</a></span></p>\r\n<span><a class="btn btn-primary" href="#">Start Your Journey</a></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- end:fh5co-hero -->\r\n\r\n<div class="fh5co-lightgray-section" id="fh5co-schedule-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2">\r\n<div class="heading-section text-center animate-box">\r\n<h2>Class Schedule</h2>\r\n\r\n<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row animate-box">\r\n<div class="col-md-10 col-md-offset-1 text-center">\r\n<ul class="schedule">\r\n	<li><a class="active" data-sched="sunday" href="#">Sunday</a></li>\r\n	<li><a data-sched="monday" href="#">Monday</a></li>\r\n	<li><a data-sched="tuesday" href="#">Tuesday</a></li>\r\n	<li><a data-sched="wednesday" href="#">Wednesday</a></li>\r\n	<li><a data-sched="thursday" href="#">Thursday</a></li>\r\n	<li><a data-sched="monday" href="#">Monday</a></li>\r\n	<li><a data-sched="saturday" href="#">Saturday</a></li>\r\n</ul>\r\n</div>\r\n\r\n<div class="row text-center">\r\n<div class="col-md-12 schedule-container">\r\n<div class="schedule-content active" data-day="sunday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="monday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="tuesday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="wednesday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="thursday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="friday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="saturday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content --></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="fh5co-lightgray-section" id="fh5co-schedule-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2">\r\n<div class="heading-section text-center animate-box">\r\n<h2>Class Schedule</h2>\r\n\r\n<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row animate-box">\r\n<div class="col-md-10 col-md-offset-1 text-center">\r\n<ul class="schedule">\r\n	<li><a class="active" data-sched="sunday" href="#">Sunday</a></li>\r\n	<li><a data-sched="monday" href="#">Monday</a></li>\r\n	<li><a data-sched="tuesday" href="#">Tuesday</a></li>\r\n	<li><a data-sched="wednesday" href="#">Wednesday</a></li>\r\n	<li><a data-sched="thursday" href="#">Thursday</a></li>\r\n	<li><a data-sched="monday" href="#">Monday</a></li>\r\n	<li><a data-sched="saturday" href="#">Saturday</a></li>\r\n</ul>\r\n</div>\r\n\r\n<div class="row text-center">\r\n<div class="col-md-12 schedule-container">\r\n<div class="schedule-content active" data-day="sunday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="monday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="tuesday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="wednesday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="thursday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="friday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content -->\r\n\r\n<div class="schedule-content" data-day="saturday">\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Body Building</h3>\r\n<span>John Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Yoga Programs</h3>\r\n<span>James Smith</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Cycling Program</h3>\r\n<span>Rita Doe</span></div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="program program-schedule"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" /> <small>06AM-7AM</small>\r\n\r\n<h3>Boxing Fitness</h3>\r\n<span>John Dose</span></div>\r\n</div>\r\n</div>\r\n<!-- END sched-content --></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="fh5co-parallax" data-stellar-background-ratio="0.5" style="background-image: url({{asset(\'\')}}front/images/home-image-3.jpg);">\r\n<div class="overlay">&nbsp;</div>\r\n\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">\r\n<div class="fh5co-intro fh5co-table-cell animate-box">\r\n<h1 class="text-center">Commit To Be Fit</h1>\r\n\r\n<p>Made with love by the fine folks at <a href="http://freehtml5.co">FreeHTML5.co</a></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- end: fh5co-parallax -->\r\n\r\n<div id="fh5co-programs-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2">\r\n<div class="heading-section text-center animate-box">\r\n<h2>Our Programs</h2>\r\n\r\n<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row text-center">\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-dumbell.svg" />\r\n<h3>Body Combat</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="" src="{{asset(\'\')}}front/images/fit-yoga.svg" />\r\n<h3>Yoga Programs</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="" src="{{asset(\'\')}}front/images/fit-cycling.svg" />\r\n<h3>Cycling Program</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="Cycling" src="{{asset(\'\')}}front/images/fit-boxing.svg" />\r\n<h3>Boxing Fitness</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="" src="{{asset(\'\')}}front/images/fit-swimming.svg" />\r\n<h3>Swimming Program</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="program animate-box"><img alt="" src="{{asset(\'\')}}front/images/fit-massage.svg" />\r\n<h3>Massage</h3>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n<span><a class="btn btn-default" href="#">Join Now</a></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="fh5co-lightgray-section" id="fh5co-team-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2">\r\n<div class="heading-section text-center animate-box">\r\n<h2>Meet Our Trainers</h2>\r\n\r\n<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row text-center">\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-1.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>John Doe</h3>\r\n<span>Body Trainer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-2.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>James Smith</h3>\r\n<span>Swimming Trainer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-3.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>John Doe</h3>\r\n<span>Chief Executive Officer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-4.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>John Doe</h3>\r\n<span>Chief Executive Officer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-5.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>John Doe</h3>\r\n<span>Chief Executive Officer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-4 col-sm-6">\r\n<div class="team-section-grid animate-box" style="background-image: url({{asset(\'\')}}front/images/trainer-6.jpg);">\r\n<div class="overlay-section">\r\n<div class="desc">\r\n<h3>John Doe</h3>\r\n<span>Chief Executive Officer</span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\r\n\r\n<p class="fh5co-social-icons">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="fh5co-parallax" data-stellar-background-ratio="0.5" style="background-image: url({{asset(\'\')}}front/images/home-image-2.jpg);">\r\n<div class="overlay">&nbsp;</div>\r\n\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-6 col-md-offset-3 col-md-pull-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 fh5co-table">\r\n<div class="fh5co-intro fh5co-table-cell box-area">\r\n<div class="animate-box">\r\n<h1>Fitness Classes this summer</h1>\r\n\r\n<p>Pay now and get 25% Discount</p>\r\n<a class="btn btn-primary" href="#">Become A Member</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- end: fh5co-parallax -->\r\n\r\n<div class="fh5co-pricing fh5co-lightgray-section" id="fh5co-pricing-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-8 col-md-offset-2">\r\n<div class="heading-section text-center animate-box">\r\n<h2>Pricing Plan</h2>\r\n\r\n<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row">\r\n<div class="pricing">\r\n<div class="col-md-3 animate-box">\r\n<div class="price-box animate-box">\r\n<h2 class="pricing-plan">Starter</h2>\r\n\r\n<div class="price"><sup class="currency">$</sup>9<small>/month</small></div>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<ul class="classes">\r\n	<li>15 Cardio Classes</li>\r\n	<li class="color">10 Swimming Lesson</li>\r\n	<li>10 Yoga Classes</li>\r\n	<li class="color">20 Aerobics</li>\r\n	<li>10 Zumba Classes</li>\r\n	<li class="color">5 Massage</li>\r\n	<li>10 Body Building</li>\r\n</ul>\r\n<a class="btn btn-default" href="#">Select Plan</a></div>\r\n</div>\r\n\r\n<div class="col-md-3 animate-box">\r\n<div class="price-box animate-box">\r\n<h2 class="pricing-plan">Basic</h2>\r\n\r\n<div class="price"><sup class="currency">$</sup>27<small>/month</small></div>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<ul class="classes">\r\n	<li>15 Cardio Classes</li>\r\n	<li class="color">10 Swimming Lesson</li>\r\n	<li>10 Yoga Classes</li>\r\n	<li class="color">20 Aerobics</li>\r\n	<li>10 Zumba Classes</li>\r\n	<li class="color">5 Massage</li>\r\n	<li>10 Body Building</li>\r\n</ul>\r\n<a class="btn btn-default" href="#">Select Plan</a></div>\r\n</div>\r\n\r\n<div class="col-md-3 animate-box">\r\n<div class="price-box animate-box popular">\r\n<h2 class="pricing-plan pricing-plan-offer">Pro <span>Best Offer</span></h2>\r\n\r\n<div class="price"><sup class="currency">$</sup>74<small>/month</small></div>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<ul class="classes">\r\n	<li>15 Cardio Classes</li>\r\n	<li class="color">10 Swimming Lesson</li>\r\n	<li>10 Yoga Classes</li>\r\n	<li class="color">20 Aerobics</li>\r\n	<li>10 Zumba Classes</li>\r\n	<li class="color">5 Massage</li>\r\n	<li>10 Body Building</li>\r\n</ul>\r\n<a class="btn btn-select-plan btn-sm" href="#">Select Plan</a></div>\r\n</div>\r\n\r\n<div class="col-md-3 animate-box">\r\n<div class="price-box animate-box">\r\n<h2 class="pricing-plan">Unlimited</h2>\r\n\r\n<div class="price"><sup class="currency">$</sup>140<small>/month</small></div>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n\r\n<ul class="classes">\r\n	<li>15 Cardio Classes</li>\r\n	<li class="color">10 Swimming Lesson</li>\r\n	<li>10 Yoga Classes</li>\r\n	<li class="color">20 Aerobics</li>\r\n	<li>10 Zumba Classes</li>\r\n	<li class="color">5 Massage</li>\r\n	<li>10 Body Building</li>\r\n</ul>\r\n<a class="btn btn-default" href="#">Select Plan</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div id="fh5co-blog-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-6">\r\n<div class="col-md-12">\r\n<div class="heading-section animate-box">\r\n<h2>Recent from Blog</h2>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-12 col-md-offset-0">\r\n<div class="fh5co-blog animate-box">\r\n<div class="inner-post"><a href="#"><img alt="" class="img-responsive" src="{{asset(\'\')}}front/images/blog-1.jpg" /></a></div>\r\n\r\n<div class="desc">\r\n<h3><a href="">Starting new session of body building this summer</a></h3>\r\n<span class="posted_by">Posted by: Admin</span> <span class="comment"><a href="">21</a></span>\r\n\r\n<p>Far far away, behind the word mountains</p>\r\n<a class="btn btn-default" href="#">Read More</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-12 col-md-offset-0">\r\n<div class="fh5co-blog animate-box">\r\n<div class="inner-post"><a href="#"><img alt="" class="img-responsive" src="{{asset(\'\')}}front/images/blog-1.jpg" /></a></div>\r\n\r\n<div class="desc">\r\n<h3><a href="">Starting new session of body building this summer</a></h3>\r\n<span class="posted_by">Posted by: Admin</span> <span class="comment"><a href="">21</a></span>\r\n\r\n<p>Far far away, behind the word mountains</p>\r\n<a class="btn btn-default" href="#">Read More</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-6">\r\n<div class="col-md-12">\r\n<div class="heading-section animate-box">\r\n<h2>Upcoming Events</h2>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-12 col-md-offset-0">\r\n<div class="fh5co-blog animate-box">\r\n<div class="meta-date text-center">\r\n<p><span class="date">14</span><span>June</span><span>2016</span></p>\r\n</div>\r\n\r\n<div class="desc desc2">\r\n<h3><a href="">Starting new session of body building this summer</a></h3>\r\n<span class="posted_by">Posted by: Admin</span> <span class="comment"><a href="">21</a></span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n<a class="btn btn-default" href="#">Read More</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-12 col-md-offset-0">\r\n<div class="fh5co-blog animate-box">\r\n<div class="meta-date text-center">\r\n<p><span class="date">13</span><span>June</span><span>2016</span></p>\r\n</div>\r\n\r\n<div class="desc desc2">\r\n<h3><a href="">Starting new session of body building this summer</a></h3>\r\n<span class="posted_by">Posted by: Admin</span> <span class="comment"><a href="">21</a></span>\r\n\r\n<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>\r\n<a class="btn btn-default" href="#">Read More</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', '', '', '', '', NULL, '2017-01-31 09:33:59', 1, 0, NULL, '/'),
(12, 'page', 'aboutus', 'About Us', NULL, '', '<section class="inr-bnr-area clrlist">\r\n<div class="container0">\r\n<div class="inr__bnr__img"><img alt="about" src="front/images/about-bnr.jpg" /></div>\r\n\r\n<div class="inr__bnr__cont valigner col-sm-8 col-sm-offset-1">\r\n<div class="valign">\r\n<h2 class="anime-left">Our vision is to make your life easier</h2>\r\n\r\n<h4 class="anime-left delay1s">We realise everyone needs a helping hand sometimes.<br />\r\nThats why we are here to help</h4>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="about-area">\r\n<div class="container">\r\n<div class="about__box col-sm-12">\r\n<div class="about__lft col-sm-6">\r\n<div class="about__lft__cont">\r\n<h3>About Us</h3>\r\n\r\n<p>Our number one ambition at TaskMatch is to build a trusted online community. We know how busy life can be so whether you need some help with a task or are looking to earn extra money we are here to help.</p>\r\n\r\n<p>Whether you are moving apartments, installing a TV or creating your new company logo we will match you with right person to complete your task.</p>\r\n\r\n<p>Read reviews and agree prices all from the comfort of your home. Stop worrying and start living.</p>\r\n\r\n<p>TaskMatch - Outsourcing made easy</p>\r\n</div>\r\n</div>\r\n\r\n<div class="about__rgt col-sm-6">\r\n<div class="about__rgt__cont">\r\n<p>&quot;Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work.<br />\r\n<br />\r\nThe only way to do great work is to love what you do.&quot;</p>\r\n</div>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n', '', 'about-us', 'Home / About Us', 'taskmatch,about us', NULL, '2017-05-02 07:32:42', 1, 0, NULL, 'aboutus'),
(13, 'page', 'how_to_order', 'How To Order', NULL, '', '<section class="page-bnr-area bg-full valigner" style="background-image:url(\'{{asset(\'\')}}front/images/bnr-how.jpg\');">\r\n<div class="container">\r\n<div class="bnr__cont valign white text-center col-sm-12 anime-flipInX">\r\n<h2>DON&rsquo;T JUST THINK YOU&rsquo;RE HEALTHY, <span>KNOW YOU&rsquo;RE HEALTHY</span></h2>\r\n\r\n<p>Ordering tests when you need to know made easy</p>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="how-step-area pt50 pb50">\r\n<div class="container">\r\n<div class="step__cont col-sm-5">\r\n<div class="step__img"><img alt="" src="{{asset(\'\')}}front/images/how1.png" /></div>\r\n</div>\r\n\r\n<div class="step__cont col-sm-7 valigner anime-left">\r\n<div class="step_list clrlist listview valign">\r\n<h3>STEP 1</h3>\r\n\r\n<ul>\r\n	<li>Find your lab test(s) of choice.</li>\r\n	<li>Check out with your credit card.</li>\r\n	<li>Your lab order will be located in your email securely and confidentially.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n\r\n<div class="step__cont col-sm-7 valigner anime-left">\r\n<div class="step_list clrlist listview valign">\r\n<h3>STEP 2</h3>\r\n\r\n<ul>\r\n	<li>Test results on average take 72 hours and will be emailed to you as soon as results are complete. You may print your lab results and take it with you to discuss with your doctor if needed.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n\r\n<div class="step__cont col-sm-5">\r\n<div class="step__img"><img alt="" src="{{asset(\'\')}}/front/images/how3.png" /></div>\r\n</div>\r\n\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n\r\n\r\n<div class="step__cont col-sm-5">\r\n<div class="step__img"><img alt="" src="{{asset(\'\')}}front/images/how2.png" /></div>\r\n</div>\r\n\r\n\r\n\r\n<div class="step__cont col-sm-7 valigner anime-left">\r\n<div class="step_list clrlist listview valign">\r\n<h3>STEP 3</h3>\r\n\r\n<ul>\r\n	<li>Print your lab order and take it with you to one of our 2,300 Patient Service Center locations. Walk-ins are always welcome!</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n\r\n<div class="step__cont col-sm-7 valigner anime-left">\r\n<div class="step_list clrlist listview valign">\r\n<h3>Convenient Ordering</h3>\r\n\r\n<ul>\r\n	<li>Order labs and view results from your desk top or any mobile device.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class="step__cont col-sm-5">\r\n<div class="step__img"><img alt="" src="{{asset(\'\')}}front/images/how4.png" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n', '', '', '', '', NULL, '2017-01-11 12:18:07', 1, 0, NULL, 'how_to_order'),
(14, 'page', 'faq', 'Faq', NULL, '', '<section class="faqs-section m0 p0">\r\n<div class="container m0 p0">\r\n<div class="faqs col-sm-12 m0 p0">\r\n<div class="faqs__inner">\r\n<div class="faqs__inner__bg"><img src="front/images/ipad2c.png" /></div>\r\n\r\n<div class="faqs__inner__bg__cont col-sm-12 m0 p0 text-center">\r\n<h1>Want to Learn More?</h1>\r\n\r\n<p>Find out more about:</p>\r\n<a href="posting-a-task"><button class="posting__task">Posting a Task</button></a>\r\n<a href="earn-with-us"><button class="posting__task">Completing a Task</button></a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="faqs-accordion">\r\n<div class="container-fluid">\r\n<div class="col-sm-6 col-sm-offset-1">\r\n<div class="faqs__accordion__hdng">\r\n<h1>FAQs</h1>\r\n</div>\r\n<button class="accordion">What is TaskMatch? +</button>\r\n\r\n<div class="panel">\r\n<p>TaskMatch is a trusted community marketplace for people to outsource tasks, find local services or complete flexible jobs to earn money online or via mobile.</p>\r\n</div>\r\n<button class="accordion">What tasks can I get done on TaskMatch? +</button>\r\n\r\n<div class="panel">\r\n<p>You can post almost any task you need done on TaskMatch. From tasks around the house (like gardening or cleaning) to office tasks (like data entry or website support) to tasks for business (like promotional staffing or mystery shopper).</p>\r\n</div>\r\n<button class="accordion">How can I get my tasks completed on TaskMatch? +</button>\r\n\r\n<div class="panel">\r\n<p>It&#39;s very simple. You Start by Posting a Task - it&#39;s FREE to post and takes just a few minutes. TaskMatch Workers will then make offers to complete your task. Check out their profile and once you&#39;ve found the TaskMatch worker that&#39;s perfect for your task, accept their offer to assign them to the task.</p>\r\n\r\n<p>When accepting the offer you will Add Funds for the task via TaskMatch Pay, which are securely held in an TaskMatch Trust Account until the task has been completed. This allows the TaskMatch Worker to start working knowing there are Funds secured for the task. Funds are only released to the TaskMatch Worker once both of you are satisfied that the task has been completed.</p>\r\n</div>\r\n<button class="accordion">How much does it cost to post a task? +</button>\r\n\r\n<div class="panel">\r\n<p>It is FREE to post a task. Once you have accepted the agreed task price you will need to Add Funds for the task via TaskMatch Pay, which are securely held in an TaskMatch Trust Account until the task has been completed. You will need to Release Funds upon task completion. There are no additional fees or hidden costs and cash payments are not supported on TaskMatch. (We deduct a Service Fee from the TaskMatch Worker who completes your job.)</p>\r\n</div>\r\n<button class="accordion">Once I post a task, do I have to hire a TaskMatch Worker? +</button>\r\n\r\n<div class="panel">\r\n<p>No, it is FREE to post a task and there are no obligations to select an TaskMatch Worker unless you find the right person for the job and accept their offer.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<script>\r\n\r\nvar acc = document.getElementsByClassName("accordion");\r\nvar i;\r\n\r\nfor (i = 0; i < acc.length; i++) {\r\n    acc[i].onclick = function(){\r\n        /* Toggle between adding and removing the "active" class,\r\n        to highlight the button that controls the panel */\r\n        this.classList.toggle("active");\r\n\r\n        /* Toggle between hiding and showing the active panel */\r\n        var panel = this.nextElementSibling;\r\n        if (panel.style.display === "block") {\r\n            panel.style.display = "none";\r\n        } else {\r\n            panel.style.display = "block";\r\n        }\r\n    }\r\n}\r\n\r\n</script>', '', '', '', '', NULL, '2017-05-02 08:26:29', 1, 0, NULL, 'faq');
INSERT INTO `content` (`id`, `type`, `code`, `title`, `subject`, `teaser`, `body`, `language_id`, `metaTitle`, `metaDescription`, `keywords`, `created_at`, `updated_at`, `status`, `deleted`, `image`, `url`) VALUES
(15, 'page', 'terms', 'Terms', NULL, '', '<section class="terms-conditions">\r\n<div class="container">\r\n<div class="terms__condtn hdng col-sm-12">\r\n<div class="terms__condtn hdng__inner">\r\n<h2>TaskMatch Terms &amp; Conditions</h2>\r\n</div>\r\n</div>\r\n\r\n<div class="acord__sec col-sm-6 col-sm-offset-3">\r\n<div class="terms__inner"><button class="accordion">WWW.TASKMATCH.IE +</button>\r\n\r\n<div class="panel">\r\n<p>The Website and the TaskMatch Service is a website operated by TaskMatch Limited (&quot;TaskMatch&quot;, &quot;we&quot;, &quot;us&quot; or &quot;our&quot;) operates an online platform (&quot;TaskMatch.ie&quot;) allowing users (&quot;User&quot; or &quot;You&quot;) to connect and perform business and personal services and outsource tasks and buy and sell goods through TaskMatch&#39;s platform including through our website at https://www.TaskMatch.ie/, or any other platform we may introduce in the future (&quot;TaskMatch Platform&quot;). We are incorporated in Ireland under company number 580200 and our registered office is The Granary, 8 Cecilia Street, Dublin 2. PLEASE READ THESE TERMS and ALL TASKMATCH POLICIES including the Privacy Policy, the Marketplace Rules, and the Prohibited Services and Items Policy, Service Fee Schedule (collectively the &quot;AGREEMENT&quot;) carefully before using the services offered by TaskMatch. Use of the TaskMatch Platform or use of the TaskMatch Services confirms that You agree to be bound by this Agreement.</p>\r\n</div>\r\n<button class="accordion">SCOPE OF TASKMATCH SERVICE +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch provides a platform for connecting people who have tasks that need doing, or who need to buy a particular item (&quot;Job Posters&quot;) with those willing to do the job or provide the item (&quot;TaskMatch Workers&quot;).</p>\r\n\r\n<p>2.A reference to User in this agreement includes Job Posters, TaskMatch Workers and any other person that visits or views the TaskMatch Platform.</p>\r\n\r\n<p>3.A User creates an account with TaskMatch when s/he validly completes a registration form on the TaskMatch Platform.</p>\r\n\r\n<p>4.TaskMatch provides the platform only. Apart from enabling a Job Poster to find a TaskMatch Worker to provide a particular service or supply a particular good, TaskMatch accepts no liability for any aspect of the Job Poster and TaskMatch Worker interaction, including but not limited to the description of goods and services offered, the performance of services and the delivery of goods. TaskMatch has no obligation to any User to assist or involve itself in any way in any dispute between a Job Poster and a TaskMatch Worker.</p>\r\n\r\n<p>5.All information related to services to be performed or goods to be provided is supplied by TaskMatch Users. TaskMatch does not have any ability or responsibility to review, approve or verify any User provided information prior to publication on the TaskMatch Platform.</p>\r\n\r\n<p>6.You expressly agree that TaskMatch has no responsibility and makes no warranty as to the truth or accuracy of any aspect of any information provided by Users (including all TaskMatch Workers and Job Posters), including, but not limited to, the ability of TaskMatch Workers to perform tasks or supply items, or the honesty or accuracy of any information provided by Job Posters or the Job Posters&rsquo; ability to pay for the services or goods requested.</p>\r\n</div>\r\n<button class="accordion">TASKMATCH SERVICE DESCRIPTION +</button>\r\n\r\n<div class="panel">\r\n<p>1.The TaskMatch Service will be reviewed and updated from time to time. In addition to the points set out below, certain portion(s) of the TaskMatch Service may be separately described on the TaskMatch Platform.</p>\r\n\r\n<p>2.A Job Poster with a requirement for a service to be provided or a good to be acquired (1) creates and account with TaskMatch and (2) posts an accurate and complete description of the good to be acquired or service to be provided (including, but not limited to, the payment offered to the TaskMatch Worker) (&quot;Posted Task&quot;). Posted Tasks are subject to further terms set out for Posting Tasks and Making Offers below.</p>\r\n\r\n<p>3.A TaskMatch Worker with an ability to perform services and/or to provide the goods (1) creates an account with TaskMatch and (2) reviews Posted Tasks uploaded by Job Posters.</p>\r\n\r\n<p>4.If a TaskMatch Worker desires to provide services or sell goods to a Job Poster, the TaskMatch Worker must make an offer in response to the Posted Task (&quot;Offer&quot;). By making an Offer the TaskMatch Worker confirms that s/he is legally entitled to and capable of supplying the goods or services described in the Posted Task. Offers are subject to further terms set out for Posting Tasks and Making Offers below.</p>\r\n\r\n<p>5.If a Job Poster in any way updates a Posted Task after it has been published on the TaskMatch Platform and before an offer has been accepted, then TaskMatch may, at its discretion, cancel all Offers for that Posted Task that were made prior to the update and recommence the offer process.</p>\r\n\r\n<p>6.If the Job Poster agrees to a TaskMatch Worker&#39;s offer, the Job Poster will accept that TaskMatch Worker&#39;s Offer by using the functionality on the TaskMatch Platform and paying the agreed price into the payment facility escrow account accessible via the TaskMatch Platform (&quot;Escrow Account&quot;).</p>\r\n\r\n<p>7.The TaskMatch Worker and Job Poster use a public question and answer feature on the TaskMatch Platform to communicate directly about posted requests for services or goods. Once a Job Poster has accepted an offer from a TaskMatch Worker, the Job Poster and the TaskMatch Worker can communicate privately using a TaskMatch private messaging service. Each time a TaskMatch User receives a message from the TaskMatch messaging service, a notification may be sent to the User via their currently active communication channel.</p>\r\n\r\n<p>8.Once a Job Poster has accepted an offer from a TaskMatch Worker and paid the agreed price into the Escrow Account then the Job Poster and the TaskMatch Worker will be deemed to have entered into a separate contract under which the Job Poster agrees to purchase, and the Buyer agrees to provide the Job Poster with the services or goods (&quot;Task Contract&quot;).</p>\r\n\r\n<p>9.The terms of the Task Contract incorporate the terms of this Agreement (to the extent they apply to the Job Poster and the TaskMatch Worker) and any additional terms and conditions agreed between the Job Poster and the TaskMatch Worker, including the description and price of the services or goods to be provided. You agree not to enter into any contractual provisions in a Task Contract that conflict with this Agreement. The terms of this Agreement incorporated into a Task Contract take priority over any other terms agreed between a Job Poster and a TaskMatch Worker in the Task Contract to the extent of any inconsistency.</p>\r\n\r\n<p>10.The TaskMatch Worker must provide the services or goods to the Job Poster in accordance with the Task Contract, unless the services, goods or transaction is prohibited by law, by this Agreement, an agreement between the User and a Third Party Provider or by any of our Policies.</p>\r\n\r\n<p>11.Once a TaskMatch Worker has provided the goods or services requested by a Job Poster in accordance with the Task Contract, the TaskMatch Worker must provide notice that the task is complete by using the TaskMatch Platform functionality. It will be material breach of this Agreement and the Task Contract if a TaskMatch Worker provides notice that a task is complete for a particular Posted Task without having provided the relevant goods or services. If s/he is supplying goods, a TaskMatch Worker must not provide notice that a task is complete until the goods have actually been delivered to the address specified by the Job Poster.</p>\r\n\r\n<p>12.Once a TaskMatch Worker has provided the relevant goods or services in accordance with the Task Contract and has provided notice that the task is complete relating to a particular Posted Task, the Job Poster must release the agreed price for the goods or services supplied to the TaskMatch Worker by using the TaskMatch Platform functionality. It will be material breach of this Agreement and the Task Contract if, after a TaskMatch Worker has provided the relevant goods or services in accordance with the Task Contract and provided notice that a task is complete for a particular Posted Task, the Job Poster fails to release the agreed price.</p>\r\n\r\n<p>13.Once the Job Poster has released the payment, the fee will be paid from the Escrow Account to the TaskMatch Worker after the deduction of the relevant fees set out in the Service Fee Schedule and described in Fees below (&quot;Service Fees&quot;).</p>\r\n\r\n<p>14.Once a transaction is concluded, both the TaskMatch Worker and Job Poster are strongly encouraged to complete a TaskMatch service review using any feedback features on the TaskMatch Platform. TaskMatch will, from time to time, contact You to remind You to complete a TaskMatch service review.</p>\r\n\r\n<p>15.The TaskMatch Platform may from time to time include location/ map-based functionality. The TaskMatch Platform may display the location of Job Posters and TaskMatch Workers to persons browsing the TaskMatch Platform. Each Job Poster will be asked to provide the street and suburb where the task is to be performed or the goods are to be delivered. A Job Poster should never disclose personal details such as Job Poster&#39;s full name, street number, phone number or email address in a Posted Task or in any other public communication on the TaskMatch Platform.</p>\r\n\r\n<p>16.TaskMatch may from time to time include on the TaskMatch Platform a &quot;Hire Me&quot; feature which enables TaskMatch Users who are browsing other User profiles to post a request for goods and services based on the skills, reputation or other profile attributes of a particular User. When this occurs, the TaskMatch User who the Job Poster would like to make an Offer will be automatically notified (i) of the new Posted Task, and (ii) that the Job Poster would like him/ her to make an Offer to supply the goods or services. However, that User will not have any further special rights and will need to follow the normal offer process to become the TaskMatch Worker for the Posted Task.</p>\r\n\r\n<p>17.From time to time, TaskMatch may enter into agreements with business partners (&quot;Business Partners&quot;) to enable Business Partners to acquire aspects or functions of the TaskMatch Platform, and may seek to engage TaskMatch Workers in the provision of goods or services which the Business Partner will on sell to other parties (such as its customers). For the purposes of these Terms, when a Business Partner is engaged in this way, the Business Partner will be a Job Poster and the task will be a Posted Task for the purposes of these Terms. TaskMatch Workers who agree to perform Posted Tasks for Business Partners acknowledge and agree that TaskMatch and the Business Partner may on sell the goods or services supplied to third parties for an increased fee.</p>\r\n\r\n<p>18.Business Partners may require TaskMatch Workers engaged as contemplated in clause 7 (TaskMatch Service Desciption) to be approved or hold particular qualifications and TaskMatch may be asked by Business Partners to locate potentially suitably qualified TaskMatch Workers. TaskMatch makes no warranty that it will promote any or all suitably qualified TaskMatch Workers to Business Partners.</p>\r\n\r\n<p>19.Before a Business Partner enters into a contract with a TaskMatch Worker, it may require the TaskMatch Worker to enter into additional terms for the engagement (&quot;Business Partner Contract&quot;) before the TaskMatch Worker can undertake any work introduced by a Business Partner.</p>\r\n\r\n<p>20.Where a TaskMatch Worker accepts a Posted Task with a Business Partner: (a) the TaskMatch Worker must provide the goods or services to the Business Partner in accordance with the Task Contract created by these Terms and any applicable Business Partner Contract; and (b) the terms of the Business Partner Contract will prevail to the extent of any inconsistency.</p>\r\n</div>\r\n<button class="accordion">PAYMENT, REFUNDS AND CREDIT +</button>\r\n\r\n<div class="panel">\r\n<p>1.Where a Job Poster pays an agreed price for goods or services into the Escrow Account in respect of a Posted Task (&quot;Task Payment&quot;) that Task Payment will be used to pay the TaskMatch Worker and TaskMatch in accordance with this Agreement. If that payment cannot be made for any reason the Task Payment moneys will be handled in accordance with this clause 3.</p>\r\n\r\n<p>2.If the Job Poster and the TaskMatch Worker mutually agree to cancel the Task Contract or if, following reasonable attempts by a Job Poster to contact a TaskMatch Worker to perform the Task Contract, TaskMatch is satisfied that the Task Payment should be refunded and there is no dispute between the Job Poster and the TaskMatch Worker, then TaskMatch will refund the Task Payment back into the Job Poster&#39;s nominated account.</p>\r\n\r\n<p>3.If, for any reason, the Task Payment cannot be transferred or otherwise made to the TaskMatch Worker or the Job Poster (as the case may be) or no claim is otherwise made for a Task Payment, the Task Payment will remain in the Escrow Account until paid or otherwise for up to three months from the date the Job Poster initially paid the Task Payment into the Escrow Account.</p>\r\n\r\n<p>4.Following the 3 months referred to in clause 3 (Payments, Refunds &amp; Credit) and provided there is still no dispute in respect of the Task Payment the Task Payment will be automatically converted into TaskMatch credit and credited to the relevant Job Poster or TaskMatch Worker (&quot;Holder&quot;). The credit will be to the account of the Job Poster except where the TaskMatch Worker has claimed the Task Contract has been completed but for whatever reason the Task Payment was not able to be transferred.</p>\r\n\r\n<p>5. (a) can be used by the Holder to pay for any new tasks via the TaskMatch Platform; (b) is not refundable or redeemable for cash; and (c) expires 12 months from the last date the TaskMatch credit is topped-up or the Stored Value Card is purchased by the Holder whether by operation of clause 3.4 or any other means.</p>\r\n</div>\r\n<button class="accordion">PAYMENT FACILITY AND ESCROW ACCOUNT +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch may use a related entity or a third party service provider to provide payment services acting as a payment facilitator and escrow agent on behalf of the Job Poster and TaskMatch Worker (&quot;Escrow Provider&quot;).</p>\r\n\r\n<p>2.By buying or selling goods or services using the TaskMatch Services You agree to be bound by the Escrow Provider&#39;s Privacy Policy which is located at http://promisepay.com/ privacy and hereby consent and authorise TaskMatch and the Escrow Provider to share any information and payments instructions You provide with one another and, to the extent required to complete your transaction, with any other third party service provider(s). By registering and creating an account with TaskMatch, You agree to be bound by Escrow Provider&#39;s terms and conditions which are located at https://www.paypal.com/us/webapps/mpp/ua/legalhub-full. TaskMatch confirms that the services offered by the Escrow Provider are Third Party Services and subject to further terms set out for Third Party Services below.</p>\r\n</div>\r\n<button class="accordion">THIRD PARTY SERVICES +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch may from time to time include on the TaskMatch Platform promotions for and links to services offered by third parties (&quot;Third Party Services&quot;). These Third Party Services are not provided by TaskMatch.</p>\r\n\r\n<p>2.Third Party Services are offered to You pursuant to terms and conditions offered by the third party. Third Party Services may be promoted on the TaskMatch Platform as a convenience to our Users who may find the Third Party Services of interest or of use.</p>\r\n\r\n<p>3.If You engage with any Third Party Service provider your agreement will be directly between You and that Third Party Service provider.</p>\r\n\r\n<p>4.TaskMatch makes no representation or warranty as to the Third Party Services. However, to help us continue to improve our TaskMatch Platform, please let us know of any issue that You experience using a Third Party Service by contacting us at support.TaskMatch.com.</p>\r\n</div>\r\n<button class="accordion">INSURANCE +</button>\r\n\r\n<div class="panel">\r\n<p>1.All TaskMatch users must make sure they have the relevant insurance needed to carry out the tasks they are preforming. TaskMatch does not offer any Insurance for any tasks booked through the https://www.TaskMatch.ie/ website platform.</p>\r\n</div>\r\n<button class="accordion">IDENTITY VERIFICATION +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch may include tools to help TaskMatch Users to verify the identity of other TaskMatch Platform Users (&ldquo;Identity Verification Services&rdquo;). These tools may include mobile phone verification technology, verification of payment information, a &quot;Reference&quot; feature (allowing a User of the TaskMatch Service to request other Users to post a reference on the TaskMatch Platform endorsing that User), and integration with social networking sites such as Facebook, Twitter and Linked In.</p>\r\n\r\n<p>2.You agree that TaskMatch Identity Verification Services may not be fully accurate as all TaskMatch Services are dependent on User-supplied information.</p>\r\n\r\n<p>3.TaskMatch Platform Users are solely responsible for identity verification and TaskMatch accepts no responsibility for any use that is made of a TaskMatch Identity Verification Service.</p>\r\n\r\n<p>4.The TaskMatch Platform may also include a User-initiated feedback system to help evaluate TaskMatch User(s).</p>\r\n\r\n<p>5.The TaskMatch Services Identity Verification Services may be modified at any time.</p>\r\n</div>\r\n<button class="accordion">ELIGIBILITY TO REGISTER +</button>\r\n\r\n<div class="panel">\r\n<p>1.A TaskMatch account can only be created in the name of an individual. However, a TaskMatch User registering as a TaskMatch Worker or Job Poster may specify within the &lsquo;&quot;account description&quot; field that s/he is representing a business entity (including a company).</p>\r\n\r\n<p>2.To create and account and use the TaskMatch Services You must be able to form legally binding contracts under applicable law. TaskMatch Services are not available to persons under 18 years of age. If You do not qualify to use the TaskMatch Services, You must not use the services.</p>\r\n\r\n<p>3.While You are registered with TaskMatch You must maintain control of your TaskMatch account. You may not deal with your account (including feedback and associated UserID) in any way (including by allowing others to use your account or by transferring or selling the account or any of its content to another person).</p>\r\n\r\n<p>4.At its absolute discretion, TaskMatch may refuse to allow any person to register or create an account with TaskMatch or cancel or suspend any existing account.</p>\r\n</div>\r\n<button class="accordion">USER&#39;S OBLIGATIONS +</button>\r\n\r\n<div class="panel">\r\n<p>1.You agree that at all times: (a) You will comply with this Agreement (including all TaskMatch Policies) and all applicable laws and regulations; (b) You will post only accurate information on the TaskMatch Platform; (c) You will promptly and efficiently perform all your obligations to other TaskMatch Users under a Task Contract and to TaskMatch under this Agreement; (d) all content (whether provided by TaskMatch, a User or a third party) on the TaskMatch Platform may not be used on third party sites or for other business purposes without TaskMatch&#39;s prior permission; and (e) You will ensure that You are aware of any laws that apply to You as a Job Poster or a TaskMatch Worker, or in relation to any other way(s) that You use the TaskMatch Platform.</p>\r\n\r\n<p>2.You must not use the TaskMatch Platform for any illegal or immoral purpose.</p>\r\n\r\n<p>3.You grant to TaskMatch an unrestricted, worldwide, royalty-free licence to use, reproduce, modify and adapt any content and information posted on the TaskMatch Platform for the purpose of including that material and information on the TaskMatch Platform and as otherwise may be required to provide the TaskMatch Service, for the general promotion of the TaskMatch Service and as permitted by this Agreement.</p>\r\n\r\n<p>4.Any information posted on TaskMatch Platform must not, in any way whatsoever, be potentially or actually harmful to TaskMatch or any other person. &quot;Harm&quot; includes, but is not limited to, economic loss that will or may be suffered by TaskMatch. Without limiting any provision of this Agreement, any information You supply to TaskMatch must be up to date and kept up to date and must not: (a) be false, inaccurate or misleading or deceptive; (b) be fraudulent or involve the sale of counterfeit or stolen items; (c) infringe any third party&#39;s copyright, patent, trademark, trade secret or other proprietary rights or intellectual property rights, rights of publicity, confidentiality or privacy; (d) violate any applicable law, statute, ordinance or regulation (including, but not limited to, those governing export and import control, consumer protection, unfair competition, criminal law, antidiscrimination and trade practices/fair trading laws); (e) be defamatory, libellous, threatening or harassing; (f) be obscene or contain any material that, in TaskMatch&#39;s sole and absolute discretion, is in any way inappropriate or unlawful, including, but not limited to obscene, inappropriate or unlawful images; (g) contain any malicious code, data or set of instructions that intentionally or unintentionally causes harm or subverts the intended function of any TaskMatch Platform, including, but not limited to viruses, Trojan horses, worms, time bombs, cancelbots, Easter eggs or other computer programming routines that may damage, modify, delete, detrimentally interfere with, surreptitiously intercept, access without authority or expropriate any system, data or personal information.</p>\r\n\r\n<p>5.You must have the right to provide goods and services under a Task Contract and to work in Ireland. You must comply with your tax obligations in relation to any payment received under a Tasker Contract.</p>\r\n\r\n<p>6.When You enter into a Task Contract using the TaskMatch Platform You create a legally binding contract with another TaskMatch User, unless the transaction is prohibited by law or by this Agreement (including the TaskMatch Policies). If You do not comply with your obligations to another TaskMatch User under a Task Contract, You may become liable to that other User. If another User breaches any obligation to You, You - not TaskMatch - are responsible for enforcing any rights that You may have with that User.</p>\r\n</div>\r\n<button class="accordion">FEES +</button>\r\n\r\n<div class="panel">\r\n<p>1.Registering and creating an account with TaskMatch is free. There is no charge for a Job Poster to Post Tasks, or for other TaskMatch Users to review content on the TaskMatch Platform, including Posted Tasks.</p>\r\n\r\n<p>2.If a Job Poster has accepted your offer, You agree that the Service Fees as set out in the Service Fee Schedule will be deducted from the agreed fee and paid to TaskMatch (or its nominee), which are inclusive of VAT.</p>\r\n\r\n<p>3.The Service Fees will be deducted and paid to TaskMatch when the agreed price is released from the Escrow Account to the TaskMatch Worker.</p>\r\n\r\n<p>4.To be clear, Service Fees will be calculated only on the amount that the TaskMatch Worker agrees to charge the Job Poster to provide the service or goods. This means that if a Job Poster requires a TaskMatch Worker to incur costs in completing a task (for example, if the task is purchasing groceries, TaskMatch Worker might agree to pay for the groceries upfront and then be reimbursed by the Job Poster), then the cost incurred will not be included in any calculation of the Service Fees.</p>\r\n\r\n<p>5.If a TaskMatch Worker agrees to pay some of the costs as part of completing a task, TaskMatch Worker is solely responsible for having those costs reimbursed by the Job Poster. TaskMatch will not be responsible for obtaining any reimbursement from a Job Poster. We advise TaskMatch Workers not to agree to incur any costs for a Job Poster in advance of payment by the Job Poster, unless the TaskMatch Worker is confident that the Job Poster will reimburse the costs promptly.</p>\r\n\r\n<p>6.TaskMatch may from time to time change the Service Fees and the terms applying to their payment. Any change relating to the Service Fees is effective fourteen (14) days after TaskMatch notifies You of that change by sending a message to your TaskMatch account.</p>\r\n\r\n<p>7.TaskMatch may choose to temporarily modify the Service Fees, or the terms applying to their payment, in its sole discretion. Notification of temporary modifications will also be sent to your TaskMatch account.</p>\r\n\r\n<p>8.All fees and charges payable to TaskMatch are non-cancellable and non-refundable, subject to your rights under any Non-Excludable Conditions (defined below).</p>\r\n\r\n<p>9.If TaskMatch introduces a new service on the TaskMatch Platform, the fees applying to that service will be payable as from the launch of the service and will also be &quot;Service Fees&quot; under this Agreement.</p>\r\n\r\n<p>10.Where a TaskMatch Worker is supplying goods or services, the TaskMatch Worker must not charge a Job Poster Service Fees on top of the agreed price for the goods or Services under the Tasker Contract.</p>\r\n</div>\r\n<button class="accordion">POSTING TASKS AND MAKING OFFERS +</button>\r\n\r\n<div class="panel">\r\n<p>1.A Posted Task must include the following details in order to be accepted by TaskMatch: (a) Deadline for completion: the deadline for completion of the task or delivery of the goods; (b) Price: the price that the Job Poster is prepared to pay for the goods or services required; and (c) Description: a description of the goods or services.</p>\r\n\r\n<p>2.Service Fees do not include any fees that may be due to Third Party Service Providers. All Third Party Service fees must be paid pursuant to your separate agreement with the Third Party Service provider.</p>\r\n\r\n<p>3.As well as the mandatory details mentioned in 1.(posting tasks and making offer) , a Job Poster may choose to specify additional terms that s/he wants to apply to the transaction.</p>\r\n\r\n<p>4.Job Posters must describe the service or goods fully and accurately and include all terms of sale or supply that You want to apply, as well as any information required by law. Posted Tasks may only include text. You must have the legal right to use any content that You post. TaskMatch may, in its sole discretion, remove any Posted Task for any reason.</p>\r\n\r\n<p>5.Prior to the Job Poster accepting an offer from a TaskMatch Worker, all communications between a Job Poster and the TaskMatch User(s) making and Offer to supply the goods or services are visible to all other TaskMatch Users and can also be viewed by other internet users. All Users of the TaskMatch Platform must take care when communicating at the offer stage and must not disclose any private contact details such as full name or address, phone number or email address.</p>\r\n\r\n<p>6.In making an Offer, a TaskMatch Worker agrees to provide the relevant goods or services within the timeframe and on the terms and conditions specified in the Posted Task (unless the TaskMatch Worker and the Job Poster agree to vary the timeframe or the terms and conditions in the course of negotiations, in which case the TaskMatch Worker must provide the goods or services in accordance with the agreed timeframe and terms and conditions).</p>\r\n\r\n<p>7.Once a Job Poster accepts an offer from a TaskMatch Worker in accordance with clause (TaskMatch Service Desciption) , no further negotiations are permitted on the TaskMatch Platform in relation to the provision of the relevant goods or services. If, after the Job Poster accepts an offer from a TaskMatch Worker, the TaskMatch Worker and Job Poster wish to negotiate on price or timeframe for completion (for example, where Job Poster is not satisfied with the TaskMatch Worker&rsquo;s performance) they may use the TaskMatch private messaging system, but otherwise those negotiations (and the resolution of any disputes) must be done outside the TaskMatch Platform.</p>\r\n\r\n<p>8.If a TaskMatch User, TaskMatch Worker or Job Poster wishes to complain about any comment made on the TaskMatch Platform, please either use the &quot;Report&quot; function available from time to time on the TaskMatch Platform, or email TaskMatch using the contact information on the TaskMatch Platform.</p>\r\n\r\n<p>9.Job Posters may choose not to accept any Offer received and is not required to accept the lowest Offer. Job Posters may withdraw any Posted Task at any time prior to accepting an offer from a TaskMatch Worker in accordance with clause 6.(TaskMatch Service Desciption)</p>\r\n\r\n<p>10.A TaskMatch User or TaskMatch Worker has no obligation whatsoever to make an Offer on any Posted Task.</p>\r\n</div>\r\n<button class="accordion">FEEDBACK +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch is entitled to suspend or terminate your account at any time if TaskMatch, in its sole and absolute discretion, is concerned by any feedback about You, or considers your feedback rating to be problematic for other TaskMatch Users.</p>\r\n\r\n<p>2.To continue to improve our TaskMatch Platform, please let us know of any issue that You experience using a Third Party Service by contacting us at support.TaskMatch.ie</p>\r\n</div>\r\n<button class="accordion">LIMITATION OF LIABILITY +</button>\r\n\r\n<div class="panel">\r\n<p>1.Except for liability in relation to breach of any implied condition, warranty or guarantee, including under the COMPETITION AND CONSUMER PROTECTION ACT 2014, the exclusion of which from a contract would contravene any statute or cause any part of this Agreement to be void (&quot;Non-excludable Condition&quot;), to the extent permitted by law TaskMatch specifically disclaims all liability for any loss or damage (actual, special, direct, indirect and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed (including, without limitation, loss or damage relating to any inaccuracy of information provided, or the lack of fitness for purpose of any goods or service supplied), arising out of or in any way connected with any transaction between Job Posters and TaskMatch Workers.</p>\r\n\r\n<p>2.Except for liability in relation to a breach of any Non-excludable Condition, to the extent permitted by law, TaskMatch specifically disclaims all liability for any loss or damage (actual, special, direct, indirect and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed (including, without limitation, loss or damage relating to any inaccuracy of information provided, or the lack of fitness for purpose of any goods or service supplied), arising out of or in any way connected with any transaction between You and any Third Party Service Provider who may be included from time to time on the TaskMatch Platform.</p>\r\n\r\n<p>3.Except for liability in relation to a breach of any Non-excludable Condition, TaskMatch&#39;s liability to any User of this service is limited to the total amount of fees paid by that User to TaskMatch during the twelve month period prior to any incident causing liability of TaskMatch.</p>\r\n\r\n<p>4.TaskMatch&#39;s liability to You for a breach of any Non-excludable Condition (other than a Non-excludable Condition that by law cannot be limited) is limited, at our option to any one of resupplying, replacing or repairing, or paying the cost of resupplying, replacing or repairing goods in respect of which the breach occurred, or supplying again or paying the cost of supplying again, services in respect of which the breach occurred.</p>\r\n</div>\r\n<button class="accordion">DEFAULT IN TRANSACTIONS +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch&rsquo;s Privacy Policy, which is available at TaskMatch.ie applies to all Users and forms part of this Agreement. Use of the TaskMatch Platform confirms that You consent to, and authorise, the collection, use and disclosure of your personal information in accordance with TaskMatch&rsquo;s Privacy Policy.</p>\r\n\r\n<p>2.Third Party Service providers will provide their service pursuant to their own Privacy Policy. Prior to acceptance of any service from a third party You must review and agree to their terms of service including their privacy policy.</p>\r\n</div>\r\n<button class="accordion">NO WARRANTY+</button>\r\n\r\n<div class="panel">\r\n<p>1.Except for liability in relation to any Non-excludable Condition, the TaskMatch service is provided on an &quot;as is&quot; basis, and without any warranty or condition, express or implied. To the extent permitted by law, we and our suppliers specifically disclaim any implied warranties of title, merchantability, fitness for a particular purpose and non-infringement to extent allowed by law.</p>\r\n</div>\r\n<button class="accordion">MODIFICATIONS TO THE AGREEMENT +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch may modify this Agreement and/ or the Policies (and update the TaskMatch pages on which they are displayed) from time to time. TaskMatch will send notification of such modifications to your TaskMatch account. You should check that account regularly.</p>\r\n\r\n<p>2.Except as stated in this Agreement, in a Policy, or as otherwise notified to You, all amended terms will automatically be effective 30 days after they are initially notified to You. Each time You use the TaskMatch Platform in any manner after the expiry of that 30 day period or such other notice period notified to You, You acknowledge any changes to the Agreement (including the Policies) and confirm your agreement to be bound by the Agreement as it may have been varied.</p>\r\n\r\n<p>3.If You do not agree with any changes to this Agreement (or any of our Policies), You must terminate your TaskMatch account and stop using the TaskMatch Services. Except as specifically described in or authorised by this Agreement, the terms of this Agreement and any TaskMatch Policy cannot be amended except in writing signed by You and TaskMatch.</p>\r\n</div>\r\n<button class="accordion">NO AGENCY +</button>\r\n\r\n<div class="panel">\r\n<p>1.No agency, partnership, joint venture, employee-employer or other similar relationship is created by this Agreement. In particular You have no authority to bind TaskMatch, its related entities or affiliates in any way whatsoever. TaskMatch confirms that all Third Party Services that may be promoted on the TaskMatch service are provided solely by such Third Party Service providers. To the extent permitted by law, TaskMatch specifically disclaims all liability for any loss or damage incurred by You in any manner due to the performance or non performance of such Third Party Service.</p>\r\n</div>\r\n<button class="accordion">NOTICES +</button>\r\n\r\n<div class="panel">\r\n<p>1.Except as stated otherwise, any notices must be given by registered ordinary post or by email, either to TaskMatch&#39;s contact address as displayed on the TaskMatch Platform, or to TaskMatch Users&rsquo; contact address as provided at registration. Any notice shall be deemed given: (a) if sent by email, 24 hours after email is sent, unless the Job Poster is notified that the email address is invalid or the email is undeliverable, and (b) if sent by pre-paid post, three Business Days after the date of posting, or on the seventh Business Day after the date of posting if sent to or posted from outside Ireland. In this section, &quot;Business Day&quot; means a day on which banks are open for general business in Dublin, Ireland, other than a Saturday, Sunday or public holiday. (c) Notices related to performance of any Third Party Service must be delivered to such Third Party as set out in Third Party Service Provider terms and conditions.</p>\r\n</div>\r\n<button class="accordion">MEDIATION AND DISPUTE RESOLUTION +</button>\r\n\r\n<div class="panel">\r\n<p>1.TaskMatch encourages You to try and resolve disputes (including claims for returns or refunds) with other TaskMatch Users directly. Accordingly, You acknowledge and agree that TaskMatch may, in its absolute discretion, provide such of Your information as it decides is suitable to other parties involved in the dispute.</p>\r\n\r\n<p>2.TaskMatch may provide access to a third party dispute resolution service (&quot;Third Party Dispute Service&quot;). If such a service is provided, either party may require the other party to submit the Third Party Dispute Service if the parties have failed to resolve the dispute directly. Terms and conditions for the Third Party Dispute Service are located at [https://www.paypal.com/us/webapps/mpp/ua/legalhub-full]. TaskMatch confirms that Third Party Dispute Service is a Third Party Service and subject to further terms set out for Third Party Services. The Users are responsible for paying any costs associated with the Third Party Dispute Service in accordance with the Third Party Dispute Service terms and conditions.</p>\r\n\r\n<p>3.Disputes with any Third Party Service provider must proceed pursuant to any dispute resolution process set out in the terms of service of the Third Party Service Provider.</p>\r\n\r\n<p>4.TaskMatch has the right to hold any Task Payment the subject of a dispute in the Escrow Account, until the dispute has been resolved.</p>\r\n\r\n<p>5.If You have a complaint about the TaskMatch Service please contact us at support.TaskMatch.ie</p>\r\n\r\n<p>6.If TaskMatch provides information of other TaskMatch Users to You for the purposes of resolving disputes under this clause, You acknowledge and agree that such information will be used only for the purpose of resolving the dispute (and no other purpose) and that you will indemnify TaskMatch against any claims relating to any other use of information not permitted by this Agreement.</p>\r\n</div>\r\n<button class="accordion">TERMINATION +</button>\r\n\r\n<div class="panel">\r\n<p>1.Either party may terminate an account and this Agreement at any time for any reason.</p>\r\n\r\n<p>2.Termination of this Agreement does not affect any Task Contract that has been formed between TaskMatch Users. If You have entered a Task Contract You must comply with the terms of that Task Contract including providing the goods or services or paying the price as applicable.</p>\r\n\r\n<p>3.Third Party Services are subject to Third Party Service provider terms and conditions.</p>\r\n\r\n<p>4.Sections (Fees), (Limitation of Liability), and (Mediation and Dispute Resolution) and any other terms which by their nature should continue to apply, will survive any termination or expiration of this Agreement.</p>\r\n</div>\r\n<button class="accordion">GENERAL +</button>\r\n\r\n<div class="panel">\r\n<p>1.This Agreement is governed by the laws of Ireland. You and TaskMatch submit to the exclusive jurisdiction of the courts of Ireland.</p>\r\n\r\n<p>2.The provisions of this Agreement are severable, and if any provision of this Agreement is held to be invalid or unenforceable, such provision may be removed and the remaining provisions will be enforceable.</p>\r\n\r\n<p>3.This Agreement may be assigned or novated by TaskMatch to a third party without your consent. In the event of an assignment or novation You will remain bound by this Agreement.</p>\r\n\r\n<p>4.This Agreement sets out the entire understanding and agreement between You and TaskMatch with respect to its subject matter. Revised April 2016 &copy; TaskMatch 2016</p>\r\n</div>\r\n<button class="accordion">SERVICE FEE SCHEDULE +</button>\r\n\r\n<div class="panel">\r\n<p>The Service Fee for an offer that has been accepted by a Job Poster is equal to fifteen percent (15%) of the amount that was accepted.</p>\r\n</div>\r\n</div>\r\n\r\n<div class="container terms__condtn__btm__area">\r\n<p>These are our terms and conditions (&quot;Terms&quot;), contact us if you have any futher questions. Thanks for connecting.</p>\r\n\r\n<p>The team at TaskMatch.ie</p>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<script>\r\n\r\nvar acc = document.getElementsByClassName("accordion");\r\nvar i;\r\n\r\nfor (i = 0; i < acc.length; i++) {\r\n    acc[i].onclick = function(){\r\n        /* Toggle between adding and removing the "active" class,\r\n        to highlight the button that controls the panel */\r\n        this.classList.toggle("active");\r\n\r\n        /* Toggle between hiding and showing the active panel */\r\n        var panel = this.nextElementSibling;\r\n        if (panel.style.display === "block") {\r\n            panel.style.display = "none";\r\n        } else {\r\n            panel.style.display = "block";\r\n        }\r\n    }\r\n}\r\n\r\n</script>', '', '', '', '', NULL, '2017-08-02 17:19:27', 1, 0, NULL, 'terms');
INSERT INTO `content` (`id`, `type`, `code`, `title`, `subject`, `teaser`, `body`, `language_id`, `metaTitle`, `metaDescription`, `keywords`, `created_at`, `updated_at`, `status`, `deleted`, `image`, `url`) VALUES
(16, 'page', 'privacy', 'Privacy', NULL, '', '<section class="privacy-policy">\r\n<div class="container">\r\n<div class="privacy__policy hdng col-sm-12">\r\n<div class="privacy__policy hdng__inner">\r\n<h2>TaskMatch Privacy Policy</h2>\r\n</div>\r\n</div>\r\n\r\n<div class="acord__sec privacy__policy col-sm-6 col-sm-offset-3">\r\n<div class="policy__inner"><button class="accordion">Privacy Policy Title</button>\r\n\r\n<div class="panel">\r\n<p>TaskMatch Limited Ireland regitered under company number 580200 (&quot;TaskMatch&quot; or &quot;We&quot;) manages the information that We collect from you in accordance with all applicable privacy legislation in Ireland.</p>\r\n\r\n<p>This Privacy Policy describes how TaskMatch handles your personal information, and sets out the rights and obligations that both you and TaskMatch have in relation to your personal information.</p>\r\n\r\n<p>By accessing www.TaskMatch.ie (the &quot;Site&quot;) you accept and agree to the terms and conditions of TaskMatch&#39;s user agreement (&quot;User Agreement&quot;), which includes your consent to, and authorization of, the collection, use and disclosure of your personal information in accordance with this Privacy Policy. Note that under TaskMatch&#39;s terms and conditions of use, you must not use the Site if you are under 18 years old.</p>\r\n</div>\r\n<button class="accordion">1. Collection and Use of Your Personal Information</button>\r\n\r\n<div class="panel">\r\n<p>TaskMatch provides a unique, innovative, community-based online outsourcing service. A failure by you to provide information requested by us may mean that We are unable to provide some or all of our services to you.</p>\r\n\r\n<p>TaskMatch collects personal information when you register with TaskMatch. This may include, but is not limited to, your name, address, phone number, contact details, birth date, gender, credit card and account details, occupation and interests. We might also give you the option of providing a photo or video to be associated with your TaskMatch user ID. If your personal details change, it is your responsibility to update your TaskMatch account with those changes, so that We can keep our records complete, accurate and up to date.</p>\r\n\r\n<p>You are not anonymous to us when you log into the Site or post any content (including tasks, items to be supplied, bids, comments or feedback) on the Site or any associated forum. To enable us to improve our existing services and to create new service features, TaskMatch may collect, and share with other users and service providers on TaskMatch, information about the way you use our services, including (but not limited to) the transactions you enter into on the Site, your feedback rating (including any references requested using our &#39;Reference&#39; feature), the bids you make, the comments you post, and the transactions you enter into with our valued affiliate service providers.</p>\r\n\r\n<p>When you: ? use the Site to post a task or item to be supplied, or make a bid, or comment on a bid, or provide feedback on other users; or ? otherwise communicate in a public forum on the Site, your user ID and all the material that you post is visible to other TaskMatch users and is also publicly available to other internet users. We strongly encourage you to use caution and discretion when posting. TaskMatch does not in any way control, and does not accept any responsibility or liability whatsoever for, the disclosure or use of personal information which is voluntarily posted by you in any publicly accessible area of the Site.</p>\r\n\r\n<p>TaskMatch may also receive and record information from your internet browser and computer, including IP addresses, cookies (see section 2 below), software and hardware attributes and your page requests. In addition to data collected from your submissions, we may also collect data on your internet behavior from Facebook, other social media sites, and other services. If you use a location-enabled TaskMatch service, We may collect and process information about your actual location (for example, GPS signals sent by your mobile device). We may also use a range of different technologies to confirm your location.</p>\r\n\r\n<p>TaskMatch may use the information collected by it to provide the TaskMatch Services, including (but not limited to): ? identification and authentication; ? to protect TaskMatch and the users of the Site; ? to customise the content and any advertising displayed on the Site; ? to provide, maintain, protect and improve our services; ? providing your information to a user with whom you have or had a contract facilitated by TaskMatch; ? as required by law, order of a court, tribunal or regulator or if TaskMatch reasonably believes that the use or disclosure of the information is reasonably necessary for enforcement related activities; ? to develop new service features; ? to ensure that TaskMatch receives payment of the fees due to it; ? to contact you (for example, to inform you about upcoming changes or improvements to our services); ? to conduct research; ? to permit content on the Site (such as postings or third party advertisements) to be targeted, on an aggregate basis, to the users for whom it is most likely to be relevant; ? to expand our user base; ? to develop our relationships with affiliate service providers; and ? to generate data reports on an aggregated, non-personally identifiable basis, for both internal and third party use, but subject to any applicable laws (for example, We may show advertisers or investors trends relating to the general use of TaskMatch&#39;s services). Your contact information may also be used for accounting, invoicing and billing purposes, marketing purposes, by third party service providers to TaskMatch, and to respond to any enquiry you make, in some cases information we collect is transferred overseas. When you contact TaskMatch, We may keep a record of the communication(s) between you and TaskMatch to help resolve any issues you might have. If other user(s) of the Site already have your userID (or other information identifying you), and you have chosen to upload a photo or other personal information to your TaskMatch account, We may show those user(s) that personal information.</p>\r\n\r\n<p>In certain circumstances We will use your email address to administer our User Agreement (for example, We may notify you of a breach, or action a request for a Take Down Notice in response to a claim of copyright infringement).</p>\r\n</div>\r\n<button class="accordion">2. Cookies and Anonymous Identifiers</button>\r\n\r\n<div class="panel">\r\n<p>If you have registered on the Site then your computer or device may store an identifying cookie or anonymous identifier, which can save you time each time you re-visit the Site, by accessing your information when you sign-in to TaskMatch.</p>\r\n\r\n<p>TaskMatch may use cookies and anonymous identifiers for a number of purposes including to access your information when you sign in, keep track of your preferences, direct specific content to you, report on TaskMatch&#39;s user base, and to improve TaskMatch&#39;s services. We may also use cookies or anonymous identifiers when you interact with our affiliate service providers (for example, when you integrate your TaskMatch account with your Facebook profile).</p>\r\n\r\n<p>You agree that if you change the settings on your internet browser to block or restrict cookies (including cookies associated with TaskMatch&#39;s services), or to indicate when a cookie is being set by TaskMatch, the TaskMatch Services may not work as intended. You should remember that, while you may still be able to use the Site if your cookies are disabled, our services may not function properly on your device and you may not be able to take advantage of certain TaskMatch features.</p>\r\n</div>\r\n<button class="accordion">3. Protecting and Maintaining Personal Information</button>\r\n\r\n<div class="panel">\r\n<p>Your account is protected by a password for your privacy and security. We will take all reasonable steps to protect the information We hold about you from unauthorized access, use and disclosure, however We cannot guarantee the absolute security of that information, or that our systems will be completely free from third party interception or are incorruptible from viruses. We cannot and do not guarantee that information you send from your computer to us over the Internet will be protected by any form of encryption (encoding software). In light of this, We cannot and do not ensure or warrant the security or privacy of your personal information, including payment and account details. You transmit your personal information to us at your own risk.</p>\r\n\r\n<p>You are entirely responsible for maintaining the security of your passwords and/or account information.</p>\r\n</div>\r\n<button class="accordion">4. Third Parties</button>\r\n\r\n<div class="panel">\r\n<p>The Site may contain links to third party websites including the networks of our valued affiliate service providers, advertisers, and PayPal. If you follow a link to any of these websites, for instance PayPal payment system, note that they have their own privacy policies. If you use our website to link to another site you will be subject to that site&#39;s terms and conditions of use, privacy policy and security statement. We strongly encourage you to view these before disclosing any of your personal information on such sites.</p>\r\n\r\n<p>TaskMatch does not control, and does not accept any responsibility or liability for, the privacy policy of, and use of personal information by, any party other than TaskMatch, including any user of the Site and the operators of any website to which the Site links.</p>\r\n\r\n<p>If TaskMatch is involved in a merger, acquisition or asset sale, We will give affected users of the Site notice before their personal information is transferred to another entity or becomes subject to a different privacy policy.</p>\r\n\r\n<p>Third-party vendors, including Google, may show TaskMatch ads on sites across the Internet. The TaskMatch website and Mobile Apps make use of the Google Maps API to assist with location based information. More information about the Google Maps API and the related privacy policy can be found here http://www.google.com/privacy.html</p>\r\n</div>\r\n<button class="accordion">5. Marketing</button>\r\n\r\n<div class="panel">\r\n<p>When you register on the Site you may be given the opportunity to elect (&quot;opt-in&quot;) to receive updates on our latest services, news and special offers, and those of our valued affiliate service providers (&quot;Marketing Material&quot;), via your TaskMatch account, personal e-mail address, post or telephone. If you conclude a transaction on the Site, you may also be given the opportunity to opt- in to receive Marketing Material from TaskMatch and our valued affiliate service providers.</p>\r\n\r\n<p>Once you opt-in to receive Marketing Material, You may, at any time, opt-out of receiving Marketing Material. To opt-out go to the &#39;Manage Account&#39; link on the Site, choose &#39;Settings&#39;, then &#39;Alerts&#39; and update your preferences. You can also click on the &quot;unsubscribe&quot; link in any email containing Marketing Material that We send you, or you can request an opt-out by emailing TaskMatch using the contact information provided on the Site. If you no longer consent to receiving Marketing Material then you must opt-out in one of these ways.</p>\r\n\r\n<p>TaskMatch may contact you as the result of a referral by another user of the Site who has provided us with contact information, such as your name and email address. The use of contact information received in connection with a referral will be governed by this Privacy Policy. You may, at any time, opt-out of TaskMatch&#39;s referral system by emailing TaskMatch using the contact information provided on the Site.</p>\r\n\r\n<p>TaskMatch uses Remarketing with Google Analytics to advertise online. Using the Ads Settings in a personal Google account, visitors can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads. Marketing TaskMatch and third-party vendors, including Google, use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the DoubleClick cookie) together to inform, optimise, and serve ads based on someone&#39;s past visits to the TaskMatch website.</p>\r\n\r\n<p>Using the Ads Settings in a personal Google account, visitors can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads.</p>\r\n</div>\r\n<button class="accordion">6. Administrative Communications</button>\r\n\r\n<div class="panel">\r\n<p>TaskMatch reserves the right to send you administrative and account&shy;related messages that you may not opt-out of. To cease all communications to you from TaskMatch you must close down your TaskMatch account.</p>\r\n</div>\r\n<button class="accordion">7. Accessing Personal Information</button>\r\n\r\n<div class="panel">\r\n<p>We will allow you, at any time, to access, edit, update and/ or delete the personal information that We hold about you, unless: ? We are legally permitted or required to deny you access to, and/ or to retain, the information; or ? you make request(s) that are unreasonably repetitive, require TaskMatch to make a disproportionate technical effort (for example, to develop an entirely new system), risk the privacy of others, or would be highly impractical to comply with. If you wish to access the personal information We hold about you, or to delete your TaskMatch account, you should contact TaskMatch using the contact information provided at support.TaskMatch.com. We need to prevent information in our systems from being accidentally or maliciously destroyed. This means that, where you delete information from our services, residual copies of that information on our active servers, as well as any corresponding information on our back&shy;up systems, may not be immediately deleted.</p>\r\n</div>\r\n<button class="accordion">8. Privacy Concerns</button>\r\n\r\n<div class="panel">\r\n<p>If you have concerns about how We handle your personal information or require further information, please email TaskMatch using the contact form provided on the Site. If you make a formal written complaint in relation to our compliance with this Privacy Policy, We will contact you to follow up on that complaint. Other than material that you voluntarily choose to post on the Site (such as Sender postings, bids and comments on other users), the only information about you that We will post publicly is your TaskMatch public profile.</p>\r\n\r\n<p>TaskMatch may, from time to time, modify this Privacy Policy (and update the web page on which it is displayed). TaskMatch will send notification of any such modification to your TaskMatch account and/or your registered email address. You should check that account regularly. TaskMatch.ie 580200. 2016 All rights reserved.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<script>\r\n\r\nvar acc = document.getElementsByClassName("accordion");\r\nvar i;\r\n\r\nfor (i = 0; i < acc.length; i++) {\r\n    acc[i].onclick = function(){\r\n        /* Toggle between adding and removing the "active" class,\r\n        to highlight the button that controls the panel */\r\n        this.classList.toggle("active");\r\n\r\n        /* Toggle between hiding and showing the active panel */\r\n        var panel = this.nextElementSibling;\r\n        if (panel.style.display === "block") {\r\n            panel.style.display = "none";\r\n        } else {\r\n            panel.style.display = "block";\r\n        }\r\n    }\r\n}\r\n\r\n</script>', '', '', '', '', NULL, '2017-08-02 17:06:39', 1, 0, NULL, 'privacy'),
(17, 'page', 'contact', 'Contact us', NULL, '', '<section class="touch-area">\r\n<div class="container">\r\n			<div class="hed">\r\n				<h2>GET IN <span>TOUCH</span></h2>\r\n				<p>Thanks for getting in touch with us</p>\r\n			</div>\r\n			<div class="row">\r\n			<div class="touch__box clrlist list-icon col-sm-4">\r\n				<div class="touch__inr">\r\n					<ul>\r\n						<li><i class="fa fa-phone"></i>\r\n							<h4>PHONE NUMBER</h4>\r\n							<p>+012 345 6789 / +012 345 6789</p>\r\n						</li>\r\n					</ul>\r\n				</div>\r\n			</div>\r\n			<div class="touch__box clrlist list-icon col-sm-4">\r\n				<div class="touch__inr">\r\n					<ul>\r\n						<li><i class="fa fa-envelope"></i>\r\n							<h4>EMAIL ADDRESS</h4>\r\n							<p>info@companylisting.com</p>\r\n						</li>\r\n					</ul>\r\n				</div>\r\n			</div>\r\n			<div class="touch__box clrlist list-icon col-sm-4">\r\n				<div class="touch__inr">\r\n					<ul>\r\n						<li><i class="fa fa-map-marker"></i>\r\n							<h4>LOCATION ADDRESS</h4>\r\n							<p>123 W Street Name New York 100</p>\r\n						</li>\r\n					</ul>\r\n				</div>\r\n			</div>\r\n			</div>\r\n		</div>\r\n	</section>\r\n	\r\n	<section class="contact-area">\r\n		<div class="container">\r\n			<div class="hed">\r\n				<h2>CONTACT <span>US</span></h2>\r\n				<p>Thanks for getting in touch with us</p>\r\n			</div>\r\n			<div class="contact__form col-sm-12 p0">\r\n				<form>\r\n					<div class="row">\r\n						<div class="form-group col-sm-6">\r\n						  <input type="text" class="form-control" id="firstName" placeholder="First Name">\r\n						</div>\r\n						<div class="form-text col-sm-6">\r\n						  <input type="text" class="form-control" id="lastName" placeholder="Last Name">\r\n						</div>\r\n					</div>\r\n					<div class="row">\r\n						<div class="form-group col-sm-6">\r\n						  <input type="text" class="form-control" id="email" placeholder="Email Address">\r\n						</div>\r\n						<div class="form-text col-sm-6">\r\n						  <input type="text" class="form-control" id="phoneNumber" placeholder="Phone Number">\r\n						</div>\r\n					</div>\r\n					<div class="row">\r\n						<div class="form-group col-sm-12">\r\n						  <textarea class="form-control" rows="9" id="comment" placeholder="Your Message!"></textarea>\r\n						</div>\r\n					</div>\r\n					<button type="submit" class="btn btn-default contact__submit">SUBMIT</button>\r\n				</form>\r\n			</div>\r\n		</div>\r\n	</section>', '', 'Contact us', '', '', NULL, '2017-03-09 16:00:17', 1, 0, NULL, NULL),
(18, 'page', 'get-started', 'Get Started', NULL, '', '<p>&nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', 'Get Started', '', '', NULL, NULL, 1, 0, NULL, NULL),
(19, 'page', 'how-it-works', 'How-It-Works', NULL, '', '<section class="work-bnr-area clrlist">\r\n<div class="container">\r\n<div class="work-bnr-box col-sm-4">\r\n<div class="work__bnr__inr">\r\n<div class="work__bnr__img work__bnr__img--1">\r\n<div class="work__bulb work__bnr__img__abs anime-down"><img alt="idea bulb" src="front/images/ideaBulb.png" /></div>\r\n\r\n<div class="work__chair work__bnr__img__abs anime-left"><img alt="chair icon" src="front/images/chairpic.png" /></div>\r\n\r\n<div class="work__paint work__bnr__img__abs anime-left"><img alt="paint" src="front/images/paint.png" /></div>\r\n\r\n<div class="work__hammer work__bnr__img__abs anime-right"><img alt="hammer" src="front/images/hammerPic.png" /></div>\r\n\r\n<div class="work__boxMove work__bnr__img__abs anime-left"><img alt="box" src="front/images/boxMove.png" /></div>\r\n\r\n<div class="work__womanTask work__bnr__img__abs anime-in"><img alt="woman task" src="front/images/womanTask1.png" /></div>\r\n\r\n<div class="work__camera work__bnr__img__abs anime-right"><img alt="camera" src="front/images/cameraAnim.png" /></div>\r\n\r\n<div class="work__laptop work__bnr__img__abs anime-right"><img alt="laptop" src="front/images/laptopAnim.png" /></div>\r\n</div>\r\n\r\n<div class="work__bnr__cont">\r\n<h4 class="anime-left">What do you need done?</h4>\r\n\r\n<p class="anime-up">Describe the task you need completed and whether you need it done in person or online. Post any task from handyman to web design in just two minutes &ndash; for free! There&#39;s no obligation to hire.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="work-bnr-box delay1s col-sm-4">\r\n<div class="work__bnr__inr">\r\n<div class="work__bnr__img work__bnr__img--2">\r\n<div class="work__woman__msr work__bnr__img__abs--two anime-down"><img alt="idea bulb" src="front/images/womanMeasure.png" /></div>\r\n\r\n<div class="work__womanTask--two work__bnr__img__abs--two anime-left"><img alt="chair icon" src="front/images/womanTask2.png" /></div>\r\n\r\n<div class="work__drill work__bnr__img__abs--two anime-in"><img alt="drill" src="front/images/drill.png" /></div>\r\n\r\n<div class="work__manTool work__bnr__img__abs--two anime-right"><img alt="hammer" src="front/images/manTools2.png" /></div>\r\n</div>\r\n\r\n<div class="work__bnr__cont">\r\n<h4 class="anime-left">Choose the best Worker for your Task</h4>\r\n\r\n<p class="anime-up">View verified profiles and reviews to pick the best TaskMatch Worker for the task. After you accept an offer, you will need to add funds securely via TaskMatch Pay, which are held until completion. You can then private message and call the TaskMatch Worker.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="work-bnr-box delay2s col-sm-4">\r\n<div class="work__bnr__inr">\r\n<div class="work__bnr__img work__bnr__img--3">\r\n<div class="work__msr__tool work__bnr__img__abs--three anime-right"><img alt="paint" src="front/images/womanMeasureToolMan3.png" /></div>\r\n\r\n<div class="work__womanTask--three work__bnr__img__abs--three anime-left"><img alt="chair icon" src="front/images/womanTask3.png" /></div>\r\n\r\n<div class="work__ratings work__bnr__img__abs--three anime-right clrlist">\r\n<ul>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class="work__bnr__cont delay2s">\r\n<h4 class="anime-left">Task Completed!</h4>\r\n\r\n<p class="anime-up">Once your task is completed you can release the task funds held with TaskMatch Pay. When this has be done, you can leave a review for the TaskMatch Worker to help improve our Community.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n</div>\r\n</section>\r\n\r\n<section class="category-area task-area">\r\n<div class="container">\r\n<div class="sect__title text-center anime-left col-sm-12">\r\n<h3>Choose from Categories</h3>\r\n\r\n<p>You can get anything done on TaskMatch, whether it&#39;s installing lights in your home or help moving house. Simply describe a task you would like to have done, set a budget and start receiving offers from TaskMatch workers</p>\r\n</div>\r\n\r\n<div class="task-main col-sm-12">\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task1.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Home &amp; Cleaning</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task2.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Delivery &amp; Removals</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task3.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Handyman &amp; Trades</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task4.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Marketing &amp; Design</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task5.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Events &amp; Photography</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task6.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Computer &amp; IT</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task7.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Business &amp; Admin</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="task-box anime-zoomIn col-sm-3">\r\n<div class="task__inr">\r\n<div class="task__img"><a href="#"><img alt="task" src="front/images/task8.jpg" /> </a></div>\r\n\r\n<div class="task__cont valigner">\r\n<div class="valign">\r\n<h4><a href="#">Fun &amp; Quirky</a></h4>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="sect__title sect__title__inr text-center anime-left col-sm-12">\r\n<h3>Customer Support</h3>\r\n\r\n<p class="mb30">Whether it&rsquo;s putting together furniture or cleaning your home, just describe a task you&rsquo;d like to get done, set a budget and you&rsquo;ll start to receive offers from TaskMatch runners.</p>\r\n\r\n<div class="ctg__spt"><a href="#"><img alt="support" src="front/images/support.png" /> </a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="verified-area">\r\n<div class="container">\r\n<div class="verf__lft valigner col-sm-7">\r\n<div class="verf__lft__cont valign">\r\n<h3>Choose from verified workers</h3>\r\n\r\n<p>Task Runners are waiting to complete tasks, you will receive offers within minutes.</p>\r\n\r\n<p>Review TaskMatch workers skills and completed work. You can also view worker profiles and ask questions about their necessary experience.</p>\r\n\r\n<p>Find out more information from workers on why they are perfect for your task. Once you have chosen your TaskMatch worker, you can reach out and discuss your task further.</p>\r\n</div>\r\n</div>\r\n\r\n<div class="verf__rgt col-sm-5">\r\n<ul>\r\n	<li class="anime-right"><img alt="chat" src="front/images/chat11.png" /></li>\r\n	<li class="anime-left"><img alt="chat" src="front/images/chat12.png" /></li>\r\n	<li class="anime-right delay1s"><img alt="chat" src="front/images/chat13.png" /></li>\r\n	<li class="anime-left delay2s"><img alt="chat" src="front/images/chat14.png" /></li>\r\n	<li class="anime-right delay2s"><img alt="chat" src="front/images/chat15.png" /></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="location-area">\r\n<div class="container">\r\n<div class="loc__box col-sm-6">\r\n<div class="loc__cont anime-in">\r\n<h4 class="anime-left">Near to you</h4>\r\n\r\n<p class="anime-left">Find workers in your local area available to complete your Task. All tasks are posted to our map to allow users to search for tasks close to them.</p>\r\n\r\n<p class="anime-left">Tasks are available all over Ireland. TaskMatch allows users to filter the tasks they want based on their location.</p>\r\n</div>\r\n</div>\r\n\r\n<div class="loc__img anime-right col-sm-6">\r\n<div class="loc__img__inr"><img alt="map task" src="front/images/mapTasks.png" /></div>\r\n\r\n<div class="loc__img__pin anime-down delay2s"><img alt="map pin" src="front/images/map-pin.png" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="work-pay-area">\r\n<div class="container">\r\n<div class="pay__lft col-sm-6">\r\n<h3>Secure Payment</h3>\r\n\r\n<p>TaskMatch Pay is a quick and secure way to get your tasks completed. Once an offer is accepted on a task, the agreed upon amount will be securely held in a TaskMatch Trust Account until the task is completed. When the task is completed, you&rsquo;ll need to mark the task as completed and the funds will be transferred to their bank account. Make sure you have this set up before you start completing tasks.</p>\r\n\r\n<div class="pay__lft__icons">\r\n<div class="pay__lft__icon__img"><img alt="card icon" src="front/images/cardIcon.png" /></div>\r\n\r\n<div class="pay__lft__icon__img--2"><img alt="pin icon" src="front/images/PinIcon.png" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n', '', 'how-it-works', '', 'taskmatch,how-it-works', NULL, '2017-05-02 07:47:27', 1, 0, NULL, NULL),
(20, 'page', 'marketplace', 'Market Place', NULL, '', '<section class="market-place">\r\n<div class="container">\r\n<div class="market__place hdng col-sm-12">\r\n<div class="market__place hdng__inner">\r\n<h2>TaskMatch MarketPlace Rules</h2>\r\n</div>\r\n</div>\r\n\r\n<div class="acord__sec market__place col-sm-6 col-sm-offset-3">\r\n<div class="market__inner"><button class="accordion">General Information</button>\r\n\r\n<div class="panel">\r\n<p>The following rules are to make sure all users of TaskMatch have a safe and rewarding experience, it is important for everyone to act responsibly and with respect.</p>\r\n</div>\r\n<button class="accordion">Rules</button>\r\n\r\n<div class="panel">\r\n<p>1. All users must be over the age of 18 to use Taskmatch.</p>\r\n\r\n<p>2. Users can only have one single, active account on the platform and any duplicate accounts will be removed.</p>\r\n\r\n<p>3. Users must ensure that they have provided Taskmatch with the correct banking and personal details for payments to be transferred into.</p>\r\n\r\n<p>4. Inappropriate content with vulgarities, used in the context of a personal attack/insult will be removed immediately and result in suspension and deactivation of accounts.</p>\r\n\r\n<p>5. Tasks posted on Taskmatch must be all legal and all Taskmatch Workers must be legally entitled to perform the task.</p>\r\n\r\n<p>6. Once an offer is accepted a Task Poster must add funds to a task via a VISA or Mastercard credit/debit card. Payment is securely held in an Taskmatch Trust Account. When a task is completed the assigned Taskmatch Worker will request payment and the Task Poster will then confirm the task is completed by releasing funds.</p>\r\n\r\n<p>7. Taskmatch accounts must represent the user completing the job with valid personal details. If your offer is accepted, you are legally responsible for the provision of the agreed services as part of the Task Contract. Subcontracting of tasks is not permitted.</p>\r\n\r\n<p>8. When an offer is accepted a Job Poster is not obliged to pay any additional fees within the Taskmatch marketplace. Pricing for offers placed is final.</p>\r\n\r\n<p>9. You must maintain control of your account at all times, its is your responsibility and it&#39;s not transferrable</p>\r\n\r\n<p>10. All Offers placed must not be hourly or quote based (e.g. &quot;I will work for &euro;10/hour&quot;) They all must be total to the full amount per task.</p>\r\n\r\n<p>11. Any disclosure of private contact details or 3rd party links is not allowed to be shared in any public area of the site (e.g. Business Websites, Facebook/LinkedIn/twitter contact details). This also includes within comments or attachments or anywhere on TaskMatch.</p>\r\n\r\n<p>12. Taskmatch is a marketplace for services and not a discussion forum.Any Comment that is considered off-topic, aimed at influencing other user&#39;s offers or derogatory towards a Task Poster will not be supported and may be deleted at any time and users accounts from where the comments have come may be suspended if felt appropriate.</p>\r\n\r\n<p>13. All Taskmatch members must comply with our marketplace rules. Failure to comply will result in suspension from the marketplace and deactivation of your account.</p>\r\n</div>\r\n<button class="accordion">What you can&#39;t post on TaskMatch.</button>\r\n\r\n<div class="panel">\r\n<p>1. Any tasks or comments that are considered trolling, derogatory or non compliant.</p>\r\n\r\n<p>2. Tasks that are non-task based. e.g. If it does not have a clearly described title and scope of work, a specific start / end time or an unconfirmed price or hourly budget.</p>\r\n\r\n<p>3. Any tasks, attachments or comments which advertise private contact details, third party services or any other websites.</p>\r\n\r\n<p>4. Any Assignment or assessment related tasks, e.g. &quot;finish my college assignment&quot;.</p>\r\n\r\n<p>5. Services that are illegal, scams or any request related to drugs including prescription drugs.</p>\r\n\r\n<p>6. Anything posts related to weapons or unlawful activity. This includes soliciting, inducing or encouraging illegal acts or requesting goods or services in furtherance of a act of crime or terrorism.</p>\r\n\r\n<p>7. Any adult or escort related services, tasks or content which is obscene or sexually explicit in nature.</p>\r\n\r\n<p>8. Business advertisements, offering of services provided or any fishing user contact details, lead generation/sign up requests or competitions.</p>\r\n\r\n<p>9. Services that promote racist, hatred or violence against specific people.</p>\r\n\r\n<p>10. Any commission or sales based tasks, e.g. &quot;earn &euro;50 for every sale you make&quot;, &quot;&euro;1 for every person you get to sign up&quot;.</p>\r\n\r\n<p>11. Financial return or financial transaction based requests, e.g. &quot;Need &euro;1000 and will pay 10% return&quot;.</p>\r\n\r\n<p>12. Any comments or actions that may cause distress or jeopardise other members on the marketplace.</p>\r\n</div>\r\n<button class="accordion">Task Guidelines</button>\r\n\r\n<div class="panel">\r\n<p>All tasks posted must comply with Irish laws and regulations. Do not share your own or any other individual&rsquo;s private contact details (such as full name, email, home address or phone number) in any public area of Taskmatch. For your safety and to maintain the integrity of the Taskmatch marketplace, we suggest that you only communicate through Taskmatch private messaging system. Members have control over the final amount they wish to offer to complete a task (i.e. they may offer higher or lower than the listed price). Any comments that are considered trolling, aimed at influencing/defaming other member&#39;s offers or derogatory towards the Job Poster&rsquo;s budget will not be supported. If you suspect a task or member who may have violated these policies, please report it to Taskmatch using the &quot;Report as inappropriate&rdquo; feature (can be found both in the Task or Comments section) or Contact Us so we can review it accordingly. Members who breach the marketplace rules may have their account suspended or deactivated. Please read the Taskmatch Privacy Policy and Terms of Use in addition to the above rules.</p>\r\n</div>\r\n<button class="accordion">Disputes</button>\r\n\r\n<div class="panel">\r\n<p>As engagements are agreed between you and a 3rd party of your choosing, all agreements are final and Taskmatch is not responsible for refunds for any reason. If you feel you may have been the victim of a crime, document what happened and contact the Gardai. If a crime has taken place in relation to a Taskmatch transaction, provide us with a copy of the Gardai report or guide the authorities to contact us so that we can follow up and take appropriate action.</p>\r\n</div>\r\n<button class="accordion">Personal Conduct</button>\r\n\r\n<div class="panel">\r\n<p>Taskmatch members are other people from your local community and they may even be someone you already know, so treat people as you would like to be treated yourself and use common sense so that we can all enjoy being part of the Taskmatch community. If you experience any abusive communications sent via Taskmatch please let us know by using the report button or <a>Contact</a> Us.</p>\r\n</div>\r\n\r\n<div class="container market__place__btm__area">\r\n<p>Thanks for reading.</p>\r\n\r\n<p>The team at TaskMatch.ie</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<script>\r\n\r\nvar acc = document.getElementsByClassName("accordion");\r\nvar i;\r\n\r\nfor (i = 0; i < acc.length; i++) {\r\n    acc[i].onclick = function(){\r\n        /* Toggle between adding and removing the "active" class,\r\n        to highlight the button that controls the panel */\r\n        this.classList.toggle("active");\r\n\r\n        /* Toggle between hiding and showing the active panel */\r\n        var panel = this.nextElementSibling;\r\n        if (panel.style.display === "block") {\r\n            panel.style.display = "none";\r\n        } else {\r\n            panel.style.display = "block";\r\n        }\r\n    }\r\n}\r\n\r\n</script>', '', 'market-place', 'Home / Market Place', '', NULL, '2017-08-02 17:06:04', 1, 0, NULL, NULL),
(22, 'page', 'posting-a-task', 'Posting a Task', NULL, '', '<section class="posting-bnr-area">\r\n<div class="container">\r\n<div class="posting-bnr-cont valigner col-sm-12">\r\n<div class="posting-bnr-title valign">\r\n<h1>How to Post a Task</h1>\r\n\r\n<h5>The best place to post and outsource tasks</h5>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="fill-area">\r\n<div class="container">\r\n<div class="fill__lft col-sm-6">\r\n<h3>1. Fill in your task form</h3>\r\n\r\n<p>Describe the task you need completed and whether you need it done in person or online. Post any task from cleaning to a delivery in just a few minutes &ndash; completely free of charge! Remember, there&#39;s no obligation to hire.</p>\r\n\r\n<p>Task Runners are waiting to complete tasks, you will receive offers within minutes.</p>\r\n</div>\r\n\r\n<div class="fill__rgt col-sm-6">\r\n<div class="fill__img"><img alt="fill form" src="front/images/form1.png" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="offers-area">\r\n<div class="container">\r\n<div class="offers__lft col-sm-6">\r\n<div class="offers__img"><img alt="offers" src="front/images/post-offers.png" /></div>\r\n</div>\r\n\r\n<div class="offers__rgt col-sm-6">\r\n<h3>2. Receive multiple offers from trusted workers</h3>\r\n\r\n<p>View profiles, verifications and reviews to pick the best Taskrunner for the task. When you accept an offer, you&rsquo;ll add funds securely via Taskmatch Pay, which are safely held in an account until the task has been marked as complete. Once assigned, you can private message and call the assigned Taskrunner.</p>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="fill-area funds-area">\r\n<div class="container">\r\n<div class="fill__lft col-sm-6">\r\n<h3>3. Add funds to the Task account</h3>\r\n\r\n<p>Once you have found the right TaskRunner and accepted their offer, you will then need to pay for your task to be completed.</p>\r\n\r\n<p>With our quick and easy process, you can add your card details and complete the payment process in just a few steps. You can even save your card details for a more convenient checkout process next time.</p>\r\n</div>\r\n\r\n<div class="fill__rgt col-sm-6">\r\n<div class="fill__img"><img alt="add funds" src="front/images/post-funds.png" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class="offers-area choose-task-area">\r\n<div class="container">\r\n<div class="offers__lft col-sm-6">\r\n<div class="offers__img"><img alt="choose task" src="front/images/choose-task.png" /></div>\r\n</div>\r\n\r\n<div class="offers__rgt col-sm-6">\r\n<h3>4. Choose when suits you to complete your task</h3>\r\n\r\n<p>Once your designated Taskmatcher has completed your task, you can mark the task as complete and release the task funds held with Taskmatch Pay. Once released, you can leave a review for the Taskrunner to help other users make the right choice!</p>\r\n</div>\r\n</div>\r\n</section>\r\n', '', 'posting-a-task', '', '', NULL, '2017-08-02 17:03:25', 1, 0, NULL, NULL);
INSERT INTO `content` (`id`, `type`, `code`, `title`, `subject`, `teaser`, `body`, `language_id`, `metaTitle`, `metaDescription`, `keywords`, `created_at`, `updated_at`, `status`, `deleted`, `image`, `url`) VALUES
(23, 'page', 'priceguide', 'Price Guide', NULL, '', '<section class="price-area">\r\n		<div class="container">\r\n			<div class="price-guide-box">\r\n				<div class="price__title text-center">\r\n					<h2>TaskMatch Price Guide</h2>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price-guide-bnr">\r\n					<div class="price-guide-img">\r\n						<img src="front/images/price-guide-img.jpg" alt=" ">\r\n					</div>\r\n					<div class="price-bnr-cont">\r\n						<h3>Not sure how much how much it will cost to have your task completed? Find a guide below</h3>\r\n					</div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__posting text-center clrlist col-sm-8 col-sm-offset-2">\r\n					<div class="price__post__cont">\r\n						<h4 class="mb20">When posting a task, you will be asked to enter a budget to indicate roughly how much you would like to pay. TaskMatch Workers will then bid on your task until you each fair price.</h4>\r\n						<h4>As a guide, we’ve listed out approximate rates for a range of task below:</h4>\r\n						<div class="col-sm-4 text-center">\r\n							<ul>\r\n								<li><a href="#handymanTrades">Handyman & Trades</a></li>\r\n								<li><a href="#deliveryRemovals">Delivery & Removals</a></li>\r\n								<li><a href="#cleaning">Cleaning</a></li>\r\n							</ul>\r\n						</div>\r\n						<div class="col-sm-4 text-center">\r\n							<ul>\r\n								<li><a href="#homeGarden">Home & Garden</a></li>\r\n								<li><a href="#designPhotography">Design & Photography</a></li>\r\n								<li><a href="#officeAdmin">Office & Admin</a></li>\r\n								<li><a href="#computerWebsite">Computer & Website</a></li>\r\n							</ul>\r\n						</div>\r\n						<div class="col-sm-4 text-center">\r\n							<ul>\r\n								<li><a href="#research">Research</a></li>\r\n								<li><a href="#promotionalStuff">Promotional Staff</a></li>\r\n								<li><a href="#furnitureAssembly">Furniture Assembly</a></li>\r\n							</ul>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<p class="price__post__tip mt30">Tip: If you are unsure how much to budget for your task you can simply post it up with an hourly rate of €10 and let our community of TaskMatch Workers offer prices they think are fair.</p>\r\n						\r\n					</div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="handymanTrades">\r\n					<div class="price__task__title">\r\n						<h2>Handyman & Trade Tasks</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Basic</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>No specialist skills required</li>\r\n										<li>Basic tools only</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Skilled</h5>\r\n									<span>€15/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Some special skills required</li>\r\n										<li>Some tools needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Professional</h5>\r\n									<span>€35/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Professional skills required</li>\r\n										<li>Specific tools needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="deliveryRemovals">\r\n					<div class="price__task__title">\r\n						<h2>Delivery & Removals</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Small Item</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Easy to carry</li>\r\n										<li>On foot/Public transport Ok</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Larger Item</h5>\r\n									<span>€20/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Larger box or package</li>\r\n										<li>Car required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Moving/Removals</h5>\r\n									<span>€35/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Furniture or Heavy Items</li>\r\n										<li>Truck or Van required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="cleaning">\r\n					<div class="price__task__title">\r\n						<h2>Cleaning</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Routine Clean</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Apartment or house</li>\r\n										<li>Supplies provided</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Deeper Clean</h5>\r\n									<span>€12/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Windows or other items</li>\r\n										<li>Most supplies provided</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Spring Clean</h5>\r\n									<span>€15/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Major clean up</li>\r\n										<li>Specialist tools needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="homeGarden">\r\n					<div class="price__task__title">\r\n						<h2>Home & Garden</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>General Maintenance</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Light gardening</li>\r\n										<li>Tools provided</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Weeding / Mowing</h5>\r\n									<span>€15/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Moderate amount of labour</li>\r\n										<li>Basic tools needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Specialist Gardening</h5>\r\n									<span>€25/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>More difficult tasks</li>\r\n										<li>Specialist tools needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="designPhotography">\r\n					<div class="price__task__title">\r\n						<h2>Design & Photography</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Basic Design</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Simple tweaks or photoshop</li>\r\n										<li>Basic design knowledge</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Photography /Graphics</h5>\r\n									<span>€20/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Photoshoot or brand design</li>\r\n										<li>Some equipment required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Design Projects</h5>\r\n									<span>€25/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Major brand / photo project</li>\r\n										<li>Specialist skills or equipment</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="officeAdmin">\r\n					<div class="price__task__title">\r\n						<h2>Office & Admin Tasks</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Basic Tasks</h5>\r\n									<span>€12/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Simple research or data entry</li>\r\n										<li>No specialist skills required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Semi skilled tasks</h5>\r\n									<span>€16/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Spreadsheet or call centre work</li>\r\n										<li>Basic admin skills</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Specialist Tasks</h5>\r\n									<span>€25/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Acconting or Marketing work</li>\r\n										<li>Some specialist skills needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="computerWebsite">\r\n					<div class="price__task__title">\r\n						<h2>Computer & Website</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Basic panel example</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Simple tech issue or website tweak</li>\r\n										<li>Basic skills required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Technical tasks</h5>\r\n									<span>€20/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Software / website tech help</li>\r\n										<li>Computer skills needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Specialist required</h5>\r\n									<span>€25/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Programming, development etc</li>\r\n										<li>Professional skills needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="research">\r\n					<div class="price__task__title">\r\n						<h2>Research</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Basic Data Collection</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Email,phone or in-store research</li>\r\n										<li>No Skills required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Field Research</h5>\r\n									<span>€14/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Interviewing, mystery shopping etc</li>\r\n										<li>Basic research skills required</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Research Participants</h5>\r\n									<span>€16/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Focus groups or product testing</li>\r\n										<li>Special candidates requirements</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="promotionalStuff">\r\n					<div class="price__task__title">\r\n						<h2>Promotional Staff</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Leaflet Handouts</h5>\r\n									<span>€10/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Letterbox drops, flyer handouts etc</li>\r\n										<li>Simple, friendly staff</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Event / Sales</h5>\r\n									<span>€15/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Promotional or event help</li>\r\n										<li>Basic sales or catering skills</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-4">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Specialist Staff</h5>\r\n									<span>€20/hour</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Trades eg plumbing, electrical</li>\r\n										<li>Special tools or licence needed</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n				<div class="price__seg text-center col-sm-12" id="furnitureAssembly">\r\n					<div class="price__task__title">\r\n						<h2>Furniture Assembly</h2>\r\n					</div>\r\n					<div class="price__seg__main col-sm-12">\r\n						<div class="price__seg__box col-sm-3">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Simple</h5>\r\n									<span>€10/per Item</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Chair, table, or high chair</li>\r\n										<li>Laptop table</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-3">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Small</h5>\r\n									<span>€20/per Item</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Bookcases</li>\r\n										<li>TV stand</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-3">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Medium</h5>\r\n									<span>€40/per Item</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Sofa-bed or futon</li>\r\n										<li>Chest of drawers</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="price__seg__box col-sm-3">\r\n							<div class="price__seg__inr">\r\n								<div class="price__seg__title">\r\n									<h5>Large</h5>\r\n									<span>€50/per Item</span>\r\n								</div>\r\n								<div class="price__seg__list">\r\n									<ul>\r\n										<li>Bed single, double or queen</li>\r\n										<li>Wardrobe</li>\r\n									</ul>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n						<div class="price__top__btn">\r\n							<a href="#">Back to Top</a>\r\n						</div>\r\n						<div class="clearfix"></div>\r\n					</div>\r\n					<div class="clearfix"></div>\r\n				</div>\r\n				<div class="clearfix"></div>\r\n			</div>\r\n			<div class="clearfix"></div>\r\n		</div>\r\n	</section>', '', 'price-guide', 'Home / Price Guide', '', NULL, NULL, 1, 0, NULL, NULL),
(24, 'page', 'newuserfaq', 'New User FAQ', NULL, '', '<section class="user-faqs-list-section">\r\n<div class="container">\r\n<div class="user-faqs-list">\r\n<div class="user__faqs__list__inner">\r\n<ul>\r\n	<li><a href="#list__item1">What is TaskMatch?</a></li>\r\n	<li><a href="#list__item2">What tasks can I get done on TaskMatch?</a></li>\r\n	<li><a href="#list__item3">How can I get my tasks completed on TaskMatch?</a></li>\r\n	<li><a href="#list__item4">How much does it cost to post a task?</a></li>\r\n	<li><a href="#list__item5">Once I post a task, do I have to hire a TaskMatch Worker?</a></li>\r\n	<li><a href="#list__item6">What Trust &amp; Quality features does TaskMatch provide?</a></li>\r\n	<li><a href="#list__item7">How do I choose the best person to complete my task?</a></li>\r\n	<li><a href="#list__item8">How do I know how much to pay for a task?</a></li>\r\n	<li><a href="#list__item9">What are the payment options for each task?</a></li>\r\n	<li><a href="#list__item10">How do I discuss the task before assigning an TaskMatch Worker to my task?</a></li>\r\n	<li><a href="#list__item11">Who can complete work and earn money on TaskMatch?</a></li>\r\n	<li><a href="#list__item12">How do I start earning money through TaskMatch?</a></li>\r\n	<li><a href="#list__item13">How do I receive payment for completing a task?</a></li>\r\n</ul>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner mt30" id="list__item1">\r\n<h4>What is TaskMatch?</h4>\r\n\r\n<p>TaskMatch is a trusted community marketplace for people to outsource tasks, find local services or complete flexible jobs to earn money - online or via mobile.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item2">\r\n<h4>What tasks can I get done on TaskMatch?</h4>\r\n\r\n<p>You can post almost any task you need done on TaskMatch. From tasks around the house (like gardening or cleaning) to office tasks (like data entry or website support) to tasks for business (like promotional staffing or mystery shopper).</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item3">\r\n<h4>How can I get my tasks completed on TaskMatch?</h4>\r\n\r\n<p>It&#39;s very simple. You Start by Posting a Task - it&#39;s FREE to post and takes just a few minutes. TaskMatch Workers will then make offers to complete your task. Check out their profile and once you&#39;ve found the TaskMatch worker that&#39;s perfect for your task, accept their offer to assign them to the task.</p>\r\n\r\n<p>When accepting the offer you will Add Funds for the task via TaskMatch Pay, which are securely held in an TaskMatch Trust Account until the task has been completed. This allows the TaskMatch Worker to start working knowing there are Funds secured for the task. Funds are only released to the TaskMatch Worker once both of you are satisfied that the task has been completed.</p>\r\n\r\n<p>Read more about <a>How it Works</a></p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item4">\r\n<h4>How much does it cost to post a task?</h4>\r\n\r\n<p>It is FREE to post a task. Once you have accepted the agreed task price you will need to Add Funds for the task via TaskMatch Pay, which are securely held in an TaskMatch Trust Account until the task has been completed. You will need to Release Funds upon task completion. There are no additional fees or hidden costs and cash payments are not supported on TaskMatch. (We deduct a Service Fee from the TaskMatch Worker who completes your job.)</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item5">\r\n<h4>Once I post a task, do I have to hire a TaskMatch Worker?</h4>\r\n\r\n<p>No, it is FREE to post a task and there are no obligations to select an TaskMatch Worker unless you find the right person for the job and accept their offer.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item6">\r\n<h4>What Trust &amp; Quality features does TaskMatch provide?</h4>\r\n\r\n<p>To make sure you have a rewarding TaskMatch experience, we&#39;ve built a range of features which will help deliver you peace of mind. We provide verified IDs, secure payments via TaskMatch Pay and a Customer Support Team ready to assist any time.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item7">\r\n<h4>How do I choose the best person to complete my task?</h4>\r\n\r\n<p>Usually you&#39;ll receive offers from 2-3 people who can complete your task. You can ask them questions, request further details about their skills and check out their profile to make sure that you find the right person for the task. There are no obligations to hire.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item8">\r\n<h4>How do I know how much to pay for a task?</h4>\r\n\r\n<p>The price that you offer is simply a starting point and experienced TaskMatch Workers will make offers so that you reach a fair price. There are no strict rules about how much to offer but as a starting point, think of how much time it might take the person to complete the task and what a fair hourly rate could be depending on the experience or skills required. You can also <a>Browse Tasks</a> and check out similar tasks before making your offer or check our <a>Price Guide</a></p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item9">\r\n<h4>What are the payment options for each task?</h4>\r\n\r\n<p>When accepting an offer you are required to Add Funds. Your payment method can be either a VISA or Mastercard Debit/Credit card. Cash Payments are not supported on TaskMatch, once you&#39;ve added Funds you have paid for the task to be completed. Back to the Top</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item10">\r\n<h4>How do I discuss the task before assigning a TaskMatch Worker to my task?</h4>\r\n\r\n<p>Whether you&#39;re a Job Poster or an TaskMatch Worker, ensuring your tasks are completed requires communication. We&#39;ve built a dedicated commenting wall for each task, where you can comment on the task and ask questions.</p>\r\n\r\n<p>Once the Job Poster has accepted a TaskMatch Worker&#39;s offer and they&#39;ve been assigned to a task, you can communicate safely through Private Messages to discuss further details.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item11">\r\n<h4>Who can complete work and earn money on TaskMatch?</h4>\r\n\r\n<p>Almost anyone can <a>create an account</a> and start bidding for work on TaskMatch. To make sure you&#39;re competitive when making offers for tasks, you&#39;ll need to build your TaskMatch profile with social connections and by earning positive reviews on completed tasks.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item12">\r\n<h4>How do I start earning money through TaskMatch?</h4>\r\n\r\n<p>Create an account and then start building your profile to make yourself attractive to people hiring on TaskMatch. Once you&#39;re ready to get going: <a>Browse Tasks</a>, start commenting and Make An Offer if you think you&#39;re the best person for the job.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n\r\n<div class="user-list-area">\r\n<div class="user__list__area__inner" id="list__item13">\r\n<h4>How do I receive payment for completing a task?</h4>\r\n\r\n<p>Once your offer has been accepted the Job Poster has already securely added Funds for the task, held in an TaskMatch Trust Account until it has been completed. When completed, select Request Payment and the Job Poster will be notified to Release Funds held into your verified Bank or PayPal account.</p>\r\n<a href="#top">Back to the top</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n', '', 'new-user-faq', 'Home / new-user-faq', '', NULL, '2017-08-02 17:15:57', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE `educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `title`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Engineering', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 'UCD', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(3, 'Digital Marketing Institute', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(4, 'web design', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(5, 'web development', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(6, 'social media', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(7, 'digital marketing', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(8, 'education', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(9, 'Machining mechanics', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(10, 'Mechanical technician', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(11, 'Computer engineering', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(12, 'Space Mechanics and Control MSc', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(13, 'Software Engineering', 0, 1, '2017-09-06 21:14:27', '2017-09-06 21:14:27'),
(14, 'Postgraduated Software Engineering', 0, 1, '2017-09-06 21:14:57', '2017-09-06 21:14:57'),
(15, 'Graduated Industrial Automation', 0, 1, '2017-09-06 21:16:47', '2017-09-06 21:16:47'),
(16, 'Admin', 0, 1, '2017-09-06 22:43:16', '2017-09-06 22:43:16'),
(17, 'economist', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(18, 'finance', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(19, 'energy and natural gas', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(20, 'BSSE', 0, 1, '2017-09-15 16:29:41', '2017-09-15 16:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `alt` varchar(20) DEFAULT NULL,
  `filePath` varchar(50) DEFAULT NULL,
  `fileType` varchar(15) NOT NULL,
  `status` int(2) DEFAULT '1',
  `deleted` int(2) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `relatedTo` varchar(15) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `friendId` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`id`, `user_id`, `friendId`, `name`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 38, 10154940546747654, 'Cecilia Boyadjian', 0, 1, '2017-09-06 21:13:02', '2017-09-06 21:13:02'),
(2, 38, 301836196895388, 'John Bynre', 0, 1, '2017-09-06 21:13:02', '2017-09-06 21:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `title`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 'French', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(3, 'German', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(4, 'Spanish', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(5, 'Polish', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(6, 'Portuguese', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(7, 'English and Portuguese', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(8, 'Espanhol ', 0, 1, '2017-09-06 22:43:16', '2017-09-06 22:43:16'),
(9, 'italian', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to` int(11) DEFAULT NULL,
  `from` int(11) NOT NULL,
  `reply_to` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `key` text COLLATE utf8_unicode_ci NOT NULL,
  `seenDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `thread_id`, `message`, `subject`, `to`, `from`, `reply_to`, `deleted`, `status`, `created_at`, `updated_at`, `key`, `seenDate`) VALUES
(1, 1, 'Hi Paul,\r\n\r\nThanks for the offer. I will message you now', '', 3, 2, NULL, 0, 1, '2017-08-31 12:06:31', '2017-09-02 12:44:23', 'pW84iN1X1109115E', '2017-09-02 12:44:23'),
(2, 1, 'Hi Paul, thanks for the great job', '', 3, 2, NULL, 0, 1, '2017-08-31 12:06:53', '2017-09-02 12:44:23', '', '2017-09-02 12:44:23'),
(3, 2, 'Hi Thanks, I will message you now.', '', 3, 6, NULL, 0, 1, '2017-09-01 09:23:10', '2017-09-02 12:44:22', 'UF50579rasG20714', '2017-09-02 12:44:22'),
(4, 3, 'Thanks Keith - Offer accepted ', '', 15, 3, NULL, 0, 1, '2017-09-04 08:59:11', '2017-09-04 08:59:11', '105D1u55o4Z5s15Q', NULL),
(5, 4, 'Great, can you come this weekend ?', '', 45, 38, NULL, 0, 1, '2017-09-09 13:45:32', '2017-09-11 12:12:45', '1oA3744029gob5P6', '2017-09-11 12:12:45'),
(6, 5, 'Great, can you come this weekend ?', '', 45, 38, NULL, 0, 1, '2017-09-09 13:45:36', '2017-09-11 12:25:07', '30er14g69RY7456C', '2017-09-11 12:25:07'),
(7, 5, 'Is stripe working for you now', '', 38, 45, NULL, 0, 1, '2017-09-11 12:12:35', '2017-09-11 12:23:53', '', '2017-09-11 12:23:53'),
(8, 5, 'Nope still not working\r\n', '', 45, 38, NULL, 0, 1, '2017-09-11 12:24:00', '2017-09-11 12:25:07', '', '2017-09-11 12:25:07'),
(9, 5, 'Matt had trouble connecting to it swell, I will contact golpik now?\r\nSo you juts can\'t connect?\r\n', '', 38, 45, NULL, 0, 1, '2017-09-11 12:25:43', '0000-00-00 00:00:00', '', NULL),
(10, 5, 'Im in class in dundalk\r\n', '', 38, 45, NULL, 0, 1, '2017-09-11 12:26:43', '0000-00-00 00:00:00', '', NULL),
(11, 6, 'Okay Done!!!', '', 75, 74, NULL, 0, 1, '2017-09-15 16:26:40', '2017-09-15 16:28:51', 'yk8F40q52091d058', '2017-09-15 16:28:51'),
(12, 6, 'Thanks for accepting!!', '', 74, 75, NULL, 0, 1, '2017-09-15 16:27:20', '0000-00-00 00:00:00', '', NULL),
(13, 6, 'Okay Okay1!!!!!', '', 75, 74, NULL, 0, 1, '2017-09-15 16:27:43', '2017-09-15 16:28:51', '', '2017-09-15 16:28:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notify_to` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seen` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `type_id`, `user_id`, `notify_to`, `description`, `deleted`, `status`, `created_at`, `updated_at`, `seen`) VALUES
(1, 'task_offer', 1, 3, 2, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'https://www.taskmatch.ie/offers/15G1lG1800I5465Q\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Assemble Ikea Furniture </strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-08-31 12:04:16', '2017-09-11 12:11:54', 1),
(2, 'task_accept', 1, 2, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/7121504108826.jpg\\\' alt = \\\'John Bynre\\\' title=\\\'John Bynre\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/9j71u54C82111a10\\\'><span>John Bynre</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Assemble Ikea Furniture </strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-08-31 12:06:31', '2017-09-08 14:51:25', 1),
(3, 'message_receive', 2, 2, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/7121504108826.jpg\\\' alt = \\\'John Bynre\\\' title=\\\'John Bynre\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/9j71u54C82111a10\\\'><span>John Bynre</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Assemble Ikea Furniture </strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-08-31 12:06:53', '2017-09-08 14:51:25', 1),
(4, 'payment_made', 1, 2, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/7121504108826.jpg\\\' alt = \\\'John Bynre\\\' title=\\\'John Bynre\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/l41Y373OM610DI95\\\'><span>John Bynre</span> <small><i class=\\"fa fa-check\\"></i> mark completed & made payment on </small><strong>Assemble Ikea Furniture </strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-08-31 12:07:05', '2017-09-08 14:51:25', 1),
(5, 'task_review', 1, 2, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/7121504108826.jpg\\\' alt = \\\'John Bynre\\\' title=\\\'John Bynre\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/my-reviews\\\'><span>John Bynre</span> <small><i class=\\"fa fa-eye\\"></i> gave review on</small> <strong>Assemble Ikea Furniture </strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-08-31 12:08:23', '2017-09-08 14:51:25', 1),
(6, 'task_offer', 2, 3, 6, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'https://www.taskmatch.ie/offers/5457DMu10O2007H0\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Gardening Needed</strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-09-01 09:21:40', '2017-09-01 09:22:51', 1),
(7, 'task_accept', 2, 6, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/6.png\\\' alt = \\\'Patrick  Fagan\\\' title=\\\'Patrick  Fagan\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/204J1z557609v7Dq\\\'><span>Patrick  Fagan</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Gardening Needed</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-01 09:23:10', '2017-09-08 14:51:25', 1),
(8, 'task_offer', 3, 15, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/15.png\\\' alt = \\\'Keith Wallace\\\' title=\\\'Keith Wallace\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'https://www.taskmatch.ie/offers/4273Sj50045A15Ng\\\'><span>Keith Wallace</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Build a wordpress website</strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-09-02 13:04:10', '2017-09-08 14:51:25', 1),
(9, 'task_accept', 3, 3, 15, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/10u51I7555c15cn4\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Build a wordpress website</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-04 08:59:11', '0000-00-00 00:00:00', 0),
(10, 'task_comment', 1, 1, 8, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <div class=\\"notifi__img\\"><img src = \\\'https://www.taskmatch.ie/front/images/default.png\\\' alt = \\\'TaskMatch Admin\\\' title = \\\'TaskMatch Admin\\\' /></div>\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/j48185872n0952xh\\\'><span>TaskMatch Admin</span> <small><i class=\\"fa fa-comment\\"></i> commented</small> <strong>Clean my Air bnb apartment </strong></a></h4>\n  </div>\n</div>\n\n', 0, 1, '2017-09-06 22:00:49', '0000-00-00 00:00:00', 0),
(11, 'task_offer', 4, 45, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/45.png\\\' alt = \\\'Conor Anthony\\\' title=\\\'Conor Anthony\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'https://www.taskmatch.ie/offers/y7144M97pBn510N6\\\'><span>Conor Anthony</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-09-07 10:20:14', '2017-09-11 12:14:45', 1),
(12, 'task_offer', 5, 3, 35, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt = \\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'https://www.taskmatch.ie/offers/04i8953q3611i477\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Cleaner needed</strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-09-07 10:21:24', '0000-00-00 00:00:00', 0),
(13, 'task_comment', 2, 63, 3, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <div class=\\"notifi__img\\"><img src = \\\'https://www.taskmatch.ie/front/images/default.png\\\' alt = \\\'Simon Williams\\\' title = \\\'Simon Williams\\\' /></div>\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/203AW9oG15g487H1\\\'><span>Simon Williams</span> <small><i class=\\"fa fa-comment\\"></i> commented</small> <strong>Paint a two bed apartment</strong></a></h4>\n  </div>\n</div>\n\n', 0, 1, '2017-09-08 14:16:23', '2017-09-08 14:51:25', 1),
(14, 'task_comment', 3, 63, 5, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <div class=\\"notifi__img\\"><img src = \\\'https://www.taskmatch.ie/front/images/default.png\\\' alt = \\\'Simon Williams\\\' title = \\\'Simon Williams\\\' /></div>\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/511zLe55103gn4H8\\\'><span>Simon Williams</span> <small><i class=\\"fa fa-comment\\"></i> commented</small> <strong>Window cleaner</strong></a></h4>\n  </div>\n</div>\n\n', 0, 1, '2017-09-08 14:46:08', '0000-00-00 00:00:00', 0),
(15, 'task_quote', 2, 3, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt=\\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/203AW9oG15g487H1\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-quote-left\\"></i> has invited you to quote on their task</small> <strong>Paint a two bed apartment</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-08 14:52:13', '2017-09-11 12:14:45', 1),
(16, 'task_quote', 2, 3, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3.png\\\' alt=\\\'Paul  Kennedy\\\' title=\\\'Paul  Kennedy\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/task/203AW9oG15g487H1\\\'><span>Paul  Kennedy</span> <small><i class=\\"fa fa-quote-left\\"></i> has invited you to quote on their task</small> <strong>Paint a two bed apartment</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-08 14:54:17', '2017-09-11 12:14:45', 1),
(17, 'task_accept', 4, 38, 45, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3631504732368.jpg\\\' alt = \\\'Keith Wallace\\\' title=\\\'Keith Wallace\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/I0kb496241a7Y5O3\\\'><span>Keith Wallace</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-09 13:45:32', '2017-09-11 12:25:03', 1),
(18, 'task_accept', 5, 38, 45, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3631504732368.jpg\\\' alt = \\\'Keith Wallace\\\' title=\\\'Keith Wallace\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/47VGDg6Q5x963401\\\'><span>Keith Wallace</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-09 13:45:36', '2017-09-11 12:25:03', 1),
(19, 'message_receive', 7, 45, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/45.png\\\' alt = \\\'Conor Anthony\\\' title=\\\'Conor Anthony\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/47VGDg6Q5x963401\\\'><span>Conor Anthony</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-11 12:12:35', '2017-09-11 12:14:45', 1),
(20, 'message_receive', 8, 38, 45, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/3631504732368.jpg\\\' alt = \\\'Keith Wallace\\\' title=\\\'Keith Wallace\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/47VGDg6Q5x963401\\\'><span>Keith Wallace</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-11 12:24:00', '2017-09-11 12:25:03', 1),
(21, 'message_receive', 9, 45, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/45.png\\\' alt = \\\'Conor Anthony\\\' title=\\\'Conor Anthony\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/47VGDg6Q5x963401\\\'><span>Conor Anthony</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-11 12:25:43', '0000-00-00 00:00:00', 0),
(22, 'message_receive', 10, 45, 38, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'https://www.taskmatch.ie/uploads/users/profile/45.png\\\' alt = \\\'Conor Anthony\\\' title=\\\'Conor Anthony\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'https://www.taskmatch.ie/message/47VGDg6Q5x963401\\\'><span>Conor Anthony</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Light needs fixing</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-11 12:26:43', '0000-00-00 00:00:00', 0),
(23, 'task_comment', 4, 75, 74, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <div class=\\"notifi__img\\"><img src = \\\'http://localhost/projects/laravel/taskmatch/web/front/images/default.png\\\' alt = \\\'Task Runner\\\' title = \\\'Task Runner\\\' /></div>\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'http://localhost/projects/laravel/taskmatch/web/task/y10V8t45i55923h4\\\'><span>Task Runner</span> <small><i class=\\"fa fa-comment\\"></i> commented</small> <strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>\n\n', 0, 1, '2017-09-15 15:20:57', '2017-09-15 16:26:25', 1),
(24, 'task_comment', 5, 75, 74, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <div class=\\"notifi__img\\"><img src = \\\'http://localhost/projects/laravel/taskmatch/web/front/images/default.png\\\' alt = \\\'Task Runner\\\' title = \\\'Task Runner\\\' /></div>\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'http://localhost/projects/laravel/taskmatch/web/task/y10V8t45i55923h4\\\'><span>Task Runner</span> <small><i class=\\"fa fa-comment\\"></i> commented</small> <strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>\n\n', 0, 1, '2017-09-15 15:32:00', '2017-09-15 16:26:25', 1),
(25, 'task_offer', 6, 75, 74, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'http://localhost/projects/laravel/taskmatch/web/uploads/users/profile/75.png\\\' alt = \\\'Task Runner\\\' title=\\\'Task Runner\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4><a href=\\\'http://localhost/projects/laravel/taskmatch/web/offers/409131c9bXns55l5\\\'><span>Task Runner</span> <small><i class=\\"fa fa-inbox\\"></i> offered on</small> <strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>\n\n								\n', 0, 1, '2017-09-15 16:12:15', '2017-09-15 16:26:25', 1),
(26, 'task_accept', 6, 74, 75, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <img src = \\\'http://localhost/projects/laravel/taskmatch/web/front/images/default.png\\\' alt = \\\'Task Poster\\\' title = \\\'Task Poster\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'http://localhost/projects/laravel/taskmatch/web/message/210808Sf95c504mn\\\'><span>Task Poster</span> <small><i class=\\"fa fa-check\\"></i> accepted your offer on </small><strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-15 16:26:40', '2017-09-15 16:28:47', 1),
(27, 'message_receive', 12, 75, 74, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n                      \n        <img class=\\\'img-circle\\\' src=\\\'http://localhost/projects/laravel/taskmatch/web/uploads/users/profile/75.png\\\' alt = \\\'Task Runner\\\' title=\\\'Task Runner\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'http://localhost/projects/laravel/taskmatch/web/message/210808Sf95c504mn\\\'><span>Task Runner</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-15 16:27:20', '0000-00-00 00:00:00', 0),
(28, 'message_receive', 13, 74, 75, '<div class=\\"media notifi-area col-sm-12\\">\n  <a class=\\"notifi__img pull-left\\" href=\\"#\\">\n            <img src = \\\'http://localhost/projects/laravel/taskmatch/web/front/images/default.png\\\' alt = \\\'Task Poster\\\' title = \\\'Task Poster\\\' />\n      </a>\n  <div class=\\"media-body\\">\n      <h4> <a href=\\\'http://localhost/projects/laravel/taskmatch/web/message/210808Sf95c504mn\\\'><span>Task Poster</span> <small><i class=\\"fa fa-envelope-o\\"></i> sent you a message on </small><strong>Golpik Create Marketplace</strong></a></h4>\n  </div>\n</div>', 0, 1, '2017-09-15 16:27:43', '2017-09-15 16:28:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications_settings`
--

CREATE TABLE `notifications_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` int(11) NOT NULL,
  `notification` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications_settings`
--

INSERT INTO `notifications_settings` (`id`, `user_id`, `type`, `email`, `notification`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 2, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 2, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 2, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 2, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 3, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 3, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 3, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 3, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 3, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 3, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 3, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 3, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 3, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 3, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 3, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 3, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 3, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 4, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 4, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 4, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 4, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 4, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 4, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 4, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 4, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 4, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 4, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 4, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 4, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 4, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 5, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 5, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 5, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 5, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 5, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 5, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 5, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 5, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 5, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 5, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 5, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 5, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 5, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 6, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 6, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 6, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 6, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 6, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 6, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 6, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 6, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 6, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 6, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 6, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 6, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 6, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 7, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 7, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 7, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 7, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 7, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 7, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 7, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 7, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 7, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 7, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 7, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 7, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 7, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 8, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 8, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 8, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 8, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 8, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 8, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 8, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 8, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 8, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 8, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 8, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 8, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 8, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 9, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 9, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 9, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 9, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 9, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 9, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 9, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 9, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 9, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 9, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 9, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 9, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 9, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 10, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 10, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 10, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 10, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 10, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 10, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 10, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 10, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 10, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 10, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 10, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 10, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 10, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 11, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 11, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 11, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 11, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 11, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 11, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 11, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 11, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 11, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 11, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 11, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 11, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 11, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 12, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 12, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 12, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 12, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 12, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 12, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 12, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 12, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 12, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 12, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 12, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 12, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 12, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 13, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 13, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 13, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 13, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 13, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 13, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 13, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 13, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 13, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 13, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 13, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 13, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 13, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 14, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 14, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 14, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 14, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 14, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 14, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 14, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 14, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 14, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 14, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 14, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 14, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 14, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 15, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 15, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 15, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 15, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 15, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 15, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 15, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 15, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 15, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 15, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 15, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 15, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 15, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 16, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 16, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 16, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 16, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 16, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 16, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 16, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 16, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 16, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 16, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 16, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 16, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 16, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 19, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 19, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 19, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 19, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 19, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 19, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 19, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 19, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 19, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 19, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 19, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 19, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 19, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 20, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, 20, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, 20, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 20, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 20, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 20, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 20, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 20, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 20, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 20, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 20, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 20, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 20, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 21, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 21, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 21, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 21, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 21, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 21, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 21, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 21, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 21, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 21, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 21, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 21, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 21, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 22, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 22, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 22, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 22, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, 22, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 22, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 22, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 22, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 22, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 22, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 22, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 22, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 22, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 23, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 23, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 23, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 23, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 23, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 23, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 23, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 23, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 23, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 23, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 23, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 23, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 23, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 24, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 24, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 24, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 24, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 24, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 24, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 24, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 24, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 24, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 24, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 24, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 24, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 24, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 25, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 25, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 25, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 25, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 25, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 25, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 25, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 25, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 25, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 25, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 25, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, 25, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 25, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 26, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 26, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 26, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 26, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 26, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 26, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 26, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, 26, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, 26, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, 26, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 26, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 26, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, 26, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 28, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, 28, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 28, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, 28, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 28, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, 28, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 28, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, 28, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 28, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 28, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 28, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, 28, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, 28, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, 29, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, 29, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, 29, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, 29, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, 29, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, 29, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, 29, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, 29, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, 29, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, 29, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, 29, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, 29, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, 29, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, 30, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, 30, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, 30, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, 30, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, 30, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, 30, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, 30, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, 30, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, 30, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, 30, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, 30, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, 30, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, 30, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, 31, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, 31, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, 31, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, 31, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, 31, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, 31, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, 31, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, 31, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, 31, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, 31, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, 31, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, 31, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, 31, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 32, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, 32, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, 32, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, 32, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, 32, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, 32, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, 32, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, 32, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, 32, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, 32, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, 32, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, 32, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, 32, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, 33, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, 33, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, 33, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, 33, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 33, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, 33, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, 33, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, 33, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, 33, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, 33, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, 33, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, 33, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, 33, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, 34, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, 34, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, 34, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, 34, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, 34, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, 34, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, 34, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, 34, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, 34, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, 34, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, 34, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, 34, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, 34, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, 35, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, 35, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, 35, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, 35, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, 35, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, 35, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, 35, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, 35, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, 35, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, 35, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, 35, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, 35, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, 35, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, 36, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, 36, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, 36, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, 36, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, 36, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, 36, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, 36, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, 36, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, 36, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, 36, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, 36, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, 36, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, 36, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, 37, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, 37, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, 37, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, 37, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, 37, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, 37, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, 37, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, 37, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, 37, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, 37, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, 37, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, 37, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, 37, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, 38, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, 38, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, 38, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, 38, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, 38, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, 38, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, 38, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, 38, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, 38, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, 38, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, 38, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, 38, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, 38, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, 39, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, 39, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, 39, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, 39, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, 39, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, 39, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, 39, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, 39, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, 39, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, 39, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, 39, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, 39, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, 39, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, 40, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, 40, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, 40, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, 40, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, 40, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, 40, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, 40, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, 40, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, 40, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, 40, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, 40, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, 40, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, 40, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, 41, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, 41, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, 41, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, 41, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, 41, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, 41, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, 41, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, 41, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, 41, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, 41, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, 41, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, 41, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, 41, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, 42, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(483, 42, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(484, 42, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(485, 42, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, 42, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, 42, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, 42, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, 42, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, 42, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, 42, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, 42, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(493, 42, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(494, 42, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(495, 43, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(496, 43, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(497, 43, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(498, 43, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(499, 43, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(500, 43, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(501, 43, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(502, 43, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(503, 43, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(504, 43, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(505, 43, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(506, 43, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(507, 43, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(508, 44, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(509, 44, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(510, 44, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(511, 44, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(512, 44, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(513, 44, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(514, 44, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(515, 44, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(516, 44, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(517, 44, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(518, 44, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(519, 44, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(520, 44, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(521, 45, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(522, 45, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(523, 45, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(524, 45, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(525, 45, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(526, 45, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(527, 45, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(528, 45, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(529, 45, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(530, 45, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(531, 45, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(532, 45, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(533, 45, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(534, 46, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(535, 46, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(536, 46, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(537, 46, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(538, 46, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(539, 46, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(540, 46, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(541, 46, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(542, 46, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(543, 46, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(544, 46, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(545, 46, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(546, 46, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(547, 47, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(548, 47, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(549, 47, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(550, 47, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(551, 47, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(552, 47, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(553, 47, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(554, 47, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(555, 47, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(556, 47, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(557, 47, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(558, 47, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(559, 47, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(560, 48, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(561, 48, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(562, 48, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(563, 48, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(564, 48, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(565, 48, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(566, 48, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(567, 48, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(568, 48, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(569, 48, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(570, 48, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(571, 48, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(572, 48, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(573, 49, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(574, 49, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(575, 49, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(576, 49, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(577, 49, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(578, 49, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(579, 49, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(580, 49, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(581, 49, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(582, 49, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(583, 49, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(584, 49, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(585, 49, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(586, 50, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(587, 50, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(588, 50, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(589, 50, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(590, 50, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(591, 50, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(592, 50, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(593, 50, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(594, 50, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(595, 50, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(596, 50, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(597, 50, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(598, 50, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(599, 52, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(600, 52, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(601, 52, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(602, 52, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `notifications_settings` (`id`, `user_id`, `type`, `email`, `notification`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(603, 52, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(604, 52, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(605, 52, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(606, 52, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(607, 52, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(608, 52, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(609, 52, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(610, 52, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(611, 52, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(612, 53, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(613, 53, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(614, 53, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(615, 53, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(616, 53, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(617, 53, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(618, 53, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(619, 53, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(620, 53, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(621, 53, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(622, 53, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(623, 53, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(624, 53, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(625, 54, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(626, 54, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(627, 54, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(628, 54, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(629, 54, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(630, 54, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(631, 54, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(632, 54, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(633, 54, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(634, 54, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(635, 54, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(636, 54, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(637, 54, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(638, 55, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(639, 55, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(640, 55, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(641, 55, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(642, 55, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(643, 55, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(644, 55, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(645, 55, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(646, 55, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(647, 55, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(648, 55, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(649, 55, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(650, 55, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(651, 56, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(652, 56, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(653, 56, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(654, 56, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(655, 56, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(656, 56, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(657, 56, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(658, 56, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(659, 56, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(660, 56, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(661, 56, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(662, 56, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(663, 56, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(664, 57, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(665, 57, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(666, 57, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(667, 57, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(668, 57, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(669, 57, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(670, 57, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(671, 57, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(672, 57, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(673, 57, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(674, 57, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(675, 57, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(676, 57, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(677, 58, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(678, 58, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(679, 58, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(680, 58, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(681, 58, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(682, 58, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(683, 58, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(684, 58, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(685, 58, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(686, 58, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(687, 58, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(688, 58, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(689, 58, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(690, 61, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(691, 61, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(692, 61, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(693, 61, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(694, 61, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(695, 61, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(696, 61, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(697, 61, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(698, 61, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(699, 61, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(700, 61, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(701, 61, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(702, 61, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(703, 62, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(704, 62, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(705, 62, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(706, 62, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(707, 62, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(708, 62, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(709, 62, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(710, 62, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(711, 62, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(712, 62, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(713, 62, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(714, 62, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(715, 62, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(716, 63, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(717, 63, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(718, 63, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(719, 63, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(720, 63, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(721, 63, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(722, 63, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(723, 63, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(724, 63, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(725, 63, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(726, 63, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(727, 63, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(728, 63, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(729, 64, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(730, 64, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(731, 64, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(732, 64, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(733, 64, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(734, 64, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(735, 64, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(736, 64, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(737, 64, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(738, 64, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(739, 64, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(740, 64, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(741, 64, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(742, 65, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(743, 65, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(744, 65, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(745, 65, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(746, 65, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(747, 65, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(748, 65, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(749, 65, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(750, 65, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(751, 65, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(752, 65, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(753, 65, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(754, 65, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(755, 66, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(756, 66, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(757, 66, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(758, 66, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(759, 66, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(760, 66, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(761, 66, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(762, 66, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(763, 66, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(764, 66, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(765, 66, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(766, 66, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(767, 66, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(768, 67, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(769, 67, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(770, 67, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(771, 67, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(772, 67, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(773, 67, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(774, 67, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(775, 67, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(776, 67, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(777, 67, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(778, 67, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(779, 67, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(780, 67, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(781, 68, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(782, 68, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(783, 68, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(784, 68, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(785, 68, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(786, 68, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(787, 68, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(788, 68, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(789, 68, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(790, 68, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(791, 68, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(792, 68, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(793, 68, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(794, 69, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(795, 69, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(796, 69, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(797, 69, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(798, 69, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(799, 69, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(800, 69, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(801, 69, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(802, 69, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(803, 69, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(804, 69, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(805, 69, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(806, 69, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(807, 70, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(808, 70, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(809, 70, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(810, 70, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(811, 70, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(812, 70, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(813, 70, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(814, 70, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(815, 70, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(816, 70, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(817, 70, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(818, 70, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(819, 70, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(820, 71, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(821, 71, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(822, 71, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(823, 71, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(824, 71, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(825, 71, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(826, 71, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(827, 71, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(828, 71, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(829, 71, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(830, 71, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(831, 71, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(832, 71, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(833, 72, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(834, 72, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(835, 72, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(836, 72, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(837, 72, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(838, 72, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(839, 72, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(840, 72, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(841, 72, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(842, 72, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(843, 72, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(844, 72, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(845, 72, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(846, 73, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(847, 73, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(848, 73, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(849, 73, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(850, 73, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(851, 73, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(852, 73, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(853, 73, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(854, 73, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(855, 73, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(856, 73, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(857, 73, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(858, 73, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(859, 74, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(860, 74, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(861, 74, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(862, 74, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(863, 74, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(864, 74, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(865, 74, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(866, 74, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(867, 74, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(868, 74, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(869, 74, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(870, 74, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(871, 74, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(872, 75, 'task_comment', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(873, 75, 'task_offer', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(874, 75, 'offer_reject', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(875, 75, 'task_accept', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(876, 75, 'task_cancel', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(877, 75, 'task_quote', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(878, 75, 'money_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(879, 75, 'payment_made', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(880, 75, 'message_receive', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(881, 75, 'reviews_awaiting', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(882, 75, 'tasks_posted', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(883, 75, 'task_review', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(884, 75, 'offer_ammend', 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `offerStatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `task_id`, `user_id`, `message_id`, `offerStatus`, `deleted`, `status`, `created_at`, `updated_at`, `message`) VALUES
(1, 1, 3, NULL, 'completed', 0, 1, '2017-08-31 12:04:16', '2017-08-31 12:07:02', 'Hi John, I have lots of experience assembling ikea furniture and can do this no problem. I have tools. Thanks'),
(2, 4, 3, NULL, 'assigned', 0, 1, '2017-09-01 09:21:40', '2017-09-01 09:23:10', 'Hi Patrick, I can complete this no problem and available anytime. Thanks'),
(3, 9, 15, NULL, 'assigned', 1, 1, '2017-09-02 13:04:10', '2017-09-04 08:59:11', 'I\'d be interested in talking about what you need on the site'),
(4, 12, 45, NULL, 'assigned', 0, 1, '2017-09-07 10:20:14', '2017-09-09 13:45:36', 'Hi Keith, I can complete this. Cheers'),
(5, 11, 3, NULL, 'pending', 0, 1, '2017-09-07 10:21:24', '2017-09-07 10:21:24', 'Hi, Conor here. Can you accept this offer please'),
(6, 13, 75, NULL, 'completed', 0, 1, '2017-09-15 16:12:15', '2017-09-15 16:31:56', 'I want to do this job ! ');

-- --------------------------------------------------------

--
-- Table structure for table `offer_prices`
--

CREATE TABLE `offer_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `offerPrice` double(8,2) NOT NULL,
  `commission` double(8,2) NOT NULL,
  `finalPrice` double(8,2) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `key` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offer_prices`
--

INSERT INTO `offer_prices` (`id`, `offerPrice`, `commission`, `finalPrice`, `offer_id`, `deleted`, `status`, `created_at`, `updated_at`, `key`) VALUES
(1, 100.00, 15.00, 85.00, 1, 0, 1, '2017-08-31 12:04:16', '2017-08-31 12:04:16', '15G1lG1800I5465Q'),
(2, 60.00, 15.00, 51.00, 2, 0, 1, '2017-09-01 09:21:40', '2017-09-01 09:21:40', '5457DMu10O2007H0'),
(3, 1.00, 15.00, 170.00, 3, 0, 1, '2017-09-02 13:04:10', '2017-09-02 13:04:10', '4273Sj50045A15Ng'),
(4, 1.00, 15.00, 21.25, 4, 0, 1, '2017-09-07 10:20:14', '2017-09-07 10:20:14', 'y7144M97pBn510N6'),
(5, 30.00, 15.00, 25.50, 5, 0, 1, '2017-09-07 10:21:24', '2017-09-07 10:21:24', '04i8953q3611i477'),
(6, 15.00, 15.00, 12.75, 6, 0, 1, '2017-09-15 16:12:15', '2017-09-15 16:12:15', '409131c9bXns55l5');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments_credit_cards`
--

CREATE TABLE `payments_credit_cards` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cardNumber` bigint(18) NOT NULL,
  `cardCVC` varchar(6) NOT NULL,
  `type` varchar(11) DEFAULT NULL,
  `cardExpiry` varchar(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paypal`
--

CREATE TABLE `paypal` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `paymentId` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `payerID` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_reports`
--

CREATE TABLE `profile_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_updates`
--

CREATE TABLE `profile_updates` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `profile` int(11) DEFAULT NULL,
  `account` int(11) NOT NULL DEFAULT '0',
  `payment` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_updates`
--

INSERT INTO `profile_updates` (`id`, `user_id`, `profile`, `account`, `payment`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-11 12:05:10'),
(2, 2, 30, 100, 0, 0, 1, '0000-00-00 00:00:00', '2017-08-31 12:05:28'),
(3, 3, 90, 100, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-10 17:57:30'),
(4, 4, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-08-31 13:08:20'),
(5, 5, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-08-31 13:10:45'),
(6, 6, 20, 100, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 09:09:54'),
(7, 7, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-01 08:29:46'),
(8, 8, 60, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-01 09:37:13'),
(9, 9, 100, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-02 13:16:49'),
(10, 10, 50, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-01 10:38:25'),
(11, 11, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-01 16:21:44'),
(12, 12, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-01 19:37:21'),
(13, 13, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-02 07:25:07'),
(14, 14, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 11:49:34'),
(15, 15, 30, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-02 13:02:03'),
(16, 16, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 10:58:58'),
(17, 19, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 16:57:25'),
(18, 20, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 21:50:31'),
(19, 21, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 22:17:02'),
(20, 22, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 23:12:34'),
(21, 23, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-04 23:30:19'),
(22, 24, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 07:55:40'),
(23, 25, 80, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 09:14:13'),
(24, 26, 90, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 11:52:23'),
(25, 28, 50, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 11:53:41'),
(26, 29, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 14:50:12'),
(27, 30, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 16:54:45'),
(28, 31, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 18:14:31'),
(29, 32, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-05 19:36:42'),
(30, 33, 70, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-11 09:15:38'),
(31, 34, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 14:07:56'),
(32, 35, 30, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:03:25'),
(33, 36, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:02:36'),
(34, 37, 80, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:17:05'),
(35, 38, 30, 100, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 13:47:16'),
(36, 39, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:26:36'),
(37, 40, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:38:36'),
(38, 41, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 21:56:55'),
(39, 42, 40, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 22:43:16'),
(40, 43, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-06 23:39:35'),
(41, 44, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 00:45:33'),
(42, 45, 80, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-11 12:24:57'),
(43, 46, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 14:38:59'),
(44, 47, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 14:55:57'),
(45, 48, 80, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 22:28:22'),
(46, 49, 50, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 17:31:06'),
(47, 50, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 18:44:51'),
(48, 52, 40, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 23:39:10'),
(49, 53, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-07 23:42:24'),
(50, 54, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 03:47:03'),
(51, 55, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 07:26:55'),
(52, 56, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 07:48:09'),
(53, 57, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 10:07:55'),
(54, 58, 30, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 10:32:45'),
(55, 61, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 10:59:04'),
(56, 62, 30, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 15:40:13'),
(57, 63, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 14:11:14'),
(58, 64, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 15:05:13'),
(59, 65, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 18:50:59'),
(60, 66, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-08 19:33:26'),
(61, 67, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 06:52:02'),
(62, 68, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 10:25:36'),
(63, 69, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 14:13:43'),
(64, 70, 20, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-09 19:53:14'),
(65, 71, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-10 03:52:09'),
(66, 72, 60, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-11 09:30:19'),
(67, 73, 10, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-11 12:41:05'),
(68, 74, 10, 100, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-15 16:26:18'),
(69, 75, 60, 0, 0, 0, 1, '0000-00-00 00:00:00', '2017-09-15 16:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate_to` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `report` int(11) NOT NULL DEFAULT '0',
  `rate_from` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `comment`, `rate_to`, `task_id`, `deleted`, `status`, `created_at`, `updated_at`, `rating`, `report`, `rate_from`) VALUES
(1, 'Great job, not his first time assembling Ikea furniture, thats for sure!', 3, 1, 0, 1, '2017-08-31 12:08:23', '0000-00-00 00:00:00', 5, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'ambassador'),
(3, 'ambassador');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `title`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Handyman', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 'Gardening ', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(3, 'IT', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(4, 'Admin', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(5, 'Data Entry', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(6, 'Promotions', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(7, 'Events', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(8, 'Delivery', 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(9, 'Accounting', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(10, 'book keeping', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(11, 'VAT', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(12, 'Company registration', 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(13, 'Digital Marketing ', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(14, 'Marketing', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(15, 'SEO', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(16, 'Facebook', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(17, 'Google ', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(18, 'Adwords', 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(19, 'Man with Van', 0, 1, '2017-09-01 10:38:25', '2017-09-01 10:38:25'),
(20, 'Web Design', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(21, 'Web Development', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(22, 'WordPress', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(23, 'Social Media', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(24, 'Quality Assurance', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(25, 'Usability', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(26, 'UX', 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(27, 'machine operation', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(28, 'CNC programmer', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(29, 'Computer Programmer', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(30, 'C', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(31, 'C++', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(32, 'C#', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(33, 'Java', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(34, 'MySQL', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(35, 'HTML', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(36, 'HTML5', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(37, 'CSS', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(38, 'Software Engineer', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(39, 'Hardware Design', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(40, 'Requirement Engineer', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(41, 'Manufacturing Planning and Control', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(42, 'Control engineer', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(43, 'Mathematical modeling', 0, 1, '2017-09-05 11:49:13', '2017-09-05 11:49:13'),
(44, 'Mathematical modeling and simulation', 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(45, 'PERSONAL PROFILE Contract Manager: to control and to monitor projects and physical and financial schedules. Carry out the follow-up of the project phases and control the values of the projects to identify possible deviation', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(46, ' Accompany the development of projects', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(47, ' instructing suppliers and customers', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(48, ' keeping informed about procedures for billing releases and payments', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(49, ' controlling and approving events of the contracts. Prepare manager reports on the progress and/or finalization of contracts. To monitor and to control the cost composition of the emergency services performed. Elaboration', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(50, ' negotiation', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(51, ' monitoring and collection of proposals (services)', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(52, ' controlling the receipt flow.  Administrative and Financial Routines: Create and analyze area indicators', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(53, ' check commission spreadsheets and expenses report of vendors', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(54, ' bank control and cash flow', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(55, ' collection of defaulters', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(56, ' accounts payable and accounts receivable. Internal Audit: programming', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(57, ' planning and participation in internal audit', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(58, ' creation and monitoring of Non-Conformities.', 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(59, 'Building', 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(60, 'Carpentry', 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(61, 'Dog walking ', 0, 1, '2017-09-06 21:03:25', '2017-09-06 21:03:25'),
(62, 'Programmer ', 0, 1, '2017-09-06 21:09:14', '2017-09-06 21:09:14'),
(63, 'JavaScript ', 0, 1, '2017-09-06 21:12:52', '2017-09-06 21:12:52'),
(64, 'Objective-C', 0, 1, '2017-09-06 21:12:52', '2017-09-06 21:12:52'),
(65, 'Swift', 0, 1, '2017-09-06 21:12:52', '2017-09-06 21:12:52'),
(66, 'React Native', 0, 1, '2017-09-06 21:13:28', '2017-09-06 21:13:28'),
(67, 'Rest API', 0, 1, '2017-09-06 21:13:28', '2017-09-06 21:13:28'),
(68, 'Electrician ', 0, 1, '2017-09-07 10:17:15', '2017-09-07 10:17:15'),
(69, 'AV Engineer', 0, 1, '2017-09-07 10:17:15', '2017-09-07 10:17:15'),
(70, 'Cleaning Services. Window cleaning', 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(71, ' gutter cleaning', 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(72, ' PVC cleaning', 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(73, ' Power Washing', 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(74, 'Plumber', 0, 1, '2017-09-08 10:32:45', '2017-09-08 10:32:45'),
(75, 'Construction ', 0, 1, '2017-09-08 12:01:50', '2017-09-08 12:01:50'),
(76, 'Graphic design', 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(77, 'bussiness card design', 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(78, 'computer repair', 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(79, 'business', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(80, 'project management', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(81, 'energy', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(82, 'cleaner', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(83, 'stock counter', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(84, 'kitchen assistant', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(85, 'catering staff', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(86, 'general operative', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(87, 'office admin', 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profileUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `accountId` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`id`, `user_id`, `type`, `profileUrl`, `deleted`, `status`, `created_at`, `updated_at`, `accountId`) VALUES
(1, 2, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F301836196895388%2F', 0, 1, '2017-08-30 16:00:26', '2017-09-11 12:24:15', '301836196895388'),
(2, 3, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Ftaskmatch-ireland-9668b3120', 0, 1, '2017-08-31 11:25:17', '2017-08-31 11:25:17', 'wQSRR8pfkk'),
(3, 2, 'twitter', 'http%3A%2F%2Fwww.twitter.com%2FTask_Match', 0, 1, '2017-08-31 12:08:58', '2017-08-31 12:08:58', '731200057932910597'),
(4, 2, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Ftaskmatch-ireland-9668b3120', 0, 1, '2017-08-31 12:09:04', '2017-08-31 12:09:04', 'wQSRR8pfkk'),
(5, 2, 'instagram', 'https%3A%2F%2Finstagram.com%2Ftaskmatch', 0, 1, '2017-08-31 12:09:24', '2017-08-31 12:09:24', '4652252679'),
(6, 5, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10159205175055585%2F', 0, 1, '2017-08-31 13:10:45', '2017-08-31 13:10:45', '10159205175055585'),
(7, 11, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10155943218488115%2F', 0, 1, '2017-09-01 16:21:44', '2017-09-01 16:21:44', '10155943218488115'),
(8, 12, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10155171290363043%2F', 0, 1, '2017-09-01 19:37:21', '2017-09-01 19:37:21', '10155171290363043'),
(9, 21, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1720288981316860%2F', 0, 1, '2017-09-04 22:17:02', '2017-09-04 22:17:02', '1720288981316860'),
(10, 22, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1397977850251790%2F', 0, 1, '2017-09-04 23:12:34', '2017-09-04 23:12:34', '1397977850251790'),
(11, 23, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fmarina-schmidt-mognhol-9606b6149', 0, 1, '2017-09-04 23:30:19', '2017-09-04 23:30:19', 'I5NFOHZbNe'),
(12, 24, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1576755945708979%2F', 0, 1, '2017-09-05 07:55:40', '2017-09-05 07:55:40', '1576755945708979'),
(13, 25, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F2021267418102901%2F', 0, 1, '2017-09-05 08:57:06', '2017-09-05 08:57:06', '2021267418102901'),
(14, 1, 'instagram', 'https%3A%2F%2Finstagram.com%2Fmuhammadanas1992', 0, 1, '2017-09-05 09:32:19', '2017-09-05 09:42:08', '5698097587'),
(15, 1, 'google', 'https%3A%2F%2Fplus.google.com%2F113059036837216643412', 0, 1, '2017-09-05 09:39:23', '2017-09-05 09:39:23', '113059036837216643412'),
(16, 26, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Frafael-jos%25C3%25A9-mota-ocariz-9a643010b', 0, 1, '2017-09-05 10:49:46', '2017-09-05 10:49:46', 'XEaMdEfAvm'),
(17, 28, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fjeieli-costa-51685644', 0, 1, '2017-09-05 11:46:59', '2017-09-05 11:46:59', 'Cviyi2bqU9'),
(18, 29, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Ffelipe-mariano-soares-506083a0', 0, 1, '2017-09-05 14:50:12', '2017-09-05 14:50:13', 'F7g9QcW-aM'),
(19, 30, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1813370035346063%2F', 0, 1, '2017-09-05 16:54:45', '2017-09-09 08:01:02', '1813370035346063'),
(20, 35, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10154940546747654%2F', 0, 1, '2017-09-06 20:54:49', '2017-09-06 20:54:49', '10154940546747654'),
(21, 36, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1412814615433404%2F', 0, 1, '2017-09-06 21:02:36', '2017-09-06 21:02:36', '1412814615433404'),
(22, 37, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1539014986150574%2F', 0, 1, '2017-09-06 21:07:09', '2017-09-06 21:07:09', '1539014986150574'),
(23, 38, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10158857002080431%2F', 0, 1, '2017-09-06 21:12:48', '2017-09-11 12:14:00', '10158857002080431'),
(24, 39, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1420814034634200%2F', 0, 1, '2017-09-06 21:26:36', '2017-09-06 21:26:36', '1420814034634200'),
(25, 40, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1643541855717685%2F', 0, 1, '2017-09-06 21:38:36', '2017-09-06 21:38:36', '1643541855717685'),
(26, 41, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1552478321482881%2F', 0, 1, '2017-09-06 21:56:55', '2017-09-06 21:56:55', '1552478321482881'),
(27, 42, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fcrislaine-alves-2151b77a', 0, 1, '2017-09-06 22:17:27', '2017-09-06 22:17:27', 'NHRvGtrS7V'),
(28, 43, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1480790422015800%2F', 0, 1, '2017-09-06 23:39:35', '2017-09-06 23:39:35', '1480790422015800'),
(29, 47, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1231471330332780%2F', 0, 1, '2017-09-07 14:55:57', '2017-09-07 14:55:57', '1231471330332780'),
(30, 52, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1394304827283552%2F', 0, 1, '2017-09-07 23:37:16', '2017-09-07 23:37:16', '1394304827283552'),
(31, 56, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fdavid-duignan-72231337', 0, 1, '2017-09-08 07:48:09', '2017-09-08 07:48:09', 'ARKKqFcUE2'),
(32, 57, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fhugovasconcelos', 0, 1, '2017-09-08 10:07:55', '2017-09-08 10:07:55', 'LGytJ13ROg'),
(33, 64, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fuiux-designer-%25E2%2598%2585%25E2%2598%2585%25E2%2598%2585%25E2%2598%2585%25E2%2598%2585-a1264810b', 0, 1, '2017-09-08 15:05:13', '2017-09-08 15:05:13', 'IiqkcG6dtX'),
(34, 65, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fmichelly-carvalho-0ba17a62', 0, 1, '2017-09-08 18:50:59', '2017-09-08 18:50:59', 'B2X3xj8p9C'),
(35, 66, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fmuhammad-danish-68aa681b', 0, 1, '2017-09-08 19:33:26', '2017-09-08 19:40:42', 'FluDxtF6fy'),
(36, 67, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fwminhoto', 0, 1, '2017-09-09 06:52:02', '2017-09-09 06:52:02', 'GEZgBbgY3K'),
(37, 69, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10212826089559116%2F', 0, 1, '2017-09-09 14:13:43', '2017-09-09 14:13:43', '10212826089559116'),
(38, 70, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10207973473215230%2F', 0, 1, '2017-09-09 19:53:14', '2017-09-09 19:53:14', '10207973473215230'),
(39, 48, 'facebook', '', 0, 1, '2017-09-09 22:20:43', '2017-09-09 22:20:43', '10212963923666323'),
(40, 48, 'linkedin', 'https%3A%2F%2Fwww.linkedin.com%2Fin%2Fvidalhoaxer', 0, 1, '2017-09-09 22:21:31', '2017-09-09 22:21:31', 'jTS2FAY4i7'),
(41, 72, 'facebook', 'https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F10203416375206883%2F', 0, 1, '2017-09-11 09:19:00', '2017-09-11 09:19:00', '10203416375206883');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `title` varchar(22) NOT NULL,
  `code` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='latin1_swedish_ci';

-- --------------------------------------------------------

--
-- Table structure for table `stripe_accounts`
--

CREATE TABLE `stripe_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `accountId` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stripe_accounts`
--

INSERT INTO `stripe_accounts` (`id`, `user_id`, `accountId`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 'acct_1Aj64bKPiqffJHfO', 0, 1, '2017-08-31 12:03:19', '0000-00-00 00:00:00'),
(2, 15, 'acct_1AgXINL36ExC44rQ', 0, 1, '2017-09-02 13:03:09', '0000-00-00 00:00:00'),
(3, 9, 'acct_1AxbSRBQRyZkCjJp', 0, 1, '2017-09-02 13:17:43', '0000-00-00 00:00:00'),
(4, 45, 'acct_1AqfLzG8G6EDwUL7', 0, 1, '2017-09-07 10:19:44', '0000-00-00 00:00:00'),
(5, 75, 'acct_1AbUzlABIhSFJ2Bd', 0, 1, '2017-09-15 15:11:38', '2017-09-15 15:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_customers`
--

CREATE TABLE `stripe_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `stripeCustomerId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stripe_customers`
--

INSERT INTO `stripe_customers` (`id`, `user_id`, `card_id`, `stripeCustomerId`, `source`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'cus_BJUPHQ41kwPNyF', 'card_1AwrQJJ8Yn8Wbtlra3PhjR9j', 0, 1, '2017-08-31 12:05:28', '0000-00-00 00:00:00'),
(2, 6, 2, 'cus_BJp0LaFFnAn8e8', 'card_1AxBMSJ8Yn8Wbtlr9mGyn2Nk', 0, 1, '2017-09-01 09:22:49', '0000-00-00 00:00:00'),
(3, 3, 4, 'cus_BKwIdU1t3IAdg2', 'card_1AyGQ4J8Yn8WbtlrMVVyw7RY', 0, 1, '2017-09-04 08:59:01', '0000-00-00 00:00:00'),
(4, 38, 5, 'cus_BMt2vwbIp3Ibzr', 'card_1B09GPJ8Yn8WbtlroVlqLmFb', 0, 1, '2017-09-09 13:44:52', '0000-00-00 00:00:00'),
(6, 74, 8, 'cus_BPB0cDWFTQHZhm', 'card_1B2MehKH6aAoqaI5MCE5PcOd', 0, 1, '2017-09-15 16:26:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sessionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `workersRequired` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taskStatus` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `price` int(11) NOT NULL,
  `dueDate` date NOT NULL,
  `toBeCompleted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `longitude` double(20,10) NOT NULL,
  `latitude` double(20,10) NOT NULL,
  `draft` int(11) NOT NULL DEFAULT '0',
  `priceType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hours` double NOT NULL DEFAULT '0',
  `hourlyRate` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `user_id`, `sessionId`, `description`, `key`, `location`, `city`, `state`, `zip`, `country`, `workersRequired`, `taskStatus`, `price`, `dueDate`, `toBeCompleted`, `deleted`, `status`, `created_at`, `updated_at`, `longitude`, `latitude`, `draft`, `priceType`, `hours`, `hourlyRate`) VALUES
(1, 'Assemble Ikea Furniture ', 2, NULL, 'Assemble Ikea Furniture \r\n1 x Double Bed\r\n1 x Wardrobe \r\n2 x Bed Side Lockers\r\n\r\nAll items are unpacked and ready to go.\r\n\r\nThanks', 'l41Y373OM610DI95', 'Swords, Ireland', NULL, NULL, NULL, NULL, '', 'completed', 100, '2017-09-09', 'inperson', 0, 1, '2017-08-30 17:15:28', '2017-08-31 12:07:02', 0.0000000000, 0.0000000000, 0, 'total', 0, 0),
(2, 'Paint a two bed apartment', 3, NULL, 'Paint a two bed apartment in Dublin City . All paint supplied and apartment is empty. Feel free to ask any questions in comment section. Thanks ', '203AW9oG15g487H1', 'Dublin 4, Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'open', 500, '2017-09-06', 'inperson', 0, 1, '2017-08-31 11:17:19', '0000-00-00 00:00:00', -6.2275049000, 53.3293700000, 0, 'total', 0, 0),
(3, 'Window cleaner', 5, NULL, 'Hi,\r\n\r\nI need a window cleaner for the Dublin 2 & Dublin 8 area.', '511zLe55103gn4H8', 'Dublin 8, Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'open', 20, '2017-09-06', 'inperson', 0, 1, '2017-08-31 13:12:15', '0000-00-00 00:00:00', -6.2728703000, 53.3337713000, 0, 'total', 0, 0),
(4, 'Gardening Needed', 6, NULL, 'Gardening Needed to cut front and back garden and general garden tidy. All gear supplied. I would predict about 3 hours in total. ', '329420m1A5b7a405', 'Sandymount, Ireland', NULL, NULL, NULL, NULL, '', 'assigned', 60, '2017-09-15', 'inperson', 0, 1, '2017-09-01 08:24:30', '2017-09-01 09:23:10', -6.2157427000, 53.3305546000, 0, 'total', 0, 0),
(5, 'Promo work in Dublin', 7, NULL, 'Hi,\r\n\r\nI need an energetic young person to hand out flyers and freebies in Dublin. There is potential for more days after this. Working in a group.', 'g02i502K514m8E4w', 'Grafton Street, Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'open', 80, '2017-09-16', 'inperson', 0, 1, '2017-09-01 08:33:22', '0000-00-00 00:00:00', -6.2597468000, 53.3422998000, 0, 'total', 0, 0),
(6, 'Clean my Air bnb apartment ', 8, NULL, 'Hi, \r\n\r\nI need someone to clean my Air bnb apartment in Dublin 8, All new bed linen and cleaning products are there for you. 1 hour quick turnaround. 11am on wed 6th. thanks ', 'j48185872n0952xh', 'Dublin 8, Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'open', 35, '2017-09-06', 'inperson', 0, 1, '2017-09-01 09:41:12', '0000-00-00 00:00:00', -6.2728703000, 53.3337713000, 0, 'total', 0, 0),
(7, 'Moving apartment ', 9, NULL, 'Hi, I am moving apartments in Grand Canal Dock. They are about 5 mins drive apartment. All contents of a 1 bed apartment need to be removed and moved into new place. lifts in both blocks with easy access. \r\n\r\nThanks ', 'i0962Yg3V014t45O', 'Grand Canal Dock, Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'open', 150, '2017-09-16', 'inperson', 0, 1, '2017-09-01 10:06:34', '0000-00-00 00:00:00', -6.2412809000, 53.3424227000, 0, 'total', 0, 0),
(8, 'Labourer needed', 10, NULL, 'Hi, I need a hand on a job for a day on Friday the 8th Sept, We will be emptying a 1 bed apartment and moving it to a new one. Approx 4 hours work paid by the hour', '356THl2Q001G6452', 'Clontarf, Ireland', NULL, NULL, NULL, NULL, '', 'open', 60, '2017-09-08', 'inperson', 0, 1, '2017-09-01 10:42:43', '0000-00-00 00:00:00', -6.2045103000, 53.3660165000, 0, 'hourly', 4, 15),
(9, 'Build a wordpress website', 3, NULL, 'Build a wordpress website - 5 pages with all images and content supplied.', '145z0eoB5636a369', 'Dublin, Ireland', NULL, NULL, NULL, NULL, '', 'assigned', 200, '2017-09-08', 'online', 0, 1, '2017-09-02 12:46:36', '2017-09-04 08:59:11', -6.2603097000, 53.3498053000, 0, 'total', 0, 0),
(10, 'Electrician Needed', 6, NULL, 'I have two Ikea lights I need replaced with existing lights in our kitchen.', '01xS466215jI9Sc5', 'Sandymount, Ireland', NULL, NULL, NULL, NULL, '', 'open', 80, '2017-09-10', 'inperson', 0, 1, '2017-09-04 09:11:36', '0000-00-00 00:00:00', -6.2157427000, 53.3305546000, 0, 'total', 0, 0),
(11, 'Cleaner needed', 35, NULL, 'House cleaning needed (3 hours)', '8F173415l0f7E4d1', '151 Leeson Street Upper, D04 FA36, Ireland', NULL, NULL, NULL, NULL, '', 'open', 30, '2017-09-12', 'inperson', 0, 1, '2017-09-06 21:01:54', '0000-00-00 00:00:00', -6.2520159000, 53.3315432000, 0, 'total', 0, 0),
(12, 'Light needs fixing', 38, NULL, 'Hi, I have a bedroom light that is not working. Changed the lightbulb but think it is still not working. Need someone to come this week', '58w314n7WY48X02k', 'Ranelagh, Ireland', NULL, NULL, NULL, NULL, '', 'assigned', 25, '2017-09-12', 'inperson', 0, 1, '2017-09-06 21:14:48', '2017-09-09 13:45:36', -6.2570850000, 53.3268435000, 0, 'total', 0, 0),
(13, 'Golpik Create Marketplace', 74, NULL, 'Golpik Create MarketplaceGolpik Create MarketplaceGolpik Create MarketplaceGolpik Create MarketplaceGolpik Create MarketplaceGolpik Create MarketplaceGolpik Create Marketplace', 'y10V8t45i55923h4', 'Athlone, Ireland', NULL, NULL, NULL, NULL, '', 'completed', 15, '2017-09-21', 'online', 0, 1, '2017-09-15 14:04:19', '2017-09-15 16:31:56', -7.9406898000, 53.4239331000, 0, 'total', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_categories`
--

CREATE TABLE `task_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_categories`
--

INSERT INTO `task_categories` (`id`, `category_id`, `task_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(4, 8, 1, 0, 1, '2017-08-31 09:56:36', '2017-08-31 09:56:36'),
(5, 8, 2, 0, 1, '2017-08-31 11:17:19', '2017-08-31 11:17:19'),
(6, 2, 3, 0, 1, '2017-08-31 13:12:15', '2017-08-31 13:12:15'),
(7, 2, 4, 0, 1, '2017-09-01 08:24:30', '2017-09-01 08:24:30'),
(8, 8, 4, 0, 1, '2017-09-01 08:24:30', '2017-09-01 08:24:30'),
(9, 9, 5, 0, 1, '2017-09-01 08:33:22', '2017-09-01 08:33:22'),
(10, 11, 5, 0, 1, '2017-09-01 08:33:22', '2017-09-01 08:33:22'),
(11, 12, 5, 0, 1, '2017-09-01 08:33:22', '2017-09-01 08:33:22'),
(12, 13, 5, 0, 1, '2017-09-01 08:33:22', '2017-09-01 08:33:22'),
(13, 2, 6, 0, 1, '2017-09-01 09:41:12', '2017-09-01 09:41:12'),
(14, 7, 7, 0, 1, '2017-09-01 10:06:34', '2017-09-01 10:06:34'),
(15, 8, 7, 0, 1, '2017-09-01 10:06:34', '2017-09-01 10:06:34'),
(16, 7, 8, 0, 1, '2017-09-01 10:42:43', '2017-09-01 10:42:43'),
(17, 8, 8, 0, 1, '2017-09-01 10:42:43', '2017-09-01 10:42:43'),
(18, 5, 9, 0, 1, '2017-09-02 12:46:36', '2017-09-02 12:46:36'),
(19, 9, 9, 0, 1, '2017-09-02 12:46:36', '2017-09-02 12:46:36'),
(20, 8, 10, 0, 1, '2017-09-04 09:11:36', '2017-09-04 09:11:36'),
(21, 2, 11, 0, 1, '2017-09-06 21:01:54', '2017-09-06 21:01:54'),
(22, 8, 12, 0, 1, '2017-09-06 21:14:48', '2017-09-06 21:14:48'),
(24, 12, 13, 0, 1, '2017-09-08 19:02:54', '2017-09-08 19:02:54'),
(25, 13, 13, 0, 1, '2017-09-15 14:04:20', '2017-09-15 14:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `task_files`
--

CREATE TABLE `task_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_files`
--

INSERT INTO `task_files` (`id`, `file`, `task_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'IMG_8511.JPG', 1, 0, 1, '2017-08-30 17:30:37', '2017-08-30 17:30:37');

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(11) NOT NULL,
  `task_owner_id` int(11) NOT NULL,
  `task_runner_id` int(11) NOT NULL,
  `offer_price_id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `task_id`, `task_owner_id`, `task_runner_id`, `offer_price_id`, `key`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 3, 1, '9j71u54C82111a10', 0, 1, '2017-08-31 12:06:31', '2017-08-31 12:06:31'),
(2, 4, 6, 3, 2, '204J1z557609v7Dq', 0, 1, '2017-09-01 09:23:10', '2017-09-01 09:23:10'),
(3, 9, 3, 15, 3, '10u51I7555c15cn4', 0, 1, '2017-09-04 08:59:11', '2017-09-04 08:59:11'),
(4, 12, 38, 45, 4, 'I0kb496241a7Y5O3', 0, 1, '2017-09-09 13:45:32', '2017-09-09 13:45:32'),
(5, 12, 38, 45, 4, '47VGDg6Q5x963401', 0, 1, '2017-09-09 13:45:36', '2017-09-09 13:45:36'),
(6, 13, 74, 75, 6, '210808Sf95c504mn', 0, 1, '2017-09-15 16:26:40', '2017-09-15 16:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `gateway` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reference` text COLLATE utf8_unicode_ci,
  `assign_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `gateway`, `reference`, `assign_id`, `amount`, `type`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'stripe', 'ch_1AwrRKJ8Yn8WbtlrOzRQrjBO', 1, 100, 'sent', 0, 1, '2017-08-31 12:06:31', '2017-08-31 12:06:31'),
(2, 6, 'stripe', 'ch_1AxBMoJ8Yn8WbtlrIYNhAXEx', 2, 60, 'sent', 0, 1, '2017-09-01 09:23:10', '2017-09-01 09:23:10'),
(3, 3, 'stripe', 'ch_1AyGQEJ8Yn8WbtlrXrzqcD0P', 3, 1, 'sent', 0, 1, '2017-09-04 08:59:11', '2017-09-04 08:59:11'),
(4, 38, 'stripe', 'ch_1B09H3J8Yn8WbtlrwCroi1vP', 4, 1, 'sent', 0, 1, '2017-09-09 13:45:32', '2017-09-09 13:45:32'),
(5, 38, 'stripe', 'ch_1B09H7J8Yn8Wbtlr49CLCcXx', 5, 1, 'sent', 0, 1, '2017-09-09 13:45:36', '2017-09-09 13:45:36'),
(6, 74, 'stripe', 'ch_1B2Mf4KH6aAoqaI5SGKJmybH', 6, 15, 'sent', 0, 1, '2017-09-15 16:26:40', '2017-09-15 16:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT '2',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `isConfirmed` int(2) DEFAULT '0',
  `lastName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `joinFrom` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverImage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` double(20,10) NOT NULL,
  `latitude` double(20,10) NOT NULL,
  `dob` date DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `coverPosition` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0px',
  `rating` double(15,8) NOT NULL,
  `authy_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='utf8_unicode_ci';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `email`, `password`, `image`, `remember_token`, `role_id`, `created_at`, `updated_at`, `isConfirmed`, `lastName`, `status`, `joinFrom`, `phone`, `about`, `tagline`, `location`, `coverImage`, `longitude`, `latitude`, `dob`, `deleted`, `coverPosition`, `rating`, `authy_id`, `verified`, `key`, `countryCode`) VALUES
(1, 'TaskMatch', 'contact@taskmatch.ie', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'c5k2cVGt4jF9qsu0rnIX6XoZSbjtAeTh8Ya8AXLn7h4OwdSubcqooNvMuesv', 1, '2017-08-30 14:01:51', '2017-09-11 12:10:21', 1, 'Admin', 1, '', NULL, '', '', 'Arkle Road', NULL, -6.2088186000, 53.2778111000, '2016-01-01', 0, '0px', 123.00000000, NULL, 0, '2y$10$Dm5OQMXEHkFhm2RONbfiy', NULL),
(2, 'John', 'conlyons8@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '7121504108826.jpg', 'UbIIx6uvPNUhkfzKenCmJgR9ig6grDTKJKy1qRTp4z55mr9jJwArAnKAQVtw', 2, '2017-08-30 16:00:26', '2017-09-11 12:24:20', 1, 'Bynre', 1, 'facebook', NULL, '', '', '', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'c0OmKHDxe8LTswvzGgHSGr7GuFYVi3', NULL),
(3, 'Paul ', 'conlyons4@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3.png', 'MBqmlLF1XZogVxkzNn0yI1bCj7dQIS9ks7V70EjexlNd9nuVrwdIc5plBJ3d', 2, '2017-08-31 10:59:32', '2017-09-10 17:57:36', 1, 'Kennedy', 1, '', '838453872', 'Hi,\r\n\r\nFriendly 28 yr old from Galway, I am currently studying Engineering part time in DIT and looking to complete some casual work around Dublin. \r\nI am fairly handy around the house and with computers.\r\nFeel free to ask any questions.\r\n\r\nThanks,\r\n\r\nPaul\r\n', 'Look no further.', '', NULL, 0.0000000000, 0.0000000000, '1989-09-27', 0, '0px', 0.00000000, '44230393', 1, '6q8kEkq7GYR6aDB26v6fLQFqnytQ8r', '00353'),
(4, 'Admin', 'aftab@golpik.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 1, '2017-08-31 13:07:20', '2017-08-31 13:08:06', 1, 'Khan', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 't9aod2Z215ZB7UTo2I2mOupt6Q8kQF', NULL),
(5, 'Matthew', 'matthewtoman@hotmail.co.uk', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6621504185045.jpg', NULL, 2, '2017-08-31 13:10:45', '2017-08-31 13:10:45', 1, 'Toman', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'bek6bbFi8ciYIshlctkKmwTCLJSqAb', NULL),
(6, 'Patrick ', 'johnbynre.123456@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6.png', '8UnSDWBkTVDnPRh25LGCOc55iYMUIb6fWY7t7clWzZJHs83FfEbSah7Im9DF', 2, '2017-09-01 08:15:50', '2017-09-04 10:49:43', 1, 'Fagan', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, '1AUcWkAFNZUWD0XM0n9kSiGH4QCKMj', NULL),
(7, 'Nicole', 'nicoleduffy1234@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '7.png', 'QfBQsbzR1FLtEmsCAXBPrhJPzQRL5swFhZ3XcDJaDrjw8gWIig6R9UQfeLW5', 2, '2017-09-01 08:27:54', '2017-09-01 09:20:26', 1, 'Duffy', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'Dc2iLJwAIEgmbnrxj5vjGgBJVI4I6Z', NULL),
(8, 'Tom ', 'tomfoley234@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '8.png', 'p8lEqtF9xz8gMBVhupw0tjxy4qBR80ctvTYT6gBRwAgZFdgeUTxnOmNwEVLu', 2, '2017-09-01 09:27:48', '2017-09-01 09:46:16', 1, 'Foley', 1, '', NULL, 'Hi,\r\n\r\nAccountant specialising in small to medium businesses.\r\nCompany and VAT registration completed.\r\n\r\nRequest a quote from my profile.\r\n\r\nThanks\r\n', 'Accounting & Book keeping', 'Stillorgan Road', '5421504258276.jpg', -6.2155553000, 53.3080788000, '1953-06-15', 0, '0px', 0.00000000, NULL, 0, 'sv2ByFXdInypNKIbjYvWXGwOYawP1J', NULL),
(9, 'Jen', 'jenwalsh234@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9.png', 'DwH2Lfv9D81J4SxuMYHpFC3g7Bv6bMbsCgiCQkGruqSlAZzqegi843JBN8an', 2, '2017-09-01 09:47:06', '2017-09-02 13:18:41', 1, 'Walsh', 1, '', '838453872', 'Hi,\r\n\r\nI have 5years experience in digital marketing and specialise in SEO, Google Adwords & Facebook Ad Campaigns.\r\nAll jobs can be completed online so don\'t worry if you are not Dublin based.\r\n\r\nRequest a quote from my profile.\r\n', 'Digital Marketing ', 'Dún Laoghaire', '6391504259266.jpg', -6.1338952000, 53.2944124000, '1988-03-10', 0, '0px', 0.00000000, '44230393', 1, 'TYzZxGn34NTGXa1fd8ISG293kGboha', '00353'),
(10, 'Brian', 'brianquinn234@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '10.png', 'rnUQKHZGJ7KL2A4Uv6NwzL10rrvylsvyQbUM6dfNGhKZ43JUIy0Xt2FuvhCt', 2, '2017-09-01 10:15:56', '2017-09-01 10:49:35', 1, 'Quinn', 1, '', NULL, 'Man with van available for country wide delivery & removals.\r\n\r\nRequest a quote on my profile.', 'Man with Van', 'Clontarf', '3091504261413.png', -6.2538011962, 53.3846562154, '1983-10-26', 0, '0px', 0.00000000, NULL, 0, 'mr2dWJvKdJCMBJwjmpyqlBIXrULDIW', NULL),
(11, 'Patrick', 'patrickmassey@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9681504282904.jpg', NULL, 2, '2017-09-01 16:21:44', '2017-09-01 16:21:44', 1, 'Massey', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'VUYL1WAivkf6TZwIpLnjxSrgDdGeKv', NULL),
(12, 'Patrick', 'padgoc@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6771504294641.jpg', NULL, 2, '2017-09-01 19:37:21', '2017-09-01 19:37:21', 1, 'Cooney', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'V16hHOhGc3KeCAaHVasCtAKGALfqaT', NULL),
(13, 'Daragh', 'dquinn58@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-02 07:20:46', '2017-09-02 07:21:08', 1, 'Quinn ', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'SDeOLlHm7Yrlhao4hDB7H1NxCmxDXv', NULL),
(14, 'Kevin', 'keithwallace115@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '14.png', 'FJBTvl3nRMx4nvPC4YhWFMK9E5oZpvBPItbtRSsShXwMNnBvm7RUjBoYAYhg', 2, '2017-09-02 12:16:43', '2017-09-09 11:56:20', 1, 'Williams', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'EUAo2il2koDZkWnHLnzEeMeL4gx0Fa', NULL),
(15, 'Keith', 'keith_wallace@icloud.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '15.png', 'HM9YmUDJPSJcqLb9mII6bQ1E1J3O9F6aKBb2NOBDQfnlKdzpweeAy2YUtXBO', 2, '2017-09-02 12:47:57', '2017-09-02 13:18:44', 1, 'Wallace', 1, '', '877554688', '', '', '', NULL, 0.0000000000, 0.0000000000, '1984-07-14', 0, '0px', 0.00000000, '48482410', 1, 'xu4R0gHia3XqA5DgCRgk44BYDnQl7v', '+353'),
(16, 'James', 'jwboilers@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', '4vjRBvXQULZ28NOYoQ1GXFQkI0q0U1gCw7Fl5JoFCy2pCO99mAqyKPiPTbEa', 2, '2017-09-04 10:55:50', '2017-09-04 10:58:58', 1, 'Wolverson ', 1, '', NULL, '', '', 'Ayrfield', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'jRul9CiCE2u50QOQvJw6XxXAWcRroQ', NULL),
(17, 'Jgg', 'zdt@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-04 18:57:58', '2017-09-04 18:57:58', 0, 'Huu', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'eZGzOF9O31HIsJQgxUvm757WzaonXQ', NULL),
(18, 'Mariana ', 'mariana_quii@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-04 19:43:45', '2017-09-04 19:43:45', 0, 'Quintiliano ', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'eUANJUH0aV05razftYF5BWI3kv5Y4N', NULL),
(19, 'MARK ', 'mhcarpentryservicesltd@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'gFeib0yQMmXe1qYDyVDKx70NMwzuwSA85juL4eJX5T1KGGTU19aZ7GZWnwce', 2, '2017-09-04 20:07:00', '2017-09-05 16:57:25', 1, 'HALLIDAY', 1, '', '', '', 'Carpentry Services', NULL, NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'Wg2GZbMJs3v9VuRaW7jlzRRVdLLMKJ', NULL),
(20, 'Joelson Alves', 'joelson_oliveira@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-04 21:48:53', '2017-09-04 21:50:31', 1, 'Oliveira', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'vhd1oAr8sOfy2Pws1P6ZqfJL8Ri3qw', NULL),
(21, 'Gessica', 'eng.gessica@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3661504563422.jpg', NULL, 2, '2017-09-04 22:17:02', '2017-09-04 22:17:02', 1, 'Silva', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'VvFNlNw1Aaza7wrMoJOayUXzkF29jC', NULL),
(22, 'Carla', 'carla_santana1@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9271504566754.jpg', NULL, 2, '2017-09-04 23:12:34', '2017-09-04 23:12:34', 1, 'Santana', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'ZlnqUT29rvnk5wQ2DFqjLE9dE9gbbb', NULL),
(23, 'Marina', 'marinamognhol@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '2461504567818.jpg', NULL, 2, '2017-09-04 23:30:19', '2017-09-04 23:30:19', 1, 'Schmidt Mognhol', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'yymmTo5gIsHLXmQmfFvF3LuejEC4uc', NULL),
(24, 'Ciprian', 'ciornobreveiciprian@yahoo.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9861504598140.jpg', NULL, 2, '2017-09-05 07:55:40', '2017-09-05 07:55:40', 1, 'Ciornobrevei', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, '5Z4DIKQx2jQZXOmHFxW6OXPs7gI6Ah', NULL),
(25, 'Anna', 'info@anyadesignstudio.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '8821504601826.jpg', NULL, 2, '2017-09-05 08:57:06', '2017-09-05 09:14:13', 1, 'Kozielska', 1, 'facebook', NULL, 'I am a web designer and developer, helping SMEs, Business Owners and Entrepreneurs build their online presence and conquer the digital world - be it a new website, updating an existing one or social media management. I am also available for freelance work on projects. ', 'web designer ', 'Dublin', NULL, 0.0000000000, 0.0000000000, '1980-04-01', 0, '0px', 0.00000000, NULL, 0, 'QAjTPvPcJwxmHCQWs24Y0TRvEkNnki', NULL),
(26, 'Rafael José', 'rafael.ocariz90@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6721504608586.jpg', NULL, 2, '2017-09-05 10:49:46', '2017-09-05 11:52:23', 1, 'Mota Ocariz', 1, 'linkedin', NULL, 'I have experience with the operation of conventional machining machines and the CNC, planning and control of the production process, quality assurance and designs of parts and machines. As a computer engineer I have experience in hardware and software projects for computers, whether embedded or not, with real-time systems or not. I am currently developing skills in the area of ​​control and dynamics of space vehicles such as satellites and launch vehicles such as rockets', 'A little about myself', 'Dublin City', NULL, 0.0000000000, 0.0000000000, '1990-07-03', 0, '0px', 0.00000000, NULL, 0, 'nnUc3duHNjhfxmCX7ITnxkzxosf6AC', NULL),
(28, 'Jeieli', 'jeielicosta@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9981504612018.jpg', NULL, 2, '2017-09-05 11:46:59', '2017-09-05 11:53:41', 1, 'Costa', 1, 'linkedin', NULL, 'I live in Brasil and I want to improve my English and some day live abroad.', '', 'Brasil', NULL, 0.0000000000, 0.0000000000, '1985-02-09', 0, '0px', 0.00000000, NULL, 0, 'vQyYZE25LY7SYFmpMuKWV1aVJ74etB', NULL),
(29, 'Felipe', 'felipe.marianosoares@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '5731504623012.jpg', NULL, 2, '2017-09-05 14:50:12', '2017-09-05 14:50:12', 1, 'Mariano Soares', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'RzQccGVNWPdAZj5aEnWrE58zTTQv13', NULL),
(30, 'Jessica', 'jessicaosullivan@ymail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6131504630485.jpg', NULL, 2, '2017-09-05 16:54:45', '2017-09-05 16:54:45', 1, 'O\' Sullivan', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'c6lXqJ4VOcEp5KWJ4Yy6i5RuPpyjJ0', NULL),
(31, 'niamh', 'phelan.research@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-05 18:14:04', '2017-09-05 18:14:31', 1, 'Phelan', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'ajcvRJDWVjlQudRw28PwvWuMGSPb7c', NULL),
(32, 'Pat', 'patsgardenservice93@yahoo.ie', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'iw4gHja4sKmqRqOluksJMc3xQQ2ClWmBaeX5q1VIV0RBNtU9P6xSLRbDTs8Z', 2, '2017-09-05 19:35:57', '2017-09-05 19:36:42', 1, 'Donagh', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'FMM9Af8jOsLnAN8pEhYO5TbBEe0rJN', NULL),
(33, 'Matt', 'sendittoherlihy@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '33.png', NULL, 2, '2017-09-06 10:16:32', '2017-09-10 12:05:45', 1, 'Herlihy', 1, '', NULL, '33 male year old from dublin. Work building sets in the events industry. Build/repair bicycles as a hobby. Very handy all round. Interested in casual work/tasks around Dublin. Fee free to ask any questions', 'Handyman', 'Drumcondra Road Lower', '4531505045145.jpg', -6.2567338000, 53.3647569000, '1984-09-09', 0, '0px', 0.00000000, NULL, 0, 'vUrxrlwVU4mp2zGeRZSL42fySgFlhG', NULL),
(34, 'Raisa', 'dimo1909@mail.bg', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-06 14:07:08', '2017-09-06 14:07:34', 1, 'Marinova', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'oJgBAQi3yYsfB4eMW2P1acfIJC4k3t', NULL),
(35, 'Cecilia', 'ceboyadjian@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6651504731289.jpg', NULL, 2, '2017-09-06 20:54:49', '2017-09-06 21:03:25', 1, 'Boyadjian', 1, 'facebook', NULL, '', 'Dog walker ', '', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'jfVQQyY6pMtxhruUHdz0PmLSluBUNm', NULL),
(36, 'Yclen', 'yclen_94@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '8491504731756.jpg', NULL, 2, '2017-09-06 21:02:36', '2017-09-06 21:02:36', 1, 'Eduardo', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'epmKsHJgWXlEM0oKcaCOqBQ5AYA57a', NULL),
(37, 'Claudio', 'claudioguirunas@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '2291504732029.jpg', NULL, 2, '2017-09-06 21:07:09', '2017-09-06 21:17:05', 1, 'Guirunas', 1, 'facebook', NULL, '', 'iOS / JavaScript Developer', 'Dublin City', NULL, 0.0000000000, 0.0000000000, '1980-03-13', 0, '0px', 0.00000000, NULL, 0, 'dwHd5U1Gz2RBnPHVLEVvB219Oz1Ovu', NULL),
(38, 'Keith', 'keith_wallace@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3631504732368.jpg', 'SnT5RwJplXH64cUsZ31KfFrhH7LDUI1VeC9Wmd1BlCygz8I07Xh6lBJrXlrq', 2, '2017-09-06 21:12:48', '2017-09-09 13:47:16', 1, 'Wallace', 1, 'facebook', '877554688', '', '', '', NULL, 0.0000000000, 0.0000000000, '1984-07-14', 0, '0px', 0.00000000, '48482410', 1, 'vg9ahXYuiWbLdE7hnKUv7lQu10pjDu', '353'),
(39, 'Guilherme', 'guilhermemallmanncibulski@yahoo.com.br', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '4321504733196.jpg', NULL, 2, '2017-09-06 21:26:36', '2017-09-06 21:26:36', 1, 'Mallmann Cibulski', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 't9DS5iXeYakk9m6iIcLG84a5zZSGcP', NULL),
(40, 'Átila', 'atilaccc@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '5841504733915.jpg', NULL, 2, '2017-09-06 21:38:36', '2017-09-06 21:38:36', 1, 'Matos', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, '6MA8nzzsriMABxRPpf3OtUXQAEsXIi', NULL),
(41, 'Leandro', 'momessao@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '1991504735015.jpg', NULL, 2, '2017-09-06 21:56:55', '2017-09-06 21:56:55', 1, 'Momesso', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'OwrNcy1bELqL9H2LtfueQVOko5HuMB', NULL),
(42, 'Crislaine', 'crislaine.dos@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '1221504736247.jpg', NULL, 2, '2017-09-06 22:17:27', '2017-09-06 22:43:16', 1, 'Alves', 1, 'linkedin', NULL, '', '', '38 Ash St', NULL, -6.2769339000, 53.3401504000, '1984-07-04', 0, '0px', 0.00000000, NULL, 0, 'HVdgxdC0uYOgO5rcn71dpmrNW8Knrv', NULL),
(43, 'Henrique', 'henrique.reis94@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '4651504741175.jpg', NULL, 2, '2017-09-06 23:39:35', '2017-09-06 23:39:35', 1, 'Reis', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'T8vw9hy9WYFvhycRSLJQReEIsjkSfz', NULL),
(44, 'Denilson ', 'denilson.s29@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-07 00:44:24', '2017-09-07 00:45:12', 1, 'Silva', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'Ix9D8zCpH4Ffl2dkk1QIx1R92MJtor', NULL),
(45, 'Conor', 'conor@digiteksolutions.ie', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '45.png', 'd2CwDvsI7m5m6ngkL69b7hQxRGIYM3euuBKa0L8WPAy0ZIpYTAsNXvVh3Yiv', 2, '2017-09-07 10:02:51', '2017-09-11 12:12:55', 1, 'Anthony', 1, '', '838453872', 'Audio Visual Engineer specialising in residential installation.\r\n\r\n*Saorview / Freesat installations * Home Theatre * Multi-room audio * TV wall mounting * Home data networks & wifi\r\n', 'Audio Visual Engineer', 'Dublin', '8701504779197.png', -6.2603097000, 53.3498053000, '1983-09-27', 0, '0px', 0.00000000, '44230393', 1, 'qBkxyU67Qkhm39znTVQTeK2sm7pvgO', '00353'),
(46, 'Tatiane ', 'tcunit@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'syoem4sZkQSUexUUqtZIqpotf5wTY3mBUo7Q806yaHWWgQ59vsXaJhfG0gz5', 2, '2017-09-07 14:33:09', '2017-09-07 14:40:14', 1, 'Cristina de Oliveira', 1, '', NULL, '', '', '', NULL, 0.0000000000, 0.0000000000, '1990-11-12', 0, '0px', 0.00000000, NULL, 0, 'GKTp9ejowyTHINBMdXlJlvaNOfpZIY', NULL),
(47, 'Rafael', 'raphael541@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3291504796157.jpg', NULL, 2, '2017-09-07 14:55:57', '2017-09-07 14:55:57', 1, 'Nascimento', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'R2rwBOICmk5BUPfHH07jbd5H6ybw4s', NULL),
(48, 'Vidal', 'vidalhoaxer+taskmatch.ie@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '48.png', 'jvuMVRiupdv8Sp4KSj03j0c9e7JNlj0uehBOFuF7lN7qDUy7SDUFT4rnYwQA', 2, '2017-09-07 15:07:46', '2017-09-09 22:28:22', 1, 'Valera', 1, '', '899736450', 'I’m a Venezuelan self-taught designer and computer nerd based in Dublin. I learned almost everything myself because of my curiosity and my desire for new experience. I love to create things that make users happier and more productive with their work. I’m opened to share my experience with others, I also write articles about what I have learned. On my spare time, I love to read, build projects and do freelance work.', 'Self-taught guy', '', '5851504996018.png', 0.0000000000, 0.0000000000, '1987-01-25', 0, '0px', 0.00000000, '10144200', 1, 'xYmF393fwaZylTSvSezjCvSgJcWPKi', '353'),
(49, 'Norman', 'info@norman.ie', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '49.png', 'yzfmjeHPNzPb4Fq0eI3FBaQR0jp2tfYQeU5bOciAyLLTuZTrkba9L8sxNM5T', 2, '2017-09-07 17:23:52', '2017-09-07 17:31:06', 1, 'Cleaning Services', 1, '', NULL, 'In Business since early 2016 with over 500 returning customers. Window and gutter cleaning up to 5 storeys high. SafePass holder, MEWP (iPAF) trained and fully insured.\r\n\r\nWindow cleaning, gutter cleaning, PVC cleaning, Power Washing', 'Cleaning Services. ', '', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'FOjncDYF9TLLNDOyvjZlvUVU76wnPk', NULL),
(50, 'Sarah', 'smmccann@eircom.net', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-07 18:44:26', '2017-09-07 18:44:38', 1, 'McCann', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'WZCJvpYOm6oHH1v3rL3dp3RftWI2fZ', NULL),
(52, 'Fernando', 'fernando-guerriero@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '4351504827435.jpg', NULL, 2, '2017-09-07 23:37:16', '2017-09-07 23:39:10', 1, 'Guerriero', 1, 'facebook', NULL, '', '', 'Jundiai - SP', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'pZXkGqi21qlElJVuleywrGCisywvTP', NULL),
(53, 'Priscila', 'priscila-b-lopes@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-07 23:39:26', '2017-09-07 23:40:05', 1, 'Santos', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'J8xhsKQUzptnSG3A8BztAhV9j4gltj', NULL),
(54, 'Mark', 'mgvaluepainting@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'COckxnS4L52KI2QKDtJhOmsXgPFwEuBBO9UpmYN9uCLmakbODd4fSUbqqEJB', 2, '2017-09-08 03:45:21', '2017-09-08 03:47:03', 1, 'Cheptene', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'zPoax1p0bkBsszIvu7erbUjlzhQS0H', NULL),
(55, 'Dave', 'primeandpaintdublin@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'QGEGUGndYF9dwKspvSoIJbwgPZ1RZBvarJaWEWKfKpMHYd3tgtwDNz4lfkh0', 2, '2017-09-08 06:27:18', '2017-09-08 07:26:55', 1, 'M', 1, '', '', '', 'Prime and Paint', NULL, NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'Pzi3FiDFX0joVKGxgeLiuj6yDUhesv', NULL),
(56, 'David', 'davidmduignan@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9551504856889.jpg', 'ULJ84d04T33en8UA3E6tjKZ0EKuHBMnUtm6zwXruNmKRcyd0YGOBfggPd7YT', 2, '2017-09-08 07:48:09', '2017-09-08 07:49:46', 1, 'Duignan', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'lNwW5QqikPe8cE9W3unwqSNFypf3Jk', NULL),
(57, 'Hugo', 'hugo121204@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '9781504865275.jpg', NULL, 2, '2017-09-08 10:07:55', '2017-09-08 10:07:55', 1, 'Vasconcelos', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'qAUNFPeLrxWCJnSSuVYVjymqmh5bwQ', NULL),
(58, 'DAVID ', 'dnicoll@eircom.net', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-08 10:23:28', '2017-09-08 10:32:45', 1, 'NICOLL', 1, '', NULL, '', '', '', NULL, 0.0000000000, 0.0000000000, '2016-01-01', 0, '0px', 0.00000000, NULL, 0, 'lCaUxJjtAqHn4gY6qPmOtUYhzbsAMS', NULL),
(59, 'Christopher', 'christurleyplumber@hotmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-08 10:26:28', '2017-09-08 10:26:28', 0, 'Turley', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'kOU5yfrbnRM9C7uFXWGAieoGXCWPbD', NULL),
(61, 'Damien ', 'damienneilan@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-08 10:56:29', '2017-09-08 10:59:04', 1, 'Neilan ', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'h84wiSgl0C9fxDmRgEu8M8QYIIPNKK', NULL),
(62, 'Declan', 'shelbyltdinfo@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-08 11:54:14', '2017-09-08 15:40:38', 1, 'Oshea', 1, '', '0852061317', '', '', '', NULL, 0.0000000000, 0.0000000000, '1972-10-14', 0, '0px', 0.00000000, '43779470', 1, 'XRwvso2wLbnj9uKbsTN8kmsqQmJemL', '+353'),
(63, 'Simon', 'bermudasimon@yahoo.co.uk', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-08 13:55:18', '2017-09-08 14:05:23', 1, 'Williams', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, '9lfu21hLBkuaS5kY0wGcHZbdc4Qpah', NULL),
(64, 'UIUX', 'hussain.golpik@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3371504883113.jpg', NULL, 2, '2017-09-08 15:05:13', '2017-09-08 15:05:13', 1, 'Designer (★★★★★)', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'CGz4UIe3rp628gKHFNENdqbaejl89a', NULL),
(65, 'Michelly', 'durans.michelly@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3671504896659.jpg', NULL, 2, '2017-09-08 18:50:59', '2017-09-08 18:50:59', 1, 'Carvalho', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'k9Szr9mHB2DvqG0Wy1bZcytY1SfEb7', NULL),
(66, 'Muhammad', 'danish.mansoor85@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '3111504899206.jpg', 'vw4LYeZKqsAw6fZ3TJhNanDz1ROVJ5KC34yO6rLC554GBevAngPSrwfBKymI', 2, '2017-09-08 19:33:26', '2017-09-08 19:41:51', 1, 'Danish', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'PB0lubLsZlostMg6zo8E3XFAhcr4US', NULL),
(67, 'Wladmir', 'wminhoto@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6991504939921.jpg', NULL, 2, '2017-09-09 06:52:02', '2017-09-09 06:52:02', 1, 'Minhoto', 1, 'linkedin', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'cmjnElFJMayfuE6WR8pKqlUlIgEKOJ', NULL),
(68, 'Laísla', 'laislabjc@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-09 10:21:10', '2017-09-09 10:25:16', 1, 'Costa', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, '7maNnfWYVd7xLpod1txZr3JaUIQjKg', NULL),
(69, 'Rafaela', 'rafah.kon@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '5171504966423.jpg', NULL, 2, '2017-09-09 14:13:43', '2017-09-09 14:13:43', 1, 'Konjunski', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'Q8OD6bNvvpnLfeovD9DdPiVxbJueo7', NULL),
(70, 'Camila', 'camborvi@yahoo.com.br', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '6271504986793.jpg', NULL, 2, '2017-09-09 19:53:14', '2017-09-09 19:53:14', 1, 'Vieira', 1, 'facebook', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'ojqrAdaxZeHqPtkhmAuLKiHmi3K3XV', NULL),
(71, 'Tim', 'timfloodflooring@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', NULL, 2, '2017-09-10 03:51:29', '2017-09-10 03:51:42', 1, 'Flood', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'M6doKR3LtEY2BRrB1zIGUBXf29b0Lb', NULL),
(72, 'Henrique', 'hverdan@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '2121505121540.jpg', NULL, 2, '2017-09-11 09:19:00', '2017-09-11 09:30:19', 1, 'Verdan', 1, 'facebook', NULL, 'I am an economist with experience in working with business development. In Ireland I have been working as cleaner, office admin, kitchen assistant, catering assistant, general operative, stock controller and others casual jobs.', 'Better call Henrique', 'Cherrington', NULL, -8.4323863000, 51.8885975000, '1983-05-20', 0, '0px', 0.00000000, NULL, 0, 'xvxFreMybnWHGnJ2Bic00zliiXzDOc', NULL),
(73, 'john', 'jempainting1@gmail.com', '$2y$10$Dm5OQMXEHkFhm2RONbfiy.pf2ylaAABugcH/3mc0/DF9kv5t3esiO', '', 'A4nWKav8r7oI6xuX8Pb6Iua4r82k0ql8Vgw6b0aFWzu8KWcdJl6i4fvu8JmE', 2, '2017-09-11 12:40:18', '2017-09-11 12:41:05', 1, 'miller', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'gFB0FMCap8wAwsLGfyP0dGNIFA1Tbb', NULL),
(74, 'Task', 'anas@golpik.com', '$2y$10$Ag5RnNvOHIsuf95MSjcMfeO1opTXURHeq3W8cmvZTIhFhCTy5S9Y.', '', 'TxRnlFLRo4Vkc84tUo18fTVPbm0A6xXyPDo1LpEAbD54Olr1985a23aBlHki', 2, '2017-09-15 13:08:52', '2017-09-15 14:07:02', 1, 'Poster', 1, '', NULL, NULL, NULL, NULL, NULL, 0.0000000000, 0.0000000000, NULL, 0, '0px', 0.00000000, NULL, 0, 'HqbE7QCA8ijr57GCab6fzK0d2PtQjk', NULL),
(75, 'Task', 'anas.golpik@gmail.com', '$2y$10$ZXaRODjcPJng1WLWsN3Hr.G.BiTZNq4hD/pHqttntH3Y8mCsqdPjq', '75.png', 'cXk1TxdlrhaE9DlSNMrNUyoWsEShwSwi6KZ2BcBldKGtKDLkw5YZMTni1lGK', 2, '2017-09-15 13:11:05', '2017-09-15 16:29:41', 1, 'Runner', 1, '', '3442819161', '', 'Software Engineer', 'Drogheda', NULL, -6.3560985000, 53.7178560000, '1992-09-13', 0, '0px', 0.00000000, '46789865', 1, 'FFtUFVSYsQKESENtgOsaZIynRHR5jI', '92');

-- --------------------------------------------------------

--
-- Table structure for table `users_cards`
--

CREATE TABLE `users_cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cardNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvc` int(11) DEFAULT NULL,
  `cardholderName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billingAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_cards`
--

INSERT INTO `users_cards` (`id`, `user_id`, `cardNumber`, `cvc`, `cardholderName`, `expDate`, `billingAddress`, `postalCode`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '4242424242424242', 123, 'John Bynre', '2021-4', '345 Street', '5678', 0, 1, '2017-08-31 12:05:27', '2017-08-31 12:05:27'),
(2, 6, '4242424242424242', 123, 'Patrick Fagan', '2022-4', '546 street', '8795', 0, 1, '2017-09-01 09:22:47', '2017-09-01 09:22:47'),
(3, 3, '4319473517599467', 217, 'Keith Wallace', '2020-6', 'Apartment 1, 151 upper lesson street, dublin 2', 'Dublin', 1, 1, '2017-09-02 13:20:24', '2017-09-04 08:58:31'),
(4, 3, '4242424242424242', 123, 'Paul Kennedy', '2022-5', '4653', '354233', 0, 1, '2017-09-04 08:58:59', '2017-09-04 08:58:59'),
(5, 38, '4319473517599467', 217, 'Keith Wallace', '2020-6', 'Apt 1, 151 Upper Leeson st', 'D4', 0, 1, '2017-09-09 13:44:48', '2017-09-09 13:44:48'),
(8, 74, '4242424242424242', 123, 'Task Poster', '2018-1', '7831 Fawnwood Drive', '46278', 0, 1, '2017-09-15 16:26:16', '2017-09-15 16:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `users_educations`
--

CREATE TABLE `users_educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `education_id` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_educations`
--

INSERT INTO `users_educations` (`id`, `user_id`, `education_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 8, 2, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(3, 9, 3, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(4, 25, 4, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(5, 25, 5, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(6, 25, 6, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(7, 25, 7, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(8, 25, 8, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(21, 26, 9, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(22, 26, 10, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(23, 26, 11, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(24, 26, 12, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(31, 37, 14, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(32, 37, 15, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(33, 42, 16, 0, 1, '2017-09-06 22:43:16', '2017-09-06 22:43:16'),
(34, 72, 17, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(35, 72, 18, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(36, 72, 19, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(37, 75, 20, 0, 1, '2017-09-15 16:29:41', '2017-09-15 16:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `users_languages`
--

CREATE TABLE `users_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_languages`
--

INSERT INTO `users_languages` (`id`, `user_id`, `language_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 3, 2, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(3, 8, 1, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(4, 8, 3, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(5, 9, 1, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(6, 9, 4, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(7, 10, 1, 0, 1, '2017-09-01 10:38:25', '2017-09-01 10:38:25'),
(8, 25, 1, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(9, 25, 5, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(10, 25, 2, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(20, 26, 1, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(21, 26, 4, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(22, 26, 6, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(23, 28, 7, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(35, 37, 1, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(36, 37, 6, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(37, 42, 1, 0, 1, '2017-09-06 22:43:16', '2017-09-06 22:43:16'),
(38, 42, 8, 0, 1, '2017-09-06 22:43:16', '2017-09-06 22:43:16'),
(39, 45, 1, 0, 1, '2017-09-07 10:17:15', '2017-09-07 10:17:15'),
(42, 52, 6, 0, 1, '2017-09-07 23:39:10', '2017-09-07 23:39:10'),
(43, 52, 1, 0, 1, '2017-09-07 23:39:10', '2017-09-07 23:39:10'),
(46, 48, 1, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(47, 48, 4, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(48, 72, 1, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(49, 72, 6, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(50, 72, 4, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(51, 72, 9, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(52, 75, 1, 0, 1, '2017-09-15 16:29:41', '2017-09-15 16:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `users_skills`
--

CREATE TABLE `users_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_skills`
--

INSERT INTO `users_skills` (`id`, `user_id`, `skill_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(2, 3, 2, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(3, 3, 3, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(4, 3, 4, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(5, 3, 5, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(6, 3, 6, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(7, 3, 7, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(8, 3, 8, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(9, 8, 9, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(10, 8, 10, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(11, 8, 11, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(12, 8, 12, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(13, 9, 13, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(14, 9, 14, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(15, 9, 15, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(16, 9, 16, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(17, 9, 17, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(18, 9, 18, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(19, 10, 19, 0, 1, '2017-09-01 10:38:25', '2017-09-01 10:38:25'),
(20, 25, 20, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(21, 25, 21, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(22, 25, 22, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(23, 25, 23, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(24, 25, 16, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(25, 25, 24, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(26, 25, 25, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(27, 25, 26, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(82, 26, 27, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(83, 26, 28, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(84, 26, 29, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(85, 26, 30, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(86, 26, 31, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(87, 26, 32, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(88, 26, 33, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(89, 26, 34, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(90, 26, 35, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(91, 26, 36, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(92, 26, 37, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(93, 26, 38, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(94, 26, 39, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(95, 26, 40, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(96, 26, 41, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(97, 26, 24, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(98, 26, 42, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(99, 26, 44, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(100, 28, 45, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(101, 28, 46, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(102, 28, 47, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(103, 28, 48, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(104, 28, 49, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(105, 28, 50, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(106, 28, 51, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(107, 28, 52, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(108, 28, 53, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(109, 28, 54, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(110, 28, 55, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(111, 28, 56, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(112, 28, 57, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(113, 28, 58, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(114, 33, 59, 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(115, 33, 60, 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(116, 35, 61, 0, 1, '2017-09-06 21:03:25', '2017-09-06 21:03:25'),
(151, 37, 63, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(152, 37, 64, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(153, 37, 65, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(154, 37, 66, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(155, 37, 67, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(156, 37, 34, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(157, 45, 68, 0, 1, '2017-09-07 10:17:15', '2017-09-07 10:17:15'),
(158, 45, 69, 0, 1, '2017-09-07 10:17:15', '2017-09-07 10:17:15'),
(159, 49, 70, 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(160, 49, 71, 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(161, 49, 72, 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(162, 49, 73, 0, 1, '2017-09-07 17:30:14', '2017-09-07 17:30:14'),
(163, 58, 74, 0, 1, '2017-09-08 10:32:45', '2017-09-08 10:32:45'),
(164, 62, 75, 0, 1, '2017-09-08 12:01:50', '2017-09-08 12:01:50'),
(165, 48, 76, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(166, 48, 77, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(167, 48, 20, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(168, 48, 78, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(169, 72, 79, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(170, 72, 80, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(171, 72, 81, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(172, 72, 82, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(173, 72, 83, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(174, 72, 84, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(175, 72, 85, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(176, 72, 86, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(177, 72, 87, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `users_transports`
--

CREATE TABLE `users_transports` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_transports`
--

INSERT INTO `users_transports` (`id`, `user_id`, `transport_id`, `deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, 1, '2017-08-30 17:34:53', '2017-08-30 17:34:53'),
(2, 2, 2, 0, 1, '2017-08-30 17:34:53', '2017-08-30 17:34:53'),
(3, 2, 3, 0, 1, '2017-08-30 17:34:53', '2017-08-30 17:34:53'),
(4, 2, 5, 0, 1, '2017-08-30 17:34:53', '2017-08-30 17:34:53'),
(5, 3, 1, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(6, 3, 2, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(7, 3, 3, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(8, 3, 5, 0, 1, '2017-08-31 12:02:23', '2017-08-31 12:02:23'),
(9, 8, 2, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(10, 8, 3, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(11, 8, 5, 0, 1, '2017-09-01 09:37:13', '2017-09-01 09:37:13'),
(12, 9, 1, 0, 1, '2017-09-01 10:03:26', '2017-09-01 10:03:26'),
(13, 10, 6, 0, 1, '2017-09-01 10:38:25', '2017-09-01 10:38:25'),
(20, 25, 1, 0, 1, '2017-09-05 09:14:13', '2017-09-05 09:14:13'),
(24, 26, 1, 0, 1, '2017-09-05 11:52:23', '2017-09-05 11:52:23'),
(25, 28, 1, 0, 1, '2017-09-05 11:53:41', '2017-09-05 11:53:41'),
(26, 33, 1, 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(27, 33, 2, 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(28, 33, 3, 0, 1, '2017-09-06 10:50:31', '2017-09-06 10:50:31'),
(50, 37, 1, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(51, 37, 2, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(52, 37, 3, 0, 1, '2017-09-06 21:17:05', '2017-09-06 21:17:05'),
(54, 52, 1, 0, 1, '2017-09-07 23:39:10', '2017-09-07 23:39:10'),
(55, 52, 2, 0, 1, '2017-09-07 23:39:10', '2017-09-07 23:39:10'),
(56, 52, 3, 0, 1, '2017-09-07 23:39:10', '2017-09-07 23:39:10'),
(57, 58, 1, 0, 1, '2017-09-08 10:32:45', '2017-09-08 10:32:45'),
(59, 48, 1, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(60, 48, 2, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(61, 48, 3, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(62, 48, 5, 0, 1, '2017-09-09 22:28:22', '2017-09-09 22:28:22'),
(63, 72, 1, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(64, 72, 2, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(65, 72, 3, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(66, 72, 5, 0, 1, '2017-09-11 09:30:19', '2017-09-11 09:30:19'),
(67, 75, 1, 0, 1, '2017-09-15 16:29:41', '2017-09-15 16:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment_receives`
--

CREATE TABLE `user_payment_receives` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `receive_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_paypals`
--

CREATE TABLE `user_paypals` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `administrators_username_unique` (`username`);

--
-- Indexes for table `assigns`
--
ALTER TABLE `assigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications_settings`
--
ALTER TABLE `notifications_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_prices`
--
ALTER TABLE `offer_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payments_credit_cards`
--
ALTER TABLE `payments_credit_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paypal`
--
ALTER TABLE `paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_reports`
--
ALTER TABLE `profile_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_updates`
--
ALTER TABLE `profile_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `stripe_accounts`
--
ALTER TABLE `stripe_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stripe_customers`
--
ALTER TABLE `stripe_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tasks_key_unique` (`key`);

--
-- Indexes for table `task_categories`
--
ALTER TABLE `task_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_files`
--
ALTER TABLE `task_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_cards`
--
ALTER TABLE `users_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_educations`
--
ALTER TABLE `users_educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_languages`
--
ALTER TABLE `users_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_skills`
--
ALTER TABLE `users_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_transports`
--
ALTER TABLE `users_transports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payment_receives`
--
ALTER TABLE `user_payment_receives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_paypals`
--
ALTER TABLE `user_paypals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assigns`
--
ALTER TABLE `assigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `notifications_settings`
--
ALTER TABLE `notifications_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=885;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `offer_prices`
--
ALTER TABLE `offer_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payments_credit_cards`
--
ALTER TABLE `payments_credit_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paypal`
--
ALTER TABLE `paypal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_reports`
--
ALTER TABLE `profile_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_updates`
--
ALTER TABLE `profile_updates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `stripe_accounts`
--
ALTER TABLE `stripe_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stripe_customers`
--
ALTER TABLE `stripe_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `task_categories`
--
ALTER TABLE `task_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `task_files`
--
ALTER TABLE `task_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `users_cards`
--
ALTER TABLE `users_cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users_educations`
--
ALTER TABLE `users_educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `users_languages`
--
ALTER TABLE `users_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `users_skills`
--
ALTER TABLE `users_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT for table `users_transports`
--
ALTER TABLE `users_transports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `user_payment_receives`
--
ALTER TABLE `user_payment_receives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_paypals`
--
ALTER TABLE `user_paypals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
