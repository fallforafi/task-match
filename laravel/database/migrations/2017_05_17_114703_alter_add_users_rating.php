<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddUsersRating extends Migration {

    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->double('rating', 15, 8);
        });
    }

}
