<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration {

    public function up() {
        Schema::dropIfExists('activities');
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 20);
            $table->integer('type_id');
            $table->integer('user_id');
            $table->text('description');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down() {
        Schema::drop('activities');
    }

}
