<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNotificationAddSeenColumn extends Migration {

    public function up() {
        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('seen')->default(0);
        });
    }

    public function down() {
        
    }

}