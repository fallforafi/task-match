<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('messages');

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->string('subject');
            $table->integer('to');
            $table->integer('thread_id')->nullable();
            $table->integer('from');
            $table->integer('reply_to')->nullable();
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }


    public function down() {
        Schema::drop('messages');
        //
    }

}
