<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskAssignsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('assigns');
        Schema::create('assigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down()
    {
        Schema::drop('assigns');
    }
}
