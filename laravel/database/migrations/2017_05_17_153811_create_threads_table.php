<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::dropIfExists('threads');
        Schema::create('threads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->integer('task_owner_id');
            $table->integer('task_runner_id');
            $table->integer('offer_price_id');
            $table->string('key');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
