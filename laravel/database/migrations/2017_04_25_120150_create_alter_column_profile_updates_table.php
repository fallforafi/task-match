<?php

use Illuminate\Database\Migrations\Migration;

class CreateAlterColumnProfileUpdatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('UPDATE `profile_updates` SET `account` = 0 WHERE `account` IS NULL;');
        DB::statement('UPDATE `profile_updates` SET `payment` = 0 WHERE `payment` IS NULL;');
        
        DB::statement('ALTER TABLE `profile_updates` MODIFY `account` INTEGER NOT NULL default "0";');
        DB::statement('ALTER TABLE `profile_updates` MODIFY `payment` INTEGER NOT NULL default "0";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('ALTER TABLE `profile_updates` MODIFY `account` INTEGER NULL;');
        DB::statement('ALTER TABLE `profile_updates` MODIFY `payment` INTEGER NULL;');
    }

} 
