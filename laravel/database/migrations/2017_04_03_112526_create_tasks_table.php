<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

    public function up() {
        Schema::dropIfExists('tasks');

        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('user_id')->nullable();
            $table->string('sessionId')->nullable();
            $table->string('description');
            $table->string('key')->unique();
            $table->string('location');
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->integer('country')->nullable();
            $table->string('workersRequired');
            $table->string('taskStatus')->default('open');
            $table->float('price');
            $table->date('dueDate');
            $table->string('toBeCompleted');
            $table->integer('deleted')->default(0);
            $table->integer('status')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }
    
    public function down() {
        Schema::drop('tasks');
    }
}
