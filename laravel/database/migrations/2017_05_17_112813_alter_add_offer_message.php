<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddOfferMessage extends Migration {

    public function up() {
        Schema::table('offers', function (Blueprint $table) {
            $table->text('message');
        });
    }
    public function down() {
        //
    }
}
