<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOfferPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('offer_prices', function (Blueprint $table) {
            $table->text('key');
        });
    }

    public function down()
    {
        //
    }
}
