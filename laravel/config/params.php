<?php

return [
    'site_name' => 'Taskmatch',
    'commission' => '15',
    'currencies' => [
        'USD' => ['symbol' => '$', 'name' => 'US Dollors'],
        'EUR' => ['symbol' => '‎€', 'name' => 'Euro'],
    ],
    'currency_default' => 'EUR',
    'languages' => [
        'en_uk' => 'English (UK)',
        'en_us' => 'English (US)',
    ],
    'language_default' => 'en_uk',
    'contentTypes' => [
        'page' => 'Page',
        'email' => 'Email',
        'block' => 'Block',
    ],
    'transportTypes' => [
        1 => 'Online',
        2 => 'Walk',
        3 => 'Bicycle',
        4 => 'Scooter',
        5 => 'Car',
        6 => 'Truck',
    ],
    'order_prefix' => "tsk",
    'task_status' => [
        'open' => 'Open',
        'assigned' => 'Assigned',
        'completed' => 'Completed'
        ],
];