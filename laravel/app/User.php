<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get the phone record associated with the user.
     */
    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function bank_details() {
        return $this->hasOne('App\BankDetails');
    }

    public static function searchUser($search) {

        $result = User::where("deleted", '=', 0);

        if (isset($search['firstName']) && $search['firstName'] != "") {
            $firstName = $search['firstName'];
            $result = $result->where('firstName', 'LIKE', "%" . $firstName . "%");
        }

        if (isset($search['lastName']) && $search['lastName'] != "") {
            $lastName = $search['lastName'];
            $result = $result->where('lastName', 'LIKE', "%" . $lastName . "%");
        }
        if (isset($search['email']) && $search['email'] != "") {
            $email = $search['email'];
            $result = $result->where('email', 'LIKE', "%" . $email . "%");
        }
        $result = $result->where('role_id','!=',1);
        $result = $result->orderBy('id', 'desc');
        $result = $result->paginate(5);
        $result->setPath('users');
        return $result;
    }

    public static function findUsers($dateTime) {
        $user = self::where('created_at', '=', $dateTime)->where('status', 1)->get();
        return $user;
    }

}
