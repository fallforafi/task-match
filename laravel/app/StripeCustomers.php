<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stripe;

class StripeCustomers extends Model {

    //
    protected $table = 'stripe_customers';

    public static function getStripeId($user, $input, $card_id) {

        $stripe = Stripe::setApiKey(env('STRIPE_SECRET_SK'));
        $stripeUser = StripeCustomers::where(['user_id' => $user->id,'deleted'=> 0])->first();
        $token = $stripe->tokens()->create([
            'card' => [
                'number' => $input['cardNumber'],
                'exp_month' => $input['expMonth'],
                'exp_year' => $input['expYear'],
                'cvc' => $input['cvc'],
            ],
        ]);

        if (count($stripeUser) == 0) {

            $customer = StripeCustomers::createStripeCustomer($user, $input, $token);
            $stripeCustomerId = $customer['id'];
            $defaultSource = $customer['default_source'];
            $insert_id = StripeCustomers::insertGetId(array('user_id' => $user->id, 'stripeCustomerId' => $stripeCustomerId, 'source' => $defaultSource, 'created_at' => date("Y-m-d H:i:s"), "card_id" => $card_id));
        } else {
            $customer = Stripe::customers()->find($stripeUser->stripeCustomerId);
            if (isset($customer['deleted']) && $customer['deleted'] == 1) {
                $customer = StripeCustomers::createStripeCustomer($user, $input, $token);
                $stripeCustomerId = $customer['id'];
                $defaultSource = $customer['default_source'];
                StripeCustomers::where('card_id', $card_id)->update(array('user_id' => $user->id, 'stripeCustomerId' => $stripeCustomerId, 'source' => $defaultSource, 'updated_at' => date("Y-m-d H:i:s")));
            }
        }
        return $customer;
    }

    public static function createStripeCustomer($user, $input, $token) {
        return $customer = Stripe::customers()->create([
            'source' => $token['id'],
            'email' => $user->email,
            'metadata' => [
                "User ID" => $user->id,
                "Name" => $input['cardholderName'],
                "email" => $user->email,
                "Billing Address" => $input['billingAddress'],
                "Postal Code" => $input['postalCode']
        ]]);
    }

}
