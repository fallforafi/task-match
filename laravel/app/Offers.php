<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\StripeAccounts;
use App\Tasks;
use App\User;
use App\Functions\Functions;

class Offers extends Model {

    protected $table = 'offers';

    public static function get($search) {
        $subSql = "";

        if ($search['task_id'] != "") {
            $task_id = $search['task_id'];
            $subSql .= " and c.task_id = '$task_id'";
        }
        $sql = "select c.created_at,c.id as offer_id,c.offerStatus,c.message,u.id as user_id,u.firstName,u.lastName,u.image,op.key,u.key as user_key
from offers c 
left join offer_prices op on op.offer_id=c.id
left join users u on u.id=c.user_id
where 1 " . $subSql . " order by c.id desc";

        return $result = DB::select($sql);
    }

    public static function getOffers($input) {
        $subSql = "";

        if (isset($input['user_id'])) {
            $user_id = $input['user_id'];
            $subSql .= " and o.user_id='$user_id'";
        }

        if (isset($input['taskStatus'])) {
            $taskStatus = $input['taskStatus'];
            $subSql .= " AND t.taskStatus='$taskStatus'";
        }

        if (isset($input['offerStatus'])) {
            $offerStatus = $input['offerStatus'];
            $subSql .= " AND o.offerStatus='$offerStatus'";
        }

        if (isset($input['offer_id'])) {
            $offer_id = $input['offer_id'];
            $subSql .= " AND o.id='$offer_id'";
        }

        $sql = "SELECT t.id as task_id,t.key as taskKey,t.title,t.description,t.dueDate,t.taskStatus,t.price,o.id as offer_id,o.offerStatus,o.created_at,o.message as offerMessage,op.offerPrice,m.id as message_id,m.subject,m.message,m.key,u.firstName,u.lastName,u.email,u.image  FROM tasks t 
            LEFT JOIN offers o ON o.task_id=t.id
            LEFT JOIN offer_prices op ON op.offer_id=o.id
            LEFT JOIN messages m ON m.id=o.message_id
            LEFT JOIN users u ON u.id=t.user_id
            WHERE  1 " . $subSql;

        return $result = DB::select($sql);
    }

    public static function getOfferData($input) {
        $subSql = "";
        if (isset($input['offer_id'])) {
            $offer_id = $input['offer_id'];
            $subSql .= " AND of.id='$offer_id'";
        }

        $sql = "SELECT t.`title`,t.`price`,t.`key` AS taskKey,u.`firstName`,u.`lastName`,of.`created_at`,of.`offerStatus`,op.`offerPrice`,th.`key` FROM offers of
LEFT JOIN tasks t ON t.`id` = of.`task_id`
LEFT JOIN users u ON u.`id` = t.`user_id`
LEFT JOIN offer_prices op ON op.`offer_id` = of.`id`
LEFT JOIN threads th ON th.`task_id` = t.`id`
            WHERE  1 " . $subSql;

        return $result = DB::select($sql);
    }

    public static function checkOffer($user_id) {
        $errors = array();
        $stripeAccounts = StripeAccounts::where('user_id', $user_id)->first();
        if (count($stripeAccounts) == 0) {
            $errors['payment'] = true;
        }
        if (Auth::user()->image == "") {
            $errors['image'] = true;
        }
        if (Auth::user()->phone == "" && Auth::user()->verified == 0) {
            $errors['phone'] = true;
        } elseif (Auth::user()->phone != "" && Auth::user()->verified == 0) {
            $errors['phone'] = true;
        }
        if (Auth::user()->dob == "") {
            $errors['dob'] = true;
        }
        return $errors;
    }

    public static function offersCompletionAlert($dateTime) {
        $offer = self::where([
                    'offerStatus' => 'assigned',
                    'deleted' => 0,
                    'status' => 1,
                    'updated_at' => $dateTime,
                ])->get();
        if (count($offer) > 0) {
            foreach ($offer as $value) {
                $task = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                        ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                        ->select('tasks.title', 'op.key', 'tasks.user_id')
                        ->where('of.id', $value->id)
                        ->where('tasks.id', $value->task_id)
                        ->where('of.updated_at', '=', $dateTime)
                        ->first();
                $user = User::find($task->user_id);
                $taskRunner = User::find($value->user_id);
                $notification['task'] = $task;
                $notification['name'] = $user->firstName . ' ' . $user->lastName;
                $notification['taskRunner'] = $taskRunner->firstName;
                $subject = view('emails.crons.offer_not_completed_seven_days.subject');
                $body = view('emails.crons.offer_not_completed_seven_days.body', compact('notification'));
                Functions::sendEmail($user->email, $subject, $body);
                continue;
            }
        } else {
            return false;
        }
    }

    public static function NotRespondingOffers($dateTime) {
        $query = "select u.firstName, u.email,count(of.id) offers, group_concat(op.`key`) offer_key
 from users as u
left join tasks as t on t.user_id = u.id
left join offers as of on of.task_id = t.id
left join offer_prices as op on op.offer_id = of.id
where of.offerStatus = 'pending' and t.deleted = 0 and of.created_at = '$dateTime'
group by u.id";
        return $result = DB::select($query);
    }

}
