<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentReceives extends Model {

    protected $fillable = ['receive_id','type','user_id'];
    protected $table = 'user_payment_receives';

}
