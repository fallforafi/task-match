<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categories extends Model {

    protected $table = 'categories';

    public static function byTaskId($id) {
        $sql = "select c.id,c.name,c.class from categories c left join task_categories tc on c.id=tc.category_id where tc.task_id=$id";
        return $result = DB::select($sql);
    }

}
