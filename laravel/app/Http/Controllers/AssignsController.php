<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\User;
use App\Tasks;
use App\Offers;
use App\OfferPrices;
use App\Threads;
use App\Messages;
use App\Assigns;
use App\UsersCards;
use App\StripeCustomers;
use App\Transactions;
use Illuminate\Http\Request;
use App\Functions\UsersNotifications;
use Stripe;
use Config;
use App\StripeAccounts;
use DB;
use App\Functions\Functions;

class AssignsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        
    }

    public function reject(Request $request) {
        $rules = array(
            'reason' => 'required|min:6|max:100',
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            // $errors = json_decode($errors);
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input = $request->all();
            unset($input['_token']);
            $data['model'] = OfferPrices::where('key', $input['key'])->orderBy('id', 'desc')->where('status', 1)->where('deleted', 0)->first();
            $offer = Offers::where('id', $data['model']->offer_id);
            $update = $offer->update(array('offerStatus' => 'cancel', 'updated_at' => date('Y-m-d H:i:s')));
            $offer = $offer->first();
            $user = User::find($offer->user_id);
            //$task = Tasks::find($offer->task_id);
            $task = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                    ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                    ->select('tasks.title', 'op.key')
                    ->where('of.id', $offer->id)
                    ->where('op.id', $data['model']->id)
                    ->first();
            $notification['task'] = $task;
            $notification['reason'] = $input['reason'];
            $notification['notify_to'] = $user->id;
            $notification['notify_email'] = $user->email;
            $notification['notify_name'] = $user->firstName . ' ' . $user->lastName;
            $notification['type'] = 'offer_reject';
            $notification['type_id'] = $data['model']->id;
            UsersNotifications::save($notification);
//        \Session::flash('success', 'You have successfully rejected the offer');
        }
        return json_encode($response);
    }

    public function offers($key) {
        $data['model'] = OfferPrices::where('key', $key)->orderBy('id', 'desc')->where('status', 1)->where('deleted', 0)->first();
        $offer = Offers::find($data['model']->offer_id);
        $user = User::find($offer->user_id);
        $task = Tasks::join('users as u', 'u.id', '=', 'tasks.user_id')
                ->select('u.firstName', 'u.lastName', 'tasks.*')
                ->where('tasks.id', $offer->task_id)
                ->first();
        $data['cards'] = UsersCards::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->where('status', 1)->where('deleted', 0)->get();
        $data['offer'] = $offer;
        $data['user'] = $user;
        $data['task'] = $task;
        $data['threadsModel'] = Threads::where('task_id', $offer->task_id)->where('status', 1)->where('deleted', 0)->first();
        ;
        return view('front.offers.assign', $data);
    }

    public function assign(Request $request) {


        $currency = Config::get('params.currency_default');
        $validation = array(
            'card_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'message' => 'required|max:300|min:10',
            'offer_price_id' => 'required|numeric',
            'offer_id' => 'required|numeric',
        );

        $input = $request->all();

        $validator = Validator::make($input, $validation);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $assign = Assigns::where('offer_price_id', $request->offer_price_id)->first();

        if (count($assign) == 0) {

            if (Auth::user()->id == $request->user_id) {

                $offerPrice = OfferPrices::find($request->offer_price_id);
                $offer = Offers::find($offerPrice->offer_id);
                $user = User::find($offer->user_id);
                $task = Tasks::find($offer->task_id);
                $description = Auth::user()->firstName . " " . Auth::user()->lastName . " assign a task to " . $user->firstName . " " . $user->lastName;

                $cards = UsersCards::find($request->card_id);
                $stripeCard = StripeCustomers::where([
                    'card_id' => $request->card_id,
                    'deleted' => 0])->first();
                //$stripeAccount = StripeAccounts::where('user_id', $offer->user_id)->first();
                $stripe = Stripe::setApiKey(env('STRIPE_SECRET_SK'));

                $task_runner_id = $offer->user_id;
                $task_owner_id = $request->user_id;

                $taskLink = url('task/' . $task->key);
                $runnerLink = url('user/' . $user->key);
                $ownerLink = url('user/' . Auth::user()->key);

                $charge = Stripe::charges()->create([
                    'amount' => $offerPrice->offerPrice,
                    'currency' => $currency,
                    'customer' => $stripeCard->stripeCustomerId,
                    'source' => $stripeCard->source,
                    'description' => $description,
                    "transfer_group" => $offer->task_id,
                    'metadata' => [
                        "Task id" => $task->id,
                        "Task title" => $task->title,
                        "Task link" => $taskLink,
                        "Task description" => $task->description,
                        "Taskowner id" => Auth::user()->id,
                        "Taskowner name" => Auth::user()->firstName . " " . Auth::user()->lastName,
                        "Taskowner profile" => $ownerLink,
                        "Taskrunner id" => $user->id,
                        "Taskrunner name" => $user->firstName . " " . $user->lastName,
                        "Taskrunner profile" => $runnerLink,
                    ],
                    'capture' => true
                ]);
                if (isset($charge['id'])) {
                    DB::beginTransaction();

                    try {

                        $offer = Offers::where('id', $offerPrice->offer_id);
                        $update = $offer->update(array('offerStatus' => 'assigned', 'updated_at' => date('Y-m-d H:i:s')));
                        $taskUpdate = Tasks::where('id', $task->id)->update(array('taskStatus' => 'assigned', 'updated_at' => date('Y-m-d H:i:s')));

                        $model = new Assigns();
                        $model->offer_price_id = $request->offer_price_id;
                        $model->created_at = date("Y-m-d H:i:s");
                        $model->save();
                        $assign_id = $model->id;
                        $model = new Transactions();
                        $model->assign_id = $assign_id;
                        $model->user_id = Auth()->user()->id;
                        $model->reference = $charge['id'];
                        $model->gateway = 'stripe';
                        $model->type = 'sent';
                        $model->amount = $offerPrice->offerPrice;
                        $model->created_at = date("Y-m-d H:i:s");
                        $model->save();

                        $threadsModel = new Threads();
                        $threadsModel->offer_price_id = $request->offer_price_id;
                        $threadsModel->task_id = $task->id;
                        $threadsModel->task_runner_id = $task_runner_id;
                        $threadsModel->task_owner_id = $task_owner_id;
                        $threadsModel->key = str_shuffle(Functions::generateRandomString(6) . time());
                        $threadsModel->created_at = date("Y-m-d H:i:s");
                        $threadsModel->save();


                        $messageModel = new Messages();
                        $messageModel->to = $task_runner_id;
                        $messageModel->message = $request->message;
                        $messageModel->thread_id = $threadsModel->id;
                        $messageModel->key = str_shuffle(Functions::generateRandomString(6) . time());
                        $messageModel->from = $task_owner_id;
                        $messageModel->created_at = date("Y-m-d H:i:s");
                        $messageModel->save();

                        $input['thread_id'] = $threadsModel->id;
                        $Model = Tasks::leftJoin('threads as th', 'th.task_id', '=', 'tasks.id')
                                ->select('tasks.title', 'tasks.key as taskKey', 'th.key')
                                ->where('th.id', $input['thread_id'])
                                ->where('tasks.id', $task->id)
                                ->first();

                        $notification['task'] = $Model;
                        $notification['notify_to'] = $user->id;
                        $notification['notify_email'] = $user->email;
                        $notification['notify_name'] = $user->firstName . ' ' . $user->lastName;
                        $notification['type'] = 'task_accept';
                        $notification['type_id'] = $model->id;
                        UsersNotifications::save($notification);

                        $subject = view('emails.accept_task.subject');
                        $body = view('emails.accept_task.body', compact('notification'));
                        Functions::sendEmail(Auth::user()->email, $subject, $body);

                        $response['id'] = $model->id;
                        DB::commit();
                        \Session::flash('success', 'You have successfully assign this task to ' . $notification['notify_name']);
                        return redirect()->back();
                    } catch (Exception $ex) {
                        DB::rollBack();
                        $validator->errors()->add('error_db', 'Somthing went wrong try again');
                    }
                }
            } else {
                $validator->errors()->add('valid_user', 'Not a valid user');
            }
        } else {
            $validator->errors()->add('alerady_assign', 'You have already assign this task.');
        }

        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    public function payassignee(Request $request) {
        //d($request->all(), 1);
        $currency = Config::get('params.currency_default');
        $response['error'] = 1;
        $response['errorMsg'] = "";
        $validation = array();
        $validator = Validator::make($request->all(), $validation);

        $assign = Assigns::where('offer_price_id', $request->offer_price_id)->first();
        if (count($assign) == 1) {
            if (Auth::user()->id == $request->user_id) {

                $offerPrice = OfferPrices::find($request->offer_price_id);
                $offer = Offers::where('id', $offerPrice->offer_id);
                $update = $offer->update(array('offerStatus' => 'completed', 'updated_at' => date('Y-m-d H:i:s')));
                $offer = $offer->first();
                $user = User::find($offer->user_id);
                Tasks::where('id', $offer->task_id)->update(array('taskStatus' => 'completed', 'updated_at' => date('Y-m-d H:i:s')));
                $task = Tasks::find($offer->task_id);
                $description = Auth::user()->firstName . " " . Auth::user()->lastName . " assign a task to " . $user->firstName . " " . $user->lastName;

                $response['error'] = 0;
                $stripeAccount = StripeAccounts::where('user_id', $offer->user_id)->first();
                $stripe = Stripe::setApiKey(env('STRIPE_SECRET_SK'));

                $taskLink = url('task/' . $task->key);
                $runnerLink = url('user/' . $user->id);
                $ownerLink = url('user/' . Auth::user()->id);

                $transfer = Stripe::transfers()->create([
                    'amount' => $offerPrice->finalPrice,
                    'currency' => $currency,
                    'description' => $description,
                    'destination' => $stripeAccount->accountId,
                    'transfer_group' => $offer->task_id,
                    'metadata' => [
                        "Task id" => $task->id,
                        "Task title" => $task->title,
                        "Task link" => $taskLink,
                        "Task description" => $task->description,
                        "Taskowner id" => Auth::user()->id,
                        "Taskowner name" => Auth::user()->firstName . " " . Auth::user()->lastName,
                        "Taskowner profile" => $ownerLink,
                        "Taskrunner id" => $user->id,
                        "Taskrunner name" => $user->firstName . " " . $user->lastName,
                        "Taskrunner profile" => $runnerLink,
                    ]
                ]);
                
                //To TaskPoster after task has been marked as completed
                $subjectPoster = view('emails.complete_task.subject');
                $bodyPoster = view('emails.complete_task.body', compact('user'));
                Functions::sendEmail(Auth::user()->email, $subjectPoster, $bodyPoster);
                
                //To TaskRunner after task has been marked as completed
                $subjectRunner = view('emails.task_complete.subject');
                $bodyRunner = view('emails.task_complete.body', compact('user'));
                Functions::sendEmail($user->email, $subjectRunner, $bodyRunner);
                
                //When payment is sent to the TaskRunner’s account
                $model = Tasks::join('users as u', 'u.id', '=', 'tasks.user_id')
                        ->select('u.firstName', 'u.lastName', 'tasks.*')
                        ->where('tasks.id', $offer->task_id)
                        ->first();
                $notification['task'] = $model;
                $notification['notify_to'] = $user->id;
                $notification['notify_email'] = $user->email;
                $notification['name'] = $user->firstName . ' ' . $user->lastName;
                $notification['image'] = $user->image;
                $notification['type'] = 'payment_made';
                $notification['type_id'] = $assign->id;
                $notification['taskRunner'] = $user->firstName;
                UsersNotifications::save($notification);
                
            }
            \Session::flash('success', 'Payment submitted successfully!');
        } else {
            $validator->errors()->add('alerady_pay', "Already paid for this task!");
        }

        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

}
