<?php

namespace App\Http\Controllers;

use DB;
use App\Categories;
use App\Products;
use App\Content;
use App\ProductsImages;
use App\Attributesvalues;
use Illuminate\Http\Request;
use App\Functions\Functions;
//use App\User;

class HomeController extends Controller {

    protected $categories = array();
    protected $layout = 'layouts.search';

    public function __construct() {
        $this->categories = 1;
    }

    public function index() {
        $data = array();
//        $dateTime = date('Y-m-d H:i:s', strtotime('-1 day'));
//        $query = "select u.firstName, u.email,count(of.id) offers, group_concat(op.`key`) offer_key
//from users as u
//left join tasks as t on t.user_id = u.id
//left join offers as of on of.task_id = t.id
//left join offer_prices as op on op.offer_id = of.id
//where of.offerStatus = 'pending' and t.deleted = 0
//group by u.id
//";
//        $result = DB::select($query);
////d($result->email);
//        foreach ($result as $key => $value) {
//            $data = $value->email;
//            $keys = explode(',', $value->offer_key);
////            foreach ($keys as $k) {
////                if (count($k) > 1) {
////                    $data = $k;
////                } else {
////                    $data = $keys;
////                }
////            }
//
//            d($data);
        return view('front.index', $data);
    }

    public function page() {

        $model = Content::where('code', 'home')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function aboutus() {
        $model = Content::where('code', 'aboutus')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function marketplace() {
        $model = Content::where('code', 'marketplace')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

//    public function earnwithus() {
//        $model = Content::where('code', 'earn-with-us')->first();
//        $model->body = Functions::setTemplate($model->body, array());
//        return view('front.page', compact('model'));
//    }
    public function earnwithus() {
        return view('front.earn-with-us');
    }

    public function findMore() {
        return view('front.find-out-more');
    }

    public function faqs() {
        $model = Content::where('code', 'faq')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function privacy() {
        $model = Content::where('code', 'privacy')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function terms() {
        $model = Content::where('code', 'terms')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function howitworks() {
//        $model = Content::where('code', 'how-it-works')->first();
//        $model->body = Functions::setTemplate($model->body, array());
        return view('front.how-it-works');
    }

    public function postingatask() {
        $model = Content::where('code', 'posting-a-task')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function priceguide() {
        $model = Content::where('code', 'priceguide')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function newuserfaq() {
        $model = Content::where('code', 'newuserfaq')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

//    public function contact() {
//        $model = Content::where('code', 'contact')->first();
//        $model->body = Functions::setTemplate($model->body, array());
//        return view('front.page', compact('model'));
//    }

    public function getproduct($id) {
        $product = Products::find($id);
        $productImages = array();
        $availabeInColors = array();
        $productImages = ProductsImages::where('product_id', '=', $id)->get();
        $availabeInColors = Attributesvalues::getImagesAndColors($id);
        $relatedProducts = array();
//$relatedProducts=Products::where('category_id','=',$product->category_id)->limit(3)->get();
//$category= Categories::find($product->category_id);
        $category = array();
        $attributes = DB::table('attributes as a')
                ->where('pa.product_id', '=', $id)
                ->leftJoin('products_attributes as pa', 'pa.attribute_id', '=', 'a.id')
                ->leftJoin('attributes_values as av', 'av.attribute_id', '=', 'a.id')
                ->select('a.id as attribute_id', 'a.name as name', 'a.type as type', DB::raw('group_concat(av.id) as value_id'), DB::raw('group_concat(av.name) as value_names'), DB::raw('group_concat(av.price) as value_price'))
                ->groupBy('a.id')
                ->orderBy('av.sortOrder', 'asc')
                ->get();


        return view('front.product', compact('product', 'category', 'relatedProducts', 'attributes', 'productImages', 'availabeInColors'))->with('id', $id);
    }

    public function sale() {
        $category = "";
        $products = Products::where('sale', '=', 1)->where('price', '>', 'salePrice')->get();
        return view('front.products', compact('products', 'category'));
    }

    public function products($id = 4) {
        $category = Categories::find($id);
        $products = Products::getProducts(array('category_id' => $id));
        return view('front.products', compact('products', 'category'));
    }

    public function apparel($id = 1) {
        $category = Categories::find($id);
        $products = Products::getProducts(array('category_id' => $id));
        return view('front.products', compact('products', 'category'));
    }

    public function messagePost(Request $request) {
        $validation = array('name' => 'required|max:30',
            'email' => 'required|email|max:30',
            'captcha' => 'required|captcha',
            'message' => 'required|min:6|max:200');

        $messages = [
            'captcha' => 'The :attribute field is invalid.',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $guestbook = new Guestbook;
        $guestbook->name = $request->name;
        $guestbook->email = $request->email;
        $guestbook->message = $request->message;
        $guestbook->save();
        return redirect('guestbook')->withInput();
    }

}
