<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\UsersCards;
use App\BankDetails;
use Session;
use Illuminate\Http\Request;
use Stripe;
use App\StripeAccounts;
use App\Transactions;
use App\User;
use App\StripeCustomers;
use App\ProfileUpdates;

class PaymentsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data = array();
        $user_id = Auth::user()->id;
        $model = UsersCards::where([
                    'user_id' => $user_id,
                    'deleted' => 0,
                    'status' => 1
                ])->orderBy('id', 'desc')->get();
        $data['model'] = $model;
        $account = StripeAccounts::where('user_id', $user_id)->first();
        if (isset($account->accountId) && $account->accountId != Null) {
            $data['account'] = 1;
        } else {
            $data['account'] = 0;
        }
        $data['details'] = BankDetails::
                where('user_id', $user_id)
                ->first();
        $transactionsSent = Transactions::leftJoin('assigns as a', 'a.id', '=', 'transactions.assign_id')
                        ->leftJoin('offer_prices as op', 'op.id', '=', 'a.offer_price_id')
                        ->leftJoin('offers as o', 'o.id', '=', 'op.offer_id')
                        ->join('tasks as t', 't.id', '=', 'o.task_id')
                        ->leftJoin('users as u', 'u.id', '=', 'o.user_id')
                        ->select('u.firstName', 'u.lastName', 'transactions.*', 't.title')
                        ->where([
                            'transactions.user_id' => $user_id,
                            'transactions.deleted' => 0,
                            'transactions.status' => 1
                        ])->orderBy('transactions.id', 'desc')->get();
        $data['transactionsSent'] = $transactionsSent;
        $transactionsReceive = Transactions::leftJoin('assigns as a', 'a.id', '=', 'transactions.assign_id')
                        ->leftJoin('offer_prices as op', 'op.id', '=', 'a.offer_price_id')
                        ->leftJoin('offers as o', 'o.id', '=', 'op.offer_id')
                        ->join('tasks as t', 't.id', '=', 'o.task_id')
                        ->leftJoin('users as u', 'u.id', '=', 'transactions.user_id')
                        ->select('u.firstName', 'u.lastName', 'transactions.*', 't.title', 'op.finalPrice')
                        ->where([
                            'o.user_id' => $user_id,
                            'transactions.deleted' => 0,
                            'transactions.status' => 1
                        ])->orderBy('transactions.id', 'desc')->get();
        $data['transactionsReceive'] = $transactionsReceive;
        //d($transactionsReceive, 1);
        return view('front.payments.index', $data);
    }

    public function store(Request $request) {
        $user_id = Auth::user()->id;
        $validation = array(
            'cardNumber' => 'required|max:16',
            'cardholderName' => 'required|max:30',
            'cvc' => 'required|min:1|max:4',
            'expDate' => 'date_format:"Y-m"|required|after:now',
            'billingAddress' => 'required|max:50',
            'postalCode' => 'required',
        );
        $input = $request->all();
        $input['expDate'] = $request->expYear . '-' . $request->expMonth;


        $message = [
            'expDate.after' => 'Kindly add date of the day after today or month',
        ];

        $validator = Validator::make($input, $validation, $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $usersCard = new UsersCards();
        $usersCard->user_id = $user_id;
        $usersCard->cardNumber = $input['cardNumber'];
        $usersCard->cardholderName = $input['cardholderName'];
        $usersCard->cvc = $input['cvc'];
        $usersCard->expDate = $input['expDate'];
        $usersCard->billingAddress = $input['billingAddress'];
        $usersCard->postalCode = $input['postalCode'];
        $usersCard->save();

        try {
            $user = User::find($user_id);
            $customer = StripeCustomers::getStripeId($user, $input, $usersCard->id);
            ProfileUpdates::where('user_id', $user_id)->update(array('account' => 100));
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
        Session::flash('success', 'Successfully Submitted');
        return redirect()->back();
    }

    public function edit($id) {
        $data = array();
        //  $user_id = Auth::user()->id;
        $model = UsersCards::where('id', $id)->first();
        $data['model'] = $model;
        return view('front.payments.edit', $data);
    }

    public function update(Request $request, $id) {
        $validation = array(
            'cardNumber' => 'required|max:16',
            'cardholderName' => 'required|max:30',
            'cvc' => 'required|min:1|max:4',
            'expDate' => 'date_format:"Y-m"|required|after:now',
            'billingAddress' => 'required|max:50',
            'postalCode' => 'required',
        );
        $input = $request->all();
        $input['expDate'] = $request->expYear . '-' . $request->expMonth;

        $message = [
            'expDate.after' => 'You have entered an invalid expiration date.',
        ];

        $validator = Validator::make($input, $validation, $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        array_forget($input, '_token');
        UsersCards::where('id', $id)->update([
        'cardNumber' => $input['cardNumber'],
        'cardholderName' => $input['cardholderName'],
        'cvc' => $input['cvc'],
        'expDate' => $input['expDate'],
        'billingAddress' => $input['billingAddress'],
        'postalCode' => $input['postalCode'],

        ]);
        Session::flash('success', 'Successfully Updated');
    return redirect('payments?tabName=storeCards');
    }

    public function delete($id, Request $request) {
        UsersCards::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        StripeCustomers::where('card_id', $id)->update([
            'deleted' => 1, 
        ]);
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function storeDetails(Request $request) {
        $user_id = Auth::user()->id;
        $validation = array(
            'accountTitle' => 'required|min:3|max:20',
            'bic' => 'required|min:3',
            'iban' => 'required|min:3',
            'address' => 'required|min:3|max:50',
            'bankName' => 'required|min:3|max:30',
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
            return json_encode($response);
        }
        $input = $request->all();
        if (BankDetails::where('user_id', $user_id)->exists()) {
            array_forget($input, '_token');
            BankDetails::where('user_id', $user_id)->update($input);
        } else {
            BankDetails::create([
                'user_id' => $user_id,
                'accountTitle' => $request->accountTitle,
                'bic' => $request->bic,
                'iban' => $request->iban,
                'address' => $request->address,
                'bankName' => $request->bankName
            ]);
        }
        ProfileUpdates::where('user_id', $user_id)->update(array('payment' => 100));
        return json_encode($response);
    }

}
