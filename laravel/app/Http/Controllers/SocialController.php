<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use App\SocialAccounts;
use Illuminate\Http\Request;
use Mbarwick83\TwitterApi\TwitterApi;
use Session;

class SocialController extends Controller {

    public function __construct(TwitterApi $twitter) {
        $this->twitter = $twitter;
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;
        $data['model'] = SocialAccounts::where('user_id', $user_id)->where('status', 1)->where('deleted', 0)->orderBy('id', 'desc')->get();

        $accounts = array();

        foreach ($data['model'] as $account) {
            $accounts[$account->type] = $account->profileUrl;
        }
        $data['accounts'] = $accounts;
        return view('front.social_accounts.index', $data);
    }

    public function twitteroauth(Request $request) {

        $token = $request->oauth_token;
        $verify = $request->oauth_verifier;
        $user_id = Auth::user()->id;
        if (isset($request->oauth_token) && Session::get('oauth_token') !== $request->oauth_token) {
            Session::forget('oauth_token');
            Session::forget('oauth_token_secret');
            abort(404);
        }

        $twitter = new TwitterApi(Session::get('oauth_token'), Session::get('oauth_token_secret'));
        $accessToken = $twitter->accessToken($request->oauth_verifier);
        SocialAccounts::updateSocialAccount('twitter', 'http://www.twitter.com/' . $accessToken['screen_name'], $user_id, $accessToken['user_id']);
        return redirect('accounts');
    }

    public function twitter() {
        $user_id = Auth::user()->id;
        $url = $this->twitter->authorizeUrl();
        return redirect($url);
    }

    public function verify(Request $request) {
//        d($request->type,1);
        $user_id = Auth::user()->id;
        $result = SocialAccounts::updateSocialAccount($request->type, $request->link, $user_id, $request->accountId);
        return json_encode($result);
    }

}
