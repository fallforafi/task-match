<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Input;
use Illuminate\Http\Request;
use App\Comments;
use App\User;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;
use App\Tasks;
use App\Functions\UsersNotifications;

class CommentsController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        // return view('front.contact-us');
    }

    public function store(Request $request) {
//        d($_FILES,1);
//                
        $rules = array(
            'file' => 'mimes:jpeg,png,jpg,pdf,docx|max:2048',
            'comment' => 'required|min:5|max:500'
        );
        $message = [
            'file.max' => 'File size must be less or equal to 2mb.'
        ];
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules, $message);
        $fileName = "";
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $notification = array();
            $input = $request->all();
            unset($input['_token']);

            $input['created_at'] = date("Y-m-d H:i:s");
            if (Input::hasFile('file')) {
                $file = Input::file('file');
                $destinationPath = public_path() . '/uploads/comments/';
                $fileName = Functions::saveImage($file, $destinationPath);
                pathinfo(public_path() . '/uploads/comments/' . $fileName, PATHINFO_EXTENSION);
                $input['file'] = $fileName;
            }
            $id = Comments::insertGetId($input);
            $response['id'] = $id;
            $task = Tasks::find($input['task_id']);
            $user = User::find($task->user_id);
            $notification['task'] = $task;
            $notification['notify_to'] = $task->user_id;
            $notification['notify_name'] = $user->firstName . ' ' . $user->lastName;
            $notification['notify_email'] = $user->email;
            $notification['type'] = 'task_comment';
            $notification['type_id'] = $id;
            UsersNotifications::save($notification);
        }
        return json_encode($response);
    }

//    public function getcomments(Request $request) {
//        $search['task_id'] = $request->task_id;
//        $model = Comments::get($search);
//        $data['model'] = $model;
//        $data['task_id'] = $request->task_id;
//        $task = Tasks::find($data['task_id']);
//            $notification=array();
//            $input = $request->all();
//            unset($input['_token']);
//            
//            $input['created_at'] = date("Y-m-d H:i:s");
//            $input['user_id'] = Auth::user()->id;
//            $id = Comments::insertGetId($input);
//            $response['id'] = $id;
//            $task=Tasks::find($input['task_id']);
//            $user=User::find($task->user_id);
//            $notification['task']=$task;
//            $notification['notify_to']=$task->user_id;
//            $notification['notify_name']=$user->firstName.' '.$user->lastName;
//            $notification['notify_email']=$user->email;
//            $notification['type']='task_comment';
//            $notification['type_id']=$id;
//            UsersNotifications::save($notification);
//        return json_encode($response);
//    }

    public function getcomments(Request $request, $task_id) {
//d($_POST,1);
        $search['task_id'] = $task_id;
        $model = Comments::get($search);
        $data['model'] = $model;
        $data['task_id'] = $request->task_id;
        $task = Tasks::find($data['task_id']);
        $data['taskStatus'] = $task->taskStatus;
        
        return view('front.comments.ajax.list', $data);
    }

}
