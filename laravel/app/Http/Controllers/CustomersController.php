<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use Auth;
use Hash;
use App\Categories;
use App\ProfileUpdates;
//use App\BankDetails;
use App\StripeAccounts;
use App\UsersCards;
use App\Languages;
use App\UsersLanguages;
use App\Skills;
use App\UsersSkills;
use App\UsersTransports;
use App\Educations;
use App\UsersEducations;
use File;
use Session;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

class CustomersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;
        $data['user'] = User::findOrFail($user_id);
        //ProfileUpdates::updateProfile($data['user']);
        $data['profile'] = ProfileUpdates::where('user_id', $user_id)->first();
        //d($data['profile']);

        $userCards = UsersCards::where('user_id', $user_id)->where('deleted', 0)->first();
        if (count($userCards) != 0) {
            $data['payment'] = 100;
        } else {
            $data['payment'] = 0;
        }
        $stripeAccounts = StripeAccounts::where('user_id', $user_id)->where('deleted', 0)->first();
        if (count($stripeAccounts) != 0) {
            $data['account'] = 100;
        } else {
            $data['account'] = 0;
        }
        //d($data,1);

        $data['categories'] = Categories::where('status', 1)->where('parent_id', '>', '0')->get();
        return view('front.customers.dashboard', $data);
    }

    public function changepassword() {
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        return view('front.customers.change_password')->with('user_id', $user_id);
    }

    public function changeprofileimage(Request $request) {
        $response['success'] = 0; 
        $input = $request->all();
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        array_forget($input, '_token');
        $data = $input['image'];

        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);

        $data = base64_decode($data);
        $imageName = $user->id . '.png';
        $destinationPath = public_path() . '/uploads/users/profile/';
        $file1 = $destinationPath . $user->image;
        File::delete($file1);
        file_put_contents($destinationPath . $imageName, $data);
        $input['image'] = $imageName;
        User::where('id', '=', $user_id)->update($input);
        ProfileUpdates::updateProfile($user_id);
        $response['success'] = 1; 
        return json_encode($response);
    }

    public function postchangepassword(Request $request) {
        $user_id = Auth::user()->id;
        $rules = array(
            'old_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
                // 'password_confirmation' => 'required_with:password|min:6',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $user = User::findOrFail($user_id);
        if (Hash::check($request->old_password, $user->password)) {
            $data = $request->all();
            array_forget($data, 'password_confirmation');
            array_forget($data, 'old_password');
            array_forget($data, '_token');
            $data['password'] = bcrypt($request->password);
            $user->update($data);
            Session::flash('success', 'Your password has been changed.');
            return redirect()->back();
        } else {
            Session::flash('danger', 'Your current password is incorrect.');
            return redirect()->back();
        }
    }

    public function profile() {
        $user_id = Auth::user()->id;
        $data['user'] = User::findOrFail($user_id);
        $data['education'] = NULL;
        $data['language'] = NULL;
        $data['language'] = NULL;
        $data['skill'] = NULL;
        $data['transport'] = NULL;
        if (isset($data['user']->dob)) {
            list($year, $month, $date) = explode('-', $data['user']->dob);
            $data['user']->date = $date;
            $data['user']->month = $month;
            $data['user']->year = $year;
        }
        $getEdu = Educations::getEducations($user_id);
        foreach ($getEdu as $value) {
            $paramsEdu[] = $value->name;
            $data['education'] = implode(',', $paramsEdu);
        }

        $getLang = Languages::getLanguages($user_id);
        foreach ($getLang as $value) {
            $params[] = $value->name;
            $data['language'] = implode(',', $params);
        }

        $getSkill = Skills::getSkills($user_id);
        foreach ($getSkill as $value) {
            $param[] = $value->name;
            $data['skill'] = implode(',', $param);
        }
        $getTransport = UsersTransports::where('user_id', '=', $user_id)->get();
        foreach ($getTransport as $value) {
            $paramss[] = $value->transport_id;
            $data['transport'] = $paramss;
        }
        return view('front.customers.profile', $data)->with('user_id', $user_id);
    }

    public function picture() {

        $user_id = Auth::user()->id;
        $data['user'] = User::findOrFail($user_id);

        return view('front.customers.change_profile_picture', $data)->with('user_id', $user_id);
    }

//    public function changeprofileimage(Request $request) {
//        $rules['image'] = 'required|mimes:jpeg,png,jpg|max:3584';
//        $message = [
//            'max' => 'The Profile Image size not more than 3mb.',
//        ];
//        $validator = Validator::make($request->all(), $rules, $message);
//        $fileName = "";
//        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator->errors())->withInput();
//        }
//        $user_id = Auth::user()->id;
//        $user = User::findOrFail($user_id);
//        if (Input::hasFile('image')) {
//            $file = Input::file('image');
//            $destinationPath = public_path() . '/uploads/users/';
//            $destinationPathThumb = $destinationPath . 'thumbnail/';
//            $destinationPathProfile = $destinationPath . 'profile/';
//            $file1 = $destinationPath . $user->image;
//            $file2 = $destinationPathThumb . $user->image;
//            $file3 = $destinationPathProfile . $user->image;
//            File::delete($file1, $file2, $file3);
//            $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
//            $upload = Image::make($destinationPath . $fileName)->fit(30)->save($destinationPathThumb . $fileName);
//            $upload2 = Image::make($destinationPath . $fileName)->fit(300)->save($destinationPathProfile . $fileName);
//            $ext = pathinfo(public_path() . '/uploads/users/' . $fileName, PATHINFO_EXTENSION);
//            $input['image'] = $fileName;
//        }
//        $affectedRows = User::where('id', '=', $user_id)->update($input);
//        Session::flash('success', 'Profile Picture Changed Successfully.');
//        return redirect()->back();
//    }

    public function changeCoverImage(Request $request) {
        $rules['coverImage'] = 'required|mimes:jpeg,png,jpg|max:3584';
        $message = [
            'max' => 'The Cover Image size not more than 3mb.',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        $fileName = "";
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $file = Input::file('coverImage');

        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        if (Input::hasFile('coverImage')) {
            $file = Input::file('coverImage');
            $destinationPath = public_path() . '/uploads/users/cover/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $file1 = $destinationPath . $user->coverImage;
            $file2 = $destinationPathThumb . $user->coverImage;
            File::delete($file1, $file2);
            $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
            //$upload = Image::make($destinationPath . $fileName)->resize(1000)->save($destinationPath . $fileName);
            $upload2 = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
            $ext = pathinfo(public_path() . '/uploads/users/' . $fileName, PATHINFO_EXTENSION);
            $input['coverImage'] = $fileName;
        }
        $input['coverPosition'] = '0px';
        $affectedRows = User::where('id', '=', $user_id)->update($input);
        Session::flash('success', 'Cover Photo Changed Successfully.');
        return redirect()->back();
    }

    public function updateprofile(Request $request) {
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        $rules = array(
            'firstName' => 'required|max:50',
            'lastName' => 'required|max:50',
            'email' => 'required|min:6|email|unique:users,email,' . $user->id,
            'tagline' => 'min:8|max:30',
            'about' => 'min:20|max:500',
        );



        $validator = Validator::make($request->all(), $rules);
        // $checkEmail=User::where('email','!=',$user->email)->where('email',$request->email)->count();
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $input = $request->all();

            array_forget($input, '_token');
            array_forget($input, 'education');
            array_forget($input, 'language');
            array_forget($input, 'skill');
            array_forget($input, 'transport');
            array_forget($input, 'year');
            array_forget($input, 'date');
            array_forget($input, 'month');

            if (!empty($request->education)) {
                $education = $request->education;

                Educations::setEducations($education);
            } else {
                UsersEducations::where('user_id', '=', $user_id)->delete();
            }

            if (!empty($request->language)) {
                $language = $request->language;
                //d($language,1);
                Languages::setLanguages($language);
            } else {
                UsersLanguages::where('user_id', '=', $user_id)->delete();
            }
            if (!empty($request->skill)) {
                $skill = $request->skill;
                Skills::setSkills($skill);
            } else {
                UsersSkills::where('user_id', '=', $user_id)->delete();
            }
            // Improved the code, thanks to @boynet for the idea!
            if (!empty($request['transport'])) {
                $transportString = $request->get('transport');
                UsersTransports::where('user_id', '=', $user_id)->delete();
                foreach ($transportString as $value) {
                    $uTransportModel = new UsersTransports();
                    $uTransportModel->user_id = $user_id;
                    $uTransportModel->transport_id = $value;
                    $uTransportModel->save();
                }
            } else {
                UsersTransports::where('user_id', '=', $user_id)->delete();
            }
            $input['dob'] = $request->year . "-" . $request->month . "-" . $request->date;
            ProfileUpdates::updateProfile($user_id);
            $affectedRows = User::where('id', '=', $user_id)->update($input);
            echo $affectedRows;
            Session::flash('success', 'Your profile has been updated.');
            return redirect()->back();
        }
    }

    public function accounts() {
        return view('front.customers.accounts');
    }

    public function myReviews() {
        return view('front.customers.my-reviews');
    }

    public function notifications() {
        return view('front.customers.notifications');
    }

}
