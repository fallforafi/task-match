<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Reviews;
use App\Tasks;
use App\User;
use App\Functions\UsersNotifications;
use App\Offers;
use Session;

class CronController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $offer = Offers::where([
                    'offerStatus' => 'completed',
                    'deleted' => 0,
                    'status' => 1,
//                    'updated_at' => $dateTime,
                ])->get();
        if (count($offer) > 0) {
            foreach ($offer as $value) {
                if (!Reviews::where('task_id', $value->task_id)->exists()) {
                    $task = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                            ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                            ->select('tasks.title', 'op.key', 'tasks.user_id')
                            ->where('of.id', $value->id)
                            ->where('tasks.id', $value->task_id)
                            //->wherefind('updated_at', '=', $dateTime)
                            ->first();
                    if (User::where('id',$task->user_id)->exists() && User::where('id',$value->user_id)->exists()) {
                        $user = User::find($task->user_id);
                        $taskRunner = User::findorfail($value->user_id);

                        $notification['task'] = $task;
                        $notification['notify_to'] = $task->user_id;
                        $notification['notify_email'] = $user->email;
                        $notification['notify_name'] = $user->firstName . ' ' . $user->lastName;
                        $notification['taskRunner'] = $taskRunner->firstName . ' ' . $taskRunner->lastName;
                        $notification['image'] = $user->image;
                        $notification['type'] = 'reviews_awaiting';
                        $notification['type_id'] = $value->task_id;
                        UsersNotifications::save($notification);
                        continue;
                    } else {
                        continue;
                    }
                }
            }
            Session::flash('success', 'Successfully Sent!');
            return redirect('admin/tasks');
        } else {
            return redirect('admin/tasks');
        }
    }

}
