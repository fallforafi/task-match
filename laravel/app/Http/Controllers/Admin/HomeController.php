<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Contactus;
use App\Tasks;

class HomeController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array();

        $data['totalUsers'] = User::where('role_id', '!=', 1)->get();
        
        $data['recentUsers'] = User::where('role_id', '!=', 1)->orderBy('id','desc')->limit(10)->get();

        $data['totalContacts'] = Contactus::get();

        $data['totalTasks'] = Tasks::where('deleted',0)->where('status',1)->get();
        $data['recentTasks'] = Tasks::where('deleted',0)->where('status',1)->orderBy('id','desc')->limit(10)->get();

       // $data['totalTransactions'] = Transactions::where('status', 1)->where('deleted', 0)->get();

        return view('admin.home', $data);
    }

}
