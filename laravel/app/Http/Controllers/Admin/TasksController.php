<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Redirect;
use App\Tasks;
use Auth;
use App\Functions\Functions;
use App\Offers;
use Session;
use App\Comments;
use App\Transactions;
use App\Functions\UsersNotifications;
use Illuminate\Http\Request;

class TasksController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        if (isset($_GET['filter_search'])) {
            $data['filter_search'] = $_GET['filter_search'];
        }
        if (isset($_GET['task_status'])) {
            $data['task_status'] = $_GET['task_status'];
        }
        $data['page'] = $page;
        return view('admin.tasks.index', $data);
    }

    public function listing(Request $request) {
        // d($request->all(),1);
        $data['search'] = $request->all();
        $data['model'] = Tasks::searchTasks($data['search']);
        return view('admin.tasks.ajax.list', $data);
    }

    public function delete($id) {
        Tasks::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function taskDetails($id) {

        $data['model'] = Tasks::leftJoin('countries', 'countries.id', '=', 'tasks.country')
                        ->leftJoin('states', 'states.code', '=', 'tasks.state')
                        ->select('tasks.*', 'countries.name', 'states.title as state')
                        ->where('tasks.id', '=', $id)->first();
        $search['task_id'] = $id;
        $data['check'] = Transactions::checkPay($search['task_id']);
        // d($data['check'],1);
        $offers = Offers::get($search);
        $data['offers'] = $offers;
        $data['task_id'] = $id;
        $comments = Comments::get($search);
        $data['comments'] = $comments;
        //d($data['offers'],1);
        return view('admin.tasks.details', $data)->with('task_id', $id);
    }

    public function savePayment(Request $request) {
        $input = $request->all();
        unset($input['_token']);
        unset($input['task_id']);
        $id = Transactions::insertGetId($input);
        $model = Tasks::Join('users', 'users.id', '=', 'tasks.user_id')
                        ->select('tasks.*', 'users.*')
                        ->where('tasks.id', '=', $request->task_id)->first();

        $notification['task'] = $model;
        $notification['notify_to'] = $input['user_id'];
        $notification['type'] = 'money_receive';
        $notification['type_id'] = $id;
        $notification['name'] = $model->firstName . ' ' . $model->lastName;
        $notification['image'] = $model->image;
        $notification['user_id'] = $model->user_id;
        //d($notification,1);
        UsersNotifications::save($notification);
        Session::flash('success', 'Successfully Submitted!');
        return redirect()->back();
    }

    public function edit($id) {
        $task_id = $id;
        $data = array();
        $data['model'] = Tasks::where(['id' => $id,
                    'deleted' => 0])->first();
        return view('admin.tasks.edit', $data)->with('task_id', $task_id);
    }

    public function update(Request $request) {
        $id = $request->id;
        $rules = array(
            'title' => 'required|min:10|max:50',
            'description' => 'required|min:25|max:100',
            //'categories' => 'required',
            // 'location' => 'required',
            'toBeCompleted' => 'required',
            'price' => 'required',
            'dueDate' => 'required|date|after:today'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $input = $request->all();
            $input['dueDate'] = date('Y-m-d', strtotime($request->dueDate));
            if (isset(Auth::user()->id)) {
                $input['user_id'] = Auth::user()->id;
            } else {
                session_start();
                $input['sessionId'] = session_id();
            }
            unset($input['password']);
            unset($input['email']);
            unset($input['_token']);
            unset($input['categories']);
            unset($input['publish']);
            if (isset($input['priceType']) && $input['priceType'] == 'hourly') {
                $total = $input['hourlyRate'] * $input['hours'];
                $input['price'] = $total;
            }
            if (isset($input['priceType']) && $input['priceType'] == 'total') {
                $input['hourlyRate'] = 0;
                $input['hours'] = 0;
            }
            $input['key'] = str_shuffle(Functions::generateRandomString(6) . time());
            $input['updated_at'] = date("Y-m-d H:i:s");

            Tasks::where('id', '=', $id)->update($input);
        }
        Session::flash('success', 'Successfully Updated!');
        return redirect()->back();
    }

    public function deleteComment($id) {
        Comments::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function deleteOffer($id) {
        Offers::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
