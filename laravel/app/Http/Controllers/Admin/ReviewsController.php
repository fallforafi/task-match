<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Reviews;
use Session;

class ReviewsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['model'] = Reviews::join('tasks as t','t.id','=','reviews.task_id')
                ->join('users as u','u.id','=','t.user_id')
                ->select('t.title','u.firstName','u.lastName','reviews.*')
                ->where('t.taskStatus','completed')
                ->where('report', 1)
                ->orderby('reviews.id','desc')
                ->paginate(10);
        return view('admin.reviews.list', $data);
    }

    public function delete($id) {
        Reviews::where('id', '=', $id)->update([
            'status' => 0,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
