<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Friends;
use Illuminate\Http\Request;

class FriendsController extends Controller {

    public function __construct() {
        // $this->middleware('auth');
    }

    public function index() {
        
    }

    public function getFriends(Request $request) {

        $user_id = $request->user_id;
        $friends = $request->friends;

        foreach ($friends as $friend) {
            $friendExists = Friends::where([
                'user_id' => $user_id, 'friendId' => $friend['id'],'name' => $friend['name']
            ])->first();
            if(count($friendExists) == 0) {
                Friends::create(
                    ['user_id' => $user_id, 'friendId' => $friend['id'],'name' => $friend['name']] );
            } else {
                continue;    
            }
//            Friends::firstOrCreate(
//                    ['user_id' => $user_id, 'friendId' => $friend['id'],'name' => $friend['name']] );
        }
    }
}
