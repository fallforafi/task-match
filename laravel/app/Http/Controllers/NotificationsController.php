<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use App\Notifications;
use App\NotificationsSettings;
use Illuminate\Http\Request;

class NotificationsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;

        Notifications::where('notify_to', $user_id)->update(array('seen' => 1));
        $data['model'] = Notifications::where('notify_to', $user_id)->where('status', 1)->where('deleted', 0)->orderBy('id', 'desc')->limit(100)->get();
        return view('front.notifications.index', $data);
    }

    public function settings() {
        $user_id = Auth::user()->id;
        $data['model'] = NotificationsSettings::where('user_id', $user_id)->where('status', 1)->where('deleted', 0)->orderBy('id', 'desc')->get();
        $userSettings = array();
        foreach ($data['model'] as $setting) {
            $userSettings[$setting->type]['notification'] = $setting->notification;
            $userSettings[$setting->type]['email'] = $setting->email;
        }

        $data['userSettings'] = $userSettings;
        return view('front.notifications.settings', $data);
    }

    public function update(Request $request) {
        $user_id = Auth::user()->id;


        $settings = $request->all();
        unset($settings['_token']);

        NotificationsSettings::where('user_id', $user_id)->delete();
        foreach ($settings as $key => $setting) {
            $model = new NotificationsSettings();
            $model->user_id = $user_id;
            $model->type = $key;
            if (isset($setting['notification'])) {
                $model->notification = 1;
            }

            if (isset($setting['email'])) {
                $model->email = 1;
            }
            $model->save();
        }
        return redirect('notifications/settings');
    }

    function unseen() {
        $user_id = Auth::user()->id;
        return Notifications::where('notify_to', $user_id)->where('seen', 0)->count();
    }

}
