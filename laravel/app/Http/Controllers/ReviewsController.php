<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Reviews;
use App\Tasks;
use App\User;
use Session;
use App\Functions\UsersNotifications;

class ReviewsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $search['user_id'] = Auth::user()->id;
        $data = Reviews::get($search);
        $data['model'] = Reviews::getReviews($search);
        return view('front.reviews.index', $data);
    }

    public function feedback($task_id) {

        $data['task'] = Tasks::find($task_id);
        if (Reviews::where('task_id', $task_id)->where('rate_from', Auth::user()->id)->orderBy('id', 'desc')->exists()) {
            Session::flash('danger', 'You have already reviewed this task!');
            return redirect()->back();
        } else {
            return view('front.reviews.feedback', $data);
        }
    }

    public function getTaskReview($task_id) {

        $data['task'] = Tasks::find($task_id);
        $data['user'] = User::leftJoin('offers as of','of.user_id','=','users.id')
                ->select('users.*')
                ->where('of.task_id',$task_id)
                ->first();
        $review = Reviews::where('task_id', $task_id)->where('rate_to', Auth::user()->id)->orderBy('id', 'desc')->first();
        $data['review'] = $review;
        return view('front.reviews.ajax.review', $data);
    }

    public function listing(Request $request) {
        $search = $request->all();
        $data['model'] = Reviews::getReviewsListing($search);
        //d($request->all(),1);
        return view('front.profile.reviews.list', $data);
    }

    public function store(Request $request) {
        $rules = array(
            'comment' => 'required|min:10|max:200',
            'rating' => 'required|numeric',
        );
        $response['error'] = 0;
        $response['login'] = 0;

        $task = Tasks::leftJoin('offers as o', 'o.task_id', '=', 'tasks.id')
                ->select('o.user_id as rate_to')
                ->where('tasks.id', $request->task_id)
                ->where('o.offerStatus', 'completed')
                ->where('tasks.user_id', Auth::user()->id)
                ->first();
        // d($task,1);  
        $taskRunner = Tasks::leftJoin('offers as o', 'o.task_id', '=', 'tasks.id')
                ->select('tasks.user_id as rate_to')
                ->where('tasks.id', $request->task_id)
                ->where('o.offerStatus', 'completed')
                ->first();
        //d($task1,1);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $notification = array();
            $input = $request->all();
            unset($input['_token']);
            unset($input['user_id']);
            // d($task->rate_to,1);
            if (isset($task->rate_to)) {
                $input['rate_to'] = $task->rate_to;
            } else {
                $input['rate_to'] = $taskRunner->rate_to;
                $input['rate_from'] = Auth::user()->id;
            }

            $input['created_at'] = date("Y-m-d H:i:s");

            $id = Reviews::insertGetId($input);
            $response['id'] = $id;
            $notification['task'] = Tasks::find($request->task_id);
            $notification['notify_to'] = $input['rate_to'];
            $user = User::find($notification['notify_to']);
            $notification['notify_email'] = $user->email;
            $notification['type'] = 'task_review';
            $notification['type_id'] = $id;
            UsersNotifications::save($notification);
        }
        return json_encode($response);
    }

    public function report(Request $request) {
        $response['error'] = 0;
        $input = $request->all();
        unset($input['_token']);
        $id = $input['id'];
        $affectedRow = Reviews::where('id', $id)->update($input);
        if ($affectedRow != 1) {
            $response['error'] = 1;
        }
        return json_encode($response);
    }

}
