<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Input;
use Auth;
use App\User;
use App\Categories;
use App\TaskCategories;
use App\Tasks;
use DB;
use App\Offers;
use App\TaskFiles;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\Functions\RecentActivities;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class TasksController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $data = array();
        $model = Tasks::where('status', 1)->where('deleted', 0)->where('draft', 0)->orderBy('id', 'desc')->first();
        if(count($model)>0) {
        $data['model'] = $model;
        $data['key'] = $model->key;    
        } else{
           $data['model'] = NULL;
        $data['key'] = NULL;     
        }
        
        if (isset($_GET['cat_id'])) {
            $data['cat_id'] = $_GET['cat_id'];
        }
        $data['categories'] = Categories::where('status', 1)->where('parent_id', '>', '0')->get();
        return view('front.tasks.task', $data);
    }

    public function listing(Request $request) {
        $search = $request->all();
        $taskData = Tasks::search($search);
        $perPage = 10;
        $page = Input::get('page', 1);
        if ($page > count($taskData) or $page < 1) {
            $page = 1;
        }
        $offset = ($page * $perPage) - $perPage;
        $tasks = array_slice($taskData, $offset, $perPage);
        $model = new Paginator($tasks, count($taskData), $perPage);
        $data['model'] = $model->setPath('tasks');
        if ($request->ajax()) {
            return view('front.tasks.ajax.list', $data);
        }
        return view('front.tasks.ajax.list', $data);
    }

    public function task($key) {
        $data = array();
        $data['key'] = $key;
        $data['model'] = Tasks::where('key', $key)->first();
        $data['categories'] = Categories::where('status', 1)->where('parent_id', '>', '0')->get();

        return view('front.tasks.task', $data);
    }

    public function view($key) {

        $data = array();
        $model = Tasks::where('key', $key)->first();
        $data['taskFiles'] = TaskFiles::where('task_id', $model->id)->get();

        $offer = array();

        if (isset(Auth::user()->id)) {
            $offer = Offers::where('user_id', Auth::user()->id)->where('task_id', $model->id)->first();
        }

        $data['offer'] = $offer;
        $data['task'] = $model;
        $user = User::find($model->user_id);
        $data['user'] = $user;
        $categories = Categories::byTaskId($model->id);
        $data['categories'] = $categories;
        return view('front.tasks.ajax.view', $data);
    }

    public function create() {
        $data = array();
        return view('front.tasks.create', $data);
    }

    public function form() {
        $data = array();
        return view('front.tasks.ajax.form', $data);
    }

    public function getCat($id) {
        //print_r()
        $cat = DB::table('categories')->where('id', $id)->first();
        return json_encode($cat);
    }

    public function store(Request $request) {

        // $input['dueDate'] = date("d-m-Y", strtotime($request->dueDate));
        $rules = array(
            'title' => 'required|min:10|max:50',
            'description' => 'required|min:25',
            'categories' => 'required',
            'location' => 'required',
            'toBeCompleted' => 'required',
            'price' => 'required',
            'hourlyRate' => 'required',
            'hours' => 'required',
            'dueDate' => 'required|after:' . date('d-m-Y', strtotime("-1 day")) . '|before:' . date('d-m-Y', strtotime("+1 month"))
        );
        $message = [
            'price.required' => 'Please enter a budget between €5 and €2,000 for your task',
            'hourlyRate.required' => 'Please enter a budget between €5 and €2,000 for your task',
        ];

        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            // $errors = json_decode($errors);
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input = $request->all();
            $input['user_id'] = NULL;
            if (isset(Auth::user()->id)) {
                $input['user_id'] = Auth::user()->id;
                $response['login'] = 1;
            } else {
                session_start();
                $input['sessionId'] = session_id();
                return json_encode($response);
            }

            unset($input['password']);
            unset($input['email']);
            unset($input['chooseDate']);
            unset($input['_token']);
            unset($input['categories']);
            unset($input['cat']);
            if (isset($input['priceType']) && $input['priceType'] == 'hourly') {
                $total = $input['hourlyRate'] * $input['hours'];
                $input['price'] = $total;
            }
            if (isset($input['priceType']) && $input['priceType'] == 'total') {
                $input['hourlyRate'] = 0;
                $input['hours'] = 0;
            }
            $minBudget = 5;
            $maxBudget = 2000;

            if ($input['price'] < $minBudget || $input['price'] > $maxBudget) {
                $validator->errors()->add('price_error', 'Please enter a budget between €5 and €2,000 for your task');
                $errors = $validator->errors();
                $response['error'] = 1;
                $response['errors'] = $errors;
                return json_encode($response);
            }

            $input['key'] = str_shuffle(Functions::generateRandomString(6) . time());
            $input['created_at'] = date("Y-m-d H:i:s");
            $input['dueDate'] = date("Y-m-d", strtotime($request->dueDate));
            $id = Tasks::insertGetId($input);

            if (!empty($request->categories)) {
                foreach ($request->categories as $category_id) {
                    $categoryModel = new TaskCategories;
                    $categoryModel->task_id = $id;
                    $categoryModel->category_id = $category_id;
                    //d($categoryModel->task_id,1);
                    $categoryModel->save();
                }
            }
            $response['id'] = $id;
            $response['key'] = $input['key'];
            if (isset(Auth::user()->id)) {

                $user = User::find(Auth::user()->id);
                $data['title'] = $request->title;
                $data['name'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;
                $data['image'] = $user->image;
                $data['user_id'] = Auth::user()->id;
                $data['key'] = $input['key'];
                RecentActivities::save('task_create', $id, $data);

                $notification['name'] = Auth::user()->firstName;
                $notification['task'] = Tasks::where('id', $id)->first();

                $subject = view('emails.task_posted.subject');
                $body = view('emails.task_posted.body', $notification);
                Functions::sendEmail(Auth::user()->email, $subject, $body);
            }
        }
        return json_encode($response);
    }

    public function draft(Request $request) {
        $rules = array(
            'title' => 'required|min:10|max:50',
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            // $errors = json_decode($errors);
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input = $request->all();
            $input['user_id'] = NULL;
            if (isset(Auth::user()->id)) {
                $input['user_id'] = Auth::user()->id;
                $response['login'] = 1;
            } else {
                session_start();
                $input['sessionId'] = session_id();
                return json_encode($response);
            }

            unset($input['password']);
            unset($input['email']);
            unset($input['chooseDate']);
            unset($input['_token']);
            unset($input['categories']);
            unset($input['cat']);
            $input['key'] = str_shuffle(Functions::generateRandomString(6) . time());
            $input['created_at'] = date("Y-m-d H:i:s");
            $input['draft'] = 1;
            $input['dueDate'] = date("Y-m-d", strtotime($request->dueDate));
            $id = Tasks::insertGetId($input);
            if (!empty($request->categories)) {
                foreach ($request->categories as $category_id) {
                    $categoryModel = new TaskCategories;
                    $categoryModel->task_id = $id;
                    $categoryModel->category_id = $category_id;
                    $categoryModel->save();
                }
            }
            $response['id'] = $id;
            $response['key'] = $input['key'];
        }
        return json_encode($response);
    }

    public function getLoc(Request $request) {

        $search = $request->all();
        $search['filter_search'] = "";
        if (!empty($search)) {
            $model = Tasks::search($search);
        } else {
            $model = Tasks::where('status', 1)->where('deleted', 0)->where('draft', 0)->orderBy('id', 'desc')->get();
        }

        return json_encode($model);
    }

    public function report(Request $request) {
        $rules = array(
            'category' => 'required',
            'reason' => 'required|min:6|max:100',
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            // $errors = json_decode($errors);
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input = $request->all();
            unset($input['_token']);
            $notification = array();
            $task_id = $input['task_id'];
            $input['from'] = Auth::user()->id;
            $notification['user'] = User::find($input['from']);
            $notification['task'] = Tasks::find($task_id);
            $notification['category'] = $input['category'];
            $notification['reason'] = $input['reason'];
            $admins = User::where('role_id', 1)->where('deleted', 0)->where('status', 1)->get();
            foreach ($admins as $row) {
                $subject = view('emails.report_task.subject');
                $body = view('emails.report_task.body', $notification);
                Functions::sendEmail($row->email, $subject, $body);
            }
        }
        return json_encode($response);
    }

}
