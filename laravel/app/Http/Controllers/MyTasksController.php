<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use File;
use Validator,
    Redirect;
use App\TaskFiles;
use Auth;
use App\User;
use App\Functions\UsersNotifications;
use Session;
use App\TaskCategories;
use App\Tasks;
use Illuminate\Http\Request;
use App\Functions\Functions;

class MyTasksController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data = array();
        $user_id = Auth::user()->id;
        $taskPostedOpen = Tasks::where([
                    'user_id' => $user_id,
                    'taskStatus' => 'open',
                    'deleted' => 0,
                    'status' => 1,
                    'draft' => 0
                ])->orderBy('id', 'desc')->get();
        $taskPostedDrafts = Tasks::where([
                    'user_id' => $user_id,
                    'deleted' => 0,
                    'status' => 1,
                    'draft' => 1
                ])->orderBy('id', 'desc')->get();
        $taskPostedCompleted = Tasks::where([
                    'user_id' => $user_id,
                    'taskStatus' => 'completed',
                    'deleted' => 0,
                    'status' => 1,
                    'draft' => 0
                ])->orderBy('id', 'desc')->get();
        $taskPostedAssigned = Tasks::where([
                    'user_id' => $user_id,
                    'taskStatus' => 'assigned',
                    'deleted' => 0,
                    'status' => 1,
                    'draft' => 0
                ])->orderBy('id', 'desc')->get();
        $taskPostedCancelled = Tasks::where([
                    'user_id' => $user_id,
                    'taskStatus' => 'cancelled',
                    'deleted' => 0,
                    'status' => 1,
                    'draft' => 0
                ])->orderBy('id', 'desc')->get();
        $data['taskPostedCancelled'] = $taskPostedCancelled;
        $data['taskPostedAssigned'] = $taskPostedAssigned;
        $data['taskPostedCompleted'] = $taskPostedCompleted;
        $data['taskPostedDrafts'] = $taskPostedDrafts;
        $data['taskPostedOpen'] = $taskPostedOpen;
        $bid_on = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                        ->select('tasks.*')
                        ->where([
                            'of.user_id' => $user_id,
                            'of.offerStatus' => 'pending',
                            'tasks.taskStatus' => 'open',
                            'tasks.deleted' => 0,
                            'tasks.status' => 1,
                        ])->orderBy('id', 'desc')->get();
        $taskAssigned = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                        ->select('tasks.*')
                        ->where([
                            'of.user_id' => $user_id,
                            'of.offerStatus' => 'assigned',
                            'tasks.taskStatus' => 'assigned',
                            'tasks.deleted' => 0,
                            'tasks.status' => 1,
                        ])->orderBy('id', 'desc')->get();
        $taskCompleted = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                        ->select('tasks.*')
                        ->where([
                            'of.user_id' => $user_id,
                            'of.offerStatus' => 'completed',
                            'tasks.taskStatus' => 'completed',
                            'tasks.deleted' => 0,
                            'tasks.status' => 1,
                        ])->orderBy('id', 'desc')->get();

        $data['bid_on'] = $bid_on;
        $data['taskAssigned'] = $taskAssigned;
        $data['taskCompleted'] = $taskCompleted;
        return view('front.my-tasks.index', $data);
    }

    public function add($id) {
        $data['id'] = $id;
        return view('front.my-tasks.add_file', $data);
    }

    public function postSimilar($id) {
        $task = Tasks::where(['id' => $id,
                    'deleted' => 0])->first();
        $taskModel = new Tasks();
        $taskModel->title = $task->title;
        $taskModel->user_id = $task->user_id;
        $taskModel->description = $task->description;
        $taskModel->key = str_shuffle(Functions::generateRandomString(6) . time());
        $taskModel->location = $task->location;
        $taskModel->taskStatus = 'open';
        $taskModel->price = $task->price;
        $taskModel->priceType = $task->priceType;
        $taskModel->hours = $task->hours;
        $taskModel->hourlyRate = $task->hourlyRate;
        $taskModel->dueDate = $task->dueDate;
        $taskModel->created_at = date("Y-m-d H:i:s");
        $taskModel->longitude = $task->longitude;
        $taskModel->latitude = $task->latitude;
        $taskModel->draft = 0;
        $taskModel->save();
        $selectedCategories = TaskCategories::where('task_id', $id)->get();
        foreach ($selectedCategories as $category) {
            $categoryModel = new TaskCategories();
            $categoryModel->task_id = $taskModel->id;
            $categoryModel->category_id = $category->category_id;
            $categoryModel->save();
        }
        Tasks::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Similar Task Added Successfully.!');
        return redirect('my-tasks?tabName=tasksPosted');
    }

    public function save(Request $request) {
        $rules['file'] = 'required|mimes:pdf,docx,jpeg,jpg|max:3584|unique:task_files';
        $file = TaskFiles:: where('deleted', '=', 0)->get();
        foreach ($file as $value) {
            if ($value->file === $request->file->getClientOriginalName()) {
                Session::flash('danger', 'File already exists.');
                return redirect()->back();
            }
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        if (isset($request->file)) {
            $folder = $request->task_id;
            $path = public_path() . '/uploads/task_files/' . $folder . '/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $fileName = $request->file->getClientOriginalName();
            $request->file->move($path, $fileName);
        }
        TaskFiles::create([
            'file' => $fileName,
            'task_id' => $request->task_id
        ]);
        Session::flash('success', 'File Added Successfully.');
        return redirect()->back();
    }

    public function edit($id) {
        $data = array();
        $data['model'] = Tasks::where(['id' => $id,
                    'deleted' => 0])->first();
        $data['files'] = TaskFiles::where(['task_id' => $id,
                    'deleted' => 0])->get();
        $selectedCategories = TaskCategories::where('task_id', $id)->get();
        $data['tasksCategories'] = array();
        foreach ($selectedCategories as $tc) {
            $params[] = $tc->category_id;
            $data['tasksCategories'] = $params;
        }
        return view('front.my-tasks.create', $data);
    }

    public function update(Request $request, $id) {
        $rules = array(
            'title' => 'required|min:10|max:50',
            'description' => 'required|min:25',
            'categories' => 'required',
            'location' => 'required',
            'toBeCompleted' => 'required',
            'price' => 'required',
            'hourlyRate' => 'required',
            'hours' => 'required',
            'dueDate' => 'required|after:' . date('d-m-Y', strtotime("-1 day")) . '|before:' . date('d-m-Y', strtotime("+1 month"))
        );
        $message = [
            'price.required' => 'Please enter a budget between €5 and €2,000 for your task
You should not be able to put up a task that is under €5',
            'hourlyRate.required' => 'Please enter a budget between €5 and €2,000 for your task
You should not be able to put up a task that is under €5',
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $input = $request->all();
            $input['dueDate'] = date('Y-m-d', strtotime($request->dueDate));
            if (isset(Auth::user()->id)) {
                $input['user_id'] = Auth::user()->id;
            } else {
                session_start();
                $input['sessionId'] = session_id();
            }
            unset($input['password']);
            unset($input['email']);
            unset($input['_token']);
            unset($input['chooseDate']);
            unset($input['categories']);
            unset($input['publish']);
            if (isset($request->publish)) {
                $input['draft'] = 0;
            }
            if (isset($input['priceType']) && $input['priceType'] == 'hourly') {
                $total = $input['hourlyRate'] * $input['hours'];
                $input['price'] = $total;
            }
            if (isset($input['priceType']) && $input['priceType'] == 'total') {
                $input['hourlyRate'] = 0;
                $input['hours'] = 0;
            }
            $input['key'] = str_shuffle(Functions::generateRandomString(6) . time());
           // $input['updated_at'] = date("Y-m-d H:i:s");

            if (!empty($request->categories)) {
                TaskCategories::where('task_id', '=', $id)->delete();
                foreach ($request->categories as $category_id) {
                    $categoryModel = new TaskCategories();
                    $categoryModel->task_id = $id;
                    $categoryModel->category_id = $category_id;
                    $categoryModel->save();
                }
            }
            Tasks::where('id', '=', $id)->update($input);
        }
        $request->session()->flash('success', 'Successfully Updated!');
        return redirect()->back();
    }

    public function delete($id, Request $request) {
        Tasks::where('id', '=', $id)->delete();
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function cancel($id) {
        $user_id = Auth::user()->id;
        Tasks::where('id', '=', $id)->update([
            'taskStatus' => 'cancelled',
        ]);
        $task = Tasks::join('offers as of', 'of.task_id', '=', 'tasks.id')
                        ->select('tasks.*', 'of.user_id as taskRunnerId')
                        ->where([
                            'tasks.user_id' => $user_id,
                            'of.offerStatus' => 'assigned',
                            'tasks.id' => $id,
                            'of.task_id' => $id,
                            'tasks.deleted' => 0,
                            'tasks.status' => 1,
                        ])->first();
        //d($task,1);
        $notification['task'] = $task;
        $notification['notify_to'] = $task->taskRunnerId;
        $user = User::find($task->taskRunnerId);
        $notification['notify_email'] = $user->email;
        $notification['notify_name'] = $user->firstName;
        $notification['type'] = 'task_cancel';
        $notification['type_id'] = $id;
        UsersNotifications::save($notification);

        //Email sent to TaskPoster email address
        $notification['notify_name'] = Auth::user()->firstName;
        $subject = view('emails.task_cancel.subject');
        $body = view('emails.task_cancel.body', compact('notification'));
        Functions::sendEmail(Auth::user()->email, $subject, $body);
        
        Session::flash('success', 'Successfully Cancelled!');
        return redirect('my-tasks?tabName=tasksPosted');
    }

    public function deleteFile($id, Request $request) {
        TaskFiles::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        $file = TaskFiles::findOrFail($id);
        $path = public_path() . '/uploads/task_files/' . $file->task_id . '/';
        $file1 = $path . $file->file;
        File::delete($file1);
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
