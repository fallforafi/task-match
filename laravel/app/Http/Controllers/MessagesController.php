<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Redirect;
use Auth;
use App\User;
use App\Tasks;
use App\Offers;
use App\OfferPrices;
use App\Messages;
use App\Threads;
use Illuminate\Http\Request;
use App\Functions\UsersNotifications;
use DB;

class MessagesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data = array();
        $user_id = Auth::user()->id;
        $data['user_id'] = $user_id;

        $model = array();
        $data['model'] = $model;
        return view('front.messages.index', $data);
    }

    public function getmessages(Request $request) {
        $data = array();
        $user_id = Auth::user()->id;
        $search['filter_search'] = $request->search;
        $search['thread_id'] = NULL;
        $search['user_id'] = $user_id;

        $model = Threads::get($search);
        //d($model,1);

        $data['model'] = $model;
        return view('front.messages.ajax.list', $data);
    }

    public function message($key) {
        $data = array();
        $threadModel = Threads::where('key', $key)->first();
        $user_id = Auth::user()->id;
        $data['user_id'] = $user_id;
        $data['key'] = $threadModel->key;
        return view('front.messages.message', $data);
    }

    public function view($key) {

        $user_id = Auth::user()->id;
        $threadModel = Threads::where('key', $key)->first();
        $data['threadModel'] = $threadModel;
        if (count($threadModel) > 0) {
            $model = Messages::where('thread_id', $threadModel->id)->get();
            Messages::where('to', $user_id)->where('thread_id', $threadModel->id)->update(array('seenDate' => date('Y-m-d H:i:s')));
            $data['model'] = $model;
            $fromModel = User::find($threadModel->task_owner_id);
            $data['from'] = $fromModel;
            // 

            $data['offer_price'] = OfferPrices::where('id', $threadModel->offer_price_id)->orderBy('id', 'desc')->where('status', 1)->where('deleted', 0)->first();
            $data['offer'] = Offers::join('users as u', 'u.id', '=', 'offers.user_id')->select('u.firstName', 'u.lastName', 'offers.*')->where('offers.id', $data['offer_price']->offer_id)->where('offers.status', 1)->where('offers.deleted', 0)->first();


            $data['task'] = Tasks::find($threadModel->task_id);
        }
        return view('front.messages.ajax.view', $data);
    }

    public function getreplies($thread_id) {
        $data = array();
        $user_id = Auth::user()->id;
        $search['search'] = "";
        $search['thread_id'] = $thread_id;

        $model = Messages::get($search);
        $data['model'] = $model;
        return view('front.messages.ajax.replies', $data);
    }

    public function store(Request $request) {
        $rules = array(
            'message' => 'required|min:2|max:500',
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $input = $request->all();
            unset($input['_token']);
            unset($input['task_id']);


            DB::beginTransaction();
            try {
                $threadModel = Threads::find($input['thread_id']);

                if ($threadModel->task_owner_id == Auth::user()->id) {
                    $to = $threadModel->task_runner_id;
                } else {
                    $to = $threadModel->task_owner_id;
                }

                $input['message'] = $input['message'];
                $input['created_at'] = date("Y-m-d H:i:s");
                $input['from'] = Auth::user()->id;
                $input['thread_id'] = $input['thread_id'];
                $input['to'] = $to;
                $id = Messages::insertGetId($input);
                $Model = Tasks::leftJoin('threads as th', 'th.task_id', '=', 'tasks.id')
                        ->select('tasks.title', 'th.key')
                        ->where('th.id', $input['thread_id'])
                        ->where('tasks.id', $request->task_id)
                        ->first();
                $notification['task'] = $Model;
                $user = User::find($to);
                $notification['notify_to'] = $to;
                $notification['notify_email'] = $user->email;
                $notification['notify_name'] = $user->firstName . ' ' . $user->lastName;
                $notification['type'] = 'message_receive';
                $notification['type_id'] = $id;
                UsersNotifications::save($notification);
                $response['id'] = $id;
                DB::commit();
            } catch (Exception $ex) {
                DB::rollBack();
                $validator->errors()->add('error_db', 'Somthing went wrong try again');
                $errors = $validator->errors();
                $response['error'] = 1;
                $response['errors'] = $errors;
            }
        }

        return json_encode($response);
    }

    function countUnseenMessages() {
        $user_id = Auth::user()->id;

        return $messageUser = Messages::where('to', $user_id)->whereNull('seenDate')->count();
    }

}
