<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Tasks;
use App\ProfileUpdates;
use App\SocialAccounts;
use App\NotificationsSettings;
use Auth;
use DB;
use App\Categories;
use Session;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Functions\RecentActivities;
use Intervention\Image\Facades\Image as Image;

class SignupController extends Controller {

    use AuthenticatesAndRegistersUsers;

    protected $loginPath = 'login';

    //public $mailchimp;

    public function __construct(Guard $auth, Registrar $registrar, \Mailchimp $mailchimp) {
        $this->auth = $auth;
        $this->registrar = $registrar;
        //$this->listId = env('MAILCHIMP_LIST_ID');
        $this->middleware('guest', ['except' => 'success']);
        // $this->mailchimp = $mailchimp;
    }

    public function login() {
        return view('front.customers.login');
    }

    public function register() {
        $pathInfo = (explode("/", $_SERVER['REQUEST_URI']));
        $view = 'front.customers.register_' . end($pathInfo);
        return view($view, compact('specialities'));
    }

    public function fblogin(Request $request) {
        session_start();
        $user = User::where('email', $request->email)->first();
        $response = array();
        if (count($user) == 0) {

            $destinationPath = public_path() . '/uploads/users/';
            // $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathProfile = $destinationPath . 'profile/';
            $name = rand(111, 999) . time() . '.jpg';
            $image = Image::make($request->image)->save($destinationPath . $name);
            $image = Image::make($destinationPath . $name)->fit(250)->save($destinationPathProfile . $name);
            // $image = Image::make($destinationPath . $name)->fit(80)->save($destinationPathThumb . $name);

            $confirmation_code = str_random(30);
            $user = new User;
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->role_id = 2;
            $user->joinFrom = $request->joinFrom;
            $user->isConfirmed = 0;
            $user->image = $name;
            $user->status = 1;
            $user->password = 1;
            $user->key = $confirmation_code;
            $user->save();
            ProfileUpdates::updateProfile($user->id);
            $data = self::saveNotiSetting($user->id);
            NotificationsSettings::insert($data);

            $subject = view('emails.confirm_email.subject');
            $body = view('emails.confirm_email.body', compact('confirmation_code'));
            Functions::sendEmail($request->email, $subject, $body);
        }
        $response['login'] = false;

        if (Auth::loginUsingId($user->id)) {
            $sessionId = session_id();
            $response['key'] = self::checkTask($sessionId);
            $response['login'] = true;
            $response['id'] = $user->id;
        }
        SocialAccounts::updateSocialAccount($request->joinFrom, $request->link, $user->id, $request->accountId);
        echo json_encode($response);
    }

    public function signup() {
        return view('front.customers.signup');
    }

    public function forgot_password() {
        return view('front.customers.forgot');
    }

    public function postLogin(Request $request) {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->email)->first();

        if (count($user) > 0) {
            if ($user->isConfirmed != 1) {
                Session::flash('danger', "You haven't activated your account yet!.");
                return redirect('login')->withInput();
            }
        }

        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            // session_destroy();
            session_start();
            $role = Auth::user()->role->role;
            self::checkTask(session_id());
            ProfileUpdates::updateProfile($user->id);
            if ($role == 'admin') {
                return redirect('admin');
            }
            return redirect()->intended('dashboard');
        }

        return redirect($this->loginPath())
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }

    public function store(Request $request) {
        $validation = array(
            'firstName' => 'required|max:20',
            'lastName' => 'required|max:20',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6',
                //'g-recaptcha-response' => 'required|recaptcha',
        );
//
//        $messages = [
//            "recaptcha" => 'The :attribute field is not correct.',
//        ];
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        DB::beginTransaction();

        try {
            $confirmation_code = str_random(30);

            $user = new User;
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->role_id = $request->role_id;
            $user->password = bcrypt($request->password);
            $user->status = 1;
            $user->key = $confirmation_code;
            $user->save();
//            try {
//                $this->mailchimp->lists->subscribe($this->listId, ['email' => $user->email]);
//            } catch (\Mailchimp_List_AlreadySubscribed $e) {
//                return redirect()->back()->withInput($request->all())->withErrors($e->errors());
//            } catch (\Mailchimp_Error $e) {
//                return redirect()->back()->withInput($request->all())->withErrors($e->errors());
//            }
            //Email sent to verify email address
            $subject = view('emails.confirm_email.subject');
            $body = view('emails.confirm_email.body', compact('confirmation_code'));
            Functions::sendEmail($request->email, $subject, $body);

            //$credentials = $request->only('email', 'password');
            //if (Auth::attempt($credentials, 1)) {
//            $data = self::saveNotiSetting($user->id);
//            NotificationsSettings::insert($data);
//            ProfileUpdates::updateProfile($user->id);
            DB::commit();
            Session::flash('success', 'Thanks for signing up! Please check your email to activate your account.');
            return redirect()->back();
            // }
        } catch (Exception $ex) {
            DB::rollBack();
            $validator->errors()->add('error_db', 'Somthing went wrong try again');
        }
    }

    public function confirmEmail($confirmation_code) {
        if (!$confirmation_code) {
            return 'Error! Confirmation Key missing.';
        }
        $user = User::where('key', $confirmation_code)->first();
        if (!$user) {
            return 'Error! Confirmation Key missing.';
        }
        $user->isConfirmed = 1;
        $user->save();

        $data = self::saveNotiSetting($user->id);
        NotificationsSettings::insert($data);
        ProfileUpdates::updateProfile($user->id);

        //When new user confirmed registeration
        $categories = Categories::where('status', 1)->where('parent_id', '!=', 0)->get();
        $subjectRegister = view('emails.user_register.subject');
        $bodyRegister = view('emails.user_register.body', compact('categories'));
        Functions::sendEmail($user->email, $subjectRegister, $bodyRegister);

        return redirect('register/success/' . $user->id);
    }

    public function success($id) {
        $this->middleware('auth');
        $user = User::findOrFail($id);
        $data['user'] = $user;
        return view('front.customers.register_success', $data);
    }

    public static function checkTask($sessionId) {
        $task = Tasks::where("sessionId", $sessionId)->first();

        if (!empty($task)) {
            $user_id = Auth::user()->id;
            Tasks::where("id", $task->id)->update(array('user_id' => $user_id, 'sessionId' => NULL));

            $user = User::find(Auth::user()->id);
            $data['title'] = $request->title;
            $data['name'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;
            $data['image'] = $user->image;
            $data['user_id'] = Auth::user()->id;
            $data['key'] = $input['key'];
            RecentActivities::save('task_create', $id, $data);
            return $task->key;
        } else {
            return false;
        }
    }

    function saveNotiSetting($user_id) {
        $data = [
            ['user_id' => $user_id, 'type' => 'task_comment', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'task_offer', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'offer_reject', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'task_accept', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'task_cancel', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'task_quote', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'money_receive', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'payment_made', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'message_receive', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'reviews_awaiting', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'tasks_posted', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'task_review', 'notification' => 1, 'email' => 1],
            ['user_id' => $user_id, 'type' => 'offer_ammend', 'notification' => 1, 'email' => 1],
        ];

        return $data;
    }

}
