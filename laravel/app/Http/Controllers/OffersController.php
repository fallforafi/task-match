<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use App\Tasks;
use App\Offers;
use App\OfferPrices;
use Illuminate\Http\Request;
use App\Functions\UsersNotifications;
use App\Functions\Functions;
use Validator;
use App\User;
use App\Reviews;

class OffersController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        
    }

    public function create() {
        $data = array();
        return view('front.tasks.create', $data);
    }

    public function check(Request $request) {

        $user_id = Auth::user()->id;
        $errors = Offers::checkOffer($user_id);
        $task = Tasks::where('key', $request->key)->where('status', 1)->where('deleted', 0)->first();
        $data['task'] = $task;
        $data['errors'] = $errors;
        return view('front.tasks.offer', $data);
    }

    public function store(Request $request) {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;

        $rules = array(
            'description' => 'required|min:5|max:500',
            'offerPrice' => 'required|numeric',
        );
        $message = [
            'description.required' => 'Please give as much detail as possible about you offer'
        ];
        $validator = Validator::make($input, $rules, $message);

        $validator->after(function ($validator) {
            $offers = Offers::where('user_id', Auth::user()->id)->where('task_id', $_POST['task_id'])->get();
            if (count($offers) > 0) {
                $validator->errors()->add('offer_exist', 'You have already made an offer.');
            }
        });
        $response['error'] = 0;
        $response['login'] = 0;
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $task = Tasks::find($input['task_id']);
            $commission = \Config::get('params.commission');
            $minus = ($task->price / 100) * $commission;
            $actualPrice = $task->price - $minus;
            $input['created_at'] = date("Y-m-d H:i:s");

            unset($input['_token']);
            $input['key'] = str_shuffle(Functions::generateRandomString(6) . time());


            $offerModel = new Offers();
            $offerModel->task_id = $task->id;
            $offerModel->user_id = Auth::user()->id;
            $offerModel->message = $input['description'];
            $offerModel->created_at = $input['created_at'];
            $offerModel->save();
            $offer_id = $offerModel->id;

            $offerPricesModel = new OfferPrices();
            $offerPricesModel->offer_id = $offer_id;
            $offerPricesModel->finalPrice = $actualPrice;
            $offerPricesModel->commission = $commission;
            $offerPricesModel->offerPrice = $input['offerPrice'];
            $offerPricesModel->key = $input['key'];
            $offerPricesModel->save();
            $offer_price_id = $offerPricesModel->id;
            $Model = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                    ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                    ->select('tasks.title','tasks.key as taskKey', 'op.key')
                    ->where('of.id', $offer_id)
                    ->where('op.id', $offer_price_id)
                    ->first();
            $notification['task'] = $Model;
            $notification['notify_to'] = $task->user_id;
            $user = User::find($task->user_id);
            $notification['notify_email'] = $user->email;
            $notification['notify_name'] = $user->firstName;;
            $notification['type'] = 'task_offer';
            $notification['type_id'] = $offer_id;
            UsersNotifications::save($notification);

            $response['success'] = true;
        }
        return json_encode($response);
    }

    public function getoffers(Request $request) {
        $search['task_id'] = $request->task_id;
        $data['tasks'] = Tasks::find($search['task_id']);
        $model = Offers::get($search);
        $data['user_reviews'] = NULL;
        foreach ($model as $value) {
            $search['user_id'] = $value->user_id;
            $data['user_reviews'] = Reviews::get($search);
        }
        $data['model'] = $model;
        $data['task_id'] = $request->task_id;


        return view('front.offers.ajax.list', $data);
    }

    public function offersAccepted() {
        $input['user_id'] = Auth::user()->id;
        $input['taskStatus'] = 'assigned';
        $input['offerStatus'] = 'assigned';
        $model = Offers::getOffers($input);
        $data['model'] = $model;
        return view('front.offers.offers_accepted', $data);
    }

    public function offer($id) {
        $input['offer_id'] = $id;
        $model = Offers::getOfferData($input);

        $data['model'] = $model[0];
        return view('front.offers.offer', $data);
    }

    public function amendOffer($id) {
        $input['user_id'] = Auth::user()->id;
        $task = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')->
                        leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                        ->select('tasks.*', 'op.offerPrice', 'op.commission', 'op.finalPrice', 'op.id as op_id', 'of.id as of_id', 'of.message', 'of.offerStatus')
                        ->where([
                            'of.task_id' => $id,
                            'of.user_id' => $input['user_id'],
                            'of.offerStatus' => 'pending',
                            'tasks.taskStatus' => 'open',
                            'tasks.deleted' => 0,
                            'tasks.status' => 1,
                        ])->first();
        $data['task'] = $task;
        return view('front.offers.amend-offer', $data);
    }

    public function amendStore(Request $request) {
        //d($_POST,1);
        $rules = array(
            'description' => 'required|min:20|max:500',
            'offerPrice' => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rules);

        $response['error'] = 0;
        $response['login'] = 0;
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $task = Tasks::find($request->task_id);
            $commission = \Config::get('params.commission');
            $minus = ($task->price / 100) * $commission;
            $actualPrice = $task->price - $minus;
            $input['created_at'] = date("Y-m-d H:i:s");

            unset($input['_token']);
            $offer['task_id'] = $request->task_id;
            $offer['user_id'] = Auth::user()->id;
            $offer['message'] = $request->description;
            Offers::where('id', '=', $request->offer_id)->update($offer);

            $offerPrices['offer_id'] = $request->offer_id;
            $offerPrices['finalPrice'] = $actualPrice;
            $offerPrices['commission'] = $commission;
            $offerPrices['offerPrice'] = $request->offerPrice;
            OfferPrices::where('offer_id', '=', $offerPrices['offer_id'])->update($offerPrices);
            $Model = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                    ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                    ->select('tasks.title', 'op.key')
                    ->where('op.offer_id', $request->offer_id)
                    ->where('of.id', $request->offer_id)
                    ->first();

            $notification['task'] = $Model;
            $notification['notify_to'] = $task->user_id;
            $user = User::find($task->user_id);
            $notification['notify_email'] = $user->email;
            $notification['type'] = 'offer_ammend';
            $notification['type_id'] = $request->offer_id;
            UsersNotifications::save($notification);
            $response['success'] = true;
        }
        return json_encode($response);
    }

}
