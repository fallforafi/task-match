<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Authy\AuthyApi as AuthyApi;
use DB;
use Validator;
use App\ProfileUpdates;
//use Illuminate\Support\MessageBag;
use Twilio\Rest\Client as Client;

class MobileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $data = array();
        $user_id = Auth::user()->id;
        $data['user'] = User::findOrFail($user_id);

        return view('front.mobile.index', $data);
    }

    public function store(Request $request, AuthyApi $authyApi) {
        $rules = array(
            'countryCode' => 'required',
            'phone' => 'required|numeric'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $user_id = Auth::user()->id;
        $values = $request->all();

        DB::beginTransaction();
        unset($values['_token']);
        User::where('id', $user_id)->update([
            'phone' => $values['phone'],
            'countryCode' => $values['countryCode']
        ]);
        $user = User::where('id', $user_id)->first();
        ProfileUpdates::updateProfile($user_id);

        $authyUser = $authyApi->registerUser(
                $user->email, $user->phone, $user->countryCode
        );
        if ($authyUser->ok()) {
            User::where('id', $user_id)->update([
                'authy_id' => $authyUser->id(),
            ]);
            $request->session()->flash(
                    'success', "Sent successfully"
            );
            $sms = $authyApi->requestSms($user->authy_id);
            DB::commit();
            return view('front.mobile.sms', compact('user'));
        } else {
            DB::rollback();
            foreach ($authyUser->errors() as $field => $message) {
                printf("$field = $message");
            }
            //return view('front.mobile.index', ['errors' => new MessageBag($errors)]);
            //return view('newUser', ['errors' => new MessageBag($errors)]);
        }
    }

    public function verify(Request $request, AuthyApi $authyApi, Client $client) {
        $rules = array(
            'token' => 'required|min:7|max:7'
        );
        $response['error'] = 0;
        $response['login'] = 0;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $user_id = Auth::user()->id;
            $values = $request->all();
            unset($values['_token']);
            $token = $values['token'];
            $user = User::where('id', $user_id)->first();
            $verification = $authyApi->verifyToken($user->authy_id, $token);

            if ($verification->ok()) {
                User::where('id', $user_id)->update([
                    'verified' => TRUE,
                ]);
                //$this->sendSmsNotification($client, $user);
            } else {
                $errors = $verification->errors();
                $response['error'] = 1;
                $response['errors'] = $errors;
//                // foreach ($verification->errors() as $field => $message) {
//                return redirect('mobile')->withErrors($verification->errors());
//                //printf("$field = $message");
//                //}
            }
        }
        return json_encode($response);
    }

    public function verifyResend(Request $request, AuthyApi $authyApi) {
        $user_id = Auth::user()->id;
        $user = User::where('id', $user_id)->first();
        $sms = $authyApi->requestSms($user->authy_id);

        if ($sms->ok()) {
            $request->session()->flash(
                    'success', 'Verification code re-sent'
            );
            return view('front.mobile.sms', compact('user'));
        } else {
            foreach ($sms->errors() as $field => $message) {
                printf("$field = $message");
            }
        }
    }

}
