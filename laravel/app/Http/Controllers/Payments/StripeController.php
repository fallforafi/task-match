<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StripeAccounts;
use Auth;
use Session;

class StripeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function redirect(Request $request) {
        $user_id = Auth::user()->id;
        if (isset($_GET['code']) && $_GET['code'] != Null) { // Redirect w/ code
            $code = $_GET['code'];

            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'client_id' => '',
                'code' => $code,
                'client_secret' => env('STRIPE_SECRET_SK')
            );

            $req = curl_init(env('TOKEN_URI'));
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);
            $account = StripeAccounts::where('user_id', $user_id)->first();
            $date = date('Y-m-d H:i:s');
            //d($resp['stripe_user_id'],1);
            if (count($account) == 0) {
                StripeAccounts::insertGetId(array('user_id' => $user_id, 'accountId' => $resp['stripe_user_id'], 'created_at' => $date));
            } else {
                StripeAccounts::where('id', $account->id)->update(array('accountId' => $resp['stripe_user_id'], 'updated_at' => $date));
            }
            return redirect('payments?tabName=receivePay');
//            echo $resp['stripe_user_id'].'<br>';
//            echo $resp['access_token'];
        } else if (isset($_GET['error'])) { // Error
            Session::flash('danger', $_GET['error_description']);
            return redirect('payments?tabName=receivePay');
        }
//        else { // Show OAuth link
//            $authorize_request_body = array(
//                'response_type' => 'code',
//                'scope' => 'read_write',
//                'client_id' => ''
//            );
//
//            $url = env('AUTHORIZE_URI') . '?' . http_build_query($authorize_request_body);
//            echo "<a href='$url'>Connect with Stripe</a>";
//        }
    }

//    public function createAccount() {
//
//        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_SK'));
//
////creating stripe connected account
//        $acct = \Stripe\Account::create(array(
//                    "country" => "IE",
//                    "type" => "custom"
//        ));
////updating stripe connected account
//$account = \Stripe\Account::retrieve("acct_184CQUJ8Yn8Wbtlr");
//$account->first_name = "Keith";
//$account->last_name = "Wallace";
//$account->type = 'individual'
//$account->dob = 14-7-1984
//$account->save();
//    }

}
