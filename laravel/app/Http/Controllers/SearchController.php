<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect;
use App\User;
use App\Ambassadors;
use App\Countries;
use App\States;
use App\Address;
use Auth;
use App\Specialities;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

class SearchController extends Controller {
    
    public function search(Request $request) {
        
        $search = $request['q'];
        
        $data['ambassadors'] = Ambassadors::search($search);
        $data['specialities']= Specialities::get();

        return view('front.search',$data);
    }
    
    



}
