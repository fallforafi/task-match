<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\Contactus;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\User;

class ContactusController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    // use CaptchaTrait;

    public function __construct() {
        
    }

    // Contact-us
    public function index() {
        return view('front.contact-us');
    }

    public function store(Request $request) {
        // d($_POST, 1);
        $validation = array(
            'name' => 'required|max:30',
            'email' => 'required|email|max:30',
            'message' => 'required|min:6|max:200',
//            'g-recaptcha-response'  => 'required',
//            'captcha'               => 'required|min:1'
        );
//		 $messages = [
//		 'g-recaptcha-response.required' => 'Captcha is required',
//         'captcha.min'           => 'Wrong captcha, please try again.'
//      ];
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $input = $request->all();
        unset($input['_token']);

        $contactus = new Contactus;
        $contactus->name = $request->name;
        $contactus->email = $request->email;
        $contactus->message = $request->message;
        $contactus->save();
        $user = User::where('role_id', 1)->get();
        foreach ($user as $value) {
            $subject = view('emails.contactus_email.subject');
            $body = view('emails.contactus_email.body', compact('input'));
            Functions::sendEmail($value->email, $subject, $body);
        }

        $request->session()->flash('success', 'Successfully Submitted, Thanks for contacting us!');
        return redirect('contact-us');
    }

}
