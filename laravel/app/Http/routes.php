<?php

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('how-it-works', 'HomeController@howitworks');
Route::get('about-us', 'HomeController@aboutus');
Route::get('contact-us', 'ContactusController@index');
Route::get('terms-conditions', 'HomeController@terms');
Route::get('privacy-policy', 'HomeController@privacy');
Route::get('earn-with-us', 'HomeController@earnwithus');
Route::get('market-place', 'HomeController@marketplace');
Route::get('posting-a-task', 'HomeController@postingatask');
Route::get('price-guide', 'HomeController@priceguide');
Route::get('faqs', 'HomeController@faqs');
Route::get('find-out-more', 'HomeController@findMore');
Route::get('new-user-faq', 'HomeController@newuserfaq');

Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController', 'dashboard' => 'DashboardController']);

Route::post('contact-send', 'ContactusController@store');
Route::get('search', 'HomeController@search');
Route::get('page/{code}', 'PageController@view');

Route::get('/guestbook', [
    'uses' => 'HomeController@guestbook',
    'as' => 'guestbook.messages',
]);
Route::get('forgot-password', 'SignupController@forgot_password');
Route::post('reset', 'SignupController@reset_password');
Route::post('savemessage', 'HomeController@messagePost');
Route::get('page/{code}', 'PageController@view');
Route::get('register/success/{id}', 'SignupController@success');
Route::get('register/verify/{confirmation_code}', 'SignupController@confirmEmail');

Route::get('picture', 'CustomersController@picture');
Route::post('changeprofileimage', 'CustomersController@changeprofileimage');
Route::post('changecoverimage', 'CustomersController@changeCoverImage');
Route::get('dashboard', 'CustomersController@index');
Route::get('changepassword', 'CustomersController@changepassword');
Route::post('postchangepassword', 'CustomersController@postchangepassword');
Route::get('profile/edit', 'CustomersController@editprofile');
Route::get('profile', 'CustomersController@profile');
Route::post('updateprofile', 'CustomersController@updateprofile');

Route::get('user/{key}', 'ProfileController@index');
Route::get('changecoverposition', 'ProfileController@changecoverposition');
Route::post('quote/store', 'ProfileController@store');
Route::get('report/user/{id}', 'ProfileController@report');

Route::get('login', 'SignupController@login');
Route::get('signup', 'SignupController@signup');
Route::post('register', 'SignupController@store');
Route::post('postLogin', 'SignupController@postLogin');
Route::get('fblogin', 'SignupController@fblogin');

Route::get('tasks', 'TasksController@index');
Route::get('task/{key}', 'TasksController@task');
Route::get('tasks/view/{key}', 'TasksController@view');
Route::get('tasks/form', 'TasksController@form');
Route::get('task/create', 'TasksController@create');
Route::get('tasks/listing', 'TasksController@listing');
Route::post('task/report', 'TasksController@report');

Route::get('get-locations', 'TasksController@getLoc');

Route::post('task/store', 'TasksController@store');
Route::post('task/draft/save', 'TasksController@draft');
Route::post('task/update', 'TasksController@update');

Route::get('mobile', 'MobileController@index');
Route::post('mobile/store', 'MobileController@store');
Route::post('mobile/verify', 'MobileController@verify');
Route::get('verifyResend', 'MobileController@verifyResend');


Route::get('my-tasks', 'MyTasksController@index');
Route::get('my-tasks/add-file/{id}', 'MyTasksController@add');
Route::post('my-tasks/save-file', 'MyTasksController@save');
//Route::get('my-tasks/drafts', 'MyTasksController@index');
Route::get('my-tasks/cancel/{id}', 'MyTasksController@cancel');
Route::get('my-tasks/post-similar-task/{id}', 'MyTasksController@postSimilar');
Route::get('my-tasks/edit/{id}', 'MyTasksController@edit');
Route::post('my-tasks/update/{id}', 'MyTasksController@update');
Route::get('my-tasks/delete/{id}', 'MyTasksController@delete');
Route::get('my-tasks/file/delete/{id}', 'MyTasksController@deleteFile');
Route::get('my-tasks/amend-offer/{id}', 'OffersController@amendOffer');
Route::post('amend-offer/store', 'OffersController@amendStore');


Route::post('comments/store', 'CommentsController@store');
Route::get('comments/getcomments/{task_id}', 'CommentsController@getcomments');

Route::get('payments', 'PaymentsController@index');
Route::post('card/store', 'PaymentsController@store');
Route::get('card/edit/{id}', 'PaymentsController@edit');
Route::post('card/update/{id}', 'PaymentsController@update');
Route::get('card/delete/{id}', 'PaymentsController@delete');
Route::post('receive-payment/store', 'PaymentsController@storeDetails');

Route::get('messages', 'MessagesController@index');
Route::get('message/{key}', 'MessagesController@message');
Route::get('messages/unseen', 'MessagesController@countUnseenMessages');
Route::get('messages/view/{key}', 'MessagesController@view');
Route::get('getmessages', 'MessagesController@getmessages');
Route::post('messages/store', 'MessagesController@store');
Route::get('getreplies/{thread_id}', 'MessagesController@getreplies');


Route::get('/search', 'SearchController@search');
Route::get('blog', 'BlogController@index');
Route::get('blog/{q}', 'BlogController@index');
Route::get('blog/post/{id}', 'BlogController@post');

Route::post('offers/store', 'OffersController@store');
Route::get('offers/getoffers', 'OffersController@getoffers');
Route::get('offers-accepted', 'OffersController@offersAccepted');
Route::get('task/offer/{id}', 'OffersController@offer');
Route::get('makeoffer', 'OffersController@check');

Route::post('payassignee', 'AssignsController@payassignee');
Route::get('offers/{key}', 'AssignsController@offers');
Route::post('assign', 'AssignsController@assign');
Route::post('offer/reject', 'AssignsController@reject');

Route::get('notifications', 'NotificationsController@index');
Route::get('reviews-left', 'CronsController@reviewsLeft');
Route::get('notifications/settings', 'NotificationsController@settings');
Route::post('settings/update', 'NotificationsController@update');
Route::get('notifications/unseen', 'NotificationsController@unseen');

Route::post('reviews/store', 'ReviewsController@store');
Route::get('reviews/listing', 'ReviewsController@listing');
Route::get('feedback/{task_id}', 'ReviewsController@feedback');
Route::get('gettaskreview/{task_id}', 'ReviewsController@gettaskreview');
Route::get('my-reviews', 'ReviewsController@index');
Route::post('report', 'ReviewsController@report');

Route::get('social/verify', 'SocialController@verify');
Route::get('social/twitter', 'SocialController@twitter');
Route::get('social/google', 'SocialController@google');
Route::get('social/oauth/twitter', 'SocialController@twitteroauth');
Route::get('accounts', 'SocialController@index');

Route::get('friends/save', 'FriendsController@getFriends');
Route::get('/cat/get/{id}', 'TasksController@getCat');
Route::group(
        array('prefix' => 'stripe'), function() {
    $folder = "Payments\\";
    Route::get('redirect', $folder . 'StripeController@redirect');
    Route::get('create-account', $folder . 'StripeController@createAccount');
}
);
include("routes_admin.php");
