
<?php

Route::group(
        array('prefix' => 'admin'), function() {
    $admin = "Admin\\";

    ///Route::get('/', $admin . 'UsersController@index');
    Route::get('/', $admin . 'HomeController@index');
    Route::get('/users', $admin . 'UsersController@index');
    Route::get('/user/{id}', $admin . 'UsersController@userDetail');
    Route::get('/user/edit/{id}', $admin . 'UsersController@edit');
    Route::post('/user/update', $admin . 'UsersController@update');
    Route::get('users/listing', $admin . 'UsersController@listing');
    Route::get('/user/delete/{id}', $admin . 'UsersController@delete');
    Route::post('clientOrderStatus', $admin . 'UsersController@ordersBystatus');

//    Route::get('importExport', 'UsersController@importExport');
    Route::get('/download/{type}', $admin . 'UsersController@downloadExcel');
    Route::post('/importExcel', $admin . 'UsersController@importExcel');

    Route::get('cron-job', $admin . 'CronController@index');

    Route::get('/admin-users', $admin . 'AdminUsersController@index');
    Route::get('/admin-user/{id}', $admin . 'AdminUsersController@userDetail');
    Route::get('/admin-users/add', $admin . 'AdminUsersController@create');
    Route::post('/admin-users/store', $admin . 'AdminUsersController@store');

    Route::get('/tasks', $admin . 'TasksController@index');
    Route::get('tasks/listing', $admin . 'TasksController@listing');
    Route::get('/task/{id}', $admin . 'TasksController@taskDetails');
    Route::get('/task/edit/{id}', $admin . 'TasksController@edit');
    Route::post('/task/update', $admin . 'TasksController@update');
    Route::get('/task/delete/{id}', $admin . 'TasksController@delete');
    Route::get('/task/comment/delete/{id}', $admin . 'TasksController@deleteComment');
    Route::get('/task/offer/delete/{id}', $admin . 'TasksController@deleteOffer');
    Route::post('/task/payment/save', $admin . 'TasksController@savePayment');

    Route::get('/user/approve/{id}', $admin . 'UsersController@accept');
    Route::get('/user/disapprove/{id}', $admin . 'UsersController@reject');
    Route::get('user/edit/{id}', $admin . 'UsersController@edit');

    Route::get('categories', $admin . 'CategoriesController@index');
    Route::get('categories/create', $admin . 'CategoriesController@create');
    Route::get('categories/createSubcat', $admin . 'CategoriesController@create_sub_cat');
    Route::post('categories/storeSubcat', $admin . 'CategoriesController@store_sub_cat');
    Route::post('categories/insert', $admin . 'CategoriesController@insert');
    Route::get('categories/delete/{id}', $admin . 'CategoriesController@delete');
    Route::get('categories/edit/{id}', $admin . 'CategoriesController@edit');
    Route::get('categories/show', $admin . 'CategoriesController@show');
    Route::post('categories/update/{id}', $admin . 'CategoriesController@update');

    Route::get('content', $admin . 'ContentController@index');
    Route::get('content/create', $admin . 'ContentController@create');
    Route::post('content/insert', $admin . 'ContentController@insert');
    Route::get('content/edit/{id}', $admin . 'ContentController@edit');
    Route::post('content/update/{id}', $admin . 'ContentController@update');
    Route::get('content/delete/{id}', $admin . 'ContentController@delete');

    Route::get('contactus', $admin . 'ContactusController@index');
    Route::get('contactusdetail/{id}', $admin . 'ContactusController@detail');

    Route::get('reviews', $admin . 'ReviewsController@index');
    Route::get('review/delete/{id}', $admin . 'ReviewsController@delete');

    Route::get('/blog/categories', $admin . 'BlogCategoriesController@index');
    Route::get('/blog/categories/create', $admin . 'BlogCategoriesController@create');
    Route::post('/blog/categories/insert', $admin . 'BlogCategoriesController@insert');
    Route::get('/blog/categories/delete/{id}', $admin . 'BlogCategoriesController@delete');
    Route::get('/blog/categories/edit/{id}', $admin . 'BlogCategoriesController@edit');
    Route::post('/blog/categories/update/{id}', $admin . 'BlogCategoriesController@update');

    Route::get('blog/posts', $admin . 'BlogPostsController@index');
    Route::get('blog/posts/create', $admin . 'BlogPostsController@create');
    Route::post('blog/posts/insert', $admin . 'BlogPostsController@insert');
    Route::get('blog/posts/delete/{id}', $admin . 'BlogPostsController@delete');
    Route::get('blog/posts/edit/{id}', $admin . 'BlogPostsController@edit');
    Route::post('blog/posts/update/{id}', $admin . 'BlogPostsController@update');
}
);
