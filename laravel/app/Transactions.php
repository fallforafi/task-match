<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model {

    protected $table = 'transactions';
    
    protected $fillable = ['reference','user_id','assign_id','amount','gateway','type'];

    public static function checkPay($id) {
        $query = "SELECT t.assign_id AS a_id,tk.user_id,op.finalPrice,a.id AS assign_id FROM offers o
LEFT JOIN offer_prices op ON o.`id` = op.`offer_id`
LEFT JOIN assigns a ON a.`offer_price_id` = op.`id`
LEFT JOIN transactions t ON t.`assign_id` = a.`id`
LEFT JOIN tasks tk ON tk.`id` = o.`task_id`
 WHERE o.offerStatus = 'completed' AND o.deleted =0 AND o.task_id = $id";
        return $result = DB::select($query);
    }

}
