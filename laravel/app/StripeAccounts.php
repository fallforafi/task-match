<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeAccounts extends Model {

    protected $table = 'stripe_accounts';

}
