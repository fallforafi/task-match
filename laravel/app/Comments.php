<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Comments extends Model {

    protected $table = 'comments';
    protected $fillable = ['comment', 'user_id', 'task_id','file'];

    public static function get($search) {
        $subSql = "";

        if ($search['task_id'] != "") {
            $task_id = $search['task_id'];
            $subSql .= " and c.task_id = '$task_id'";
        }
        $sql = "select c.id as comment_id,c.file,c.comment,c.created_at,c.user_id,u.id as user_id,u.firstName,u.lastName,u.image,u.key
from comments c 
left join users u on u.id=c.user_id 
where 1 " . $subSql . " and c.deleted=0 order by c.id desc";

        return $result = DB::select($sql);
    }

}
