<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Friends extends Model {
    protected $table = 'friends';
        protected $fillable = ['user_id', 'friendId', 'name'];


   public static function getMutualFriends($user_id,$id){
       $sql="SELECT p1.friendId, p1.name, p2.friendId, p2.name FROM ( SELECT friendId, name FROM friends WHERE user_id = $user_id ) p1 INNER JOIN ( SELECT friendId, name FROM friends WHERE user_id = $id ) p2 ON p1.friendId = p2.friendId";
       
       $result = DB::select($sql);
       //d($result,1);
       return $result;
   }     
        
   
}
