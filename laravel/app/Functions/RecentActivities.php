<?php

namespace App\Functions;

use App\Activities;

class RecentActivities {

    public static function save($type, $type_id, $model) {

        $data['type'] = $type;
        $data['type_id'] = $type_id;
        $data['description'] = addslashes(view('front.activities.templates.' . $type, compact('type_id', 'type', 'model')));
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['user_id'] = $model['user_id'];
        Activities::insertGetId($data);
    }

}
