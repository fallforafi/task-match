<?php

namespace App\Functions;

use App\Notifications;
use Auth;
use App\NotificationsSettings;
use App\Functions\Functions;

class UsersNotifications {

    public static function save($notification) {

        $data['type'] = $notification['type'];
        $data['type_id'] = $notification['type_id'];
        $data['notify_to'] = $notification['notify_to'];

        if (!isset($notification['user_id'])) {
            $data['user_id'] = Auth::user()->id;
            $notification['name'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;
            $notification['image'] = Auth::user()->image;
        } else {
            $data['user_id'] = $notification['user_id'];
        }
        $notify = NotificationsSettings::where('user_id', $data['notify_to'])->where('type', $data['type'])->first();
        if (count($notify) > 0 && $notify->notification == 1) {
            $data['description'] = addslashes(view('front.notifications.templates.' . $notification['type'], compact('type_id', 'type', 'data', 'notification')));
            $data['created_at'] = date('Y-m-d H:i:s');
            Notifications::insertGetId($data);
        }
        if (count($notify) > 0 && $notify->email == 1) {

            $subject = view('emails.' . $notification['type'] . '.subject', compact('type_id', 'type', 'data', 'notification'));
            $body = view('emails.' . $notification['type'] . '.body', compact('type_id', 'type', 'data', 'notification'));
            $mail = Functions::sendEmail($notification['notify_email'], $subject, $body);
        }
    }

}
