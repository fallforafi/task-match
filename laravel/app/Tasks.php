<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;

class Tasks extends Model {

    protected $table = 'tasks';

    public static function search($search) {

        $subSql = "";
        $selectSubSql = "";
        if (isset($search['filter_search']) && $search['filter_search'] != "") {
            $title = $search['filter_search'];
            $subSql .= " and (t.title like '%$title%') ";
        }
        if (isset($search['user_id'])) {
            $user_id = $search['user_id'];
            $subSql .= " and t.user_id='$user_id'";
        }

        if (isset($search['fiter_category_id']) && $search['fiter_category_id'] > 0) {
            $category_id = $search['fiter_category_id'];
            $subSql .= " and tc.category_id=$category_id";
        }

        if (isset($search['task_status']) && $search['task_status'] != "") {
            $taskStatus = $search['task_status'];
            $subSql .= " and t.taskStatus='$taskStatus'";
        }
        if (isset($search['toBeCompleted']) && $search['toBeCompleted'] != "") {
            $toBeCompleted = $search['toBeCompleted'];
            $subSql .= " and t.toBeCompleted='$toBeCompleted'";
        }
        $subSql .= " group by t.id";

        if (isset($search['distance']) && $search['distance'] != "") {
            $latitude = $search['latitude'];
            $longitude = $search['longitude'];

            $distance = $search['distance'];
            $selectSubSql .= ",SQRT(POW(69.1 * (t.latitude - $latitude), 2) + POW(69.1 * ($longitude - t.longitude) * COS(t.latitude / 57.3), 2)) AS distance";
            $subSql .= " HAVING distance < $distance ";
            // print_r($selectSubSql);
        }

        if (isset($search['sort_by'])) {
            $sortBy = $search['sort_by'];
            $subSql .= " order by $sortBy";
        } else if (isset($search['distance']) && $search['distance'] != "") {
            $subSql .= " order by distance";
        } else {
            $subSql .= " order by t.id desc";
        }

        $sql = "select t.id,t.key,t.title,t.taskStatus,t.dueDate,t.key,t.description,t.price,t.location,t.user_id,t.latitude,t.longitude,t.created_at,u.firstName,u.lastName,u.email,u.image"
                . " $selectSubSql from tasks t "
                . "left join users u on u.id=t.user_id "
                . "left join task_categories tc on t.id=tc.task_id"
                . " where t.status=1 and t.deleted=0 and t.draft=0 " . $subSql;
        return $result = DB::select($sql);
    }

    public static function searchTasks($search) {

        $result = Tasks::where("deleted", '=', 0)->where("draft", '=', 0);

        if (isset($search['filter_search']) && $search['filter_search'] != "") {
            $title = $search['filter_search'];
            $result = $result->where('title', 'LIKE', "%" . $title . "%");
        }

        if (isset($search['task_status']) && $search['task_status'] != "") {
            $taskStatus = $search['task_status'];
            $result = $result->where('taskStatus', '=', $taskStatus);
        }
        $result = $result->orderBy('id', 'desc');
        $result = $result->paginate(10);
        $result->setPath('tasks');
        return $result;
    }

    public static function findTasks($dateTime) {
        $tasks = User::leftJoin('tasks as t', 't.user_id', '=', 'users.id')
                ->select('users.*', 't.title', 't.key as taskKey')
                ->where('t.created_at', '=', $dateTime)
                ->where('t.taskStatus', 'open')
                ->where('t.draft', 1)
                ->where('t.status', 1)
                ->where('t.deleted', 0)
                ->get();

        return $tasks;
    }

}
