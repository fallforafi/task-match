<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Reviews;
use App\Tasks;
use App\User;
use App\Functions\UsersNotifications;
use App\Offers;

class Reviews extends Model {

    protected $table = 'reviews';

    public static function get($search) {

        $reviews = self::where('rate_to', $search['user_id'])->where('status', 1)->orderBy('id', 'desc')->get();
        $data['reviews'] = $reviews;
        $ratings = array();
        $TotalRatings = 0;
        foreach ($reviews as $review) {
            $ratings[$review->rating][] = $review;
            $TotalRatings += $review->rating;
        }
        if (count($reviews) != 0) {
            $total = count($reviews);
            $avg = ceil($TotalRatings / $total);
            $data['average'] = $avg;
        } else {
            $avg = 00;
            $data['average'] = $avg;
        }
        $data['ratings'] = $ratings;
        return $data;
    }

    public static function getReviews($search) {
        $id = $search['user_id'];
        $sql = "SELECT 
  u.firstName,
  u.lastName,
  u.image,
  u.key,
  t.user_id,
  r.created_at,
  r.comment,
  r.rating,
  r.id AS r_id,
  r.report,
  r.rate_from,
  r.task_id,EXISTS(SELECT id FROM reviews WHERE r.rate_to=reviews.rate_from AND r.task_id=reviews.task_id)existing
FROM
  tasks t 
  LEFT JOIN reviews r 
    ON r.task_id = t.id    
  LEFT JOIN users u 
    ON u.id = r.rate_from 
WHERE t.`taskStatus` = 'completed' 
  AND r.`rate_to` = $id order by r.id desc";
        return $result = DB::select($sql);
    }

    public static function getReviewsFeedback($search) {
        $id = $search['user_id'];
        $sql = "SELECT task_id FROM reviews WHERE rate_from = $id";
        return $reviews = DB::select($sql);
    }

    public static function getReviewsListing($search) {
        $id = $search['user_id'];
        $subSql = "";
        if (isset($search['sort']) && $search['sort'] == 'created') {
            $subSql .= " order by r.created_at";
        } else if (isset($search['sort']) && $search['sort'] == 'rating') {
            $subSql .= " order by r.rating";
        } else {
            $subSql .= " order by r_id desc";
        }
        $sql = "SELECT u.id,u.key,u.firstName,u.lastName,u.image,t.user_id,r.created_at,r.comment,r.rating,r.id as r_id, r.report, r.rate_from,r.task_id FROM tasks t 
JOIN reviews r ON r.task_id = t.id
JOIN users u ON u.id = r.rate_from
WHERE r.status = 1 and r.deleted = 0 and r.`rate_to` = $id" . $subSql;
        return $result = DB::select($sql);
    }

    public static function tasksReviewsAlert($dateTime) {
        $offer = Offers::where([
                    'offerStatus' => 'completed',
                    'deleted' => 0,
                    'status' => 1,
                    'updated_at' => $dateTime,
                ])->get();
        if(count($offer) > 0) {
            foreach ($offer as $value) {
                if (!Reviews::where('task_id', $value->task_id)->exists()) {
                    $task = Tasks::leftJoin('offers as of', 'of.task_id', '=', 'tasks.id')
                            ->leftJoin('offer_prices as op', 'op.offer_id', '=', 'of.id')
                            ->select('tasks.title', 'op.key', 'tasks.user_id')
                            ->where('of.id', $value->id)
                            ->where('tasks.id', $value->task_id)
                            ->where('of.updated_at', '=', $dateTime)
                            ->first();
                    $user = User::find($task->user_id);
                    $taskRunner = User::find($value->user_id);
                    $notification['task'] = $task;
                    $notification['notify_to'] = $task->user_id;
                    $notification['notify_email'] = $user->email;
                    $notification['name'] = $user->firstName . ' ' . $user->lastName;
                    $notification['taskRunner'] = $taskRunner->firstName;
                    $notification['image'] = $user->image;
                    $notification['type'] = 'reviews_awaiting';
                    $notification['type_id'] = $value->task_id;
                    UsersNotifications::save($notification);
                    continue;
                }
            }
        } else {
            return false;
        }
    }

}
