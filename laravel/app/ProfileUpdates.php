<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\UsersEducations;
use App\UsersTransports;
use App\UsersLanguages;
use App\UsersSkills;

class ProfileUpdates extends Model {

    protected $table = 'profile_updates';

//1.	First Name & Last Name = 5%
//2.	Profile picture = 10%
//3.	Email = 5%
//4.	Education = 10%
//5.	Transport = 10%
//6.	Phone Number = 10%
//7.	Location = 10%
//8.	Tag line = 10%
//9.	About me = 10%
//10.	Skills = 10%
//11.   Language = 10%

    public static function updateProfile($user_id) {
        //d($data,1);
        $updatedProfile = 0;
        $user = User::findOrFail($user_id);
        $profile = ProfileUpdates::where('user_id', '=', $user_id)->first();
        if (count($profile) == 0) {
            $id = ProfileUpdates::insertGetId(['profile' => '0', 'user_id' => $user_id]);
        } else {
            $id = $profile->id;
        }

        if (isset($user->firstName) && isset($user->lastName)) {
            $updatedProfile += 5;
        }
        if (isset($user->email)) {
            $updatedProfile += 5;
        }
        if (isset($user->image) && $user->image != '') {
            $updatedProfile += 10;
        }
        if (isset($user->phone)) {
            $updatedProfile += 10;
        }
        if (isset($user->location) && trim($user->location) != "") {
            $updatedProfile += 10;
        }
        if (isset($user->about) && trim($user->about) != "") {
            $updatedProfile += 10;
        }

        if (isset($user->tagline) && trim($user->tagline) != "") {
            $updatedProfile += 10;
        }

        if (UsersEducations::where('user_id', $user_id)->exists()) {
            $updatedProfile += 10;
        }
        if (UsersTransports::where('user_id', $user_id)->exists()) {
            $updatedProfile += 10;
        }
        if (UsersLanguages::where('user_id', $user_id)->exists()) {
            $updatedProfile += 10;
        }
        if (UsersSkills::where('user_id', $user_id)->exists()) {
            $updatedProfile += 10;
        }
        //  d($updatedProfile,1);
        ProfileUpdates::where('user_id', $user_id)->update(['profile' => $updatedProfile]);
    }

}
