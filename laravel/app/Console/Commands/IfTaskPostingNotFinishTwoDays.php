<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Functions\Functions;
use App\Tasks;

class IfTaskPostingNotFinishTwoDays extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taskposting:aftertwodays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '2 days after if a user doesn’t finish filling out the “post a task” form';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-2 days'));
        $tasks = Tasks::findTasks($dateTime);
        foreach ($tasks as $value) {
            $subject = view('emails.crons.task_two_days.subject');
            $body = view('emails.crons.task_two_days.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
    }

}
