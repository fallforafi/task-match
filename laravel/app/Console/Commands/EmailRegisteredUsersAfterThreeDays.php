<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Functions\Functions;
use App\User;

class EmailRegisteredUsersAfterThreeDays extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emailusers:afterthreedays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '3 Days after new user registers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-3 day'));
        $user = User::findUsers($dateTime);
        foreach ($user as $value) {
            $subject = view('emails.crons.user_register_one_day.subject');
            $body = view('emails.crons.user_register_one_day.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
    }

}
