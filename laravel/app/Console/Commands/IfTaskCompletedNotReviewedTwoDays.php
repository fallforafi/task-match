<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reviews;

class IfTaskCompletedNotReviewedTwoDays extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taskcompleted:aftertwodays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '2 days after task marked as complete send email to poster if they have not left a review';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-2 days'));
        Reviews::tasksReviewsAlert($dateTime);
    }

}
