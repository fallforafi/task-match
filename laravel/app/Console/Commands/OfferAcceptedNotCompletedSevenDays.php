<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Offers;

class OfferAcceptedNotCompletedSevenDays extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offeraccepted:aftersevendays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Offer accepted but not marked as complete after 7 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-7 days'));
        Offers::offersCompletionAlert($dateTime);
    }

}
