<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Offers;
use App\Functions\Functions;

class NotRespondingOnOffersAfterSevenDays extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notrespondingoffers:aftersevendays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '7 days after no response to an offer/offers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-7 days'));
        $offers = Offers::NotRespondingOffers($dateTime);
        foreach ($offers as $key => $value) {
            $subject = view('emails.crons.offers_not_responding_seven_days.subject');
            $body = view('emails.crons.offers_not_responding_seven_days.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
    }

}
