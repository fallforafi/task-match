<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Offers;
use App\Functions\Functions;

class NotRespondingOnOfferAfterOneDay extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notrespondingoffer:afteroneday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '1 day after if a user does not respond to an offer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-1 day'));
        $offers = Offers::NotRespondingOffers($dateTime);
        foreach ($offers as $key => $value) {
            $subject = view('emails.crons.offers_not_responding_one_day.subject');
            $body = view('emails.crons.offers_not_responding_one_day.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
        }
    }
    