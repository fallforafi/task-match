<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Functions\Functions;
use App\Tasks;

class IfTaskPostingNotFinishFourHours extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taskposting:afterfourhours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '4 Hours after if a user doesn’t finish filling out the “post a task” form';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-4 hours'));
        $tasks = Tasks::findTasks($dateTime);
        foreach ($tasks as $value) {
            $subject = view('emails.crons.task_four_hours.subject');
            $body = view('emails.crons.task_four_hours.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
    }

}
