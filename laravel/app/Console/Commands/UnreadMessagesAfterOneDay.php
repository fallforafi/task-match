<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Functions\Functions;
use App\Messages;

class UnreadMessagesAfterOneDay extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unreadmessages:afteroneday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '1 day after you receive a private message if unread';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dateTime = date('Y-m-d H:i:s', strtotime('-1 day'));
        $messages = Messages::unreadMessages($dateTime);
        foreach ($messages as $value) {
            $subject = view('emails.crons.unread_messages_one_day.subject');
            $body = view('emails.crons.unread_messages_one_day.body', compact('value'));
            Functions::sendEmail($value->email, $subject, $body);
        }
    }

}
