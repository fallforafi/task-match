<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'App\Console\Commands\Inspire',
        Commands\EmailRegisteredUsersAfterOneDay::class,
        Commands\EmailRegisteredUsersAfterThreeDays::class,
        Commands\IfTaskPostingNotFinishFourHours::class,
        Commands\IfTaskPostingNotFinishTwoDays::class,
        Commands\IfTaskCompletedNotReviewedTwoDays::class,
        Commands\OfferAcceptedNotCompletedSevenDays::class,
        Commands\UnreadMessagesAfterOneDay::class,
        Commands\NotRespondingOnOfferAfterOneDay::class,
        Commands\NotRespondingOnOffersAfterThreeDays::class,
        Commands\NotRespondingOnOffersAfterSevenDays::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
//        $schedule->command('inspire')
//                ->hourly();
        $schedule->command('emailusers:afteroneday')
                ->daily();
        $schedule->command('emailusers:afterthreedays')
                ->daily();
        $schedule->command('taskposting:afterfourhours')
                ->daily();
        $schedule->command('taskposting:aftertwodays')
                ->daily();
        $schedule->command('taskcompleted:aftertwodays')
                ->daily();
        $schedule->command('offeraccepted:aftersevendays')
                ->daily();
        $schedule->command('unreadmessages:afteroneday')
                ->daily();
        $schedule->command('notrespondingoffer:afteroneday')
                ->daily();
        $schedule->command('notrespondingoffers:afterthreedays')
                ->daily();
         $schedule->command('notrespondingoffers:aftersevendays')
                ->daily();
        
    }

}
