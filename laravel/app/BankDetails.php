<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model {

    protected $table = 'bank_details';
    protected $fillable = ['user_id', 'accountTitle', 'bic', 'iban', 'address', 'bankName'];

    public function users() {
        return $this->belongsTo('App\User');
    }

}
