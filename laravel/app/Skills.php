<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\UsersSkills;

class Skills extends Model {

    protected $table = 'skills';

    public static function setSkills($param) {
        $skills = explode(',', $param);
        $user_id = Auth::user()->id;
        UsersSkills::where('user_id', '=', $user_id)->delete();
        foreach ($skills as $value) {
            if (Skills::where('title', '=', $value)->exists()) {
                $getRow = Skills::where('title', '=', $value)->first();
                $uSkillModel = new UsersSkills();
                $uSkillModel->user_id = $user_id;
                $uSkillModel->skill_id = $getRow->id;
                $uSkillModel->save();
            } else {
                $sSkillModel = new Skills();
                $sSkillModel->title = $value;
                $sSkillModel->save();
                $getRow = Skills::where('title', '=', $value)->first();
                $uSkillModel = new UsersSkills();
                $uSkillModel->user_id = $user_id;
                $uSkillModel->skill_id = $getRow->id;
                $uSkillModel->save();
            }
        }
        return true;
    }  
    public static function getSkills($param) {       
       $model =  Skills::join('users_skills','users_skills.skill_id','=','skills.id')
                ->select('skills.title as name')
                ->where('user_id', '=', $param)
                ->get();
      
        return $model;
    }
   
}
