<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccounts extends Model {

    protected $table = 'social_accounts';

    public static function updateSocialAccount($type, $profileUrl, $user_id, $accountId = "") {
        $acount = SocialAccounts::where('user_id', $user_id)->where('type', $type)->first();
        if (count($acount) == 0) {
            $model = new SocialAccounts();
            $model->user_id = $user_id;
            $model->type = $type;
            $model->accountId = $accountId;
            $model->profileUrl = urlencode($profileUrl);
            $model->created_at = date("Y-m-d H:i:s");
            $model->save();
        } else {
            $acountUpdate = SocialAccounts::where('id', $acount->id)->where('type', $type)->update(array('profileUrl' => urlencode($profileUrl), 'accountId' => $accountId));
        }
    }

    public static function byUserId($user_id) {
        $socialAccounts = SocialAccounts::where('user_id', $user_id)->where('status', 1)->where('deleted', 0)->get();

        $accounts = array();
        foreach ($socialAccounts as $account) {
            $accounts[$account->type] = $account;
        }
        return $accounts;
    }

}
