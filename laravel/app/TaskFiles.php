<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskFiles extends Model {
    protected $table = 'task_files';
    protected $fillable = ['file','task_id'];
    
}
