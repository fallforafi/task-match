<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\UsersEducations;

class Educations extends Model {
    protected $table = 'educations';
    public static function setEducations($param) {
        $edu = explode(',', $param);
        $user_id = Auth::user()->id;
        UsersEducations::where('user_id', '=', $user_id)->delete();
        foreach ($edu as $value) {
            if (Educations::where('title', '=', $value)->exists()) {
                $getRow = Educations::where('title', '=', $value)->first();
                $uEduModel = new UsersEducations();
                $uEduModel->user_id = $user_id;
                $uEduModel->education_id = $getRow->id;
                $uEduModel->save();
            } else {
                $sEduModel = new Educations();
                $sEduModel->title = $value;
                $sEduModel->save();
                $getRow = Educations::where('title', '=', $value)->first();
                $uEduModel = new UsersEducations();
                $uEduModel->user_id = $user_id;
                $uEduModel->education_id = $getRow->id;
                $uEduModel->save();
            }
        }
        return true;
    }  
    public static function getEducations($param) {       
       $model =  Educations::join('users_educations','users_educations.education_id','=','educations.id')
                ->select('educations.title as name')
                ->where('user_id', '=', $param)
                ->get();
      
        return $model;
    }
   
}
