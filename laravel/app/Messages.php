<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Messages extends Model {

    protected $table = 'messages';

    public static function get($search = array()) {

        $subSql = "";
        if (isset($search['user_id'])) {
            $user_id = $search['user_id'];
            $subSql .= " and m.to =$user_id ";
        }

        if ($search['search'] != "") {
            $message = $search['search'];
            $subSql .= " and (m.subject like '%$message%') ";
        }

        if (isset($search['thread_id'])) {
            $thread_id = $search['thread_id'];
            $subSql .= " and thread_id=$thread_id ";
        } elseif ($search['thread_id'] === NULL) {
            $subSql .= " and thread_id is NULL";
        }


        $sql = "select m.key,m.id,m.subject,m.message,m.created_at,m.`to`,m.`from`,m.thread_id,u.firstName,u.lastName,u.email,u.image 
            from messages m 
            join users u on u.id=m.from
            
where 1 " . $subSql . " order by m.id desc;";

        return $result = DB::select($sql);
    }

    public static function unreadMessages($dateTime) {
        $query = "select u.id,u.firstName, u.lastName,u.email, count(m.`to`) unread from users as u
inner join messages as m on m.`to` = u.`id`
where m.seenDate is Null and m.created_at = '$dateTime'
group by m.`to`";
        return $result = DB::select($query);
    }

}
