<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaypals extends Model {

    protected $fillable = ['email'];
    protected $table = 'user_paypals';

}
