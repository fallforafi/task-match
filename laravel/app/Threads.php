<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Threads extends Model {

    protected $table = 'threads';

    public static function get($search) {
        $subSql = "";

        if (isset($search['search']) && $search['search'] != "") {
            $filter_search = $search['search'];
            $subSql .= " and (tk.title like '%$filter_search%') ";
        }

        if (isset($search['user_id'])) {
            $user_id = $search['user_id'];
            $subSql .= " and task_owner_id=$user_id OR task_runner_id=$user_id ";
        }


        /*
          if (isset($search['thread_id'])) {
          $thread_id = $search['thread_id'];
          $subSql.=" and thread_id=$thread_id ";
          } elseif ($search['thread_id'] === NULL) {
          $subSql.=" and thread_id is NULL";
          }
         */


        if (isset($search['order'])) {
            $sqlOrder = $search['order'];
        } else {
            $sqlOrder = "t.id desc";
        }

        $sql = "SELECT t.id as thread_id,t.key as threadKey,m.id as message_id,tk.id as task_id,tk.title,tk.price as taskPrice,tk.description,tk.deleted,tk.taskStatus,o.id as offer_id,op.id as offer_price_id,op.offerPrice,task_owner.id as task_owner_id,task_owner.key as task_owner_key,task_owner.firstName as taskOwnerFirstName,task_owner.lastName as taskOwnerLastName,task_owner.image as taskOwnerImage,task_runner.id as task_runner_id,task_runner.key as task_runner_key,task_runner.firstName as taskRunnerFirstName,task_runner.lastName as taskRunnerLastName,task_runner.image as taskRunnerImage,m.created_at,count(m.id)  FROM threads t 
left join messages m on m.thread_id=t.id
right join tasks tk on tk.id = t.task_id
left join offers o on o.task_id=tk.id
left join offer_prices op on op.offer_id=o.id
left join users as task_owner on t.task_owner_id=task_owner.id
left join users as task_runner on t.task_runner_id=task_runner.id
WHERE 1 " . $subSql . " group by t.id having count(m.id) > 0 order by $sqlOrder;";

        return $result = DB::select($sql);
    }

}
