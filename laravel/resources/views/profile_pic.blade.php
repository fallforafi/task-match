<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
    <head>
        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <meta name="description" content="<?php echo Config('params.site_name'); ?> | @yield('description')">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta http-equiv="Cache-control" content="no-cache">
        <meta http-equiv="Expires" content="-1">
        <link rel="icon" type="image/png" href="{{asset('front/images/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">

        <link rel="stylesheet" href="{{asset('front/style.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/home.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/about.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/works.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/earn.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/price-guide.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/posting-a-task.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/find-out-more.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/browse-tasks.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/colorized.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/animate.css')}}">
        <!--link rel="stylesheet" href="{{asset('front/css/slidenav.css')}}"-->
        <link rel="stylesheet" href="{{asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/extralized/daterangepicker.css')}}">
        <link rel="stylesheet" href="{{asset('front/style-extra.css')}}">
        <link rel="stylesheet" href="{{asset('front/dashboard.css')}}">
        <link rel="stylesheet" href="{{asset('front/swiper.min.css')}}">

        <link rel="stylesheet" href="{{asset('front/css/croppie.css')}}">


        <link rel="stylesheet" href="{{asset('front/css/responsive.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/grasp_mobile_progress_circle-1.0.0.css')}}">
        <!--                <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />-->


        <script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
        <script src="{{asset('front/js/croppie.js')}}"></script>
        <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
        <!--
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
                <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.js"></script>-->
        <!--        <script src="{{asset('front/js/card.js')}}"></script>
                <script src="{{asset('front/js/grasp_mobile_progress_circle-1.0.0.js')}}"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
        <!--
        <script src="{{asset('front/extralized/modernizr-2.6.2.min.js')}}"></script>
         <script src="{{asset('/front/extralized/jquery.easing.1.3.js')}}"></script>
        -->
        <script src="{{asset('front/js/swiper.jquery.min.js') }}"></script>
        <script src="{{asset('/front/extralized/moment.min.js')}}"></script>
        <script src="{{asset('/front/extralized/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('/front/extralized/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{asset('/front/js/bootstrap-confirmation.js')}}"></script>
        <script src="{{asset('/front/js/css_browser_selector.js')}}" type="text/javascript"></script>
        <!--        @if(!isset(Auth::user()->id))
                <script type="text/javascript" src="http://platform.linkedin.com/in.js">
        api_key: 81
        gy6puxhv8bqp
        onLoad: OnLinkedInFrameworkLoad
                </script>
                @endif
                <script>
                    function back() {
                        window.location.href = '<?php echo URL::previous(); ?>';
                    }
                </script>
        
                <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo env('GOOGLE_API'); ?>&libraries=places" async defer></script>
                <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
                <script type="text/javascript">
                    Stripe.setPublishableKey('<?php echo env('STRIPE_SECRET_PK'); ?>');
                </script>-->
    </head>

    <body class="nav-plusminus slide-navbar slide-navbar--left">
        @include('front/common/navigation')
        <main id="page-content">
            @yield('content')
            @include('front/common/footer')

            @include('front/tasks/add_task_popup')

            @if(!isset(Auth::user()->id))
            @include('front/common/login_popup')
            @include('front/common/fb_login')
            @include('front/common/ln_login')
            @endif
        </main>
        <a href="" class="scrollToTop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        <div id="loading"></div>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo env('GOOGLE_API'); ?>&libraries=places" async defer></script>
        <script src="{{asset('/front/js/viewportchecker.js')}}"></script>
        <script src="{{asset('/front/js/kodeized.js')}}"></script>

        @include('front/common/js')
        <script>
$(document).ready(function () {
    unseenMessages();
    unseenNotifications();
});
var lastScrollizedTop = 0;
jQuery(window).scroll(function (event) {
    var scrollizedTop = jQuery(this).scrollTop();
    if (scrollizedTop > lastScrollizedTop) {
        jQuery("body").removeClass("scrollized--up");
        jQuery("body").addClass("scrollized--down");
    } else {
        jQuery("body").removeClass("scrollized--down");
        jQuery("body").addClass("scrollized--up");
    }
    lastScrollizedTop = scrollizedTop;
});
        </script>
        @include('front/common/chat')
        @include('front/common/google_analytics')
    </body>
</html>
