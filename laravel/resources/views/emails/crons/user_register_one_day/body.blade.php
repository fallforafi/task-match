<?php
$name = $value->firstName;
?>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; font-weight:300;">Welcome to TaskMatch!</h1>
            <p style="margin-bottom:0px;">Hi <?php echo $name; ?>,</p>
            <p style="margin-bottom:0px;">
                I’m Conor, one of the co-founders of TaskMatch, I just wanted to send you a quick email to personally welcome you to the TaskMatch community. 
            </p>
            <p style="margin-bottom:0px;">
                To help you get started I thought I'd answer some common questions.
            </p>
            <p style="margin-bottom:0px;font-weight:bold;">
                Q: What type of tasks can you get done with Task Match?
            </p>
            <p style="margin-bottom:0px;">
                You can get almost any task done – from <a style="color:#15c;font-size:12px;"href="<?php echo url('find-out-more') ?>">house cleaning</a> and handyman jobs to office admin, photography, quick deliveries or furniture assembly. Anything you need done around the home or office.
            </p>
            <p style="margin-bottom:0px;font-weight:bold;">
                Q: How do you know the Tasker will do a good job?
            </p>
            <p style="margin-bottom:0px;">
                Before choosing a Task Match worker, you can look at their profile and ask as many questions as you need. There are no obligations to hire until you're 100% confident.
            </p>
            <p style="margin-bottom:0px;font-weight:bold;">
                Q: Does it cost anything to use Task Match?
            </p>
            <p style="margin-bottom:0px;">
                No. It’s completely free to <a style="color:#15c;font-size:12px;"href="<?php echo url('/') ?>">post a task</a> and there are no hidden fees. You will only pay the Task Match worker the agreed price once the task is completed to your satisfaction.
            </p>
            <p style="margin-bottom:0px;font-weight:bold;">
                Q: What price should I set for my task?
            </p>
            <p style="margin-bottom:0px;">
                We've put together a <a style="color:#15c;font-size:12px;"href="<?php echo url('price-guide') ?>">price guide</a> to help you set a fair price for your task.
            </p>          
            <p style="margin-bottom:0px;">
                So let's get you started by <a style="color:#15c;font-size:12px;"href="<?php echo url('/') ?>">posting a task</a> today!
            </p>
            <p style="margin-bottom:0px;">
                Cheers,<br>
                Conor (Co-founder and CEO, TaskMatch)
            </p>
            <br>
            <div style="width:200px;margin-bottom:10px;"><a href="{{ URL::to('/') }}"><img src="{{ URL::to('front/images/logo.png') }}" alt="logo" style="width:100%"></a>
            </div>
            <h4 style="margin-bottom:0px">Stay In Touch with us</h4>
            <a href="https://www.facebook.com/TaskMatch-1528535830788580/info/?tab=page_info&edited=category" target="_blank" style="display:inline-block;">
                <img style="width:38px;height:38px;border-radius:50%;" src="{{ URL::to('front/images/emails/facebook.png') }}"></a>
            <a href="https://twitter.com/Task_Match" target="_blank" style="display:inline-block;    vertical-align: sub;"><img style="width:47px;height:47px;border-radius:50%; " src="{{ URL::to('front/images/emails/twitter.png') }}"></a>
            <a href="https://www.instagram.com/taskmatch/?hl=en" target="_blank" style="display:inline-block;"><img style="width: 35px;height: 35px;border-radius: 50%;margin-bottom: 3px" src="{{ URL::to('front/images/emails/instagram.png') }}"></a>
            <br>
            <small style="margin-bottom:0px;">
                Need more help getting started? Check out our <a style="color:#15c;font-size:12px;"href="<?php echo url('faqs') ?>">FAQs</a> or <a style="color:#15c;font-size:12px;"href="<?php echo url('contact-us') ?>">contact us</a> - We're here to help you.
            </small>
        </div>
    </body>
</html>
