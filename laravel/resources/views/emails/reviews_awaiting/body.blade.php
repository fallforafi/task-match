<?php
$task = $notification['task'];
$name = $notification['taskRunner'];
?>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; text-transform:uppercase; font-weight:300;">Please leave a review for “<?php echo $name; ?>”</h1>
            <p style="margin-bottom:0px;">Hi <?php echo $notification['notify_name']; ?>,<br>
                We hope you are happy to have had your task completed and would like to share your experience.<br>
                Can you please leave a review for <a style="color:#15c;font-size:12px;"href="<?php echo url('offers/') ?>/<?php echo $task->key; ?>"><?php echo $name; ?></a> as this will help other users choose the right person for their tasks.
            </p>
            <p style="margin-bottom:0px;">
                Would you recommend “<?php echo $name; ?>” to others?
            </p>
            <div style="width:200px;margin-bottom:10px;">
                <table>
                    <tr>
                        <td><a href=""><img src="{{ asset('front/images/emails/no.jpg') }}" style="width:100%;height:100px;"></a></td>
                        <td><a href="<?php echo url('offers/') ?>/<?php echo $task->key; ?>"><img src="{{ asset('front/images/emails/yes.jpg') }}" style="width:100%;height:100px;"> </a></td>
                    </tr>
                </table>  
            </div>
            <p style="margin-bottom:0px;font-weight:bold;">Many thanks,</p>
            <p style="margin-bottom:0px;font-weight:bold;">The TaskMatch.ie Team</p>
            <br>
            <div style="width:200px;margin-bottom:10px;"><a href="{{ URL::to('/') }}"><img src="{{ asset('front/images/logo.png') }}" alt="logo" style="width:100%"></a>
            </div>
            <h4 style="margin-bottom:0px">Stay In Touch with us</h4>
            <a href="https://www.facebook.com/TaskMatch-1528535830788580/info/?tab=page_info&edited=category" target="_blank" style="display:inline-block;">
                <img style="width:38px;height:38px;border-radius:50%;" src="{{ URL::to('front/images/emails/facebook.png') }}"></a>
            <a href="https://twitter.com/Task_Match" target="_blank" style="display:inline-block;    vertical-align: sub;"><img style="width:47px;height:47px;border-radius:50%; " src="{{ URL::to('front/images/emails/twitter.png') }}"></a>
            <a href="https://www.instagram.com/taskmatch/?hl=en" target="_blank" style="display:inline-block;"><img style="width: 35px;height: 35px;border-radius: 50%;margin-bottom: 3px" src="{{ URL::to('front/images/emails/instagram.png') }}"></a>
            <br>
            <small style="margin-bottom:0px;">
                Need more help getting started? Check out our <a style="color:#15c;font-size:12px;"href="<?php echo url('faqs') ?>">FAQs</a> or <a style="color:#15c;font-size:12px;"href="<?php echo url('contact-us') ?>">contact us</a> - We're here to help you.
            </small>
        </div>
    </body>
</html>