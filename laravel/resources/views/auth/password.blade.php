@extends('login_signup')
<?php
$title = 'Reset Password';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
    <div class="container">
        <div class="row">
            <div class="resetpassword-area col-sm-12 mt50 mb50">
               
				
				<div class="fom-area col-sm-6 pul-cntr shad-dp20  ">
				
					 @if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif

					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
				
					<div class="hed"><h2>Forgot Password</h2></div>
					<p>Enter your email below and we will send you instructions on how to reset your password</p>

					<form class="form" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label>E-Mail Address</label>
							<div class="form-group">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary">
									Reset my password
							</button>
						</div>
					</form>
				</div>

            </div>
        </div>
    </div>
</div>
@endsection
