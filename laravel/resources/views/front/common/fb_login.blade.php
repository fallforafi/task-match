<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?php echo env("FACEBOOK_API_ID"); ?>',
            status: true,
            version : 'v2.10',
            cookie: true,
            oauth   : true,
            xfbml: true
        });
    };

    (function (doc) {
        var js;
        var id = 'facebook-jssdk';
        var ref = doc.getElementsByTagName('script')[0];
        if (doc.getElementById(id)) {
            return;
        }
        js = doc.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
</script>
<script>
    function fb_login() {
        FB.login(function (response) {

            if (response.authResponse) {
                //  console.log('Welcome!  Fetching your information.... ');
                //console.log(response); // dump complete info
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

                FB.api('/me?fields=id,email,first_name,last_name,link', function (response) {
                    user_email = response.email;
                    FB.api('/me/picture?type=large&redirect=false', function (r) {

                        checkMember(response.email, response.first_name, response.last_name, 'facebook', r.data.url, response.link, response.id);
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email,user_friends,public_profile,read_insights,publish_actions'
        });
    }


</script>
<div id="fb-root"></div>
