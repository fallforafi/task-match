<script>
    
function OnLinkedInFrameworkLoad() {
  IN.Event.on(IN, "auth", OnLinkedInAuth);
}

function OnLinkedInAuth() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address","public-profile-url","picture-url","picture-urls::(original)").result(ShowProfileData);
}

function ShowProfileData(profiles) {
    var member = profiles.values[0];
    var firstName=member.firstName; 
    var lastName=member.lastName; 
    var email=member.emailAddress; 
    var publicProfileUrl = member.publicProfileUrl;
    checkMember(email,firstName,lastName,'linkedin',member.pictureUrls.values[0],publicProfileUrl,member.id)
}


function liAuth(){
   IN.User.authorize(function(){
       OnLinkedInAuth();
   });
}
</script>