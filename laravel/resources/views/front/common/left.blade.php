<?php

use App\User;

$user_id = Auth::user()->id;
$user = User::findOrFail($user_id);
$required = 'required';
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
<ul class="nav nav-pills nav-stacked">
    <li class="">
        <a href="{{ url('profile') }}">
            <div class="dboard__photo">
                <div class="dboard__img">
                    @if($user->image == '')
                    <img src="{{ url('front/images/default.png') }}" id="profilePic" alt="profile picture" />   
                    @else
                    <img src=" {{ asset('/uploads/users/profile/')}}/<?php echo $user->image; ?>" id="profilePic" alt="profile picture" />
                    @endif

                </div>
                <div class="dboard__username">
                    <h4>
                        {{ $user->firstName}} {{$user->lastName}}
                    </h4>
                </div>
            </div>
        </a>
    </li>
    <li><a href="{{ url('dashboard') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
<!--    <li><a href="{{ url('my-tasks') }}"><i class="fa fa-tasks"></i> <span>Tasks</span></a></li>-->
<!--    <li><a href="{{ url('messages') }}"><i class="fa fa-comments"></i> <span>Messages</span></a></li>
    <li><a href="{{ url('notifications') }}"><i class="fa fa-bell"></i> <span>Notifications</span></a></li>-->
    <li><a href="{{ url('my-reviews') }}"><i class="fa fa-eye"></i> <span>Reviews</span></a></li>
    <li><a href="{{ url('my-tasks') }}"><i class="fa fa-sticky-note"></i> <span>My Tasks</span></a></li>
<!--    <li><a href="{{ url('offers-accepted') }}"><i class="fa fa-briefcase"></i> <span>Unfinished Tasks - Offers Accepted</span></a></li>-->
    <li><a href="{{ url('user')}}/<?php echo Auth::user()->key; ?>"><i class="fa fa-user"></i> <span>Public Profile</span></a></li>
    <li><a href="{{ url('profile') }}"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>
    <li><a href="{{ url('payments') }}"><i class="fa fa-money"></i> <span>Edit Payment Methods</span></a></li>
    <li><a href="{{ url('picture')}}" ><i class="fa fa-picture-o"></i> <span>Change Profile Picture</span></a></li>
    <li><a href="{{ url('changepassword')}}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
    <li><a href="{{ url('mobile') }}"><i class="fa fa-cog"></i> <span>Mobile</span></a></li>
    <li><a href="{{ url('accounts') }}"><i class="fa fa-users"></i> <span>Verify Social Accounts</span></a></li>
    <li><a href="{{ url('notifications/settings') }}"><i class="fa fa-cog"></i> <span>Notifications Settings</span></a></li>
</ul>
