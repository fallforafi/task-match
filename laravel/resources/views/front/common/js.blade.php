<script >

    function checkMember(email, firstName, lastName, type, image, link, accountId) {
        $.ajax(
                {
                    url: "<?php echo url(""); ?>/fblogin",
                    type: 'get',
                    dataType: 'json',
                    data: {
                        email: email,
                        firstName: firstName,
                        lastName: lastName,
                        image: image,
                        joinFrom: type,
                        link: link,
                        accountId: accountId,
                        _token: $('#_token').val()
                    },
                    success: function (result) {
                        if (type === 'facebook') {
                            FB.api('/me/friends', function (response) {
								console.log(response);
                                if (response.data) {
                                    
                                    addFriends(response.data, result.id);
                                } else {
                                    //alert("Error!");
                                }
                            });
                        }
                        // return false;
                        if (result.login === false)
                        {
                            window.top.reload();
                        } else
                        {
                            
                            window.top.location = "<?php echo url('dashboard'); ?>";
                            return false;
                            if (result.key !== false) {
                                window.top.location = "<?php echo url('task'); ?>/" + result.key;
                            } else {
                                window.top.location = "<?php echo url('tasks'); ?>";
                            }
                        }
                    }
                });
    }

    function addFriends(friends, user_id) {

        $.ajax({
            url: "<?php echo url(""); ?>/friends/save",
            type: 'get',
            dataType: 'json',
            data: {user_id: user_id, friends: friends},
            success: function (result) {}
        });
    }

    function unseenMessages() {

        $.ajax({
            url: "<?php echo url(""); ?>/messages/unseen",
            type: 'get',
            dataType: 'html',
            success: function (result) {

                $('.unseen_messages').html(result);
            }
        });
    }

    function unseenNotifications() {

        $.ajax({
            url: "<?php echo url(""); ?>/notifications/unseen",
            type: 'get',
            dataType: 'html',
            success: function (result) {

                $('.unseen_notifications').html(result);
            }
        });
    }
</script>
