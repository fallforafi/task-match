<?php

use App\Categories;

$categoriesModel = Categories::get();
?>
<footer>
    <section class="ftr-area nav-folderized " id="footer">
        <div class="container">
            <div class="ftr__inr col-sm-offset-3 col-sm-6 p0 anime-up clrlist">
                <div class="ftr__list nav col-sm-3">
                    <h6>Company</h6>
                    <ul>
                        <li><a href="{{ url('about-us') }}">About Us</a></li>
                        <li><a href="{{ url('how-it-works') }}">How it works</a></li>
                        <li><a href="{{ url('earn-with-us') }}">Work with Us</a></li>
                        <li><a href="{{ url('terms-conditions') }}">Conditions</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('market-place') }}">Marketplace Rules</a></li>
                    </ul>
                </div>
                <div class="ftr__list nav col-sm-3">
                    <h6>Members</h6>
                    <ul>
                        <?php if (isset(Auth::user()->id)) { ?>
                        <li><a onclick="viewForm();" data-toggle="modal" data-target="#postTask">Post a Task</a></li>
                        <?php } else { ?>
                        <li><a data-toggle="modal" data-target="#signIn">Post a Task</a></li>
                         <?php } ?>
                        
                        <li><a href="{{ url('tasks') }}">Browse Tasks</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                        <li><a href="blog">Blog</a></li>
                    </ul>
                </div>
                <div class="ftr__list nav col-sm-3">
                    <h6>Learn</h6>
                    <ul>
                        <li><a href="{{ url('new-user-faq')}}">New User FAQ</a></li>
                        <li><a href="{{ url('price-guide') }}">Price Guide</a></li>
                        <li><a href="{{ url('faqs') }}">FAQs</a></li>
                        <li><a href="{{ url('posting-a-task') }}">Posting a Task</a></li>
                        <li><a href="{{ url('earn-with-us') }}">Earn with Us</a></li>
                        <li><a href="{{ url('contact-us') }}">Press</a></li>
                    </ul>
                </div>
                <div class="ftr__list nav col-sm-3">
                    <h6>Categories</h6>
                    <ul>
                        <?php foreach ($categoriesModel as $cat) { ?>
                            <?php
                            if (!$cat->parent_id == 0) {
                                ?>
                                <li><a href="{{ url('tasks?cat_id='.$cat->id) }}">{{ $cat->name }}</a></li>                                       
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="ftr__social text-center col-sm-12 clrlist">
                <p>
                    Follow us on
                </p>
                <ul>
                    <li><a href="https://www.facebook.com/TaskMatch-1528535830788580/info/?tab=page_info&edited=category" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/Task_Match" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/taskmatch/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>

        </div>
    </section>
    @include('front/common/copyright')

</footer>


<script>
    $(document).ready(function () {
        <?php if(isset(Auth::user()->id)) { ?>
        $('body').addClass('user-login');
        <?php } else { ?>
        $('body').addClass('user-logout');
        <?php } ?>
    });
    jQuery('[data-provide="datepicker"]').datepicker({
        format: "dd/mm/yyyy"
    }).on('change', function () {
        jQuery('.datepicker').hide();
    });
</script>



<script>
  /**Show Modal dialog by Hash URL**/
  jQuery(function(){ /*shortcut for $(document).ready */
      // if(window.location.hash) {          var hash = window.location.hash;          jQuery(hash).modal('toggle');      }
	jQuery(window.location.hash).modal('show');
		jQuery('a[data-toggle="modal"]').click(function(){
        window.location.hash = jQuery(this).attr('data-target');
    });

    function revertToOriginalURL() {
        var original = window.location.href.substr(0, window.location.href.indexOf('#'))
        history.replaceState({}, document.title, original);
    }

    jQuery('.modal').on('hidden.bs.modal', function () { revertToOriginalURL(); });
	jQuery('.modal').on('click', '.close' , function () { revertToOriginalURL(); });
	
  });
  /**./Show Modal dialog by Hash URL**/
  
  
  jQuery(".nav-folderized h6").click(function(){
	  jQuery(this).parent(".nav").toggleClass("open"); 
	  jQuery('html, body').animate({ scrollTop: jQuery(this).offset().top - 170 }, 1500 );
  });
  
</script>