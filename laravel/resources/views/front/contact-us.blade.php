@extends('layout')

@section('content')

<section class="contact-us-section m0 p0">
    <div class="container m0 p0">
		<div class="container">
			<div class="contact__us col-sm-12 m0 p0">
				<div class="contact__us__hdng">
					<h2>Contact Us</h2>
				</div>

				<div class="contact__us__bg text-center bg-cvr col-sm-12 mb50" style="background-image:url('{{ asset('front/images/contacting.png') }}')">
					<p class="lead black mt50 posrel">Please read our FAQs if you need any information. If you can't find what your looking for you can submit a form below</p>
					
					
					
        <div class="inr-contact-us-main col-sm-12 ">
            @include('front.common.errors')
            <div class="inr__contact__us__form col-sm-offset-4 col-sm-4">
                <form method="post" action="{{ URL('contact-send') }}">
                    <div class="form-group">
                        <label for="fname">Name</label>
                        <input type="text" name="name" class="form-control" id="fname" placeholder="Your Name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="required">
                    </div>
                    <div class="form-group">
                        <label for="email">Message</label>
                        <textarea name="message" class="form-control contact__us__msg__box mh100" rows="4" cols="60" placeholder="Your message" required="required"></textarea>
                    </div>
                    <!--                    <div class="row">
                                            <div class="form-group col-sm-12">
                                                <span class="captcha-img">{!! Recaptcha::render() !!}</span>
                                                @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                                @endif 
                                            </div>
                                        </div>-->


                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-primary " type="submit">Submit your request</button>

                </form>
            </div>
        </div>
		
		<div class="clearfix"></div>
		
				</div>
			</div>
		</div>
    </div>
</section>


<script src="https://www.google.com/recaptcha/api.js"></script>
@endsection