@extends('layout')

@section('content')
<?php
$required = "required";
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">

                <div class="tab-content dboard-tab-height">
                    <div id="editPayment">
                        <div class="edit-pay-area">
                            <div class="tab-cont-title mb20 text-center">
                                <h3><i class="fa fa-credit-card"></i> Manage your payment methods</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="edit-pay-tab task-summ-area">
                                <div class="tab-content task-tab-content">
                                    <div id="makePayments" class="tab-pane fade in active">
<!--                                        <div class='card-wrapper' style="margin-top: -50px;height: 120px;"></div>-->
                                        <div class="add-payment">
                                            <div class="add__pay__title col-sm-12">
                                                <h3>Update your payment details</h3>
                                            </div>
                                            <div class="fnc-fom placelabeler" id="fancyForm">
                                                <form name="updateCard" method="post" action="{{ url('card/update/'.$model->id) }}">
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <input type="text" class="form-control" name="cardNumber" placeholder="Card Number" id="cardNumber" maxlength="19" value="{{ $model->cardNumber }}"/>
                                                            <label><i class="fa fa-credit-card"></i>Card Number</label><ins></ins>
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <input type="text" class="form-control" name="cardholderName" placeholder="Cardholder Name" id="cardholderName" value="{{ $model->cardholderName }}" />
                                                            <label><i class="fa fa-user"></i>Cardholder Name</label><ins></ins>
                                                        </div>

                                                        <div class="form-group col-sm-4  expdate mt0">
                                                            <label><i class="fa fa-calendar"></i>Expiration Date</label><ins></ins>
                                                            <div class="clearfix"></div>
                                                            <div class="col-sm-6 pl0">
                                                                {!! Form::selectMonth('expMonth', null, ['class'=> 'form-control','id' => 'expMonth',$required,'data-stripe'=> 'exp-month'],'%m') !!}
                                                            </div>
                                                            <div class="col-sm-6 p0">
                                                                {!! Form::selectYear('expYear', date('Y'), date('Y') + 10, null, ['class'=>'form-control',$required,'data-stripe'=>'exp-year']) !!}
                                                            </div>


                                                        </div>
                                                    </div>	
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <input type="text" class="form-control" name="cvc" placeholder="CVC Number" value="{{ $model->cvc }}" id="cvc"/>
                                                            <label><i class="fa fa-credit-card"></i>CVC Number</label><ins></ins>
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <input type="text" class="form-control" name="billingAddress" placeholder="Billing Address" value="{{ $model->billingAddress }}"/>
                                                            <label><i class="fa fa-map-marker"></i>Billing Address</label><ins></ins>
                                                        </div>

                                                        <div class="form-group col-sm-4">
                                                            <input type="text" class="form-control" name="postalCode" placeholder="Postal Code" value="{{ $model->postalCode }}"/>
                                                            <label><i class="fa fa-map-marker"></i>Postal Code</label><ins></ins>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group button-btn col-sm-4">
                                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                            <input type="submit" class="btn btn-primary w100" value="Update payment details" onclick="document.getElementByName('updateCard').submit()"> 
                                                        </div>
                                                        <div class="form-group button-btn col-sm-offset-6 col-sm-2">
                                                            <a href="{{url('payments')}}" class="btn btn-default"> Back</a> 
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var card = new Card({
        // a selector or DOM element for the form where users will
        // be entering their information
        form: 'form', // *required*
        // a selector or DOM element for the container
        // where you want the card to appear
        container: '.card-wrapper', // *required*
        formSelectors: {
            numberInput: 'input#cardNumber', // optional — default input[name="number"]
            expiryInput: 'input#expDate', // optional — default input[name="expiry"]
            cvcInput: 'input#cvc', // optional — default input[name="cvc"]
            nameInput: 'input#cardholderName' // optional - defaults input[name="name"]
        },
        width: 200, // optional — default 350px
        formatting: true, // optional - default true
        // Strings for translation - optional
        messages: {
            validDate: 'valid\ndate', // optional - default 'valid\nthru'
            monthYear: 'mm/yyyy'// optional - default 'month/year'
        },
        // Default placeholders for rendered fields - optional
        placeholders: {
            number: '•••• •••• •••• ••••',
            name: 'Cardholder Name',
            expiry: '••/••',
            cvc: '•••'
        },
        masks: {
            cardNumber: '•' // optional - mask card number
        },
        // if true, will log helpful messages for setting up Card
        debug: false // optional - default false
    });




</script>
@endsection                            
