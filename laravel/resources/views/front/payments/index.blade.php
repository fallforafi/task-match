@extends('layout')

@section('content')
<?php
//$card = 4242424242424242;
//$name = "Aftab Khan";
//$cvc = 123;
//$postalCode="74550";
//$billingAddress="Billing Address ".$postalCode;
$required = "required";
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">

                <div class="tab-content dboard-tab-height">
                    <div id="editPayment">
                        <div class="edit-pay-area">
                            <div class="tab-cont-title mb20 text-center">
                                <h3><i class="fa fa-credit-card"></i> Manage your payment methods</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="edit-pay-tab task-summ-area">
                                <ul class="nav nav-tabs ">
                                    <li class="{{ !isset($_GET['tabName']) || !empty($_GET['tabName']) && $_GET['tabName'] == 'makePayments' ? 'active' : '' }}"><a data-toggle="tab" href="#makePayments">Make payments</a></li>
                                    <li class="{{ !empty($_GET['tabName']) && $_GET['tabName'] == 'storeCards' ? 'active' : '' }}"><a data-toggle="tab" href="#storeCards">Stored cards</a></li>
                                    <li class="{{ !empty($_GET['tabName']) && $_GET['tabName'] == 'receivePay' ? 'active' : '' }}"><a data-toggle="tab" href="#receivePay">Receive payments</a></li>
                                    <li class="{{ !empty($_GET['tabName']) && $_GET['tabName'] == 'paymentsHistory' ? 'active' : '' }}"><a data-toggle="tab" href="#paymentsHistory">Payments history</a></li>
                                </ul>

                                <div class="tab-content task-tab-content">
                                    <div id="makePayments" class="tab-pane {{ !isset($_GET['tabName']) || !empty($_GET['tabName']) && $_GET['tabName'] == 'makePayments' ? 'active' : '' }}">
                                        <div class="review__box__desc">
                                            <p>When you are ready to accept a TaskRunner's offer, you will be required to pay the funds required for the task. The fee will be held securely until the task has been completed and the Task Poster releases the funds to the TaskRunner.</p>
                                        </div>
                                        <!--                                        <div class='card-wrapper' style="margin-top: -50px;height: 120px;"></div>-->
                                        <div class="add-payment">
                                            <div class="add__pay__title col-sm-6">
                                                <h3>Add your payment details</h3>
												</div> 
												<div class="add__pay__title col-sm-6">
												<img class="" src="front/images/payment-logo.jpg"/>
                                            </div>
                                            <div class="fnc-fom placelabeler" id="fancyForm">
                                                {!! Form::open(array( 'class' => 'form','id' => 'form','url' =>'card/store', 'name' => 'checkout')) !!}

                                                <div class="row">
                                                    <div style="display: none;" class="payment-errors alert alert-danger"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        {!! Form::text('cardNumber',null, [

                                                        'class'                         => 'form-control',
                                                        'id'                            => 'cardNumber',
                                                        'required'                      => 'required',
                                                        'data-stripe'                   => 'number',
                                                        'data-parsley-type'             => 'number',
                                                        'maxlength'                     => '19',
                                                        'data-parsley-trigger'          => 'change focusout',
                                                        'data-parsley-class-handler'    => '#cc-group'

                                                        ]) !!}


                                                        <label><i class="fa fa-credit-card"></i>Card Number</label><ins></ins>
                                                    </div>

                                                    <div class="form-group col-sm-4 ">

                                                        {!! Form::text('cardholderName',null, ['class'=> 'form-control','id' => 'cardholderName' ,'placeholder'=> 'Cardholder Name','required'=> 'required','maxlength'=>'30']) !!}
                                                        <label><i class="fa fa-user"></i>Cardholder Name</label><ins></ins></div>

                                                    <div class="form-group col-sm-4  expdate mt0">
                                                        <label><i class="fa fa-calendar"></i>Expiration Date</label><ins></ins>
                                                        <div class="clearfix"></div>
                                                        <div class="col-sm-6 pl0">
                                                            {!! Form::selectMonth('expMonth', null, ['class'=> 'form-control','id' => 'expMonth',$required,'data-stripe'=> 'exp-month'],'%m') !!}
                                                        </div>
                                                        <div class="col-sm-6 p0">
                                                            {!! Form::selectYear('expYear', date('Y'), date('Y') + 10, null, ['class'=>'form-control',$required,'data-stripe'=>'exp-year']) !!}
                                                        </div>


                                                    </div>
                                                </div>	
                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        {!! Form::text('cvc', null, ['class'=> 'form-control','id' => 'cvc',$required,'data-stripe'=> 'cvc','data-parsley-type'=> 'number','data-parsley-trigger'=>'change focusout','maxlength'=> '4','data-parsley-class-handler'=> '#ccv-group','placeholder'=> 'CVC Number']) !!}
                                                        <label><i class="fa fa-credit-card"></i>CVC Number</label><ins></ins>
                                                    </div>

                                                <div class="form-group col-sm-4">
                                                    {!! Form::text('billingAddress', null, ['class'=> 'form-control',$required,'maxlength'=> '50','placeholder'=> 'Billing Address']) !!}
                                                    <label><i class="fa fa-map-marker"></i>Billing Address</label><ins></ins>
                                                </div>

                                                <div class="form-group col-sm-4">
                                                    {!! Form::text('postalCode', null, ['class'=> 'form-control',$required,'maxlength'=> '6','placeholder'=> 'Postal Code']) !!}
                                                    <label><i class="fa fa-map-marker"></i>Postal Code</label><ins></ins>
                                                </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group button-btn col-sm-4">
                                                        <button   id="place_card" class="btn btn-primary w100">Save payment details</button>
                                                    </div>
                                                </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="storeCards" class="tab-pane {{ !empty($_GET['tabName']) && $_GET['tabName'] == 'storeCards' ? 'active' : '' }}">
                                        <div class="review__box__desc">
                                            <p>Once you have completed a task on TaskMatch, you will see a list of your completed payments here.</p>
                                        </div>
                                        @if(count($model) > 0)
                                        <div class="table-responsive">          
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Title</th>
                                                        <th>Number</th>
                                                        <th>Expiration Date</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($model as $row)
                                                    <tr>
                                                        <td><?php
                                                            echo $i;
                                                            $i++;
                                                            ?></td>
                                                        <td>{{ $row->cardholderName }}</td>
                                                        <td>{{ $row->cardNumber }}</td>                                         
                                                        <td>{{ date('m/Y', strtotime($row->expDate)) }}</td>
                                                        <td class="text-right"><a class="btn btn-primary" href="card/edit/{{ $row->id }}"><i class="fa fa-pencil"></i></a>
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal-{{ $row->id }}"><i class="fa fa-trash"></i> </button></td>

                                                <div class="modal fade" id="myModal-{{ $row->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header text-center">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span></button>
                                                                <h4>Alert</h4>
                                                            </div>
                                                            <div class="modal-body text-center">
                                                                <img src="{{asset('')}}/front/images/delete-icon.png" alt="logo" />
                                                                <h3>Are you sure do you want to delete this?</h3>
                                                                <a class="btn btn-danger" href="card/delete/<?php echo $row->id ?>">Yes</a>

                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                            </div>

                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @else
                                        <div class="col-sm-6">
                                            <h3>No card stored yet..</h3>
                                        </div>
                                        @endif
                                    </div>
                                    <div id="receivePay" class="tab-pane {{ !empty($_GET['tabName']) && $_GET['tabName'] == 'receivePay' ? 'active' : '' }}">
                                        <div class="review__box__desc">
                                            <p>When you are ready to accept a TaskRunner's offer, you will be required to pay the funds required for the task. The fee will be held securely until the task has been completed and the Task Poster releases the funds to the TaskRunner.</p>
                                        </div>
                                        <!--                                        <div id="receiveErrors"></div>-->
                                        <div class="add-payment">
                                            <div class="col-sm-12">
                                                @if($account == 1)
                                                <div class="alert alert-success">
                                                    <i class="fa fa-check"></i>                      
                                                    Connected ! 
                                                </div>
                                                @else
                                                <div class="alert alert-warning">
                                                    <i class="fa fa-warning"></i>                    
                                                    Please, connect with Stripe!   
                                                </div>
                                                @endif
                                                <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo env('STRIPE_CLIENT_ID'); ?>&scope=read_write" class="stripe-connect dark">
                                                    <img src="{{ asset('front/images/blue-on-light.png') }}">
                                                </a>
                                                <br>
                                                <strong>TaskMatch would like you to start accepting payments with stripe !</strong>
                                
                                            </div>
                                            <!--                                            <div class="add__pay__title col-sm-12">
                                                                                            <h3>Add your bank details</h3>
                                                                                        </div>
                                                                                        <div class="fnc-fom placelabeler" id="fancyForm">
                                                                                            <form name="receiveForm" class="receiveForm">
                                                                                                <div class="row">
                                            
                                                                                                    <div class="form-group col-sm-4">
                                                                                                        <input type="text" class="form-control" name="accountTitle" placeholder="Account Title" id="cardNumber" maxlength="19" value="{{ isset($details)?$details->accountTitle:'' }}"/>
                                                                                                        <label><i class="fa fa-user"></i>Account Title</label><ins></ins>
                                                                                                    </div>
                                            
                                                                                                    <div class="form-group col-sm-4">
                                                                                                        <input type="text" class="form-control" name="bic" placeholder="Business Identification Code" id="cardholderName" value="{{ isset($details)?$details->bic:'' }}" />
                                                                                                        <label><i class="fa fa-credit-card"></i>BIC</label><ins></ins>
                                                                                                    </div>
                                                                                                    <div class="form-group col-sm-4">
                                                                                                        <input type="text" class="form-control" name="iban" placeholder="IBAN" id="cardholderName" value="{{ isset($details)?$details->iban:'' }}" />
                                                                                                        <label><i class="fa fa-credit-card"></i>IBAN</label><ins></ins>
                                                                                                    </div>
                                                                                                </div>	
                                                                                                <div class="row">
                                            
                                                                                                    <div class="form-group col-sm-8">
                                                                                                        <input type="text" class="form-control" name="address" placeholder="Billing Address" value="{{ isset($details)?$details->address:'' }}"/>
                                                                                                        <label><i class="fa fa-map-marker"></i>Billing Address</label><ins></ins>
                                                                                                    </div>
                                                                                                    <div class="form-group col-sm-4">
                                                                                                        <input type="text" class="form-control" name="bankName" placeholder="Bank Name" id="cardholderName" value="{{ isset($details)?$details->bankName:'' }}" />
                                                                                                        <label><i class="fa fa-credit-card"></i>Bank Name</label><ins></ins>
                                                                                                    </div>
                                                                                                </div>
                                            
                                                                                                <div class="row">
                                                                                                    <div class="form-group button-btn col-sm-4">
                                                                                                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                                                                        <input type="button" class="btn btn-primary w100" value="Save payment details" onclick="saveReceive();">
                                                                                                    </div>
                                                                                                </div>
                                            
                                                                                            </form>
                                                                                        </div>-->
                                        </div>
                                        
                                    </div>
                                    <div id="paymentsHistory" class="tab-pane {{ !empty($_GET['tabName']) && $_GET['tabName'] == 'paymentsHistory' ? 'active' : '' }}">
                                        <div class="review__box__desc">
                                            <p>Once you have completed a task on TaskMatch, you will see a list of your completed payments here.</p>
                                        </div>
                                        <div class="db__tasks__table table-responsive">   
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#paymentsIn">Payments In</a></li>
                                                <li><a data-toggle="tab" href="#paymentsOut">Payments Out</a></li>
                                            </ul>
                                            <div class="tab-content task-tab-content">
                                                <div id="paymentsIn" class="tab-pane fade in active">
													@if(count($transactionsReceive) > 0)
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Type</th>
                                                                <th>Task</th>
                                                                <th>Amount</th>
                                                                <th>Payment Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                            @foreach($transactionsReceive as $row)
                                                            <tr>
                                                                <td>{{ $row->firstName }} {{ $row->lastName }}</td>
                                                                <td>Payment Receive</td>
                                                                <td>{{ $row->title }}</td>  
                                                                <td><?php echo $currency; ?> {{ $row->finalPrice }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @else                                     
                                                  <div class="col-sm-6">
                                            <h3>No data stored yet..</h3>
                                        </div>
                                        @endif
                                                </div>
                                                <div id="paymentsOut" class="tab-pane">
													  @if(count($transactionsSent) > 0)
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Type</th>
                                                                <th>Task</th>
                                                                <th>Amount</th>
                                                                <th>Payment Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                          
                                                            @foreach($transactionsSent as $row)
                                                            <tr>        
                                                                <td>{{ $row->firstName }} {{ $row->lastName }}</td>
                                                                <td>Payment Sent</td>
                                                                <td>{{ $row->title }}</td>  
                                                                <td><?php echo $currency; ?> {{ $row->amount }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                   @else                                     
                                                  <div class="col-sm-6">
                                            <h3>No data stored yet..</h3>
                                        </div>
                                        @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    function saveReceive() {
        var formdata = $(".receiveForm").serialize();
        $('#receiveErrors').html("").hide();
        $.ajax({
            url: "<?php echo url('receive-payment/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });
                    errorsHtml += '</ul></div>';
                    $('#receiveErrors').html(errorsHtml).show();
                } else
                {
                    $('#receiveErrors').html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-check"></i> Successfully Submitted!</div>').show();
                }
            },
            error: function (xhr, status, response) {
            }
        });
    }

    $('#form').submit(function (event) {
        var form = $('#form');
        $('.payment-errors').hide();
        form.find('#place_card').prop('disabled', true);
        Stripe.card.createToken(form, stripeResponseHandler);

        return false;
    });


    function stripeResponseHandler(status, response) {
        var form = $('#form');
        if (response.id) {
            var token = response.id;
            form.append($('<input type="hidden" name="stripeToken" />').val(token));
            form.get(0).submit();
        } else {
            $('.payment-errors').show();
            $('.payment-errors').text(response.error.message);
            $('.payment-errors').addClass('alert alert-danger');

            var scrollPos = form.offset().top;
            $(window).scrollTop(scrollPos);
            form.find('#place_card').prop('disabled', false);
            return false;

        }

    }
</script>

<!--<script>
    var card = new Card({
        // a selector or DOM element for the form where users will
        // be entering their information
        form: 'form', // *required*
        // a selector or DOM element for the container
        // where you want the card to appear
        container: '.card-wrapper', // *required*
        formSelectors: {
            numberInput: 'input#cardNumber', // optional — default input[name="number"]
            expiryInput: 'input#expMonth', // optional — default input[name="expiry"]
            cvcInput: 'input#cvc', // optional — default input[name="cvc"]
            nameInput: 'input#cardholderName' // optional - defaults input[name="name"]
        },
        width: 200, // optional — default 350px
        formatting: true, // optional - default true
        // Strings for translation - optional
        messages: {
            validDate: 'valid\ndate', // optional - default 'valid\nthru'
            monthYear: 'mm/yyyy'// optional - default 'month/year'
        },
        // Default placeholders for rendered fields - optional
        placeholders: {
            number: '•••• •••• •••• ••••',
            name: 'Cardholder Name',
            expiry: '••/••',
            cvc: '•••'
        },
        masks: {
            cardNumber: '•' // optional - mask card number
        },
        // if true, will log helpful messages for setting up Card
        debug: false // optional - default false
    });

</script>-->
@endsection                            
