@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">

                        <div class="edit-profile-area">
                            <div class="tab-cont-title tab-cont-title--2 text-center">
                                <h3>My Tasks Summary</h3>
                            </div>
                            <div class="task-summ-area">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#taskRunning">Task Running</a></li>
                                    <li><a data-toggle="tab" href="#tasksPosted">Tasks Posted</a></li>
                                </ul>
                                <div class="tab-content task-tab-content">
                                    <div id="taskRunning" class="tab-pane fade in active">
                                            @include('front.common.errors')
                                            <div class="db__tasks__table table-responsive">          
                                                <table class="table">
                                                    <thead>
                                                        <tr>                                          
                                                            <th>Title</th>
                                                            <th>Status</th>
                                                            <th>Due Date</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @foreach($model as $row)
                                                        <tr>
                                                            <td>{{ $row->title }}</td>
                                                            <td><div class="btn btn-default btn-{{ $row->taskStatus }}">{{ $row->taskStatus }}</div></td>
                                                            <td>{{ date('d/m/Y', strtotime($row->dueDate)) }}</td>
                                                            <td><a class="btn btn-primary" href="{{ url('my-tasks/edit/'.$row->id) }}"><i class="fa fa-pencil"></i></a>
                                                                <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('my-tasks/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <div class="modal fade" id="myModal">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" style="color: red">Alert</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h3>Are you sure do you want to delete this?</h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a id="closemodal" class="btn btn-danger pull-left" href="">Yes</a>

                                                                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                </table>
                                                <?php echo $model->render(); ?>
                                        </div>
                                    </div>
                                    <div id="tasksPosted" class="tab-pane fade">
                                            @include('front.common.errors')
                                            <div class="db__tasks__table table-responsive">          
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Status</th>
                                                            <th>Due Date</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @foreach($taskPosted as $row)
                                                        <tr>

                                                            <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>

                                                            <td>
                                                                <div class="btn btn-default btn-{{ $row->taskStatus }}">{{ $row->taskStatus }}</div>
                                                            </td>

                                                            <td>{{ date('d/m/Y', strtotime($row->dueDate)) }}</td>
                                                            <td class="text-right">

                                                                <?php if ($row->taskStatus == 'open') { ?>

                                                                    <a class="btn btn-primary" href="{{ url('my-tasks/edit/'.$row->id) }}"><i class="fa fa-pencil"></i></a>

                                                                    <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('my-tasks/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <div class="modal fade" id="myModal">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" style="color: red">Alert</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h3>Are you sure do you want to delete this?</h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a id="closemodal" class="btn btn-danger pull-left" href="">Yes</a>

                                                                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                </table>
                                                <?php //echo $taskPosted->render(); ?>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>
@endsection                            