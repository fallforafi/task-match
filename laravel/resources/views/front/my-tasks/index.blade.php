@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">

                        <div class="edit-profile-area">
                            <div class="tab-cont-title text-center mb30">
                                <h3><i class="fa fa-sticky-note"></i> My Tasks</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="task-summ-area nav-tabs-url-handler">
                                <ul class="nav nav-tabs">
                                    <li class="{{ !isset($_GET['tabName']) || !empty($_GET['tabName']) && $_GET['tabName'] == 'tasksRunning' ? 'active' : '' }}"><a data-toggle="tab" href="#tasksRunning">Task Running</a></li>
                                    <li class="{{ !empty($_GET['tabName']) && $_GET['tabName'] == 'tasksPosted' ? 'active' : '' }}"><a data-toggle="tab" href="#tasksPosted">Tasks Posted</a></li>
                                </ul>
                                <div class="tab-content task-tab-content">
                                    <div id="tasksRunning" class="tab-pane {{ !isset($_GET['tabName']) || !empty($_GET['tabName']) && $_GET['tabName'] == 'tasksRunning' ? 'active' : '' }}">

                                        <div class="db__tasks__table table-responsive">
											@if(count($bid_on) > 0 || count($taskAssigned) > 0 || count($taskCompleted) > 0)  
                                            <table class="table">
                                                <thead>
                                                    <tr>                                          
                                                        <th class="title col-sm-7">Title</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($bid_on as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>Bid On </td>
                                                        <td class="text-right">
															<a class="btn btn-primary" href="{{ url('my-tasks/amend-offer/' . $row->id) }}"><i class="fa fa-pencil"></i> Amend Offer</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskAssigned as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>Assigned</td>
                                                        <td class="text-right"><a class="btn btn-primary" href="{{ url('messages') }}"> Private Message</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskCompleted as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>Completed</td>
                                                        <td class="text-right"><a class="btn btn-success btn-green" href="{{ url('feedback/'.$row->id) }}" >Leave a Review</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @else
                                            <div class="col-sm-6">
                                            <h3>No data found..</h3>
                                        </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="tasksPosted" class="tab-pane {{ !empty($_GET['tabName']) && $_GET['tabName'] == 'tasksPosted' ? 'active' : '' }}">
                                        <div class="db__tasks__table table-responsive">
											@if(count($taskPostedOpen) > 0 || count($taskPostedCancelled) > 0 || count($taskPostedAssigned) > 0 || count($taskPostedCompleted) > 0 || count($taskPostedDrafts) > 0)          
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th class="title col-sm-7">Title</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($taskPostedOpen as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>
                                                            Open
                                                        </td>
                                                        <td class="text-right">
                                                            <?php if ($row->taskStatus == 'open') { ?>

                                                                <a class="btn btn-primary" href="{{ url('my-tasks/edit/'.$row->id) }}"><i class="fa fa-pencil"></i></a>

                                                                <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('my-tasks/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskPostedCancelled as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>
                                                            Cancelled
                                                        </td>
                                                        <td>
<!--                                                            <a class="btn btn-primary" href="{{ url('my-tasks/cancel/'.$row->id) }}"><i class="fa fa-remove"></i> Cancel</a>-->
                                                             <a class="btn btn-primary" href="{{ url('my-tasks/post-similar-task/'.$row->id) }}">Repost Similar Task</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskPostedAssigned as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>
                                                            Assigned
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-primary" href="{{ url('my-tasks/cancel/'.$row->id) }}"><i class="fa fa-remove"></i> Cancel</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskPostedCompleted as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>
                                                            Completed
                                                        </td>
                                                        <td> <a class="btn btn-success btn-green" href="{{ url('feedback/'.$row->id) }}" >Leave a Review</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @foreach($taskPostedDrafts as $row)
                                                    <tr>
                                                        <td><a href="task/{{ $row->key }}">{{ $row->title }}</a></td>
                                                        <td>
                                                            Draft
                                                        </td>
                                                        <td>

                                                            <?php if ($row->taskStatus == 'open') { ?>

                                                                <a class="btn btn-primary" href="{{ url('my-tasks/edit/'.$row->id) }}"><i class="fa fa-pencil"></i></a>

                                                                <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('my-tasks/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                                <div class="modal fade" id="myModal">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" style="color: red">Alert</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3>Are you sure do you want to delete this?</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a id="closemodal" class="btn btn-danger pull-left" href="">Yes</a>

                                                                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                            </table>
                                            @else
                                            <div class="col-sm-6">
                                            <h3>No data found..</h3>
                                        </div>
                                            @endif
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>
@endsection                            
