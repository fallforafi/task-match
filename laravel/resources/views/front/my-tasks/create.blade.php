@extends('layout')

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">
                        <div class="edit-profile-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-pencil-square-o"></i> Get free quotes now!</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="db__post__task col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#detailsDB">Details</a></li>
                                    <li><a data-toggle="tab" href="#locationDB" id="editLoc">Location</a></li>
                                    <li><a data-toggle="tab" href="#budgetDB">Budget</a></li>
                                    <li><a data-toggle="tab" href="#filesDB">Files</a></li>
                                </ul>
                                @include("front.my-tasks.form")
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection