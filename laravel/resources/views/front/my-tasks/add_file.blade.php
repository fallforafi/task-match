@extends('layout')

@section('content')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        min-height: 150px;
        max-height: 150px;
        border: 1px solid #ddd;
        border-radius: 4px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
    }
    #img-upload1{
        min-height: 150px;
        max-height: 280px;
        width:100%;
        padding: 10px 10px 10px 10px;
/*        padding-left: 10px;
        padding-right: 10px;*/
        border: 1px solid #ddd;
        border-radius: 4px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
    }
    .EditPrfTitle {
        padding: 8px 0px 8px;
        border: 2px solid rgba(0, 0, 0, .075);
    }

</style>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">
                        <div class="edit-profile-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-pencil-square-o"></i> Get free quotes now!</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="db__post__task col-sm-12">
                                {!! Form::open(array('files' => true,'class' => 'form','url' => 'my-tasks/save-file', 'method' => 'post', 'id' => 'taskFile')) !!}
                                <div class="text-center form-group" aria-hidden="0" id="select">
                                    <span class="btn btn-file btn-primary">
                                        Select<input type="file" uploader="uploader" id="imgInp" name="file"> 
                                    </span>
                                    <a onclick="back();" class="btn btn-default"> Back</a><br>
                                    <span> Pdf, Docx, jpg or jpeg</span>
                                    
                                </div>
                                <div class=" text-center form-group " aria-hidden="0" id="upload" style="display:none;">
                                    <input type="hidden" name="task_id" value="{{$id}}">
                                    <button type="submit" class="btn btn-primary btn-file" >Upload</button>
                                    <button type="button" class="btn btn-default btn-flat" id="cancel">Cancel</button>

                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        document.getElementById('imgInp').addEventListener('change', function () {
            var style = this.value === '' ? 'block' : 'none';
            var upload = this.value === '' ? 'none' : 'block';
            document.getElementById('select').style.display = style;
            document.getElementById('upload').style.display = upload;
        });

        $('#cancel').on('click', function () {
            document.getElementById('upload').style.display = 'none';
            document.getElementById('select').style.display = 'block';
        }
        );
    });

</script>
@endsection
