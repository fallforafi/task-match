<?php

use App\Categories;
use App\Functions\Functions;

$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$categoriesModel = Categories::get();
$categories = Functions::getChildCategories($categoriesModel);
$required = 'required';
?>
<style>
    ul#tree1 >li {
        border: 0px solid #ccc;
        padding: 5px;
    }
    ul#tree1 .product-title[href*="delete"] {
        color: #d00;
    }
    span.actions >a {
        background-color: #eee;
        display: inline-block;
        padding: 1px 8px;
        border-radius: 3px;
        margin: 0 0 10px 10px;
        font-size: 12px;
        border: 1px solid #ccc;
        line-height: 18px;
    }

</style>
<div class="postTask__edit__form">
    <form class="form" method="post" action="{{ url('my-tasks/update/'.$model->id) }}" id="edit" name="form">
        <div class="tab-content">
            <div id="errors"></div>
            <div id="detailsDB" class="tab-pane fade in active">
                <div class="dtl-box pt10">
                    <div class="form-group task-title-input hover-anchor">
                        <label for="taskTitle">
                            Task title
                            <a href="#" title="The Task Title is a short heading for your task, Eg, Cleaner needed for my home.">
                                <i class="fa fa-question-circle grey-icon"></i>
                                <i class="fa fa-check-circle green-icon"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ $model->title }}" />
                    </div>
                    <div class="form-group task-desc hover-anchor">
                        <label for="taskDesc">
                            Task Description
                            <a href="#" title="Please enter a Task Description, give as much detail as you can about your task. Eg, Clean my four bedroom apartment for two hours">
                                <i class="fa fa-question-circle grey-icon"></i>
                                <i class="fa fa-check-circle green-icon"></i>
                            </a>
                        </label>
                        <textarea class="form-control" rows="5" name="description" id="description">{{ $model->description }}</textarea>
                    </div>
                    <div class="need-help hover-anchor">
                        <a href="#" title="Please enter a Task Description, give as much detail as you can about your task. Eg, Clean my four bedroom apartment for two hours">Need more help &nbsp;<i class="fa fa-question-circle"></i></a>
                    </div>
                </div>
            </div>
            <div id="locationDB" class="tab-pane fade">
                <div class="post-location post__task__loc__db pt10">

                    <div class="inputs p0">
                        <div class="form-group hover-anchor w60">
                            <label for="taskLocation">
                                Task Location
                            </label>
                            <div id="test">
                                <input type="text" class="form-control autocomplete" name="location" placeholder="Location" onfocus="geolocate1()" value="{{$model->location}}" id="locality">

                                <input type="hidden" name="longitude"  id="longitude" value="">
                                <input type="hidden" name="latitude"  id="latitude" value="">
                            </div>
                        </div>
                        <div class="form-group w70">
                            <label for="taskLocation">
                                Due Date
                            </label>
                            <?php
                            if (isset($model->dueDate) && $model->dueDate != '') {
                                $dueDate = date('d-m-Y', strtotime($model->dueDate));
                            } else {
                                $dueDate = date('d-m-Y', strtotime("+1 week"));
                            }
                            ?>
                            <div class="task__hours">
                                <label>
                                    <input type="radio" name="chooseDate" id="chooseDate" class="today" >Today
                                </label>
                                <label>
                                    <input type="radio" name="chooseDate" id="chooseDate" class="certainDay" checked="checked">By a certain day
                                </label>
                                <label>
                                    <input type="radio" name="chooseDate" id="chooseDate" class="withinWeek">Within 1 week
                                </label>
                            </div>
                            <input type="text"  class="form-control todaydueDate" required="required" placeholder="Due Date" readonly="readonly" value="<?php echo date('d-m-Y'); ?>" style="display:none;">

                            <input type="text" name="dueDate" class="form-control certainDaydueDate" data-provide="datepicker" required="required" placeholder="Due Date" value="<?php echo $dueDate; ?>">

                            <input type="text" class="form-control withinWeekdueDate" required="required" placeholder="Due Date" readonly="readonly" value="<?php echo date('d-m-Y', strtotime("+6 days")); ?>" style="display:none;">
                        </div>
                        <!--                        <div class="form-group maxw150">
                                                    <label for="taskLocation">
                                                        Due Date
                                                    </label>
                       
                                                    <input type="text" class="form-control" id="datepicker" name="dueDate" data-provide="datepicker" value="{{ $dueDate }}" placeholder="Due Date" />
                                                </div>-->
                    </div>

                    <div class="form-group task-tobe-comp btn-effect--ripple col-sm-6" data-toggle="buttons">
                        <label for="taskToBeCompleted">
                            To be completed
                        </label>
                        <div class="post-loc-cmplt clrlist">
                            <ul>
                                <li>

                                    <a href="#" class="btn btn-primary {{ ($model->toBeCompleted == 'online')?'active':'' }}"><i class="fa fa-laptop"></i><input type="radio" name="toBeCompleted" id="toBeCompleted" value="online" {{ ($model->toBeCompleted == 'online')?'Checked':'' }}>Online task</a></li>

                                <li><a href="#" class="btn btn-primary {{ ($model->toBeCompleted == 'inperson')?'active':'' }}"><i class="fa fa-user"></i><input type="radio" name="toBeCompleted" id="toBeCompleted" value="inperson" {{ ($model->toBeCompleted == 'inperson')?'Checked':'' }}>In person task</a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="form-group w50">
                        <label for="taskLocation">
                            Categories
                        </label>
                        <div class="dropdown post-task-ctg">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Choose Category
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu btn-effect--ripple alist" id="ctgAdding" data-toggle="buttons">
                                <?php //d($tasksCategories,1); ?>
                                <?php foreach ($categoriesModel as $cat) { ?>
                                    <?php
                                    if (!$cat->parent_id == 0) {
                                        ?>
                                        <li class="btn btn-primary <?php if (in_array($cat->id, $tasksCategories)) { ?> active  <?php } ?>" id="{{ $cat->id }}"><i class="{{ $cat->class }}"></i><input type="checkbox" id="ctgChecked" value="{{ $cat->id }}" id="check_1" name="categories[]" <?php if (in_array($cat->id, $tasksCategories)) { ?> checked="checked"  <?php } ?>/>{{ $cat->name }}</li>                                       
                                    <?php } ?>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group w50 pul-rgt">
                        <div class="post-task-sum">
                            <label for="taskSummary">
                                Summary
                            </label>
                            <div class="clrlist blist">
                                <ul>
                                    <?php foreach ($categoriesModel as $cat) { ?>
                                        <?php
                                        if (!$cat->parent_id == 0) {
                                            ?>
                                            <li class="btn btn-primary {{ $cat->id }}" data-id="{{ $cat->id }}"><i class="{{ $cat->class }}"></i><input type="checkbox" id="ctgChecked" value="Home & Cleaning" id="check_1"/> {{ $cat->name }}</li>                                       
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div id="budgetDB" class="tab-pane fade">
                <div class="budget-box">
                    <div class="bud__lft w50">
                        <div class="form-group w75 hover-anchor">

                            <div class="task__hours">
                                <label>
                                    <input type="radio" name="priceType" id="priceType" class="total" value="total" {{ ($model->priceType == 'total')?'Checked':'' }}>Total
                                </label>
                                <label>
                                    <input type="radio" name="priceType" id="priceType" class="hours" value="hourly" {{ ($model->priceType == 'hourly')?'Checked':'' }}>Per Hour
                                </label>
                            </div>

                            <label for="budgetPrice" id="total">
                                Total Price
                                <a href="#" title="Tip - Always offer a fair price as you will receive higher quality interest quicker">
                                    <i class="fa fa-question-circle grey-icon"></i>
                                </a>
                            </label>
                            <label for="budgetPrice" id="hourly" style="display:none;">
                                Per Hour Price
                                <a href="#" title="Tip - Always offer a fair price as you will receive higher quality interest quicker">
                                    <i class="fa fa-question-circle grey-icon"></i>
                                </a>
                            </label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                <input type="text" id="price" name="price" class="form-control" value="{{ $model->price }}" placeholder="Budget Price" aria-describedby="basic-addon1">
                                <input type="text" id="hourlyRate" name="hourlyRate" class="form-control" value="{{ $model->hourlyRate }}" placeholder="Hourly Price" aria-describedby="basic-addon1">
                            </div><br>
                            <label class="getPaidLabel" style="display:none;">
                                Estimated Budget
                            </label>
                            <div class="input-group getPaid" style="display:none;">
                                <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                <input type="number" class="form-control" id="getPaid" value="0" placeholder="Get Paid" disabled="disabled">
                            </div>
                            <?php if ($model->hours == 0) { ?>
                                <div class="input-group add" style="display:none;">
                                <?php } else { ?>
                                    <div class="input-group add" style="">
                                    <?php } ?>
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-clock-o">
                                        </i></span>
                                    <input type="number" name="hours" class="form-control" id="hours" placeholder="Add Hours" value="{{($model->hours == '')?'0':$model->hours}}">

                                </div>
                            </div>
                            <br>
                            <!--                            <label class="getPaidLabel">
                                                            Estimated Budget
                                                        </label>
                                                        <div class="input-group getPaid">
                                                            <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                                            <input type="number" class="form-control" id="getPaid" value="{{ $model->price }}" placeholder="" disabled="disabled">
                                                        </div>-->
                            <div class="need-help hover-anchor--2">
                                <a class="brs-ctg-nav1">Price Guide <i class="fa fa-question-circle"></i></a>
                            </div>
                            <div class="brs__filt__box1 sel " style="display:none;" data-toggle="buttons">
                                <div class="clearfix"></div>
                                <div class="brs__task__sts well col-sm-12">
                                    <select class="form-control" id="cat">
                                        <option value="0">Choose Category</option>
                                        <?php foreach ($categoriesModel as $cat) { ?>
                                            <?php
                                            if (!$cat->parent_id == 0) {
                                                ?>
                                                <option value="{{ $cat->id }}"> {{ $cat->name }}</option>                                        
                                                <?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>
                                    </select> 
                                    <div class="text-left" id="data">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="filesDB" class="tab-pane fade">              
                    <br>
                    <a href="{{ url('my-tasks/add-file/'. $model->id )}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> New</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($files)>0)
                            @foreach($files as $row)
                            <tr>
                                <td>{{ $row->file }}</td>                           
                                <td>
                                    <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="<?php echo url('my-tasks/file/delete/' . $row->id); ?>"><i class="fa fa-trash"></i> </button>

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td>
                                    No files to show...</td>
                            </tr>
                            @endif
                        </tbody>
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" style="color: red">Alert</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h3>Are you sure do you want to delete this?</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <a id="closemodal" class="btn btn-danger pull-left" href="{{ url('') }}">Yes</a>

                                        <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </table>
                    <?php //echo $files->render(); ?> 
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="actions">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-success post-task-next" onclick="" value="Update">
                <?php if ($model->draft == '1') { ?>
                    <input type="submit" class="btn btn-success post-task-next" name="publish" value="Update & Publish">
                    <a href="<?php echo url('my-tasks'); ?>" class="btn btn-danger post-task-back"> Cancel</a>
                <?php } else { ?>
                    <a href="<?php echo url('my-tasks'); ?>" class="btn btn-danger post-task-back"> Cancel</a>
                <?php } ?>
            </div>

    </form>
</div>
<script>
    $('.need-help a.brs-ctg-nav1').click(function () {
        $('.sel').toggle(".sel");
    });

    $('.alist li.active').each(function (i) {
        $(this).toggleClass("is-select");
        var nValue = $(this).attr("id");

        if ($(".blist li").hasClass(nValue)) {
            $(".blist li" + "." + nValue).addClass("showMe");
        }
    });

    $(document).on("click", ".alist li", function () {
        $(this).toggleClass("is-select");
        var nValue = $(this).attr("id");

        if ($(".blist li").hasClass(nValue)) {
            $(".blist li" + "." + nValue).addClass("showMe");
        }
    });

    $(document).on("click", ".alist li.is-select", function () {
        //$('.alist li.is-select').click(function(){
        var rValue = $(this).attr("id");
        //alert(rValue);
        $(".blist li" + "." + rValue).removeClass("showMe");
    });
    $('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
    $('.today').on('change', function () {
        jQuery('.todaydueDate').fadeIn("fast").css("display", "");
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', 'dueDate');
        jQuery('.certainDaydueDate').attr('name', '');
        jQuery('.withinWeekdueDate').attr('name', '');
    });
    $('.certainDay').on('change', function () {
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "");
        jQuery('.todaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', '');
        jQuery('.certainDaydueDate').attr('name', 'dueDate');
        jQuery('.withinWeekdueDate').attr('name', '');
    });
    $('.withinWeek').on('change', function () {
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "");
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', '');
        jQuery('.certainDaydueDate').attr('name', '');
        jQuery('.withinWeekdueDate').attr('name', 'dueDate');
    });
</script>
<script>
    $(document).ready(function () {
        //initAutocomplete1();
        $("li a#editLoc").on('click', function () {
            initAutocomplete1();
        });
    });
    var placeSearch;
    var autocomplete = {};
    var autocompletesWraps = ['test', 'test2'];
    var test_form = {locality: 'long_name'};
    var test2_form = {locality: 'long_name'};
    function initAutocomplete1() {
        $.each(autocompletesWraps, function (index, name) {
            if ($('#' + name).length === 0) {
                return;
            }
            autocomplete[name] = new google.maps.places.Autocomplete($('#' + name + ' .autocomplete')[0], {types: ['geocode']});
            autocomplete[name].setComponentRestrictions(
                    {'country': ['ie']});
            google.maps.event.addListener(autocomplete[name], 'place_changed', function () {
                var place = autocomplete[name].getPlace();
                var form = eval(name + '_form');
                //document.getElementById("location").value = place.name;

                document.getElementById("latitude").value = place.geometry.location.lat();
                document.getElementById("longitude").value = place.geometry.location.lng();

                for (var component in form) {
                    $('#' + name + '#' + component).val('');
                    $('#' + name + '#' + component).attr('disabled', false);
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (typeof form[addressType] !== 'undefined') {
                        var val = place.address_components[i][form[addressType]];
                        $('#' + name + '#' + addressType).val(val);
                    }
                }


            });
        });
    }
    function geolocate1() {
        //initAutocomplete1();
//        if (navigator.geolocation) {
//            navigator.geolocation.getCurrentPosition(function (position) {
//                var geolocation = {
//                    lat: position.coords.latitude,
//                    lng: position.coords.longitude
//                };
//                var latitude = position.coords.latitude;
//                var longitude = position.coords.longitude;
//                document.getElementById("latitude").value = latitude;
//                document.getElementById("longitude").value = longitude;
//
//            });
//        }
    }
</script>
<script>
    $('#data').html('<p>No data Found...</p>');
    $('#cat').on('change', function () {
        $.ajax({
            type: "GET",
            url: "<?php echo url('/cat/get/'); ?>/" + this.value,
            data: "",
            async: true
        }).success(function (val) {
            var response = JSON.parse(val);
            if (response === null) {
                var html = "<p>No data Found...</p>";
                $('#data').html(html);
            } else {
                if (response.description === '') {
                    var html = "<p>No data Found...</p>";
                    $('#data').html(html);
                } else {
                    var html = response.description;
                    $('#data').html(html);
                }
            }

        });
    });
    $("#hourlyRate, #hours").keyup(function (e) {
        setTotalPerhour();
    });
    $("#hourlyRate, #hours").change(function (e) {
        setTotalPerhour();
    });
    $("#price").keyup(function (e) {
        setTotalPrice();
    });
    $("#price").change(function (e) {
        setTotalPrice();
    });
    function setTotalPerhour()
    {
        var hourlyRate = $("#hourlyRate").val();
        var hours = $("#hours").val();
        var getPaid;
        getPaid = hourlyRate * hours;
        $("#getPaid").val(getPaid);
    }
    function setTotalPrice()
    {
        var price = $("#price").val();
        $("#getPaid").val(price);
    }
</script>  
<script>

    $(document).ready(function () {
        jQuery('#title').keyup(function () {
            var mintitleTxtChar = 10;
            if (document.form.title.value.length !== mintitleTxtChar)
            {
                jQuery(this).parent("div").removeClass("title-valid");
                document.form.title.focus();
            }
            if (document.form.title.value.length > mintitleTxtChar)
            {
                jQuery(this).parent("div").addClass("title-valid");
            }
        });
        jQuery('#description').keyup(function () {
            var mindescriptionTxtChar = 25;
            if (document.form.description.length !== mindescriptionTxtChar)
            {
                jQuery(this).parent("div").removeClass("title-valid");
                document.form.description.focus();
            }
            if (document.form.description.value.length > mindescriptionTxtChar)
            {
                jQuery(this).parent("div").addClass("title-valid");
            }
        });

        $('.checkall').on('change', function () {
            $(".sub_cat_" + this.id).prop('checked', $(this).prop('checked'));
        });
    });

    $(document).ready(function () {
        $(".brs__task__sts button input:radio:checked").parent('button').addClass("active");
    });
	
    $(document).ready(function () {
        jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
        $('.hours').on('change', function () {
            $("#getPaid").val(0);
            jQuery('.add').fadeIn("fast").css("display", "");
//            jQuery('.getPaid').fadeIn("fast").css("display", "");
//            jQuery('.getPaidLabel').fadeIn("fast").css("display", "");
            jQuery('#hourly').fadeIn("fast").css("display", "");
            jQuery('#hourlyRate').val('');
            jQuery('#hours').val('');
            jQuery('#total').fadeIn("fast").css("display", "none");
            jQuery('#price').fadeIn("fast").css("display", "none");
            jQuery('#price').val('0');
            jQuery('#hourlyRate').fadeIn("fast").css("display", "");
        });
        $('.total').on('change', function () {
            $("#getPaid").val(0);
            jQuery('.add').fadeIn("fast").css("display", "none");
//            jQuery('.getPaid').fadeIn("fast").css("display", "none");
//            jQuery('.getPaidLabel').fadeIn("fast").css("display", "none");
            jQuery('#hourly').fadeIn("fast").css("display", "none");
            jQuery('#total').fadeIn("fast").css("display", "");
            jQuery('#price').fadeIn("fast").css("display", "");
            jQuery('#price').val('');
            jQuery('#hourlyRate').val('0');
            jQuery('#hours').val('0');
            jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
        });
        $('.checkall').on('change', function () {
            $(".sub_cat_" + this.id).prop('checked', $(this).prop('checked'));
        });
    });



</script>


<script>
    $('#data').html('<p>No data Found...</p>');
    $('#cat').on('change', function () {
        $.ajax({
            type: "GET",
            url: "<?php echo url('/cat/get/'); ?>/" + this.value,
            data: "",
            async: true
        }).success(function (val) {
            var response = JSON.parse(val);
            if (response === null) {
                var html = "<p>No data Found...</p>";
                $('#data').html(html);
            } else {
                if (response.description === '') {
                    var html = "<p>No data Found...</p>";
                    $('#data').html(html);
                } else {
                    var html = response.description;
                    $('#data').html(html);
                }
            }

        });
    });
    jQuery('[data-provide="datepicker"]').datepicker({
        format: "dd-mm-yyyy"
    }).on('change', function () {
        jQuery('.datepicker').hide();
    });
</script>  
