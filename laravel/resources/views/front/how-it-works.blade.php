@extends('layout')
@section('content')
<?php
$title = "How-it-Works";
$description = '';
$keywords = '';
?>
@include('front/common/meta')
<section class="work-bnr-area clrlist">
    <div class="container">
        <div class="work-bnr-box col-sm-4">
            <div class="work__bnr__inr">
                <div class="work__bnr__img work__bnr__img--1">
                    <div class="work__bulb work__bnr__img__abs anime-down"><img alt="idea bulb" src="front/images/ideaBulb.png" /></div>

                    <div class="work__chair work__bnr__img__abs anime-left"><img alt="chair icon" src="front/images/chairpic.png" /></div>

                    <div class="work__paint work__bnr__img__abs anime-left"><img alt="paint" src="front/images/paint.png" /></div>

                    <div class="work__hammer work__bnr__img__abs anime-right"><img alt="hammer" src="front/images/hammerPic.png" /></div>

                    <div class="work__boxMove work__bnr__img__abs anime-left"><img alt="box" src="front/images/boxMove.png" /></div>

                    <div class="work__womanTask work__bnr__img__abs anime-in"><img alt="woman task" src="front/images/womanTask1.png" /></div>

                    <div class="work__camera work__bnr__img__abs anime-right"><img alt="camera" src="front/images/cameraAnim.png" /></div>

                    <div class="work__laptop work__bnr__img__abs anime-right"><img alt="laptop" src="front/images/laptopAnim.png" /></div>
                </div>

                <div class="work__bnr__cont">
                    <h4 class="anime-left">What do you need done?</h4>

                    <p class="anime-up">Describe the task you need completed and whether you need it done in person or online. Post any task from handyman to web design in just two minutes &ndash; for free! There&#39;s no obligation to hire.</p>
                </div>
            </div>
        </div>

        <div class="work-bnr-box delay1s col-sm-4">
            <div class="work__bnr__inr">
                <div class="work__bnr__img work__bnr__img--2">
                    <div class="work__woman__msr work__bnr__img__abs--two anime-down"><img alt="idea bulb" src="front/images/womanMeasure.png" /></div>

                    <div class="work__womanTask--two work__bnr__img__abs--two anime-left"><img alt="chair icon" src="front/images/womanTask2.png" /></div>

                    <div class="work__drill work__bnr__img__abs--two anime-in"><img alt="drill" src="front/images/drill.png" /></div>

                    <div class="work__manTool work__bnr__img__abs--two anime-right"><img alt="hammer" src="front/images/manTools2.png" /></div>
                </div>

                <div class="work__bnr__cont">
                    <h4 class="anime-left">Choose the best Worker for your Task</h4>

                    <p class="anime-up">View verified profiles and reviews to pick the best TaskMatch Worker for the task. After you accept an offer, you will need to add funds securely via TaskMatch Pay, which are held until completion. You can then private message and call the TaskMatch Worker.</p>
                </div>
            </div>
        </div>

        <div class="work-bnr-box delay2s col-sm-4">
            <div class="work__bnr__inr">
                <div class="work__bnr__img work__bnr__img--3">
                    <div class="work__msr__tool work__bnr__img__abs--three anime-right"><img alt="paint" src="front/images/womanMeasureToolMan3.png" /></div>

                    <div class="work__womanTask--three work__bnr__img__abs--three anime-left"><img alt="chair icon" src="front/images/womanTask3.png" /></div>

                    <div class="work__ratings work__bnr__img__abs--three anime-right clrlist">
                        <ul>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                        </ul>
                    </div>
                </div>

                <div class="work__bnr__cont delay2s">
                    <h4 class="anime-left">Task Completed!</h4>

                    <p class="anime-up">Once your task is completed you can release the task funds held with TaskMatch Pay. When this has be done, you can leave a review for the TaskMatch Worker to help improve our Community.</p>
                </div>
            </div>
        </div>

        <div class="clearfix">&nbsp;</div>
    </div>
</section>

<section class="category-area task-area">
    <div class="container">
        <div class="sect__title text-center anime-left col-sm-12">
            <h3>Choose from Categories</h3>

            <p>You can get anything done on TaskMatch, whether it&#39;s installing lights in your home or help moving house. Simply describe a task you would like to have done, set a budget and start receiving offers from TaskMatch workers</p>
        </div>

        <div class="task-main col-sm-12">
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=2') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task1.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Home & Cleaning</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=7') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task2.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Delivery & Removals</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=8') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task3.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Handyman & Trades</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=9') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task4.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Marketing & Design</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=11') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task5.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Events & Photography</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=5') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task6.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Computer & IT</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=10') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task7.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Business & Admin</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task8.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Fun & Quirky</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="lnk-btn inline-block find-btn text-center col-sm-12"><a href="find-out-more">Find out more</a></div>
        </div>
    </div>
</section>

<section class="verified-area">
    <div class="container">
        <div class="verf__lft valigner col-sm-7">
            <div class="verf__lft__cont valign">
                <h3>Choose from verified workers</h3>

                <p>Task Runners are waiting to complete tasks, you will receive offers within minutes.</p>

                <p>Review TaskMatch workers skills and completed work. You can also view worker profiles and ask questions about their necessary experience.</p>

                <p>Find out more information from workers on why they are perfect for your task. Once you have chosen your TaskMatch worker, you can reach out and discuss your task further.</p>
            </div>
        </div>

        <div class="verf__rgt col-sm-5">
            <ul>
                <li class="anime-right"><img alt="chat" src="front/images/chat11.png" /></li>
                <li class="anime-left"><img alt="chat" src="front/images/chat12.png" /></li>
                <li class="anime-right delay1s"><img alt="chat" src="front/images/chat13.png" /></li>
                <li class="anime-left delay2s"><img alt="chat" src="front/images/chat14.png" /></li>
                <li class="anime-right delay2s"><img alt="chat" src="front/images/chat15.png" /></li>
            </ul>
        </div>
    </div>
</section>

<section class="location-area">
    <div class="container">
        <div class="loc__box col-sm-6">
            <div class="loc__cont anime-in">
                <h4 class="anime-left">Near to you</h4>

                <p class="anime-left">Find workers in your local area available to complete your Task. All tasks are posted to our map to allow users to search for tasks close to them.</p>

                <p class="anime-left">Tasks are available all over Ireland. TaskMatch allows users to filter the tasks they want based on their location.</p>
            </div>
        </div>

        <div class="loc__img anime-right col-sm-6">
            <div class="loc__img__inr"><img alt="map task" src="front/images/mapTasks.png" /></div>

            <div class="loc__img__pin anime-down delay2s"><img alt="map pin" src="front/images/map-pin.png" /></div>
        </div>
    </div>
</section>

<section class="work-pay-area">
    <div class="container">
        <div class="pay__lft col-sm-6">
            <h3>Secure Payment</h3>

            <p>TaskMatch Pay is a quick and secure way to get your tasks completed. Once an offer is accepted on a task, the agreed upon amount will be securely held in a TaskMatch Trust Account until the task is completed. When the task is completed, you&rsquo;ll need to mark the task as completed and the funds will be transferred to their bank account. Make sure you have this set up before you start completing tasks.</p>

            <div class="pay__lft__icons">
                <div class="pay__lft__icon__img"><img alt="card icon" src="front/images/cardIcon.png" /></div>

                <div class="pay__lft__icon__img--2"><img alt="pin icon" src="front/images/PinIcon.png" /></div>
            </div>
        </div>
    </div>
</section>

@endsection