<?php if (count($model) > 0) { ?>
    @foreach($model as $row)
    <div class="col-sm-6">
        <div class="media user__review pt10 pb10">
            <a class="pull-left" href="#">
                <div class="brs__postBy__img"> @include('front/common/profile_picture')</div>
            </a>
            <div class="media-body">
                <small><a href="{{url('user/')}}/{{$row->key}}"> {{$row->firstName}} {{$row->lastName}}</a></small>
                <small>{{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>
                <div class="comment">{{$row->comment}}</div>  
                <div class="prof__rate clrlist">
                    <ul>
                        @for($i=1;$i<=$row->rating;$i++)
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        @endfor
                        @for($i=5;$i>$row->rating;$i--)
                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                        @endfor
                    </ul>                                            
                </div>   
            </div>
        </div>
    </div>



    @endforeach
<?php } else { ?>
    <div class="alert now-closed">No reviews added yet</div>
<?php
} 
