<div class="brs__rgt__commt">
    <?php // d($task,1); ?>

    <div class="hed crossline mini"><h2>Comments about this task <span class="badge">{{count($model)}}</span></h2><hr></div>

    <ul class="list-group">
        @foreach($model as $row)
        <li class="list-group-item">
            <div class="comment__user clrlist  ">
                <ul>
                    <li class="img">@include('front/common/profile_picture')</li>
                    <li class="username"><small><a href="{{url('user/')}}/{{$row->key}}">{{$row->firstName}} {{$row->lastName}}</a></small></li>
                    <li class="time">
                        <small class="ng-binding">{{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>
                    </li>
                </ul>
            </div>
            <div class="comment__cont">
                <p>{{$row->comment}}</p>
                @if(isset($row->file) && $row->file != '')
                <a href="{{ url('uploads/comments/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Uploaded file or image"><i class="fa fa-download" aria-hidden="true"></i></a>
                @endif
            </div>
        </li>
        @endforeach
    </ul>
    <?php if (isset($taskStatus) && $taskStatus != 'completed' && isset(Auth::user()->id)) { ?>
        <form class="addacomment-area col-sm-12" id="commentForm" action="<?php echo url('comments/store'); ?>" enctype="multipart/form-data" method="POST">
            <h3>Add new comment</h3>
            <div id="error_comment"></div>
            <div class="form-group addCommentText ">
                <textarea class="form-control mb10" onblur="textCounter(this, this.form.counter, 500);" onkeyup="textCounter(this, this.form.counter, 500);" id="comment" name="comment" placeholder="add a comment"></textarea>

                <span class="textcounter col-sm-3 p0 pull-right">
                    <input onblur="textCounter(this.form.recipients, this, 500);" disabled  onfocus="this.blur();" tabindex="999" maxlength="3" size="3" value="500" name="counter" class="mb0 w100"><small>characters remaining.</small></span>
            </div>
            <input type="file" name="file">
            <div class="form-group button-btn pul-rgt mt20">
                <input type="hidden" name="task_id" value="<?php echo $task_id; ?>">
                <input type="hidden" name="user_id" value="<?php echo Auth::user()->id; ?>">
                <input type="hidden" name="_token" value="{{csrf_token()}}" id="_token">
                <input type="button" id="commentBtn" class="form-control upload-image" value="Add a comment">
                <div class="commentLoading text-center" style="display: none;">
                    <!--p><img src="{{ asset('front/images/loader.gif') }}">Loading..</p>-->
					<div class="listing-loading"></div>
                </div>
            </div>
        </form>
    <?php } else { ?>
        <?php if (isset($taskStatus) && $taskStatus != 'completed' && !isset(Auth::user()->id)) { ?>
            <div class="text-center button-btn">
                <h4>To join the conversation</h4>
                <a href="{{url('signup')}}" class="btn btn-success">Join TaskMatch </a><span class="padder"> Or </span><button data-toggle="modal" data-target="#signIn" class="btn btn-success"> Sign In</button>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<script src="{{ asset('front/extralized/malsup.github.io-jquery.form.js') }}"></script>
<script type="text/javascript">
                    $('[data-toggle="tooltip"]').tooltip();
                    $('#commentBtn').click(function () {
                        $('.commentLoading').show();
                        $('#commentBtn').hide();
                        setTimeout(function () {
                            $("#commentForm").ajaxSubmit(options);
                            $('.commentLoading').hide();
                            $('#commentBtn').show();
                        }, 500);
                    });
//                    $("body").on("click", ".upload-image", function (e) {
//                        $('#error_comment').html("").hide();
//                        $("#commentForm").ajaxSubmit(options);
//                    });
                    var options = {
                        dataType: 'json',
                        async: false,
                        success: function (response)
                        {
                            if (response.error === 1) {
                                var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                                $.each(response.errors, function (key, value) {
                                    errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                                });
                                errorsHtml += '</ul></div>';
                                $('#error_comment').html(errorsHtml).show();
                            } else {
                                getComments();
                            }
                        }
                    };
</script>
<script>
    function textCounter(field, countfield, maxlimit) {
        if (field.value.length > maxlimit) {
            field.value = field.value.substring(0, maxlimit);
            field.blur();
            field.focus();
            return false;
        } else {
            countfield.value = maxlimit - field.value.length;
        }
    }

</script>
