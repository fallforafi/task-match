<?php
if (count($model) > 0) {
    $currencies = Config::get('params.currencies');
    $currency = $currencies[Config::get('params.currency_default')]['symbol'];
    ?>
    <?php
    foreach ($model as $task) {
        ?>
        <li class="">
            <a onclick="task('<?php echo $task->key; ?>')" href="#_" id="<?php echo url('task/' . $task->key); ?>">
                <div class="brs__task__inr">
                    <div class="brs__task__usr p0 col-sm-3">
                        <div class="brs__usr__img">
                            <?php if ($task->image != "") {
                                ?>                  <img src="<?php echo asset('/uploads/users/profile/') ?>/<?php echo $task->image; ?>"
                                     title="<?php echo $task->firstName . " " . $task->lastName ?>" />
                                 <?php } else { ?>
                                <img src="<?php echo asset(''); ?>front/images/default.png" >
                            <?php } ?>
                        </div>
                    </div>
                    <div class="brs__task__desc col-sm-6">
                        <h4><?php echo $task->title ?></h4>
                        <h6><i class="fa fa-map-marker"></i> <?php echo $task->location ?></h6>
                        <small><span><?php echo date('M d, Y', strtotime($task->created_at)) ?></span></small>
                    </div>
                    <div class="brs__task__price p0 col-sm-3">
                        <h2><?php echo $currency . ' ' . $task->price ?></h2>
                        <?php
                        if ($task->taskStatus == "open") {
                            ?>
                            <button class="brs__apply">Apply now</button>
                            <?php
                        } elseif ($task->taskStatus == "assigned") {
                            ?>
                            <button class="brs__apply bg-orange">Assigned</button>
                            <?php
                        } elseif ($task->taskStatus == "completed") {
                            ?>
                            <button class="brs__apply bg-red">Completed</button>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </a>
        </li>
    <?php }
    ?>
    <div><?php echo $model->render(); ?></div>
    <script>
        $(document).ready(function () {
            $('ul.pagination').hide();
        });
    </script>
<?php } ?>
<script>
    $(document).ready(function () {
        taskLink = jQuery('.brs__tasks__box ul li a');
        //jQuery(taskLink).parent("li:first").addClass("active");
        jQuery(taskLink).on('click',function () {
           // window.location.reload();
            url = window.location;
            //$('.brs__tasks__box a[href="' + url + '"]').parent("li").addClass('active');
            $('#brs-rgt-map').css('display', 'none');
            $("#rgt__map1").css('display', 'none');
            $("#rgt__map2").css('display', 'inline-block');
            $(".brs__tasks__box ul li").removeClass("active");
            jQuery(this).parent("li").addClass("active");
            jQuery(".brs-rgt").addClass("is-active");
			
			jQuery("body").addClass("task-detail-open");
			
			jQuery(".brs-area-box").addClass("detail-active");

        });

    });
</script>