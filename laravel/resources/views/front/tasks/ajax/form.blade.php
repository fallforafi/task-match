<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" id="postDraft">
        <div class="modal-header">
            <button type="button" class="close refresh" onclick="pageReload()">&times;</button>
            <h3 class="modal-title">Get free quotes now!</h3>
        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class="active" id="de"><a data-toggle="tab" href="#details">Details</a></li>
                <li id="lo" style="pointer-events: none;"><a data-toggle="tab" id="loc">Location</a></li>
                <li id="bud" style="pointer-events: none;"><a data-toggle="tab" id="budg">Budget</a></li>
            </ul>
            @include('front.tasks.form')
        </div>

    </div>
    <!-- Modal content-->
    <div class="modal-content" id="draftSuccess" style="display: none;">
        <div class="modal-header">   
            <button type="button" class="close refresh" onclick="pageReload()">&times;</button>
            <h3 class="modal-title text-center">Saved as a Draft Task</h3>
        </div>
        <div class="modal-body">
            <p>If you need to post this task later we have saved it for you in your <a href="{{ url('my-tasks') }}">My Tasks</a> area as a Draft.</p>
            <br><p>You can go back and edit the task at any time.</p><br>
            <button type="button" class="btn btn-success btn-block text-center" onclick="pageReload()"><h4>OK</h4></button>
        </div>

    </div>

</div>
<script>
    $(document).ready(function () {
               // initAutocomplete();
    });
</script>
