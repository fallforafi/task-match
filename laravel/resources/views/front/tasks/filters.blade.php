<div class="brs-ctg clrlist">
    <ul class="brs-ctg-nav">
        <li><button class="brs__ctg__srch"><i class="fa fa-search"></i> Search</button></li>
        <li><button class="brs__ctg__filt"><i class="fa fa-cog"></i> Filters</button></li>
        <li id="rgt__map1" style="display: none;"><button class="rgt__map1"><i class="fa fa-remove"></i> Hide map</button></li>
        <li id="rgt__map2"><button class="rgt__map2"><i class="fa fa-map-marker"></i> Show map</button></li>

    </ul>
    <form id="filter">
        <div class="brs__srch__box well" style="display:none;">
            <p>Start typing to search for tasks</p>
            <input type="text" class="form-control ng-pristine ng-untouched ng-valid" placeholder="Search for tasks..." id="filter_search" name="filter_search">
        </div>
        <div class="brs__filt__box well" style="display:none;" data-toggle="buttons">
            <div class="brs__task__sts col-sm-6">
                <p>Task Status</p>
                <button class="btn btn-primary"><input type="radio" name="task_status" id="task_status" value="" checked="checked">All</button>
                <button class="btn btn-primary"><input type="radio" name="task_status" id="task_status" value="open">Open</button>
                <button class="btn btn-primary"><input type="radio" name="task_status" id="task_status" value="assigned">Assigned</button>
            </div>
            <div class="brs__task__sts col-sm-6">
                <div class="form-group">
                    <label for="task-ctg">Task Category</label>
                    <select class="form-control" id="fiter_category_id" name="fiter_category_id">
                        <option value="0" label="All">All</option>
                        <?php
                        foreach ($categories as $category) {
                            ?>
                            <option value="<?php echo $category->id ?>" {{ (isset($cat_id) && $cat_id == $category->id)?'selected':'' }}><?php echo $category->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="brs__task__sts col-sm-12">
                <p>Task Type</p>
                <button class="btn btn-primary"><input type="radio" class="sortBy" name="toBeCompleted" id="toBeCompleted" value="" checked="checked">All</button>

                <button class="btn btn-primary"><input type="radio" class="sortBy" name="toBeCompleted" id="toBeCompleted" value="online">Online tasks</button>
            </div>
            <div class="brs__task__sts col-sm-12" data-toggle="buttons">
                <p>Sort by</p>
                <select class="form-control" name="sort_by" id="sort_by">
                    <option value="t.created_at desc">Most Recent</option>
                    <option value="t.created_at asc">Oldest</option>
                    <option value="t.price asc">Price Ascending</option>
                    <option value="t.price desc">Price Descending</option>
                    <option value="t.title asc">Title</option>
                </select>
            </div>         
            <div class="clearfix"></div>

            <div class="brs__task__sts col-sm-12">
                <label for="location">Base Location</label>
                <div id="test">
                    <input type="text" class="form-control autocomplete" placeholder="Location" value="Dublin, Ireland" id="locality">

                    <input type="hidden" name="longitude"  id="longitude" value="-6.267118">
                    <input type="hidden" name="latitude"  id="latitude" value="53.342686">
					<span id="test-addon"></span>
                </div>

            </div>
            <div class="brs__task__sts col-sm-8">
                <p>Show Tasks Within</p>
                <select class="form-control" name="distance" id="distance">
                    <option value="">Task with location</option>
                    <option value="5">5 km</option>
                    <option value="10">10 km</option>
                    <option value="15">15 km</option>
                    <option value="25">25 km</option>
                    <option value="50">50 km</option>
                    <option value="100">100 km</option>
                    <option value="">Everywhere</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="rgt__price__offerBtn">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <button type="submit" id="update-button" class="btn-rounded">Update</button>
                <button type="button" id="reset-button" class="btn-rounded">Reset</button>
            </div>
        </div>


    </form>
</div>
<script>
    $(document).ready(function () {
        $('#brs-rgt-map').css('display', 'none');
        $("#rgt__map2").css('display', 'inline-block');
        $("#rgt__map1").css('display', 'none');
        $("li .rgt__map1").on('click', function () {
            $('#brs-rgt-map').css('display', 'none');
            $('#task').css('display', 'block');
            $("#rgt__map1").css('display', 'none');
            $("#rgt__map2").css('display', 'inline-block');
        });
        $("li .rgt__map2").on('click', function () {
            loadMap();
            $(".brs__tasks__box ul li").removeClass("active");
            $('#brs-rgt-map').css('display', 'block');
            $('#task').css('display', 'none');
            $("#rgt__map1").css('display', 'inline-block');
            $("#rgt__map2").css('display', 'none');
        });
        $(".brs__task__sts button input:radio:checked").parent('button').addClass("active");
    });
</script>

<script>
    $(document).ready(function () {
        $("li .brs__ctg__filt").on('click', function () {
			initAutocomplete1();
			
			setTimeout(function(){ 
				$(".pac-container").prependTo(".brs__filt__box #test #test-addon");
			}, 3000); 

        });

    });
	
    var placeSearch;
    var autocomplete = {};
    var autocompletesWraps = ['test', 'test2'];
    var test_form = {locality: 'long_name'};
    var test2_form = {locality: 'long_name'};
    function initAutocomplete1() {
        $.each(autocompletesWraps, function (index, name) {
            if ($('#' + name).length === 0) {
                return;
            }
            autocomplete[name] = new google.maps.places.Autocomplete($('#' + name + ' .autocomplete')[0], {types: ['geocode']});
            autocomplete[name].setComponentRestrictions(
                    {'country': ['ie']});
            google.maps.event.addListener(autocomplete[name], 'place_changed', function () {
                var place = autocomplete[name].getPlace();
                var form = eval(name + '_form');
                document.getElementById("latitude").value = place.geometry.location.lat();
                document.getElementById("longitude").value = place.geometry.location.lng();

                for (var component in form) {
                    $('#' + name + '#' + component).val('');
                    $('#' + name + '#' + component).attr('disabled', false);
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (typeof form[addressType] !== 'undefined') {
                        var val = place.address_components[i][form[addressType]];
                        $('#' + name + '#' + addressType).val(val);
                    }
                }


            });
        });
		
    }
	
	
    function geolocate1() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;

            });
        }
    }


    $(document).ready(function () {
		
        $(".brs__task__sts button input:radio:checked").parent('button').addClass("active");

		
		
		jQuery("#reset-button").click(function() {
			var chck = ".brs__task__sts button:nth-of-type(1)";
			var notChck = ".brs__task__sts button";
			
			jQuery(notChck).removeClass("active");
			jQuery(chck).addClass("active");
			
  
		});
		
		
        $('#brs-rgt-map').css('display', 'none');
        $("#rgt__map2").css('display', 'inline-block');
        $("#rgt__map1").css('display', 'none');
        $("li .rgt__map1").on('click', function () {
            $('#brs-rgt-map').css('display', 'none');
            $('#task').css('display', 'block');
            $("#rgt__map1").css('display', 'none');
            $("#rgt__map2").css('display', 'inline-block');

        });
        $("#reset-button").click(function (e) {
            $("form").trigger('reset');
        });

    });
</script>
