@extends('layout')

@section('content')
<section class="earn-bnr-area">
    <div class="container">
        <div class="earn__bnr__lft valigner col-sm-5">
            <div class="valign">
                <div class="signup__form anime-left">
                    <?php if (!isset(Auth::user()->id)) { ?>
                        <div class="signup__form__title">
                            <h3>Sign up using your email</h3>
                        </div>
                    @include('front/common/errors')
                        <form method="POST" class="form" action="{{ url('register') }}">
                        <input type="hidden" name="role_id" value="2">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="fname">First Name</label>
                                <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" required="required" value="{{ old('firstName') }}">
                            </div>
                            <div class="form-group">
                                <label for="lname">Last Name</label>
                                <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" required="required" value="{{ old('lastName') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address *" required="required" value="{{ old('email') }}">
                            </div>
							<div class="row">
                            <div class="form-group col-sm-6">
                                <label for="pwd">Password</label>
                                 <input type="password" class="form-control" name="password" id="password" placeholder="Password *" required="required">
                            </div>
                        <div class="form-group col-sm-6">
                            <label for="pwd">ConfirmPassword</label>
                            <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" required="required">
                        </div>
						</div>
                            <div class="btns-group">
								<button type="submit" class="btn btn-default">Sign up</button>
								<span>or</span>
								<a class="btn btn-primary" data-toggle="modal" data-target="#signIn">Sign in</a>
							</div>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="signup__rgt valigner anime-right col-sm-6 col-sm-offset-1">
            <div class="signup__rgt__inr valign">
                <div class="signup__hed">
                    <h2>Be your own Boss, complete tasks on your own schedule</h2>
                </div>
                <div class="signup__img clrlist">
                   <?php if (!isset(Auth::user()->id)) { ?>
                                <ul>
                                    <li><a onclick="fb_login();" class="signup__fb"><i class="fa fa-facebook-official" aria-hidden="true"></i> Sign up with Facebook</a></li>
                                    <li><a onclick="liAuth();" class="signup__in"><i class="fa fa-linkedin-square" aria-hidden="true"></i> Sign up with LinkedIn</a></li>
                                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="brw-task-area clrlist">
    <div class="container">
        <div class="sect__title text-center col-sm-12">
            <h3>1.Browse through available tasks</h3>
        </div>
        <div class="avb__task__box col-sm-4">
            <div class="avb__task__img">
                <img src="{{ asset('front/images/avb-task1.png') }}" alt="available task">
            </div>
        </div>
        <div class="avb__task__box col-sm-4">
            <div class="avb__task__cont">
                <p>
                    Have fun browsing through available tasks. Find a task that's right for you by selecting your location and keywords that match your skills. Ensure you don't miss out on a task by setting up a TaskMatch Alerts on your profile.


                </p>
            </div>
        </div>
        <div class="avb__task__box col-sm-4">
            <div class="avb__task__img">
                <img src="{{ asset('front/images/avb-task2.png') }}" alt="available task">
            </div>
        </div>
    </div>
</section>

<section class="sel-task-area clrlist">
    <div class="container">
        <div class="sect__title text-center col-sm-12">
            <h3>2. Select a task that is right for you</h3>
        </div>
        <div class="sel__task__box text-center col-sm-4">
            <div class="sel__task__inr">
                <div class="sel__task__img">
                    <img src="{{ asset('front/images/select-task1.png') }}" alt="select task">
                </div>
                <div class="sel__task__cont">
                    <p>
                        1. Found a task you want to complete?
                    </p>
                </div>
            </div>
        </div>
        <div class="sel__task__box text-center col-sm-4">
            <div class="sel__task__inr">
                <div class="sel__task__img">
                    <img src="{{ asset('front/images/select-task2.png') }}" alt="select task">
                </div>
                <div class="sel__task__cont">
                    <p>
                        2.Select the Make An Offer button and enter a price you think is fair
                    </p>
                </div>
            </div>
        </div>
        <div class="sel__task__box text-center col-sm-4">
            <div class="sel__task__inr">
                <div class="sel__task__img">
                    <img src="{{ asset('front/images/select-task3.png') }}" alt="select task">
                </div>
                <div class="sel__task__cont">
                    <p>
                        3. Don't forget to introduce yourself and tell the Job Poster what your skills are in the comments
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="build-prof-area clrlist">
    <div class="container">
        <div class="sect__title text-center col-sm-12">
            <h3>3. Build up your profile and reviews</h3>
        </div>
        <div class="build__prof__lft col-sm-6">
            <div class="build__prof__cont">
                <p>Create an account and then start building your profile to make yourself attractive to people Hiring on TaskMatch. Once you’re ready to get going <a href="browse-task.php">Browse Tasks</a>, start commenting and Make an Offer if you think you’re the best person to complete the job.</p>
            </div>
        </div>
        <div class="build__prof__rgt col-sm-6">
            <div class="build__prof__img">
                <img src="{{ asset('front/images/build-review.png') }}" alt="build review">
            </div>
        </div>
    </div>
</section>

<section class="join-area clrlist">
    <div class="container">
        <div class="sect__title text-center col-sm-12">
            <h3>Join Task Match now</h3>
        </div>
        <div class="join__btn col-sm-12">
            <a href="{{ url('signup') }}">Join Now</a>
        </div>
    </div>
</section>
@endsection