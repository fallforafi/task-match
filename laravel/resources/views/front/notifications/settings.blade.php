@extends('layout')

@section('content')
<?php
$settings = Config('notifications_settings');
?>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height ">
                    <div class="reviews-area">
                        <div class="tab-cont-title text-center">
                            <h3><i class="fa fa-bell-o"></i> Notifications Settings</h3>
                        </div>
						
						<table class="table table-hover">
							<thead>
								<th> </th>
								<th>Notification</th>
								<th>Email</th>
							</thead>
						
						<tbody>
 						
						
                            {!! Form::open(['url' => 'settings/update', 'method' => 'post']) !!}
                            <?php
                            foreach ($settings as $key => $setting) {
                                ?>
                                <tr><td>
                                    <?php
                                    echo $setting['name'];
                                    $notificationName = $key . '[notification]';
                                    $notificationEmail = $key . '[email]';

                                    $n = false;
                                    $e = false;
                                    if (isset($userSettings[$key]) && $userSettings[$key]['notification'] == 1) {
                                        $n = true;
                                    }

                                    if (isset($userSettings[$key]) && $userSettings[$key]['email'] == 1) {
                                        $e = true;
                                    }
                                    ?>
                                </td><td>
                                    {!! Form::checkbox($notificationName, 1, $n, ['class' => 'form-control"']) !!}
                                </td><td>
                                    @if($setting['email']==1)
                                    {!! Form::checkbox($notificationEmail, 1, $e, ['class' => 'form-control"']) !!}
                                    @endif
                                </td></tr>
                                <?php
                            }
                            ?>
								
							</tbody>
						</table>
                        
						
                            <div class="form-check col-sm-4 mb30">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection                            