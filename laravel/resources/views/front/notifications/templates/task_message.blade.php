<?php 
$task=$notification['task'];
?>
<div class='recent__usr__img'>
    <?php if ($notification['image'] != '') { ?>                  
        <img class='img-circle' src='<?php echo asset('/uploads/users/thumbnail/') ?>/<?php echo $notification['image']; ?>' alt = '<?php echo $notification['name'] ?>' title='<?php echo $notification['name'] ?>' />
    <?php } else { ?>
        <img src = '<?php echo asset('front/images/default.png') ?>' alt = '<?php echo $notification['name']; ?>' title = '<?php echo $notification['name']; ?>' />
    <?php } ?>
</div>
<h4><a href='<?php echo url('task/') ?>/<?php echo $task->key; ?>'><span><?php echo $notification['name']; ?></span> messaged <strong><?php echo $task->title; ?></strong></a></h4>