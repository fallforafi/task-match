<?php 
$task=$notification['task'];
?>


<div class="media notifi-area col-sm-12">
  <a class="notifi__img pull-left" href="#">
    <?php if ($notification['image'] != '') { ?>                  
       <div class="notifi__img"><img class='img-circle' src='<?php echo url('/uploads/users/profile/') ?>/<?php echo $notification['image']; ?>' alt = '<?php echo $notification['name'] ?>' title='<?php echo $notification['name'] ?>' /></div>
    <?php } else { ?>
        <div class="notifi__img"><img src = '<?php echo url('front/images/default.png') ?>' alt = '<?php echo $notification['name']; ?>' title = '<?php echo $notification['name']; ?>' /></div>
    <?php } ?>
  </a>
  <div class="media-body">
      <h4> <a href='<?php echo url('task/') ?>/<?php echo $task->key; ?>'><span><?php echo $notification['name']; ?></span> <small><i class="fa fa-comment"></i> commented</small> <strong><?php echo $task->title; ?></strong></a></h4>
  </div>
</div>

