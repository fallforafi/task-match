<?php 
$task=$notification['task'];
?>

<div class="media notifi-area col-sm-12">
  <a class="notifi__img pull-left" href="#">
        <?php if ($notification['image'] != '') { ?>                  
        <img class='img-circle' src='<?php echo asset('/uploads/users/profile/') ?>/<?php echo $notification['image']; ?>' alt = '<?php echo $notification['name'] ?>' title='<?php echo $notification['name'] ?>' />
    <?php } else { ?>
        <img src = '<?php echo asset('front/images/default.png') ?>' alt = '<?php echo $notification['name']; ?>' title = '<?php echo $notification['name']; ?>' />
    <?php } ?>
  </a>
  <div class="media-body">
<h4><a href='<?php echo url('offers/') ?>/<?php echo $task->key; ?>'><span><?php echo $notification['name']; ?></span> <small><i class="fa fa-times"></i> rejected your offer on</small> <strong><?php echo $task->title; ?></strong></a></h4>
  </div>
</div>

