@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="notifications" class="">
                        <div class="tab-cont-title text-center">
                            <h3><i class="fa fa-bell-o"></i>Notifications</h3>
                        </div>
                        @if(count($model)==0)
                        <div class="reviews-area">
                            <div class="reviews__box">
                                <div class="clearfix"></div>
                                <div class="review__box__desc">
                                    <p>Once you have assigned or been assigned a task on TaskMatch, you will see a summary of your notifications here.</p>
                                </div>
                            </div>
                            @else
                            <div class="noti-recent-area clrlist overload">
                                <div class="recent-box0">
                                    <div class="recent__inr0">
                                        <?php foreach ($model as $row) { ?>
                                            <?php echo stripslashes($row->description); ?>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        </section>
                        @endsection                            