@extends('profile_layout')
@section('content')

<div class="main_section">
    <div class="container">
        <div class="chat_container">
            <div class="col-sm-4 chat_sidebar ">
                @include('front.messages.left')
            </div>
            <div class="col-sm-8 message_section" id='message_view'>Messages
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        message('<?php echo $key;?>');
    });
</script>

@endsection