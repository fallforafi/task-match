<ul class="list-unstyled">
    <li class="left clearfix">
        @foreach($model as $row)

        <span class="chat-img1 pull-left">
            @include('front/common/profile_picture')
        </span>
        <div class="chat-body1 clearfix">

            <div class="chat__cont"><?php echo nl2br($row->message); ?></div>
            <div class="chat_time pull-right"><?php echo showTime($row->created_at) ?></div>


        </div>
        @endforeach
    </li>
</ul>
