<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
?>
<div class="row">
    @include('front.common.errors')
    <!-- <div class="new_message_head">
         <div class="pull-left"><button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button></div><div class="pull-right"><div class="dropdown">
                 <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fa fa-cogs" aria-hidden="true"></i>  Setting
                     <span class="caret"></span>
                 </button>
                 <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                 </ul>
             </div></div>
     </div>-->
    <!--new_message_head-->
    <div class="clearfix"></div>
    <div class="task__accepted__msg">
        <div class="left clearfix">
            <span class="chat-img pull-left">
                <?php
                $row = $from;
                ?>
                @include('front/common/profile_picture')

            </span>
            <div class="chat-body clearfix">
                <div class="header_sec">
<!--                     <strong class="primary-font"><a href="{{url('/task/')}}/{{$task->key}}">{{$task->title}}</a></strong>-->
                    <strong class="primary-font">{{$task->title}}</strong>

                </div>
                <div class="header_sec">
                    <a href="{{ url('user/'.$from->key) }}"><span class="">{{$from->firstName}} {{$from->lastName}}</span></a> &nbsp; &nbsp;
                    <strong class="primary-font"></strong><span class=""><?php echo showTime($threadModel->created_at) ?></span>
                    <div class="accepted__info pull-right"> <i class="fa fa-check-circle-o"></i> <span>Accepted Offer: {{$currency}}{{$offer_price->offerPrice}}</span></div>
                </div>
            </div>

            <!--div class="chat-body clearfix">
                <div class="header_sec">
                    <strong class="primary-font">{{$from->firstName}} {{$from->lastName}}</strong> <strong class="pull-right">
            <?php echo showTime($threadModel->created_at) ?></strong>
                </div>
                <div class="contact_sec">
                    <strong class="primary-font"><a href="{{url('/task/')}}/{{$task->key}}">{{$task->title}}</a></strong> <span><strong>Accepted Offer: {{$currency}}{{$offer_price->offerPrice}} &nbsp; <i class="fa fa-check-circle-o" aria-hidden="true"></i></strong></span>
                </div>
            </div-->
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12">
        <p>{{$offer->message}}</p> 
    </div>

    <div class="col-sm-12 actions">
        <a href="{{url('offers')}}/<?php echo $offer_price->key ?>" class="btn btn-large btn-info" >View offer of <strong> {{$offer->firstName}} {{$offer->lastName}} </strong> </a>
        <span class="actions" id="review" style="display:none;" ></span>

    </div>

    <?php
    if ($task->taskStatus != "open") {
        $thread_id = $threadModel->id;
        $task_id = $threadModel->task_id;
        ?>
        @include('front/messages/chat')
    <?php } ?>
</div>
<?php if ($task->taskStatus == 'completed') { ?>
    <div class="alert now-closed">Conversation are now closed</div>
<?php } ?>  
<script>
    $(document).ready(function (e) {
        getReview(<?php echo $threadModel->task_id ?>);

    });

    function getReview(task_id)
    {
        $.ajax({url: "<?php echo url("gettaskreview/"); ?>/" + task_id,
            type: 'get',
            dataType: 'html',
            success: function (result) {
                $("#review").html(result).show();
            }
        });
    }
</script>                     
