<div class="member_list nav--activized">
    @if(count($model)>0)
    <ul class="list-unstyled ">
        <?php
        $i = 0;
        ?>
        @foreach($model as $row)
        <?php
        if ($i == 0) {
            $key = $row->threadKey;
        }
        $i++;
        ?>
        <?php if($row->taskStatus != 'cancelled' && $row->deleted == 0) { ?>
        <li onclick="message('<?php echo $row->threadKey; ?>')" class="left clearfix <?php echo $row->threadKey; ?>" >
            <span class="chat-img pull-left">

                <?php
                if (Auth::user()->id == $row->task_owner_id) {
                    $row->id = $row->task_runner_id;
                    $row->key = $row->task_runner_key;
                    $row->firstName = $row->taskRunnerFirstName;
                    $row->lastName = $row->taskRunnerLastName;
                    $row->image = $row->taskRunnerImage;
                } else {
                    $row->id = $row->task_owner_id;
                    $row->key = $row->task_owner_key;
                    $row->firstName = $row->taskOwnerFirstName;
                    $row->lastName = $row->taskOwnerLastName;
                    $row->image = $row->taskOwnerImage;
                }
                ?>

                @include('front/common/profile_picture')
            </span>
            <div class="chat-body clearfix">
                <div class="header_sec">
                    <a class="chat__name" href="{{ url('user/'.$row->key) }}"><span class="">{{$row->firstName}} {{$row->lastName}}</span></a><time class="pull-right"><?php echo showTime($row->created_at) ?></time>
					<div class="chat__contprev">{{$row->title}}</div>
                </div>
                
            </div>
        </li>
        <?php } ?>
        @endforeach
    </ul>
    @else
    <div class="alert alert-warning">
        You haven't sent or received any messages yet.
    </div>
    @endif
</div>

<script>
    jQuery(".member_list li.left").click(function () {
        //jQuery(this).toggleClass("is-active");
        jQuery(".member_list li.left").removeClass("is-active");
        jQuery(this).addClass("is-active");
    });
	
	
</script>