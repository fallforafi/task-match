@extends('profile_layout')
@section('content')
<div class="main_section ">
    <div class="container">
        <div class="chat_container">
            <div class="col-sm-4 chat_sidebar">
                @include('front.messages.left') 
            </div>
            <div class="col-sm-8 message_section" id='message_view'>
			
				<div class="chat__defview text-center p50">
					<div class="img"><img src="{{asset('')}}/front/images/chatroom-default.jpg" alt="" /></div>
					<h3>Select chat conversation from left side</h3>
					
					
				</div>
            </div>
        </div>
    </div>
</div>
@endsection                            