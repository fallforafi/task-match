<div id="custom-search-input">
    <div class="input-group col-sm-12">
        <input id="filter_search" type="text" class="search-query form-control" placeholder="Conversation" />
        <button class="btn btn-danger" type="button" onclick="messageListing();">
            <span class=" glyphicon glyphicon-search"></span>
        </button>
    </div>
</div>
<!--
<div class="dropdown all_conversation">
    <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-weixin" aria-hidden="true"></i>
        All Conversations
        <span class="caret pull-right"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <li><a href="#"> All Conversation </a>  <ul class="sub_menu_ list-unstyled">
                <li><a href="#"> All Conversation </a> </li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li><a href="#">Separated link</a></li>
    </ul>
</div>-->

<script>
    $("#filter_search").keyup(function (e) {
        messageListing();
    });

    function messageListing() {
        var search = $("#filter_search").val();
        //alert(search);
        $.ajax({
            url: "<?php echo url('getmessages'); ?>",
            type: 'get',
            dataType: 'html',
            data: {search:search},
            success: function (response) {
                $('#messages').html(response).show();
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
