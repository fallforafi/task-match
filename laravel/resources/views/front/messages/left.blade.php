@include('front/messages/filters')
<div class="chat__list" id='messages'>
</div>
<script>

    $(document).ready(function () {
        //getMessages();
        messageListing();
    });

    function message(key)
    {
        $.ajax({
            url: "<?php echo url('messages/view'); ?>/" + key,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                $('#message_view').html(response).show();
                window.history.pushState({"html": response}, "", "<?php echo url('message'); ?>/" + key);
            },
            error: function (xhr, status, response) {
            }
        });
    }

//    function getMessages() {
//
//        var search = $("#search").val();
//
//        $.ajax({url: "<?php echo url(""); ?>/getmessages",
//            type: 'get',
//            dataType: 'html',
//            data: {
//                search: search
//            },
//            success: function (result) {
//                $("#messages").html(result);
//
//            }
//        });
//    }

</script>