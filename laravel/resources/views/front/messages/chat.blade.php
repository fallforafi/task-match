
<div class="chat_area" id="replies">

</div>

<?php if ($task->taskStatus != 'completed') { ?>
    <div class="message_write">
        <form class="form-horizontal" id="replyForm">
            <div id="error_reply"></div>
            <textarea class="form-control"  placeholder="type a message" name="message" id="message"></textarea>
            <div class="clearfix"></div>
            <div class="chat_bottom">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <input type="hidden" name="from" id="from" value="<?php echo Auth::user()->id ?>">
                <input type="hidden" name="thread_id" id="thread_id" value="<?php echo $thread_id ?>">
                <input type="hidden" name="task_id" id="task_id" value="<?php echo $task_id ?>">
                <input type="button" class="pull-right btn btn-success" onclick='saveReply()' value="Send">

            </div>
        </form>
    </div>
<?php } ?>
<script>
    function saveReply() {
        var formdata = $("#replyForm").serialize();
        $('#error_reply').html("").hide();
        $.ajax({
            url: "<?php echo url('messages/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#error_reply').html(errorsHtml).show();
                } else
                {
                    $('#message').val('');
                    getReplies('<?php echo $thread_id ?>');
                }

            },
            error: function (xhr, status, response) {
            }
        });
    }

    function getReplies(thread_id)
    {
        $.ajax({url: "<?php echo url("getreplies/"); ?>/" + thread_id,
            type: 'get',
            dataType: 'html',
            success: function (result) {
                $("#replies").html(result);
            }
        });
    }

    $(document).ready(function (e) {
        getReplies('<?php echo $thread_id ?>');
    });
//    window.setInterval(function () {
//        getReplies('<?php echo $thread_id ?>');
//    }, 1000);
</script>
