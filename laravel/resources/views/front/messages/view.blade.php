@extends('layout')
@section('content')
<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
?>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="tab-content dboard-tab-height col-sm-9">
                <div class="col-sm-12">
                    <h2>{{$model->subject}} By {{$from->firstName}} {{$from->lastName}}</h2><p>{{$model->message}}</p>           
                    <h3 class="">Offer: {{$currency}} {{$offer_price->offerPrice}} </h3>
                </div>
                <div class="col-sm-12 actions">
                    <a href="{{url('offers')}}/<?php echo $offer_price->key ?>" class="btn btn-large btn-info" >View offer of {{$from->firstName}} {{$from->lastName}}</a>
                    <a href="{{url('/task/')}}/{{$task->key}}" class="btn btn-success">Go to task</a>
                </div>

                <div class="col-sm-12 actions" id="review" style="display:none;" >
                </div>

                <?php
                if ($task->taskStatus != "assigned") {
                    ?>
                    <div class="col-sm-12">
                        <h2>Reply</h2>
                        <form class="form-horizontal" id="replyForm">
                            <div id="error_reply"></div>
                            <div class="form-group">
                                <input type="text" name="message" id="message" class="form-control" placeholder="ask a question about this task" maxlength="500">
                                <small>500 chars remaining</small>
                            </div>
                            <div class="form-group button-btn pul-rgt">
                                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="from" id="from" value="<?php echo Auth::user()->id ?>">
                                <input type="hidden" name="thread_id" id="thread_id" value="<?php echo $model->id ?>">
                                <input type="button" class="form-control" onclick='saveReply()' value="Reply">
                            </div>
                        </form>
                    </div>

                <?php } ?>
                <div class="col-sm-12" id="replies">
                </div>
            </div>
        </div>
    </div>
</section>
<?php
if ($task->taskStatus != "open") {
    ?>
    <script>
        function saveReply() {
            var formdata = $("#replyForm").serialize();
            $('#error_reply').html("").hide();
            $.ajax({
                url: "<?php echo url('messages/store'); ?>",
                type: 'POST',
                dataType: 'json',
                data: formdata,
                success: function (response) {

                    if (response.error == 1) {
                        var errorsHtml = '<div class="alert alert-danger"><ul>';
                        $.each(response.errors, function (key, value) {
                            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                        });

                        errorsHtml += '</ul></div>';
                        $('#error_reply').html(errorsHtml).show();
                    } else
                    {
                        getReplies('<?php echo $model->id ?>');
                    }

                },
                error: function (xhr, status, response) {
                }
            });
        }

        function getReplies(thread_id)
        {
            $.ajax({url: "<?php echo url("getreplies/"); ?>/" + thread_id,
                type: 'get',
                dataType: 'html',
                success: function (result) {
                    $("#replies").html(result);
                }
            });
        }

        $(document).ready(function (e) {
            getReplies('<?php echo $model->id ?>');
        });
    </script>
<?php } ?>
<script>
    function saveReview() {
        var formdata = $("#reviewForm").serialize();
        $('#error_review').html("").hide();
        $.ajax({
            url: "<?php echo url('reviews/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {

                if (response.error == 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#error_review').html(errorsHtml).show();
                } else
                {
                    getReview(<?php echo $task->id ?>);
                }

            },
            error: function (xhr, status, response) {
            }
        });

    }

    $(document).ready(function (e) {
        getReview(<?php echo $task->id ?>);

    });

    function getReview(task_id)
    {
        $.ajax({url: "<?php echo url("gettaskreview/"); ?>/" + task_id,
            type: 'get',
            dataType: 'html',
            success: function (result) {
                $("#review").html(result).show();
            }
        });
    }


    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        onConfirm: function () {

<?php
if ($task->taskStatus == "assigned") {
    ?>
                action('payassignee', '<?php echo $offer_price->id; ?>', '<?php echo Auth::user()->id; ?>');


<?php } elseif ($task->taskStatus == "open") { ?>

                window.top.location = "<?php echo url("offers/".$offer_price->key); ?>";
                //action('assign', '<?php //echo $offer_price->id; ?>', '<?php //echo Auth::user()->id; ?>');
<?php } ?>
        }
    });

    function action(action, offer_id, user_id)
    {
        $.ajax({url: "<?php echo url(""); ?>/" + action,
            type: 'post',
            dataType: 'json',
            data: {offer_price_id: offer_id, user_id: user_id, _token: $("#_token").val()},
            success: function (result) {

                if (result.error == 1) {
                    alert(result.errorMsg)
                } else {
                    window.location.reload();
                }

            }
        });
    }
</script>
@endsection                            