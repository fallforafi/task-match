@extends('layout')
<?php
$title = 'Find and hire skilled workers';
$description = 'Taskmatch.ie - Find and hire skilled workers in your local area and earn extra cash';
$keywords = 'taskmatch, task match, taskmatch.ie, task finder, taskrabbit, airtasker, hire workers, Cleaning jobs, House, Window, Cleaner, Tradesman, Handyman, Carpenter, Electrician, Painter, Wedding photographer, Web Design, Laptop, Phone, TV, Repair, Job';
?>
@include('front/common/meta')
@if (Session::has('denied'))
<script>
    alert('You are not authorized for this action');
</script>
@endif
@section('content')

<section class="slider-area hover-ctrl fadeft" >
    <div id="carousel-example-generic" class="slide slider--pauseplay" data-ride="" data---interval="false">

        <!--<ol class="carousel-indicators thumbs">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li> 
        </ol>-->

        <!-- Wrapper for slides -->
        <div class="carousel-inner">


            <div class="item active">
                <img src="{{asset('')}}/front/images/slide1.jpg" alt="...">
                <div class="container posrel">
                    <div class="caro-caps">
                        <div class="slide__cont anime-left col-sm-12">
                            <h1>What do you 
                                <br>
                                need done?
                            </h1>
                            <h4>Trusted workers are waiting to help you complete
                                your tasks</h4>
                            <div class="lnk-btn inline-block start-btn">
                                <?php if (isset(Auth::user()->id)) { ?>
                                    <a onclick="viewForm();" data-toggle="modal" data-target="#postTask">Get Started</a>
                                <?php } else { ?>
                                    <a data-toggle="modal" data-target="#signIn">Get Started</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
            </a>-->


        </div>
    </div>
    <div class="dwn-scroll text-center h4"><i class="glyphicon glyphicon-menu-down"></i></div>

</section>

<section class="task-area">
    <div class="container">
        <div class="sect__title text-center anime-left col-sm-12">
            <h3>Browse Tasks</h3>
        </div>
        <div class="task-main col-sm-12">
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=2') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task1.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Home & Cleaning</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=7') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task2.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Delivery & Removals</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=8') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task3.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Handyman & Trades</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=9') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task4.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Marketing & Design</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=11') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task5.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Events & Photography</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=5') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task6.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Computer & IT</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=10') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task7.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Business & Admin</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="task-box anime-zoomIn col-sm-3">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{asset('')}}/front/images/task8.jpg" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Fun & Quirky</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="lnk-btn inline-block find-btn text-center col-sm-12"><a href="find-out-more">Find out more</a></div>
        </div>
    </div>
</section>

<section class="info-area">
    <div class="container">
        <div class="info__lft anime-left col-sm-7">
            <div class="info__box col-sm-6">
                <div class="info__inr">
                    <i class="fa fa-plus-circle"></i>
                    <div class="info__cont">
                        <h3>Free to Post</h3>
                        <p>It’s completely free to post a task, you’ll start receiving offers from Taskers. There are no hidden fees.</p>
                    </div>
                </div>
            </div>
            <div class="info__box col-sm-6">
                <div class="info__inr">
                    <i class="fa fa-credit-card"></i>
                    <div class="info__cont">
                        <h3>Secure Payment</h3>
                        <p>TaskMatch Pay makes it seamless to pay for your task, funds are held until you’re happy the task is complete.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="info__box col-sm-6">
                <div class="info__inr">
                    <i class="fa fa-clipboard"></i>
                    <div class="info__cont">
                        <h3>You're in Control</h3>
                        <p>Receive multiple offers, and then you decide which Tasker you’d like to complete your task.</p>
                    </div>
                </div>
            </div>
            <div class="info__box col-sm-6">
                <div class="info__inr">
                    <i class="fa fa-certificate"></i>
                    <div class="info__cont">
                        <h3>Trusted Workers</h3>
                        <p>Read verified reviews to make sure you hire the best person for the job.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="info__rgt anime-right col-sm-5">
            <div class="info__img">
                <img src="{{asset('')}}/front/images/info-iphone.png" alt="iphone app">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>	

<section class="work-area clrlist">
    <div class="container">
        <div class="sect__title text-center col-sm-12">
            <h3>How does it work?</h3>
        </div>
        <div class="work__main anime-zoomIn col-sm-12">
            <div class="work__box col-sm-4">
                <div class="work__inr">
                    <div class="work__img">
                        <img src="{{asset('')}}/front/images/work1.png" alt="how it works">
                    </div>
                    <div class="work__cont">
                        <p>1. Tell us what you need done</p>
                    </div>
                </div>
            </div>
            <div class="work__box col-sm-4">
                <div class="work__inr">
                    <div class="work__img">
                        <img src="{{asset('')}}/front/images/work2.png" alt="how it works">
                    </div>
                    <div class="work__cont">
                        <p>2. Receive offers in minutes</p>
                    </div>
                </div>
            </div><div class="work__box col-sm-4">
                <div class="work__inr">
                    <div class="work__img">
                        <img src="{{asset('')}}/front/images/work3.png" alt="how it works">
                    </div>
                    <div class="work__cont">
                        <p>3. Choose the best person for the job</p>
                    </div>
                </div>
            </div>
            <div class="work__desc text-center anime-right col-sm-12">
                <p>
                    TaskMatch is Ireland's first community marketplace for people and businesses to outsource tasks, find local services or hire flexible staff in minutes
                </p>
            </div>
        </div>
    </div>
</section>

<section class="recent-tasks-area clrlist">
    <div class="container">
        <div class="sect__title recent-title text-center anime-left col-sm-12">
            <h3>Recently Completed Tasks</h3>
        </div>
        <div class="row anime-down">
            <div class="recent__div col-sm-3">
                <div class="recent__box">
                    <div class="recent__inr">
                        <div class="recent__hdr">
                            <h5>Saorview installation needed</h5>
                        </div>
                        <div class="recent__inr__box">
                            <div class="recent__customer">
                                <div class="recent__cust__img">
                                    <img src="{{asset('')}}/front/images/customer1.png" alt="customer">
                                </div>
                                <div class="recent__cust__name">
                                    <p>Ciaran P</p>
                                </div>
                            </div>
                            <div class="recent__need">
                                <p>
                                    Need Saorview installed in two rooms, downstairs living room and upstairs bedroom
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="recent__outer">
                        <div class="recent__cust__review__name">
                            <p>Ciaran P:</p>
                        </div>
                        <div class="recent__cust__review">
                            <p><i>Hassle free and happy viewing. Thanks a million!</i></p>
                        </div>
                        <div class="recent__cust__rating">
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="recent__div col-sm-3">
                <div class="recent__box">
                    <div class="recent__inr">
                        <div class="recent__hdr">
                            <h5>Reliable painter needed</h5>
                        </div>
                        <div class="recent__inr__box">
                            <div class="recent__customer">
                                <div class="recent__cust__img">
                                    <img src="{{asset('')}}/front/images/customer2.png" alt="customer">
                                </div>
                                <div class="recent__cust__name">
                                    <p>Mary R</p>
                                </div>
                            </div>
                            <div class="recent__need">
                                <p>
                                    4 Bedroom, 2 Bathroom house in need of a good white paint job ASAP! Expenses included
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="recent__outer">
                        <div class="recent__cust__review__name">
                            <p>Mary R:</p>
                        </div>
                        <div class="recent__cust__review">
                            <p><i>Super quick response and the house has never looked so good! Thanks again</i></p>
                        </div>
                        <div class="recent__cust__rating">
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="recent__div col-sm-3">
                <div class="recent__box">
                    <div class="recent__inr">
                        <div class="recent__hdr">
                            <h5>Small coffee shop needs Facebook help</h5>
                        </div>
                        <div class="recent__inr__box">
                            <div class="recent__customer">
                                <div class="recent__cust__img">
                                    <img src="{{asset('')}}/front/images/customer3.png" alt="customer">
                                </div>
                                <div class="recent__cust__name">
                                    <p>Sarah K</p>
                                </div>
                            </div>
                            <div class="recent__need">
                                <p>
                                    Looking for someone to write and publish Facebook posts & ads for a busy Cafe in South Dublin
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="recent__outer">
                        <div class="recent__cust__review__name">
                            <p>Sarah K:</p>
                        </div>
                        <div class="recent__cust__review">
                            <p><i>Perfect, never had so many likes. Really helped me manage my online advertising.</i></p>
                        </div>
                        <div class="recent__cust__rating">
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="recent__div col-sm-3">
                <div class="recent__box">
                    <div class="recent__inr">
                        <div class="recent__hdr">
                            <h5>Moving apartment - van needed</h5>
                        </div>
                        <div class="recent__inr__box">
                            <div class="recent__customer">
                                <div class="recent__cust__img">
                                    <img src="{{asset('')}}/front/images/customer4.png" alt="customer">
                                </div>
                                <div class="recent__cust__name">
                                    <p>Tony B</p>
                                </div>
                            </div>
                            <div class="recent__need">
                                <p>
                                    I'm moving apartments at the end of the month and need someone to give me a hand. Van is essential.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="recent__outer">
                        <div class="recent__cust__review__name">
                            <p>Tony B:</p>
                        </div>
                        <div class="recent__cust__review">
                            <p><i>John was great and even helped me set up my TV unit!</i></p>
                        </div>
                        <div class="recent__cust__rating">
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star-o"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
</section>

<section class="signup-area">



    <div class="signup-slider hover-ctrl fadeft">
        <div id="signup-carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">

            <!--<ol class="carousel-indicators thumbs">
                  <li data-target="#signup-carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#signup-carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#signup-carousel-example-generic" data-slide-to="2"></li>
            </ol>-->

            <div class="carousel-inner">


                <div class="item active" style="background-image:url('{{asset('')}}/front/images/signup-slider1.jpg');">

                </div>
                <div class="item" style="background-image:url('{{asset('')}}/front/images/signup-slider2.jpg');">

                </div>
                <div class="item" style="background-image:url('{{asset('')}}/front/images/signup-slider3.jpg');">

                </div>








            </div>

            <a class="left carousel-control" href="#signup-carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#signup-carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>


        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="signup-main col-sm-12">

                <div class="signup__form anime-left col-sm-offset-1 col-sm-4">
                    <?php if (!isset(Auth::user()->id)) { ?>
                        <div class="signup__form__title">
                            <h3>Sign up using your email</h3>
                        </div>
                        @include('front/common/errors')
                        <form method="POST" class="form" action="{{ url('register') }}">
                            <input type="hidden" name="role_id" value="2">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label for="fname">First Name</label>
                                <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" required="required" value="{{ old('firstName') }}">
                            </div>
                            <div class="form-group">
                                <label for="lname">Last Name</label>
                                <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" required="required" value="{{ old('lastName') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address *" required="required" value="{{ old('email') }}">
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="pwd">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password *" required="required">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="pwd">ConfirmPassword</label>
                                    <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" required="required">
                                </div>
                            </div>
                            <div class="btns-group">
                                <button type="submit" class="btn btn-default">Sign up</button>
                                <span>or</span>
                                <a class="btn btn-primary" data-toggle="modal" data-target="#signIn">Sign in</a>
                            </div>
                        </form>
                    <?php } ?>
                </div>

                <div class="signup__rgt valigner anime-right col-sm-offset-1 col-sm-6">
                    <div class="signup__rgt__inr valign">
                        <div class="signup__hed">
                            <h2>Earn up to €1,000 a week</h2>
                        </div>
                        <div class="signup__img clrlist">
                            <?php if (!isset(Auth::user()->id)) { ?>
                                <ul>
                                    <li><a onclick="fb_login();" class="signup__fb"><i class="fa fa-facebook-official" aria-hidden="true"></i> Sign up with Facebook</a></li>
                                    <li><a onclick="liAuth();" class="signup__in"><i class="fa fa-linkedin-square" aria-hidden="true"></i> Sign up with LinkedIn</a></li>
                                    <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section> 
<script>
    $(document).ready(function () {
        $('body').addClass('home-page');
    });
</script>

@endsection