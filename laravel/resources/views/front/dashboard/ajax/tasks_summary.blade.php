<div id="taskRunning" class="tab-pane fade in">
    <div class="task__tab__main text-center col-sm-12">
        <div class="task__tab__box col-sm-3">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$bid_on}}</h2>
                <h4 class="task__box__cont">Bid on</h4>
            </div>
        </div>
        <div class="task__tab__box col-sm-3">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$assignee}}</h2>
                <h4 class="task__box__cont">Assigned</h4>
            </div>
        </div>
        <div class="task__tab__box col-sm-3">
            <div class="task__tab__inr">
                <h2 class="task__box__count">0</h2>
                <h4 class="task__box__cont">Awaiting Payment</h4>
            </div>
        </div>
        <div class="task__tab__box col-sm-3">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$completed}}</h2>
                <h4 class="task__box__cont">Completed</h4>
            </div>
        </div>
    </div>
</div>
<div id="tasksPosted" class="tab-pane fade">
    <div class="task__tab__main text-center col-sm-12">
        <div class="task__tab__box col-sm-4">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$open_tasks}}</h2>
                <h4 class="task__box__cont">Open for Offers</h4>
            </div>
        </div>
        <div class="task__tab__box col-sm-4">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$assigned_tasks}}</h2>
                <h4 class="task__box__cont">Assigned</h4>
            </div>
        </div>
        <div class="task__tab__box col-sm-4">
            <div class="task__tab__inr">
                <h2 class="task__box__count">{{$completed_tasks}}</h2>
                <h4 class="task__box__cont">Completed</h4>
            </div>
        </div>
    </div>
</div>