<div class="container">
    <div class="row" style="margin-top:40px;">
        <div class="col-md-6">
            <div class="row" id="post-review-box" style="display:none;">
                <div class="col-md-12">
                    @include('front.reviews.form')
                </div>
            </div>
        </div>
    </div>
</div>
