@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__rgt col-sm-8 col-md-offset-2">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="notifications" class="">
                        <div class="reviews-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-eye"></i> Post Review</h3>
                            </div><br>
                            <div class="noti-recent-area">
                                <div class="recent-box col-sm-12">
                                    <div class="recent__inr">
                                        
                                        <label>Add Your Review</label>
                                        
                                        <div class="form-group">
                                            @include('front.reviews.form')                             
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>


                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function saveReview() {
        var formdata = $("#reviewForm").serialize();
        $('#error_review').html("").hide();
        $.ajax({
            url: "<?php echo url('reviews/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#error_review').html(errorsHtml).show();
                } else
                {
                    $('#error_review').html('<div class="alert alert-success">Success!</div>').show();
                    window.location.assign('<?php echo url('my-reviews'); ?>');
                }

            },
            error: function (xhr, status, response) {
            }
        });

    }
</script>
@endsection

