@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height overload">
                    <div id="reviews" class="">
                        <div class="reviews-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-star"></i> Reviews</h3>
                            </div>
                            <div class="reviews__box">
                                @if(count($reviews)==0)
                                <div class="review__box__desc">
                                    <p>Once you have completed a task on TaskMatch, you will see a summary of your reviews here.</p>
                                </div>
                                @else
                                <div class="prof__avg__review col-sm-offset-2 col-sm-4">
                                    <h4>Average reviews</h4>
                                    <div class="prof__rate clrlist">
                                        <ul>

                                            @for($i=1;$i<=$average;$i++)
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <div class="prof__review__fm">
                                        <p>from {{count($reviews)}} review(s)</p>
                                    </div>
                                </div>
                                <div class="prof__avg__review col-sm-5">
                                    <h4>Summary</h4>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge"><?php echo!empty($ratings[5]) ? count($ratings[5]) : 0; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge"><?php echo!empty($ratings[4]) ? count($ratings[4]) : 0; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge"><?php echo!empty($ratings[3]) ? count($ratings[3]) : 0; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge"><?php echo!empty($ratings[2]) ? count($ratings[2]) : 0; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="prof__rate clrlist">
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                </ul>
                                            </li>
                                            <li class="prof__rate__graph"></li>
                                            <li class=""><span class="badge"><?php echo!empty($ratings[1]) ? count($ratings[1]) : 0; ?></span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div ></div>



                                <div class="col-md-12 reviewer-box" >
                                    <ul class="list-group mb0">
                                        @foreach($model as $row)
                                        <li>

                                            <div class="media">
                                                <a class="pull-left" href="#">
                                                    @include('front/common/profile_picture')
                                                </a>
                                                <div class="media-body">
                                                    <span class="prof__rate clrlist pul-rgt">
                                                        <ul>
                                                            @for($i=1;$i<=$row->rating;$i++)
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            @endfor
                                                            @for($i=5;$i>$row->rating;$i--)
                                                            <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                            @endfor
                                                        </ul>                                            
                                                    </span>   

                                                    <div class="__top">
                                                        <a href="{{url('user/')}}/{{$row->key}}"> {{$row->firstName}} {{$row->lastName}}</a>
                                                        <small>{{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>
                                                    </div>

                                                    <div class="cont lead mb0">{{$row->comment}}</div>                                                     


                                                    <div class="review__feeback">
                                                        <div class="btn-report pul-rgt">
                                                            <?php if ($row->report == 0) { ?>
                                                                <a data-toggle="confirmation" data-btn-ok-label="Yes" data-btn-ok-class="btn-success" data-btn-cancel-class="btn-default" data-btn-cancel-label="Not now" data-id="{{ $row->r_id }}"
                                                                   data-title="Report" data-content="Are you sure you want to report against this comment?" id="report" class="btn btn-primary">
                                                                    Report
                                                                </a>
                                                                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                                            <?php } else { ?>
                                                                <a disabled="disabled" class="btn btn-default">Reported</a>
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ($row->report == 0) { ?>							
                                                            <?php if ($row->existing == 0) { ?>

                                                                <a href="{{ url('feedback/'.$row->task_id) }}">Leave Feedback for {{$row->firstName}} {{$row->lastName}}</a>

                                                            <?php } else { ?>
                                                                <strong><p>Feedback added for {{$row->firstName}} {{$row->lastName}}</p></strong>
                                                            <?php } ?>
                                                        <?php } ?>



                                                    </div>

                                                </div>
                                            </div>


                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <script>
                                    jQuery('a#report').click(function ()
                                    {
                                        id = $(this).data('id');
                                    });
                                    $('[data-toggle=confirmation]').confirmation({
                                        rootSelector: '[data-toggle=confirmation]',
                                        onConfirm: function () {
                                            action('report', id, '<?php echo Auth::user()->id; ?>');
                                        }
                                    });

                                    function action(action, id, user_id)
                                    {
                                        $.ajax({url: "<?php echo url(""); ?>/" + action,
                                            type: 'post',
                                            dataType: 'json',
                                            data: {id: id, rate_to: user_id, report: 1, _token: $("#_token").val()},
                                            success: function (result) {

                                                if (result.error === 1) {
                                                    alert(result.errorMsg);
                                                } else {
                                                    window.location.reload();
                                                }

                                            }
                                        });
                                    }
                                </script>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
