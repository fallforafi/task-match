<?php

if ($task->taskStatus == "completed") {
    if (count($review) > 0) {
        ?>
<!--        <h2>Your Review</h2>
        <div class="col-sm-8">
            <?php echo isset($review->comment) ? $review->comment : "" ?>
        </div>
        <div class="col-sm-4">
            <?php echo isset($review->rating) ? $review->rating : "" ?>
        </div>-->
    <?php } else {
        if (count($review) < 0 && $task->user_id == Auth::user()->id) { ?>
            <a class="btn btn-success btn-green" href="{{ url('feedback/'.$task->id) }}" >Review {{ $user->firstName .' '.$user->lastName}}</a>
            @include('front/reviews/popup')

            <?php
        }
    }
}
