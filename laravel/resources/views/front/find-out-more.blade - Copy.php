@extends('layout')

@section('content')
<section class="find-area">
    <div class="container">
        <div class="task-main imgzoom--hover anime-in col-sm-12">
		
			<div class="hed text-center">
				<h2>Browse Task Suggestions</h2>
				<p>Ready to get more done? <strong>Post a task</strong></p>
			</div>
		
            <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=2') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task1.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Home & Cleaning</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					<div class="task__subtasks list-col-4">
					  <ul>
						<li><a href="#">House Cleaning</a></li>
						<li><a href="#spring-cleaning/">Spring Cleaning</a></li>
						<li><a href="#garden-maintenance/">Garden Maintenance</a></li>
						<li><a href="#airbnb-tasks/">AirBnb Tasks</a></li>
						<li><a href="#bbq-help/">BBQ Help</a></li>
						<li><a href="#car-cleaning/">Car Wash</a></li>
						<li><a href="#cooking-and-baking/">Cooking &amp; Baking</a></li>
						<li><a href="#health-and-wellness/">Health &amp; Wellness</a></li>
						<li><a href="#home-theatre-help/">Home Theatre Help</a></li>
						<li><a href="#house-sitting/">House Sitting</a></li>
						<li><a href="#landscaping/">Landscaping</a></li>
						<li><a href="#laundry/">Laundry Services</a></li>
						<li><a href="#lawn-care/">Lawncare</a></li>
						<li><a href="#nutritional-advice/">Nutritional Advice</a></li>
						<li><a href="#pet-chores/">Pet chores</a></li>
						<li><a href="#dog-walking/">Dog Walking</a></li>
						<li><a href="#steam-cleaning/">Steam Cleaning</a></li>
						<li><a href="#upholstery-and-cleaning/">Upholstery &amp; Cleaning</a></li>
						<li><a href="#vacate-clean/">Vacate Cleaning</a></li>
						<li><a href="#wardrobe-organisation/">Wardrobe Organisation</a></li>
						<li><a href="#window-cleaning/">Window Cleaning</a></li>
						<li><a href="#pool-cleaning/">Pool Cleaning</a></li>
						<li><a href="#/event-and-photography/chef/">Chef</a></li>
						<li><a href="#pet-sitting/">Pet Sitting</a></li>
						<li><a href="#organising-decluttering/">Home Organisation</a></li>
						<li><a href="#oven-cleaning/">Oven Cleaning</a></li>
					  </ul>
					</div>
                </div>
            </div>
           	<!--
		   <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=7') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task2.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Delivery & Removals</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
						</a>
						<div class="task__subtasks list-col-4">
						<ul>
						<li><a href="#/delivery-and-removals/adriano-zumbo-delivery/">Adriano Zumbo Delivery</a></li>
						<li><a href="#/delivery-and-removals/aldi-home-delivery/">ALDI Home Delivery</a></li>
						<li><a href="#/delivery-and-removals/apple-store-delivery/">Apple Store Delivery</a></li>
						<li><a href="#/delivery-and-removals/bridge-st-garage-delivery/">Bridge St Garage Delivery</a></li>
						<li><a href="#/delivery-and-removals/chat-thai-delivery/">Chat Thai Delivery</a></li>
						<li><a href="#/delivery-and-removals/chur-burger-delivery/">Chur Burger Delivery</a></li>
						<li><a href="#/delivery-and-removals/food-delivery/">Food Delivery </a></li>
						<li><a href="#/delivery-and-removals/furniture-delivery/">Furniture Delivery</a></li>
						<li><a href="#/delivery-and-removals/furniture-removal/">Furniture Removals</a></li>
						<li><a href="#/delivery-and-removals/gelato-messina-delivery/">Gelato Messina Delivery</a></li>
						<li><a href="#/delivery-and-removals/golden-century-delivery/">Golden Century Delivery</a></li>
						<li><a href="#/delivery-and-removals/grocery-shopping/">Grocery Shopping Assistants</a></li>
						<li><a href="#/delivery-and-removals/haighs-chocolate-delivery/">Haighs Chocolate Delivery</a></li>
						<li><a href="#/delivery-and-removals/heavy-item-delivery/">Heavy Item Delivery</a></li>
						<li><a href="#/delivery-and-removals/hot-chocolate-delivery/">Hot Chocolate Delivery</a></li>
						<li><a href="#/delivery-and-removals/huxtaburger-delivery/">Huxtaburger Delivery</a></li>
						<li><a href="#/delivery-and-removals/kfc-delivery/">KFC Delivery</a></li>
						<li><a href="#/delivery-and-removals/lord-of-the-fries-delivery/">Lord of the Fries Delivery</a></li>
						<li><a href="#/delivery-and-removals/mamak-delivery/">Mamak Delivery</a></li>
						<li><a href="#/delivery-and-removals/van-removals/">Van Removals</a></li>
						<li><a href="#/delivery-and-removals/mcdonalds-delivery/">McDonalds Delivery</a></li>
						<li><a href="#/delivery-and-removals/moving-and-removalist/">Moving &amp; Removalist</a></li>
						<li><a href="#/delivery-and-removals/mr-crackles-delivery/">Mr Crackles Delivery</a></li>
						<li><a href="#/delivery-and-removals/pickup-and-delivery/">Pickup &amp; Delivery</a></li>
						<li><a href="#/delivery-and-removals/mattress-removal/">Mattress Removal</a></li>
						<li><a href="#/delivery-and-removals/fridge-moving-removal/">Fridge Moving &amp; Removal</a></li>
					  </ul>
						</div>
				
                </div>
            </div>
			
		
            <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=8') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task3.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Handyman & Trades</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#/tradesman/handyman-help/">Handyman Services</a></li>
						<li><a href="#/tradesman/ikea-furniture-assembly/">Furniture Assembly</a></li>
						<li><a href="#/tradesman/carpentry/">Carpentry</a></li>
						<li><a href="#/tradesman/appliance-repair/">Appliance Repair</a></li>
						<li><a href="#/tradesman/childrens-outdoor-play-equipment-installation/">Childrens Outdoor Play Equipment Installation</a></li>
						<li><a href="#/tradesman/dishwasher-services/">Dishwasher Services</a></li>
						<li><a href="#/tradesman/dryer-services/">Dryer Services</a></li>
						<li><a href="#/tradesman/electrical-installation/">Electrical Installation</a></li>
						<li><a href="#/tradesman/electrical-work/">Electrical Services</a></li>
						<li><a href="#/tradesman/fencing-services/">Fencing Services</a></li>
						<li><a href="#/tradesman/floor-installer/">Floor Installation</a></li>
						<li><a href="#/tradesman/freezer-services/">Freezer services</a></li>
						<li><a href="#/tradesman/fridge-services/">Fridge Services</a></li>
						<li><a href="#/tradesman/guttering-tasks/">Guttering</a></li>
						<li><a href="#/tradesman/lighting-solutions/">Lighting Solutions</a></li>
						<li><a href="#/tradesman/locksmith/">Locksmith</a></li>
						<li><a href="#/tradesman/mechanic/">Mechanic</a></li>
						<li><a href="#/tradesman/microwave-services/">Microwave services</a></li>
						<li><a href="#/tradesman/oven-services/">Oven services</a></li>
						<li><a href="#/tradesman/painting/">Painting</a></li>
						<li><a href="#/tradesman/plumbing/">Plumbing</a></li>
						<li><a href="#/tradesman/pool-and-hot-tub/">Pool &amp; Hot Tub Maintenance</a></li>
						<li><a href="#/tradesman/roofing/">Roofing</a></li>
						<li><a href="#/tradesman/shed-installation/">Shed Installation</a></li>
						<li><a href="#/tradesman/sink-services/">Sink services</a></li>
						<li><a href="#/tradesman/stove-services/">Stove services</a></li>
						<li><a href="#/tradesman/trampoline-installation/">Trampoline Installation</a></li>
						<li><a href="#/tradesman/washing-machine-services/">Washing machine services</a></li>
						<li><a href="#/tradesman/waste-removal/">Waste Removal</a></li>
						<li><a href="#/tradesman/whitegoods-installation/">Whitegoods Installation</a></li>
						<li><a href="#/tradesman/window-services/">Window Services</a></li>
						<li><a href="#/tradesman/furniture-repair/">Furniture Repairs</a></li>
						<li><a href="#/tradesman/tv-mounting/">TV Mounting and Installation</a></li>
						<li><a href="#/tradesman/milan-direct-furniture-assembly/">Milan Direct Furniture Assembly</a></li>
						<li><a href="#/tradesman/temple-and-webster-furniture-assembly/">Temple &amp; Webster Furniture Assembly</a></li>
				</ul>
			  </div>
					
                </div>
            </div>
          
		  -->
		  
		  <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=9') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task4.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Marketing & Design</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#/marketing-and-design/flyer-delivery/">Flyer Delivery</a></li>
						<li><a href="#/marketing-and-design/website-and-app-testing/">Website &amp; App Testing</a></li>
						<li><a href="#/marketing-and-design/interview-candidates/">MR Interview Candidates</a></li>
						<li><a href="#/marketing-and-design/focus-group-candidates/">Focus Group Candidates</a></li>
						<li><a href="#/marketing-and-design/content-creation/">Content Creation</a></li>
						<li><a href="#/marketing-and-design/design/">Design</a></li>
						<li><a href="#/marketing-and-design/graphic-design/">Graphic Design</a></li>
						<li><a href="#/marketing-and-design/print-and-design/">Print &amp; Design</a></li>
						<li><a href="#/marketing-and-design/web-design/">Web Design</a></li>
						<li><a href="#/marketing-and-design/market-research/">Market Research</a></li>
					</ul>
					</div>
					
                </div>
            </div>
           
		   
		   <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=11') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task5.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Events & Photography</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#/event-and-photography/bartending/">Bartending</a></li>
						<li><a href="#/event-and-photography/catering/">Catering</a></li>
						<li><a href="#/event-and-photography/chef/">Chef</a></li>
						<li><a href="#/event-and-photography/dj/">DJ</a></li>
						<li><a href="#/event-and-photography/event-setup-and-cleaning/">Event Setup &amp; Cleaning</a></li>
						<li><a href="#/event-and-photography/event-staff/">Event Staff</a></li>
						<li><a href="#/event-and-photography/hair-and-make-up-artist/">Hair &amp; Make up Artist</a></li>
						<li><a href="#/event-and-photography/models/">Models</a></li>
						<li><a href="#/event-and-photography/photo-and-video-editing/">Photo &amp; Video Editing</a></li>
						<li><a href="#/event-and-photography/photography-and-video/">Photography &amp; Video</a></li>
						<li><a href="#/event-and-photography/promotional-staff/">Promotional Staff</a></li>
						<li><a href="#/event-and-photography/wedding-assistant/">Wedding Assistant</a></li>
						<li><a href="#/event-and-photography/wedding-help/">Wedding Help</a></li>
						<li><a href="#/event-and-photography/wedding-photographer/">Wedding photography</a></li>
						<li><a href="#/event-and-photography/wedding-planning/">Wedding Planning</a></li>
						<li><a href="#/event-and-photography/product-photography/">Product Photography</a></li>
						<li><a href="#/event-and-photography/photography-sydney/">Photography Sydney</a></li>
						<li><a href="#/event-and-photography/photography-melbourne/">Photography Melbourne</a></li>
						<li><a href="#/event-and-photography/photography-brisbane/">Photography Brisbane</a></li>
						<li><a href="#/event-and-photography/xmas-help/">Christmas Help</a></li>
					  </ul>
					</div>
					
                </div>
            </div>
            
		<!--
<div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=5') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task6.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Computer & IT</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					  <ul>
						<li><a href="#/computer-it-web/computer-and-it-support/">Computer &amp; IT Support</a></li>
						<li><a href="#/computer-it-web/apple-help/">Apple Help</a></li>
						<li><a href="#/computer-it-web/command-iq-help/">Command IQ Help</a></li>
						<li><a href="#/computer-it-web/hardware-setup/">Hardware Setup</a></li>
						<li><a href="#/computer-it-web/internet-setup/">Internet Setup</a></li>
						<li><a href="#/computer-it-web/itunes-help/">iTunes Help</a></li>
						<li><a href="#/computer-it-web/mobile-app-development/">Mobile App Development</a></li>
						<li><a href="#/computer-it-web/website-and-blog-support/">Website &amp; Blog Support</a></li>
						<li><a href="#/computer-it-web/website-development/">Website Development</a></li>
					  </ul>
					</div>
					
                </div>
            </div>
         
			<div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=10') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task7.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Business & Admin</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					  <ul>
						<li><a href="#/business-and-admin/office-commercial-cleaning/">Office Cleaning</a></li>
						<li><a href="#/business-and-admin/accounting/">Accounting</a></li>
						<li><a href="#/business-and-admin/admin-assistance/">Admin Assistance</a></li>
						<li><a href="#/business-and-admin/data-entry/">Data Entry</a></li>
						<li><a href="#/business-and-admin/gift-research/">Gift Research</a></li>
						<li><a href="#/business-and-admin/receipts-organisation/">Receipts Organisation</a></li>
						<li><a href="#/business-and-admin/translation/">Translation</a></li>
						<li><a href="#/business-and-admin/travel-planning/">Travel Planning</a></li>
						<li><a href="#/business-and-admin/temporary-staff/">Temporary Staff</a></li>
					  </ul>
					</div>
                </div>
            </div>
			   -->
			   
			<!--
            <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task8.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Fun & Quirky</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#/fun-and-quirky/driver-training/">Driver Training</a></li>
						<li><a href="#/fun-and-quirky/queue-line-up/">Queue Line Up</a></li>
						<li><a href="#/fun-and-quirky/sports-lessons/">Sports Lessons</a></li>
						<li><a href="#/fun-and-quirky/game-of-thrones-queue/">Game of Thrones Queue Line Up</a></li>
						<li><a href="#/fun-and-quirky/apple-queue/">iPhone 7 Line Up</a></li>
						<li><a href="#/fun-and-quirky/iphone7-queue/">iPhone 7 Line Up</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			-->
			<div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=2') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task__home-garden.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Home & garden</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					<div class="task__subtasks list-col-4">
					  <ul>
						<li><a href="#">House Cleaning</a></li>                   
						<li><a href="#">Garden Maintenance</a></li>
						<li><a href="#">Health & Wellness</a></li>
						<li><a href="#">Home Theatre Help</a></li>
						<li><a href="#">House Sitting</a></li>
						<li><a href="#">Landscaping</a></li>
						<li><a href="#">Laundry Services</a></li>
						<li><a href="#">Lawncare</a></li>
						<li><a href="#">Nutritional Advice</a></li>
						<li><a href="#">Dog Walking</a></li>
						<li><a href="#">Steam Cleaning</a></li>
						<li><a href="#">Upholstery & Cleaning</a></li>
						<li><a href="#">Vacate Cleaning</a></li>
						<li><a href="#">Wardrobe Organisation</a></li>
						<li><a href="#">Window Cleaning</a></li>
						<li><a href="#">Chef</a></li>
						<li><a href="#">Pet Sitting</a></li>
						<li><a href="#">Home Organisation</a></li>
						<li><a href="#">Oven Cleaning</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
                </div>
            </div>

			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task3.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Handyman & Trades</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Handyman Services</a></li>
						<li><a href="#">Furniture Assembly</a></li>
						<li><a href="#">Carpentry</a></li>
						<li><a href="#">Appliance Repair</a></li>
						<li><a href="#">Childrens Outdoor Play Equipment Installation</a></li>
						<li><a href="#">Dishwasher Services</a></li>
						<li><a href="#">Dryer Services</a></li>
						<li><a href="#">Electrical Installation</a></li>
						<li><a href="#">Electrical Services</a></li>
						<li><a href="#">Fencing Services</a></li>
						<li><a href="#">Floor Installation</a></li>
						<li><a href="#">Freezer services</a></li>
						<li><a href="#">Fridge Services</a></li>
						<li><a href="#">Guttering</a></li>
						<li><a href="#">Lighting Solutions</a></li>
						<li><a href="#">Locksmith</a></li>
						<li><a href="#">Mechanic</a></li>
						<li><a href="#">Microwave services</a></li>
						<li><a href="#">Oven services</a></li>
						<li><a href="#">Painting</a></li>
						<li><a href="#">Plumbing</a></li>
						<li><a href="#">Roofing</a></li>
						<li><a href="#">Shed Installation</a></li>
						<li><a href="#">Sink services</a></li>
						<li><a href="#">Stove services</a></li>
						<li><a href="#">Trampoline Installation</a></li>
						<li><a href="#">Washing machine services</a></li>
						<li><a href="#">Waste Removal</a></li>
						<li><a href="#">Whitegoods Installation</a></li>
						<li><a href="#">Window Services</a></li>
						<li><a href="#">Furniture Repairs</a></li>
						<li><a href="#">TV Mounting and Installation</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task2.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Deliveries & Removals</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Take Away Delivery</a></li>
						<li><a href="#">Lidl Home Delivery</a></li>
						<li><a href="#">Food Delivery </a></li>
						<li><a href="#">Furniture Delivery</a></li>
						<li><a href="#">Furniture Removals</a></li>
						<li><a href="#">Grocery Shopping Assistants</a></li>
						<li><a href="#">Heavy Item Delivery</a></li>
						<li><a href="#">Coffee Delivery</a></li>
						<li><a href="#">Van Removals</a></li>
						<li><a href="#">McDonalds Delivery</a></li>
						<li><a href="#">Moving & Removalists</a></li>
						<li><a href="#">Pickup & Delivery</a></li>
						<li><a href="#">Mattress Removal</a></li>
						<li><a href="#">Fridge Moving & Removal</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task__marketing-design.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Marketing & Design</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Flyer Delivery</a></li>
						<li><a href="#">Content Creation</a></li>
						<li><a href="#">Design</a></li>
						<li><a href="#">Graphic Design</a></li>
						<li><a href="#">Print & Design</a></li>
						<li><a href="#">Web Design</a></li>
						<li><a href="#">Market Research</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			
			
			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task__events-photography.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Events & Photography</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Bartending</a></li>
						<li><a href="#">Catering</a></li>
						<li><a href="#">Chef</a></li>
						<li><a href="#">DJ</a></li>
						<li><a href="#">Event Setup & Cleaning</a></li>
						<li><a href="#">Event Staff</a></li>
						<li><a href="#">Hair & Makeup Artist</a></li>
						<li><a href="#">Models</a></li>
						<li><a href="#">Photo & Video Editing</a></li>
						<li><a href="#">Photography & Video</a></li>
						<li><a href="#">Promotional Staff</a></li>
						<li><a href="#">Wedding Assistant</a></li>
						<li><a href="#">Wedding Help</a></li>
						<li><a href="#">Wedding photography</a></li>
						<li><a href="#">Wedding Planning</a></li>
						<li><a href="#">Product Photography</a></li>
						<li><a href="#">Photography</a></li>
						<li><a href="#">Xmas Help</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			


			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task6.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Computer & IT</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Computer & IT Support</a></li>
						<li><a href="#">Apple Help</a></li>
						<li><a href="#">Command IQ Help</a></li>
						<li><a href="#">Hardware Setup</a></li>
						<li><a href="#">Internet Setup</a></li>
						<li><a href="#">iTunes Help</a></li>
						<li><a href="#">Mobile App Development</a></li>
						<li><a href="#">Website & Blog Support</a></li>
						<li><a href="#">Website Development</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

			

			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task7.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Business & Admin</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Office Cleaning</a></li>
						<li><a href="#">Accounting</a></li>
						<li><a href="#">Admin Assistance</a></li>
						<li><a href="#">Assignment Help</a></li>
						<li><a href="#">Data Entry</a></li>
						<li><a href="#">Gift Research</a></li>
						<li><a href="#">Receipts Organisation</a></li>
						<li><a href="#">Translation</a></li>
						<li><a href="#">Travel Planning</a></li>
						<li><a href="#">Admin Assistance</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>
			
			
			
			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task8.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Fun & Quirky</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Driver Training</a></li>
						<li><a href="#">Queue Line Up</a></li>
						<li><a href="#">Sports Lessons</a></li>
						<li><a href="#">Music Lessons</a></li>
						<li><a href="#">Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>
			
			

			
			 <div class="task-box col-sm-12">
                <div class="task__inr">
                    <a href="{{ url('tasks?cat_id=12') }}">
                        <div class="task__img">
                            <img src="{{ asset('front/images/task__any-other-tasks.jpg') }}" alt="task">
                        </div>
                        <div class="task__cont valigner">
                            <div class="valign">
                                <h4>Any Other Tasks</h4>
                            </div>
                        </div>
                        <div class="task__cont__btm">
                            <p>Find out More</p>
                        </div>
                    </a>
					
					<div class="task__subtasks list-col-4">
					<ul>
						<li><a href="#">Any Other Tasks</a></li>
					  </ul>
					</div>
					
                </div>
            </div>

        </div>
    </div>
</section>
@endsection