@extends('layout')
<?php
$required = '';
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div class="edit-profile-area">
                        <div class="tab-cont-title text-center">
                            <h3><i class="fa fa-mobile"></i> Mobile</h3>
                        </div>
                        @include('front.common.errors')


                        <div class="clearfix"></div><br>
                        <div class="edit__inputs col-sm-12">
                             @if($user->verified)
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i> Account Status:                      
                                Verified   
                            </div>
                            @endif
                            <p>Enter your mobile to receive SMS updates and calls for tasks that you have posted or made offers on.</p>

                            <p><small>Mobile number
                                    We will send you a verification code</small></p>
                           
                            <form accept-charset="UTF-8" method="POST" action="<?php echo url('mobile/store'); ?>">
                                <div class="row">
                                    <div class="form-group col-sm-3">
                                        <!--                                            <label for="mobileNumber">Add Mobile</label>-->
                                        <input type="text" class="form-control" name="countryCode" id="countryCode" placeholder="Country Code" value="" required="required">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <!--                                            <label for="mobileNumber">Add Mobile</label>-->
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Your number" value="" required="required">
                                    </div>
                                </div>                                   
                                <div class="form-group">
                                    <div class="lnk-btn0 inline-block save-prof-btn0 mb30">
                                        <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                        <input class="btn btn-primary" type="submit" value="Send">
                                    </div>
                                </div>
                            </form>                       
                            <div class="mob__img">
                                <img src="{{asset('')}}/front/images/mobile-verify.png" alt="" />
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            </section>
            @endsection
