@extends('layout')
<?php
$required = '';
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">

                    <div class="edit-profile-area">
                        <div class="tab-cont-title text-center">
                            <h3><i class="fa fa-mobile"></i> Verification</h3>
                        </div>
                        @include('front.common.errors')
                        <div id="receiveErrors"></div>
                        <form name="receiveForm" class="receiveForm">
                            <div class="clearfix"></div><br>
                            <div class="edit__inputs col-sm-12">
                                <p><small>We send you a verification code</small></p>

                                <div class="input-group col-sm-6">
                                    <input type="number" class="form-control" name="token" id="token" placeholder="Type your code here" value="">                           <span class="input-group-addon p0"><input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                        <input class="btn btn-primary" type="button" value="Save" onclick="saveReceive();"></span>
                                </div>


                                <div class="form-group mt10">
                                    <p><small>If you haven't received a verification code</small></p>
                                    <a href="{{ url('verifyResend') }}" class="btn btn-primary">Resend Code</a>
                                </div>
                                <div class="mob__img">
                                    <img src="{{asset('')}}/front/images/mobile-verify.png" alt="" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
</section>
<script>
    function saveReceive() {
        var formdata = $(".receiveForm").serialize();
        $('#receiveErrors').html("").hide();
        $.ajax({
            url: "<?php echo url('mobile/verify'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });
                    errorsHtml += '</ul></div>';
                    $('#receiveErrors').html(errorsHtml).show();
                } else
                {
                    $('#receiveErrors').html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-check"></i> Submitted ! Verified successfully</div>').show();
                    window.location.href = '<?php echo url('mobile'); ?>';
                }
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>    
@endsection
