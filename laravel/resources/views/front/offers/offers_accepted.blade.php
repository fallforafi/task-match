@extends('layout')

@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">

                        <div class="edit-profile-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-list"></i> Unfinished Tasks - Offers Accepted</h3>
                            </div>
                            @include('front.common.errors')
                            @if(count($model)>0)
                            <div class="table-responsive">          
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Due Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($model as $row)
                                        <tr>
                                            <td><a href="{{url('task/offer/')}}/<?php echo $row->offer_id ?>"><?php
                                                    echo $i;
                                                    $i++;
                                                    ?></a></td>
                                            <td><a href="{{url('task/offer/')}}/<?php echo $row->offer_id ?>">{{ $row->title }}</a></td>
                                            <td>{{ $row->description }}</td>
                                            <td>{{ date('d/m/Y', strtotime($row->dueDate)) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div class="text-center">
                                <h4>You haven’t been assigned to any task yet browse available tasks and make your offers</h4>
                                <a href="{{ url('tasks') }}" class="btn btn-default">Browse Tasks</a>

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection                            