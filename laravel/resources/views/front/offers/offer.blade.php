@extends('layout')
@section('content')

<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
?>
<link rel="stylesheet" href="{{asset('front/css/messages.css')}}">
<div class="main_section">
    <div class="container">

        <div class="col-sm-12 message_section" >
            <div class="clearfix"></div>
            <div class="task__accepted__msg">
                <div class="left clearfix">
                    <span class="chat-img pull-left">
                        <?php
                        $row = $model;
                        ?>
                        @include('front/common/profile_picture')
                    </span>
                    <div class="chat-body clearfix">
                        <div class="header_sec">
                            <strong class="primary-font">{{$model->firstName}} {{$model->lastName}}</strong> <strong class="pull-right">
                                <?php echo showTime($model->created_at) ?></strong>
                        </div>
                        <div class="contact_sec">
                            <strong class="primary-font"><a href="{{url('/task/')}}/{{$model->taskKey}}">{{$model->title}}</a></strong> <span><strong>Task Price: {{$currency}}{{$model->price}}</strong></span>
                            <div class="clearfix"></div>
                            <span><strong>Your offered price: {{$currency}}{{$model->offerPrice}} &nbsp; <i class="fa fa-check-circle-o" aria-hidden="true"></i></strong></span>
                        </div><br>
                        <?php
                        if ($model->offerStatus == 'assigned') {
//        $thread_id =$model->message_id;
//        $task_id=$model->task_id;
                            ?>
                            <a class="col-sm-4 col-sm-offset-4 btn btn-primary btn-lg btn-flat" href="{{ url('message/'.$model->key) }}">Send Private Messages</a>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection