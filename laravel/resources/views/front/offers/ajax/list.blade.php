<?php
$reviews = $user_reviews['reviews'];
$average = $user_reviews['average'];
$ratings = $user_reviews['ratings'];
 //d($model);
?>
<div class="hed crossline mini"><h2>Offers <span class="badge">{{count($model)}}</span></h2><hr></div>
<div class="brs__rgt__commt">
    <div class="list-group reviews-area">
        @if(count($model)>0)
        @foreach($model as $row)
        <div class="reviews-box col-sm-6 overload">
			<table class="table table-border--no bdr1">
			<tr>
            <td class="brs__postBy__img">@include('front/common/profile_picture')</td>
            <td>
			<small><a href="{{url('user/')}}/{{$row->user_key}}">{{$row->firstName}} {{$row->lastName}}</a></small>
			
                <div class="prof__rate clrlist">
                    <ul>
                        @for($i=1;$i<=$average;$i++)
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        @endfor
                        @for($i=5;$i>$average;$i--)
                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                        @endfor
                    </ul>
                </div>
			</td>
			
            <td class="pull-right ml10 view-btn">
                <?php if (isset(Auth::user()->id) && Auth::user()->id == $tasks->user_id) { ?>
                    <a href="{{ url('offers/') }}/<?php echo $row->key;?>" class="btn btn-success btn-sm" tabindex="0">View</a>
                <?php } ?>
            </td>
            </tr>
			</table>
            
			
				<div class="clearfix"></div>
        </div>
		<div class="clearfix"></div>
        @endforeach
        @else
        <?php
        if (!isset(Auth::user()->id)) {
            ?>
            <p>There are no offers yet on this task. <a data-toggle = "modal" data-target = "#signIn">Why not make one?</a></p>
        <?php } else {
            ?>
            <p>There are no offers yet on this task.</p>
        <?php } ?>     
        @endif
    </div>
</div>
