@extends('layout')
<?php
$title = 'Create';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
$minus = ($task->price / 100) * $commission;
$actualPrice = $task->price - $minus;
?>

<!-- Modal content-->
<div class="modal-content" id="offerBody">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Amend Offer</h4>
    </div>
    <div class="modal-body">
        <div class="make-offer-tip">
            <p><i class="fa fa-check-circle-o"></i> &nbsp; Tip: The more information you give in your offer the higher chance you have of being successful. i.e relevant experience, availability, etc</p>
        </div>
        <div class="make__offer__inputs hover-anchor">
            <div id="errors"></div>

            <form id="offerForm" name="offerForm">
                <div class="form-group col-sm-4">
                    <label class="" for="offerPrice"><a href="#"><i class="fa fa-question-circle"></i></a>Offer price</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                        <input class="form-control" id="offerPrice" name="offerPrice" value="<?php echo $task->price; ?>" placeholder="<?php echo $task->price; ?>" type="number">
                    </div>
                </div>
                <div class="form-group col-sm-4">
                    <label class="" for="getPaid">You get Paid</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                        <input disabled="disabled" class="form-control" id="getPaid" name="getPaid" placeholder="<?php echo $actualPrice; ?>" value="<?php echo $actualPrice; ?>" aria-disabled="true">
                    </div>
                </div>
                <div class="form-group col-sm-4">
                    <label class="" for="servFee">Service Fee</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                        <input disabled="disabled" class="form-control yourPrice" id="serviceCharge" name="serviceCharge" placeholder="<?php echo $minus; ?>" value="<?php echo $minus; ?>" aria-disabled="true">
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group col-sm-12 mt20">
                    <label class="" for="offerDtl">Offer Details</label>
                    <textarea class="form-control" cols="45" id="description" name="description" placeholder="Explain why you are the right person for the job" required="" rows="3" aria-multiline="true" tabindex="0" required min="20" max="500" aria-required="true" aria-invalid="true">{{$task->message}}</textarea>
                </div>
                <div class="clearfix"></div>
                <div class="form-group button-btn pul-rgt col-sm-12 mt20">
                    <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="task_id" id="task_id" value="<?php echo $task->id; ?>">
                    <input type="hidden" name="offer_id" id="offer_id" value="<?php echo $task->of_id; ?>">
                    <input type="button" id="test1" class="form-control" value="Amend offer" onclick="saveAmend();">
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    function pageReload() {
        location.reload();
    }
    $("#offerPrice").keyup(function (e) {
        setCommission();
    });
    $("#offerPrice").change(function (e) {

        setCommission();
    });

    function setCommission()
    {
        var offerPrice = $("#offerPrice").val();
        var commission =<?php echo $commission; ?>;
        var serviceCharge;
        var getPaid;
        serviceCharge = (offerPrice / 100) * commission;
        getPaid = offerPrice - serviceCharge;
        $("#getPaid").val(getPaid);
        $("#serviceCharge").val(serviceCharge);

    }

    function saveAmend() {
        var formdata = $("#offerForm").serialize();
        $('#errors').html("").hide();
        $.ajax({
            url: "<?php echo url('amend-offer/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#errors').html(errorsHtml).show();
                } else
                {
                $('#errors').html('<div class="alert alert-success">Successfully Updated!</div>').show();
                window.location.assign('<?php echo url('my-tasks');?>');
                }
                // window.location = "<?php //echo url('task/create');                             ?>";
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
@endsection
