@extends('layout')

@section('content')
<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
// d($cards,1);
?>
<div class="container">
    <div class="dboard__main col-sm-12 mt50 mb50">
        <div class="dboard__rgt">
            <div class="tab-content dboard-tab-height">
                <div id="profile" class="tab-pane fade in active">

                    <div class="edit-profile-area overload">
                        <div class="tab-cont-title text-center">
                            <h3><i class="fa fa-list"></i> Offer from {{$user->firstName}} {{$user->lastName}} on your task '{{$task->title}}'</h3>
                        </div>
                        @include('front.common.errors')
                        @if(count($model)>0)
                        <div class="table-responsive">

                            <div class="offer col-sm-12 mt30 mb10">

                                <div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#offer" aria-controls="offer" role="tab" data-toggle="tab">Offer Details</a></li>
                                        <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Taskrunner Info</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="offer">
                                            <div class="table-responsive">

                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td class="col-sm-3">Description</td>
                                                            <td>{{$offer->message}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Created</td>
                                                            <td><?php echo relativeTime(strtotime($offer->created_at), true) ?></td>

                                                        </tr>
                                                        <tr>
                                                            <td>Price</td>
                                                            <td>{{$currency}} {{$model->offerPrice}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="info">
                                            <div class="tab-content">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td class="col-sm-3">Name</td>
                                                            <td>{{$user->firstName}} {{$user->lastName}}</td>

                                                        </tr>
                                                        <tr>
                                                            <td>Member Since</td>
                                                            <td>{{date("F d, Y",strtotime($user->created_at))}}</td>

                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td>{{$user->about}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Location</td>
                                                            <td>{{$user->location}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tagline</td>
                                                            <td>{{$user->tagline}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Public Profile</td>
                                                            <td><a class="btn btn-primary" href="{{url('user/')}}/<?php echo $user->key; ?>">View Profile</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @if($task->taskStatus == "completed" && $offer->offerStatus == "completed")
                                    <?php if (isset(Auth::user()->id) && Auth::user()->id == $user->id) { ?>
                                        <br><a class="btn btn-success btn-green" href="{{ url('feedback/'.$task->id) }}" >Review {{ $task->firstName .' '.$task->lastName}}</a>
                                    <?php } else { ?>
                                        <br><a class="btn btn-success btn-green" href="{{ url('feedback/'.$task->id) }}" >Review {{ $user->firstName .' '.$user->lastName}}</a>  
                                    <?php } ?>
                                    @endif

                                    @if($task->taskStatus == "open" && $offer->offerStatus == "pending")

                                    @if(count($cards)>0)
                                    <div class="actions" id="options">

                                        <a href="javascript:void(0)" id="accept" class="btn btn-success">Accept & Send Message</a>

                                        <a href="#" id="reject" data-toggle="modal" data-target="#confirm-submit" class="btn btn-danger">Reject</a>

                                    </div>
                                    <div class="row" style="display:none;" id="accept_option">
                                        {!! Form::open(array( 'class' => 'form','id' => 'form','url' =>'assign', 'name' => 'checkout')) !!}
                                        <div class="col-sm-12">
                                            <h3>Send Message</h3>
                                            {!! Form::textarea('message', null, ['size' => '30x5','class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-sm-12">
                                            <h3>Choose Card</h3>

                                            {!! Form::hidden('offer_price_id',$model->id)!!}
                                            {!! Form::hidden('offer_id',$offer->id)!!}
                                            {!! Form::hidden('user_id',Auth::user()->id)!!}
                                            @foreach($cards as $card)
                                            <div class="form-group col-sm-12">
                                                <label class="radio-inline">
                                                    {!! Form::radio('card_id', $card->id );!!} {{$card->cardholderName}} - {{$card->cardNumber}}
                                                </label>
                                            </div>
                                            @endforeach
                                            <div class="form-group col-sm-4" >
                                                <input type="button" name="btn" value="Accept" id="accept" data-toggle="modal" data-target="#confirm-submit" class="btn btn-success" />
                                                <a href="javascript:void(0)" id="cancel" class="btn btn-warning">Cancel</a>
                                            </div>

                                            {!! Form::close() !!}
                                        </div>

                                        @else
                                        <div class="text-center">
                                            <h4>You haven’t added any payment infomation to assign task</h4>
                                            <a href="{{ url('payments') }}" class="btn btn-default">Add Payment Method</a>
                                        </div>
                                        @endif
                                    </div>
                                    @elseif(($task->taskStatus=='assigned' && $offer->offerStatus=='assigned') &&  $task->user_id==Auth::user()->id)

                                    <div class="actions">
                                        {!! Form::open(array( 'class' => 'form','id' => 'form','url' =>'payassignee', 'name' => 'checkout')) !!}
                                        {!! Form::hidden('offer_price_id',$model->id)!!}
                                        {!! Form::hidden('offer_id',$offer->id)!!}
                                        {!! Form::hidden('user_id',Auth::user()->id)!!}
                                        <div class="form-group pt20">
                                            <input type="button" name="btn" value="Mark complete & make payment" id="paynow" data-toggle="modal" data-target="#confirm-submit" class="btn btn-success" />
                                        </div>
                                        {!! Form::close() !!}

                                    </div>
                                    <a href="{{ url('message/'.$threadsModel->key) }}" class="btn btn-primary">Send a message</a>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header" id="modalTitle">

                                    </div>
                                    <div class="modal-body" id="modalBody">
                                        Are you sure you want to submit the following details?
                                    </div>
                                    <div class="modal-body" id="modalBodyReject" style="display: none;">
                                        <div id="errors1"></div>
                                        <form class="form" name="form" id="myForm">
                                            <div class="form-group" style="width: auto; overflow: hidden;">
                                                <p>Please give us more information regarding this rejection</p>
                                                <br>
                                                <textarea name="reason" class="form-control" id="reason" placeholder="Comment (required)" spellcheck="true" style="z-index: auto; position: relative; line-height: normal; font-size: 12px; transition: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255); border: 1px solid rgba(191, 212, 219, 0.95);"></textarea>
                                            </div>
                                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                            <input type="hidden" name="key" id="key" value="<?php echo $model->key; ?>">
                                        </form>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="modalDismiss">Cancel</button>
                                        <button type="button" id="submit" class="btn btn-success success">Pay Now</button>
                                        <button type="button" id="submitReject" class="btn btn-success success">Yes</button>
                                        <button type="button" id="submitPay" class="btn btn-success success">Yes</button>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="text-center">
                                <h4>You haven’t added any payment infomation to assign task</h4>
                                <a href="{{ url('payments') }}" class="btn btn-default">Add Payment Method</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function pageReload() {
        location.reload();
    }
    function saveOfferReject() {
        var formdata = $(".form").serialize();
        $('#errors1').html("").hide();
        $.ajax({
            url: "<?php echo url('offer/reject'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#errors1').html(errorsHtml).show();
                } else
                {
                    $('#errors1').html('<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Successfully Submitted!</div>').show();

                }
            },
            error: function (xhr, status, response) {
            }
        });
    }
    $('#accept').click(function () {
        $('#modalTitle').html('<button type="button" class="close refresh" onclick="pageReload()">&times;</button><h3>Please pay <?php echo $currency . $model->offerPrice; ?> to have this task completed.</h3>');
        $('#modalBody').html("Your funds will be held securely with Taskmatch pay untill the task has been completed.");
        $('#submitPay').hide();
        $('#submitReject').hide();
        $('#submit').show();
    });

    $('#reject').click(function () {
        $('#modalTitle').html('<button type="button" class="close refresh" onclick="pageReload()">&times;</button><h3>Are you sure you want to reject this offer?</h3>');
        $('#modalBodyReject').show();
        $('#modalBody').hide();
        $('#submit').hide();
        $('#submitPay').hide();
        $('#submitReject').show();
    });

    $('#paynow').click(function () {
        $('#modalTitle').html('<button type="button" class="close refresh" onclick="pageReload()">&times;</button><h3>Please confirm the following action</h3>');
        $('#modalBody').html("Are you sure you want to finish this task and pay the taskrunner?");
        $('#submit').hide();
        $('#submitReject').hide();
        $('#submitPay').show();
    });


    $('#accept').click(function () {
        $('#accept_option').show();
        $('#options').hide();


    });

    $('#cancel').click(function () {
        $('#accept_option').hide();
        $('#options').show();
    });

    $('#submitReject').click(function () {
        saveOfferReject();
        // window.top.location = "<?php echo url("offer/reject/" . $model->key); ?>";
    });

    $('#submitPay').click(function () {
        $(this).prop('disabled', true);
        $('#modalDismiss').prop('disabled', true);
        $('.close').prop('disabled', true);
        $('#form').submit();

    });

    $('#submit').click(function () {
        $(this).prop('disabled', true);
        $('#modalDismiss').prop('disabled', true);
        $('.close').prop('disabled', true);
        $('#form').submit();

    });
</script>
@endsection                            