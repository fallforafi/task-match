@extends('layout')
@section('content')
<?php
$required = '';
?>
<section class="dashboard-area">
    <div class="container">


        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="profile" class="tab-pane fade active in">

                    <div class="profile__dtl col-sm-12">
                        @include('front.common.errors')

                        {!! Form::model($user, ['files' => true,'class' => 'form','url' => ['updateprofile'], 'method' => 'post']) !!}

                        <div class="form-group col-sm-6">
                            {!! Form::label('First Name') !!}
                            {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('Education') !!}
                            {!! Form::text('education', isset($education)?$education:null , array('class' => 'form-control',$required,'placeholder' => 'Add a tag comma separated') ) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('Last Name') !!}
                            {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('Transport') !!}<br>
                            @foreach(Config::get('params.transportTypes') as $key => $value) 
                            {!! Form::checkbox('transport[]', $key, in_array($key,$transport)) !!} {{ $value }}
                            @endforeach
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('email') !!}
                            {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('Languages') !!}
                            {!! Form::text('language',isset($language)?$language:null, array('class' => 'form-control',$required, 'placeholder' => 'Add a tag comma separated') ) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('phone') !!}
                            {!! Form::text('phone', null , array('class' => 'form-control',$required,'placeholder' => '+353') ) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('tagline') !!}
                            {!! Form::text('tagline', null , array('class' => 'form-control',$required,'placeholder' => 'Add a mini bio') ) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('Location') !!}
                            {!! Form::text('location', null , array('class' => 'form-control',$required,'placeholder' => 'Your City') ) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('about me') !!}
                            {!! Form::textarea('about', null, ['class' => 'form-control', 'rows' => '5', 'placeholder' => 'Tell us a little about yourself']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('Work Experience and Skills') !!}
                            {!! Form::text('skill',isset($skill)?$skill:null, array('class' => 'form-control',$required, 'placeholder' => 'Add a tag comma separated') ) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success">Save Profile</button>
                    <a href="{{ url('user/'. $user_id) }}" class="btn btn-default">View Profile</a>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>  
@endsection