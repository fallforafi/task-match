@extends('layout')

@section('content')

<section class="forgot-pswrd-section">
    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="forgot__pswrd">
                <div class="forgot__pswrd__hdng">
                    <h3>Forgot your password ?</h3>
                    @include('front.common.errors')
                </div>
                <div class="forgot__pswrd__sub__hdng">
                    <h3 class="small text-center">Enter your email below and we will send you instructions on how to reset your password</h3>

                </div>
                <div class="inr-contact-us-main">
                    <div class="inr__forgot__pswrd__form col-sm-8  col-sm-offset-2">
                        <form class="text-center" role="form" method="POST" action="{{ url('/reset') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required="required" />
                            </div>

                            <button class="btn btn-succcess center-block submit__btn" type="submit">Submit</button>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection