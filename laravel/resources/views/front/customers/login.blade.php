@extends('layout')
<?php
$title = 'Login';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<?php
$required = 'required';
?>
<section class="inr-signup-area">
    <div class="container">
        <div class="login-box col-sm-6 bg-white pul-cntr overload mt30 mb30">
            <div class="hed text-center"><h2>Log in and get to work</h2></div>
                    @include('front/common/errors')
            @include("front/customers/login_form")
			<div class="form-group col-sm-12 m-lg-bottom m-md-top text-right pt10 pb10 mb30">
			
				<a class="forgot__clr pul-lft" href="{{url('password/email')}}">Forgot your password?</a>
							
                <div class="pul-rgt"><span>Don't have an account?</span>
					<a data-ng-non-bindable="" class="btn btn-default" href="{{url('')}}/signup">Sign up</a>
				</div>
            </div>
        </div>
		
    </div>
</section>
@endsection