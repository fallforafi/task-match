@extends('layout')
<?php
$required = '';
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="profile" class="tab-pane fade in active">

                        <div class="edit-profile-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-pencil-square-o"></i> Edit Profile</h3>
                            </div>
                            @include('front.common.errors')

                            {!! Form::model($user, ['files' => true,'class' => 'form','url' => ['updateprofile'], 'method' => 'post']) !!}

                            <div class="clearfix"></div><br>
                            <div class="edit__inputs col-sm-12">
                                <form>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="firstName">First Name</label>
                                            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" value="{{$user->firstName}}">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="lastName">Last Name</label>
                                            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" value="{{$user->lastName}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="education" class="hover-anchor">Education
                                                <a href="#" title="Type in your education and press enter to add another">
													<i class="fa fa-question-circle blue-icon"></i>
												</a>
                                            </label><br />
                                            <div class="">
                                                <input type="text" name="education" class="form-control" id="education" data-role="tagsinput" placeholder="Education" value="{{$education}}">
                                            </div>
                                        </div>
                                        <div class="form-group transport-labels btn-effect--ripple col-sm-6" data-toggle="buttons">
                                            <!-- <div class="form-group transport-labels btn-effect--ripple col-sm-6" data-toggle="buttons">-->
                                            <label for="transport" class="hover-anchor">Transport
                                                <a href="#" title="Select your available transport modes by clicking the relevant button">
													<i class="fa fa-question-circle blue-icon"></i>
												</a>
                                            </label><br />
                                            @foreach(Config::get('params.transportTypes') as $key => $value) 
                                            @if(is_null($transport))
                                            <label class="btn btn-primary">{!! Form::checkbox('transport[]', $key,null) !!} {{ $value }}</label>
                                            @else
                                            <label class="btn btn-primary">{!! Form::checkbox('transport[]', $key, in_array($key,$transport)) !!} {{ $value }}</label>
                                            @endif
                                            @endforeach


                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="languages" class="hover-anchor">Languages
                                                <a href="#" title="Type in a language and press enter to add another">
													<i class="fa fa-question-circle blue-icon"></i>
												</a>
                                            </label>
                                            <input type="text" class="form-control" name="language" id="languages" data-role="tagsinput" placeholder="Add a Tag" value="{{$language}}">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{$user->email}}">
                                        </div>
                                    </div>
                                    <div class="row">
<!--                                        <div class="form-group col-sm-6">
                                            <label for="phone">Phone Number</label>
                                            <input type="text" class="form-control" name="phone" id="phone" placeholder="+353" value="{{$user->phone}}">
                                        </div>-->
                                        <div class="form-group col-sm-6">
                                            <label for="location">Location</label>
                                            <div id="test">
                                                <input type="text" class="form-control autocomplete" name="location" placeholder="Location" onfocus="initAutocomplete1()" value="{{$user->location}}" id="location">

                                                <input type="hidden" name="longitude"  id="longitude" value="">
                                                <input type="hidden" name="latitude"  id="latitude" value="">
                                            </div>

                                        </div>
                                    </div>

                                    <label class="hover-anchor">Date of birth <a href="#" title="Your Date of Birth will not be made public">
                                            <i class="fa fa-question-circle blue-icon"></i>

                                        </a> </label>
                                    <div class="row">
                                        <div class="form-group fnc-select col-sm-4">
                                            {!! Form::selectRange('date',1,31,null,['class' => 'form-control',$required]) !!}
                                        </div>
                                        <div class="form-group fnc-select col-sm-4">
                                            {!! Form::selectMonth('month',null, ['class' => 'form-control',$required]) !!}
                                        </div>
                                        <div class="form-group fnc-select col-sm-4">
                                            {!! Form::selectRange('year',2016,1930,null,['class' => 'form-control',$required])!!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="location">Tagline</label>
                                            <input type="text" class="form-control" name="tagline" id="tagline" placeholder="A mini bio" value="{{$user->tagline}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="aboutMe">About me</label>
                                            <textarea class="form-control" rows="5" name="about" id="aboutMe" placeholder="About me">{{$user->about}}</textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="education" class="hover-anchor">Work Experience and Skills
                                                <a href="#" title="Type in a skill and press enter to add another">
													<i class="fa fa-question-circle blue-icon"></i>
												</a>
                                            </label>
                                            <input type="text" class="form-control" name="skill" id="education" data-role="tagsinput" placeholder="Add a Tag" value="{{$skill}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-3 col-sm-offset-3 text-center">
                                            <div class="lnk-btn inline-block view-profile-btn save-prof-btn">
                                                <input type="submit" value="Save Profile">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3  text-center">
                                            <div class="lnk-btn inline-block save-view-btn">
                                                <a href="{{ url('user/'.$user->key) }}">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div id="messages" class="tab-pane fade">
                        <div class="reviews-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-envelope-o"></i> Messages</h3>
                            </div>
                            <div class="reviews__box">
                                <div class="review__box__desc">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<script>
    var placeSearch;
    var autocomplete = {};
    var autocompletesWraps = ['test', 'test2'];
    var test_form = {locality: 'long_name'};
    var test2_form = {locality: 'long_name'};
    function initAutocomplete1() {
        geolocate1();
        $.each(autocompletesWraps, function (index, name) {
            if ($('#' + name).length === 0) {
                return;
            }
            autocomplete[name] = new google.maps.places.Autocomplete($('#' + name + ' .autocomplete')[0], {types: ['geocode']});
            autocomplete[name].setComponentRestrictions(
                    {'country': ['ie']});
            google.maps.event.addListener(autocomplete[name], 'place_changed', function () {
                var place = autocomplete[name].getPlace();
                var form = eval(name + '_form');
        //document.getElementById("location").value = place.name;
        document.getElementById("latitude").value = place.geometry.location.lat();
                document.getElementById("longitude").value = place.geometry.location.lng();

                for (var component in form) {
                    $('#' + name + '#' + component).val('');
                    $('#' + name + '#' + component).attr('disabled', false);
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (typeof form[addressType] !== 'undefined') {
                        var val = place.address_components[i][form[addressType]];
                        $('#' + name + '#' + addressType).val(val);
                    }
                }


            });
        });
    }
    function geolocate1() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;

            });
        }
    }
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#proPicture')
                        .attr('src', e.target.result)
                        .width('100%')
                        .height(50);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function () {
        $(".transport-labels input:checkbox:checked").parent('label').toggleClass("active");
    });

</script>
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
@endsection
