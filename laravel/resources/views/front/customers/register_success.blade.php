@extends('layout')

@section('content')
<section class="account-area">
    <div class="container">
        
		<div class="reg-area text-center p50 mb50">
			<div class="hed">
				<h2>Thank you <span><?php echo $user->firstName ?> <?php echo $user->lastName ?></span></h2>
			</div>
			<p class="lead">You have successfully verified your account.
<!--			<br>
			<small>A confimation email has been sent to you email address.</small>-->
			</p>
			<?php if (!isset(Auth::user()->id)) { ?>
			<div class="acct__login text-center col-sm-12 clrlist">
				
				<ul>
					<li><button data-toggle="modal" data-target="#signIn" class="btn btn-primary"> Sign In</li>
				</ul>
				
			</div>
                        <?php } ?>
		</div>
    </div>
</section>
@endsection