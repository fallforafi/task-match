@extends('layout')
<?php
$required = 'required';
?>
@section('content')

<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">                                          
                    <div id="changePassword" class="fade in">
                        <div class="pwd-area">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-unlock-alt"></i> Change your password</h3>
                            </div>
                            @include('front.common.errors')
                            <div class="pwd__box">
                                <div class="edit__inputs pwd__inputs col-sm-6 col-sm-offset-3">
                                    {!! Form::open(array( 'class' => 'form','url' => 'postchangepassword', 'method' => 'post')) !!}
                                    <div class="row">
                                        <?php if (Auth::user()->password != '1') { ?>
                                        <div class="form-group col-sm-12">
                                            <label for="firstName">Current Password</label>
                                            {!! Form::password('old_password', ['class'=>'form-control','id'=>'currentPwd','placeholder'=>'Current Password', $required]) !!}

                                        </div>
                                        <?php } ?>
                                        
										<div class="form-group col-sm-12">
                                            <label for="lastName">New Password</label>
                                            {!! Form::password('password', ['class'=>'form-control','id'=>'newPwd','placeholder'=>'New Password', $required]) !!}
                                        </div>
										
                                        <div class="form-group col-sm-12">
                                            <label for="lastName">Verify Password</label>
                                            {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'verfPwd','placeholder'=>'Confirm Password', $required]) !!}

                                        </div>
                                        <div class="form-group save-pwd-btn0 text-center col-sm-12">
                                            <input type="submit" class="btn btn-primary" value="Save Password" />
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>

</section>  
@endsection