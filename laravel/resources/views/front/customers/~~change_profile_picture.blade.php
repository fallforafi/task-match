@extends('profile_pic')
<?php
$required = '';
$title = $user->firstName . ' ' . $user->lastName;
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
        /* background-image: linear-gradient(to bottom, #fff 0%, #e0e0e0 100%); */
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        min-height: 150px;
        max-height: 150px;
        border: 1px solid #ddd;
        border-radius: 4px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
    }
    #img-upload1{
        min-height: 150px;
        max-height: 280px;
        width:100%;
        padding: 10px 10px 10px 10px;
        /*        padding-left: 10px;
                padding-right: 10px;*/
        border: 1px solid #ddd;
        border-radius: 4px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
    }
    .EditPrfTitle {
        padding: 8px 0px 8px;
        border-bottom: 1px solid rgba(0, 0, 0, .075);
    }
    .prof-img img {
        border: 1px solid #000;
    }

</style>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                <div class="tab-content dboard-tab-height">
                    <div id="editProfile" class="tab-pane fade in active">

                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success')}}
                        </div>
                        @endif

                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <div class="Dash">
                            <h3 class="text-center EditPrfTitle">
                                <i class="fa fa-picture-o" aria-hidden="true"></i> Change your profile picture</h3>
                        </div>
                        <div class="container">
                            <div class="">
                                <div class="">
                                    <div class="row">
<!--                                        <i class="btn btn-primary fa fa-edit" id="edit"></i>-->
                                        <div class="col-md-4 col-md-offset-2 text-center prof-img">
                                            <div id="success"></div>

                                            <div id="vanilla-demo" style="">
                                                <div class="actions">
                                                    <i class="fa fa-rotate-left vanilla-rotate btn btn-success" data-deg="-90" aria-hidden="true"></i>
                                                    <i class="fa fa-rotate-right vanilla-rotate btn btn-success" data-deg="90" aria-hidden="true"></i>

                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <i class="btn btn-default upload-result fa fa-save"></i>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script type="text/javascript">
    var el = document.getElementById('vanilla-demo');
    vanilla = new Croppie(el, {
        viewport: {width: 200, height: 200},
        boundary: {width: 300, height: 300},
        enableResize: true,
        enableOrientation: true
    });
    //$('#imgInp').on('change', function () {
    //   var reader = new FileReader();
    //   reader.onload = function (e) {
    vanilla.bind({
        url: "{{ asset('/uploads/users/profile/')}}/<?php echo $user->image; ?>",
        zoom: 0
    }).then(function () {
        console.log('jQuery bind complete');
    });

    $('.vanilla-rotate').on('click', function (ev) {
        vanilla.rotate(parseInt($(this).data('deg')));
    });

    $('.upload-result').on('click', function (ev) {
        vanilla.result({
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            var _token = $('#_token').val();
            $.ajax({
                url: "<?php echo url('changeprofileimage'); ?>",
                type: "POST",
                dataType: 'json',
                data: {"image": resp, '_token': _token},
                success: function (data) {
                    console.log(data);
                    if (data.success === 1) {
                        var errorsHtml = '<div class="alert alert-success"> Profile Picture Changed Successfully.</div>';
                        $('#success').html(errorsHtml).show();
                        setTimeout(function () {
                            location.reload(true);
                        }, 200);
                    }
                }
            });
        });
        //
    });
    $('#edit').on('click', function (ev) {
        alert('aaa');
    });

//    vanilla.bind({
//        url: 'demo/demo-2.jpg',
//        orientation: 4
//    });
//on button click
//    vanilla.result('blob').then(function (blob) {
//        alert('ahhahahah');
//    });
//    $uploadCrop = $('#upload-demo').croppie({
//        //enableExif: true,
//        viewport: {
//            width: 200,
//            height: 200,
//            type: 'square'
//        },
//        boundary: {
//            width: 300,
//            height: 300
//        },
//        //showZoomer: true,
//        //enableResize: true,
//        enableOrientation: true
//    });

//    $('#imgInp').on('change', function () {
//        var reader = new FileReader();
//        reader.onload = function (e) {
//            $uploadCrop.croppie('bind', {
//                url: e.target.result,
//
//            }).then(function () {
//                console.log('jQuery bind complete');
//            });
//
//        };
//        reader.readAsDataURL(this.files[0]);
//    });

//    $('.upload-result').on('click', function (ev) {
//        $uploadCrop.croppie('result', {
//            type: 'canvas',
//            size: 'viewport'
//        }).then(function (resp) {
//            var _token = $('#_token').val();
//            $.ajax({
//                url: "<?php echo url('changeprofileimage'); ?>",
//                type: "POST",
//                data: {"image": resp, '_token': _token},
//                success: function (data) {
//                    location.reload();
//                }
//            });
//        });
//    });

</script>
<script>
    $(document).ready(function () {
        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload1').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp1").change(function () {
            readURL1(this);
        });
        document.getElementById('imgInp').addEventListener('change', function () {
            var style = this.value === '' ? 'block' : 'none';
            var upload = this.value === '' ? 'none' : '';
            document.getElementById('select').style.display = style;
            document.getElementById('upload').style.display = upload;
            document.getElementById('profile').style.display = style;
            document.getElementById('vanilla-demo').style.display = upload;
        });

        $('#cancel').on('click', function () {
            document.getElementById('upload').style.display = 'none';
            document.getElementById('select').style.display = 'block';
            $('#imgInp').val('');
            document.getElementById('profile').style.display = '';
            document.getElementById('vanilla-demo').style.display = 'none';
            // location.reload();
        }
        );
        document.getElementById('imgInp1').addEventListener('change', function () {
            var style = this.value === '' ? 'block' : 'none';
            var upload = this.value === '' ? 'none' : 'block';
            document.getElementById('select1').style.display = style;
            document.getElementById('upload1').style.display = upload;
        });

        $('#cancel1').on('click', function () {
            document.getElementById('upload1').style.display = 'none';
            document.getElementById('select1').style.display = 'block';

            $('#img-upload1').attr('src', '{{ asset(' / uploads / users / cover / thumbnail')}}/<?php echo $user->coverImage; ?>');
        }
        );
    });

</script>
@endsection