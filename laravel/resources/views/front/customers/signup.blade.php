@extends('layout')
<?php
$title = 'Sign up';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<?php
$required = 'required';
?>
<section class="inr-signup-area">
    <div class="container">
	
	<div class=" col-sm-6 pul-cntr mt50 mb50 shad-dp20 bg-white pt20">
        
		<div class="hed text-center"><h2>Create New Account</h2></div>
		
		<div class="text-center p0 m0">
            <div class="inr__signup__form__title">
                <h3>Sign in using your social accounts</h3>
            </div>

            <div class="social-login">
                @include('front/common/social_login_buttons')
            </div>

            <div class="inr__signup__form__title">
                <h3 class="change-click">Or Sign up using your email</h3>
            </div>
        </div>
        @include('front/common/errors')
        
            
                <div class="inr__signup__form">
					
				
                    <form method="POST" class="form" action="{{ url('register') }}">
                        <input type="hidden" name="role_id" value="2">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group col-sm-12">
                            <label for="fname">First Name</label>
                            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" required="required" value="{{ old('firstName') }}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="lname">Last Name</label>
                            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" required="required" value="{{ old('lastName') }}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address *" required="required" value="{{ old('email') }}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="pwd">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password *" required="required">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="pwd">Confirm Password</label>
                            <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" required="required">
                        </div>
                        
                        <div class="form-group col-sm-12 p0">  
							@include('front/common/terms')
                        </div>
                        
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary" id="register_button">Sign up</button>
                            &nbsp or &nbsp <a class="btn btn-default" data-toggle="modal" data-target="#signIn">Sign in</a>
                        </div>
                    </form>
                </div>
          </div>  

    </div>
</section> 
<script>
    $('.form').submit(function (event) {

        var form = $('#register');
        form.find('#register_button').prop('disabled', true);

        $('.terms-errors').hide();
        var term = check_terms_services();

        if (term === false) {
            return false;
        }
        return true;
    });
</script>
@endsection