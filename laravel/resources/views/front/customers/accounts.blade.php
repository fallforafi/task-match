@extends('layout')


<?php
$title = "";
$description = "";
$keywords = "";
?>
@include('front.common.meta')
@section('content')
<div id="fb-root"></div>
<section class="dashboard-area">
    <div class="container">
        <div class="dboard__main col-sm-12">
            <div class="dboard__lft col-sm-3">
                @include('front.common.left')
            </div>
            <div class="dboard__rgt col-sm-9">
                @include('front.common.errors')
                <div class="tab-content dboard-tab-height">
                    <div id="verfSocial" class="">
                        <div class="pwd-area verf-area clrlist">
                            <div class="tab-cont-title text-center">
                                <h3><i class="fa fa-thumbs-o-up"></i> Verify Social Media accounts</h3>
                            </div>
                            <div class="pwd__box verf__box">
                                <div class="verf__inr shake-icon text-center col-sm-12">
                                    <ul>
                                        <li>
                                            <a onclick="fb_verify()" href="#">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" class="verf__plus__icon">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('social/twitter')}}">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" class="verf__plus__icon">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="customBtn" href="#">
                                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" class="verf__plus__icon">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                            <div class="g-signin2" data-onsuccess="onSignIn"></div>

                                        </li>
                                        <li>
                                            <a onclick="liAuth()" href="#">
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" class="verf__plus__icon">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a onclick="onInstagramLoginClick()" href="#">
                                                <i class="fa fa-instagram" aria-hidden="true"></i>

                                            </a>
                                            <a href="#" class="verf__plus__icon">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div id="test"></div>
</head>

<script>
    function onInstagramLoginClick() {

        var clientId = 'ea8d6801a80e469aae8c77dd460234b7';
        var redirectUri = '<?php echo url("accounts")?>';
        var responseType = 'token';

        var url = "https://instagram.com/oauth/authorize/?client_id=" + clientId + "&redirect_uri=" + redirectUri + "&response_type=" + responseType;

        window.top.location=url;
    }
</script>


<script>

    function getInstaGramData() {
        var hash = location.hash.substr(1);

        if (hash == "") {
            return false;
        }

        $.ajax({
            url: 'https://api.instagram.com/v1/users/self/?' + hash,
            dataType: 'jsonp',
            type: 'GET',
            success: function (data) {
                verify('instagram', "https://instagram.com/" + data.data.username);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }


</script>
<script type="text/javascript" src="http://platform.linkedin.com/in.js">
    api_key: 81gy6puxhv8bqp
    onLoad: OnLinkedInFrameworkLoad
</script>

<script>

    function OnLinkedInFrameworkLoad() {
        IN.Event.on(IN, "auth", OnLinkedInAuth);
    }

    function OnLinkedInAuth() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", 'public-profile-url', 'picture-url').result(ShowProfileData);
    }

    function ShowProfileData(profiles) {
        var member = profiles.values[0];
        var publicProfileUrl = member.publicProfileUrl;
        verify('linkedin', publicProfileUrl);
    }

    function liAuth() {
        IN.User.authorize(function () {
            OnLinkedInAuth();
        });
    }
</script>



<script src="https://apis.google.com/js/api:client.js"></script>
<script>
    var googleUser = {};
    var startApp = function () {
        gapi.load('auth2', function () {
            // Retrieve the singleton for the GoogleAuth library and set up the client.
            auth2 = gapi.auth2.init({
                client_id: '682148044033-so0j3s1449u058o5f85t025luocqr6ku.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
            });
            attachSignin(document.getElementById('customBtn'));
        });
    };

    function attachSignin(element) {
        console.log(element.id);
        auth2.attachClickHandler(element, {},
                function (googleUser) {

                    verify('google', "https://plus.google.com/" + googleUser.getBasicProfile().getId());
                }, function (error) {
            alert(JSON.stringify(error, undefined, 2));
        });
    }
</script>

<script>

    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?php echo env('FACEBOOK_API_ID'); ?>',
            status: true,
            cookie: true,
            xfbml: true
        });
    };

    (function (doc) {
        var js;
        var id = 'facebook-jssdk';
        var ref = doc.getElementsByTagName('script')[0];
        if (doc.getElementById(id)) {
            return;
        }
        js = doc.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
    function fb_verify() {
        FB.login(function (response) {

            if (response.authResponse) {
                console.log('Welcome!  Fetching your information.... ');
                //console.log(response); // dump complete info
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

                FB.api('/me', function (response) {
                    user_email = response.email;


                    verify('facebook', response.link);


                });
            } else {
                //user hit cancel button
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'public_profile'
        });
    }




    function verify(type, link) {
        $.ajax({url: "<?php echo url(""); ?>/social/verify",
            type: 'get',
            dataType: 'json',
            data: {
                type: type,
                link: link
            },
            success: function (result) {
                // window.top.reload();
            }
        });


    }


    startApp();
    getInstaGramData();
</script>

@endsection                            