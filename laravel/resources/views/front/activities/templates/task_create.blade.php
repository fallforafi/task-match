<div class='recent__usr__img'>
    <?php if ($model['image'] != '') { ?>                  
        <img class='img-circle' src='<?php echo url('/uploads/users/profile/') ?>/<?php echo $model['image']; ?>' alt = '<?php echo $model['name'] ?>' title='<?php echo $model['name'] ?>' />
    <?php } else { ?>
        <img class='img-circle' src ="{{ asset('front/images/default.png') }}" alt = '<?php echo $model['name'] ?>' title = '<?php echo $model['name'] ?>' />
    <?php } ?>
</div>
<div class="recent__usr__cont"><?php echo $model['name']; ?> posted <a href='<?php echo url('task/') ?>/<?php echo $model['key'] ?>'><?php echo $model['title']; ?></a></div>