@extends('layout')
<?php
$title = $model->title;
$description = $model->description;
$keywords = "";
?>
@include('front/common/meta')

@section('content')
<style>
    #map {
        height: 500px;
        width : 100%;
    }
    #map_wrapper {
        height: 400px;
    }

    #map_canvas {
        width: 100%;
        height: 100%;
    }
</style>
<section class="browse-area">
    <div class="container">
        <div class="brs-area-box col-sm-12">
            @include("front/tasks/left")


            <div class="brs-rgt col-sm-8" >
                <div id="map_wrapper">
                    <div id="map_canvas" class="mapping"></div>
                </div>
            </div>

            <div class="brs-rgt col-sm-8" id="task">
            </div>
            <div id="brs-rgt-map" class="brs-rgt col-sm-8 pull-right">
                <div id="map">
                </div>        
            </div>
        </div>

    </div>
    <div id="aaaaaaaaaa"></div>
</section>
<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpxQvcYyGxCSZWDof-C1qF6XT_YETCfJQ"></script>

-->

<script>

    function initialize(tasks) {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        map.setTilt(45);

        // Multiple Markers
        // Multiple Markers
        var markers = [
            ['London Eye, London', 51.503454, -0.119562],
            ['Palace of Westminster, London', 51.499633, -0.124755]
        ];

        // Info Window Content
        var infoWindowContent = [
            ['<div class="info_content">' +
                        '<h3>London Eye</h3>' +
                        '<p>The London Eye is a giant Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres (394 ft).</p>' + '</div>'],
            ['<div class="info_content">' +
                        '<h3>Palace of Westminster</h3>' +
                        '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                        '</div>'],
            ['<div class="info_content">' +
                        '<h3>Palace of Westminster</h3>' +
                        '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                        '</div>']
        ];

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Loop through our array of markers & place each one on the map  
        
        
        for (i = 0; i < tasks.length; i++) {
           // alert(tasks[i].latitude);
            var position = new google.maps.LatLng(tasks[i].latitude, tasks[i].longitude);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: "asdasdasdasd"
            });

            // Allow each marker to have an info window    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }
        
        
        /*
        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });

            // Allow each marker to have an info window    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }
        */
        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });

    }
    // window.onload = initMap;
    var markers = [];
    var map;
    function initMap(tasks)
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var infowindow = new google.maps.InfoWindow();
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                // myLatlng = new google.maps.LatLng(latitude, longitude);
                myLatlng = new google.maps.LatLng(-33.874737, 151.215530);
                mapOptions = {
                    zoom: 12,
                    center: myLatlng
                };

                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                marker = new google.maps.Marker({
                    position: myLatlng,
                    animation: google.maps.Animation.DROP,
                    map: map,
                    title: "My Current Location"
                });
                google.maps.event.addListener(marker, 'click', function (e) {
                    infowindow.setContent(this.title);
                    infowindow.open(map, this);
                }.bind(marker));

                setMarkers(map, tasks)
            });
        }
    }

    function getLoc(formdata) {
        var result = null;
        $.ajax({
            url: "<?php echo url('get-locations'); ?>",
            type: "get",
            dataType: "json",
            data: formdata,
            success: function (data) {
                initialize(data)
            }
        });
        return result;
    }
    function setMarkers(map, tasks) {
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
        for (var o in tasks) {

            if (tasks.hasOwnProperty(o)) {
                lat = tasks[o].latitude;
                lng = tasks[o].longitude;
                name = tasks[o].location;

                if (lat === 0) {
                    continue;
                }
                alert(lat + " " + lng);
                var position = new google.maps.LatLng(lat, lng);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: name
                });
                google.maps.event.addListener(marker, 'click', function (e) {
                    infowindow.setContent(this.title);
                    infowindow.open(map, this);
                }.bind(marker));
            }
        }
    }
</script>

<script>
    $(document).ready(function () {
        taskListing();
        task('<?php echo $key; ?>');

    });

    $("form").submit(function (e) {
        taskListing()
        e.preventDefault();

    });

    $("form").change(function (e) {
        taskListing()
        e.preventDefault();
    });

    $("#filter_search").keyup(function (e) {
        taskListing()
    });

    function taskListing() {
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('tasks/listing'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                $('#task_listing').html(response).show();
                getLoc(formdata);
            },
            error: function (xhr, status, response) {
            }
        });
    }

    function task(key)
    {
        $.ajax({
            url: "<?php echo url('tasks/view'); ?>",
            type: 'get',
            dataType: 'html',
            data: {key: key},
            success: function (response) {
                $('#task').html(response).show();
                window.history.pushState({"html": response}, "", "<?php echo url('task'); ?>/" + key);
            },
            error: function (xhr, status, response) {
            }
        });
    }



</script>
<script>
    $(".brs-ctg-nav li").on("click", function () {
        $('.brs-ctg-nav li').not(this).removeClass("active");
        $(this).toggleClass("active");
    });

</script>

<script>
    $(document).ready(function () {
        $('.brs-ctg-nav li button.brs__ctg__srch').click(function () {
            $('.brs__srch__box').toggle(".brs__srch__box");
        });
    });

    $(document).ready(function () {
        $('.brs-ctg-nav li button.brs__ctg__filt').click(function () {
            $('.brs__filt__box').toggle(".brs__filt__box");
        });
    });

    $(document).ready(function () {
        $(".brs__rgt__close i").on('click', function () {
            $('.brs-rgt').hide();
        });
    });

    $(document).ready(function () {
        $(".rgt__map__icon a").on('click', function () {
            $('.brs__rgt__map').toggle();
        });
    });

</script>
@endsection
