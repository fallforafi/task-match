@extends('browse_tasks_layout')
@include('front/common/meta')
@section('content')

<style>
    #map {
        height: 500px;
        width : 100%;
    }
</style>
<section class="browse-area">
    <div class="container">
        <div class="brs-area-box col-sm-12">
            @include("front/tasks/left")
            <div class="brs-rgt col-sm-8" id="task">
               
            </div>
            <div id="brs-rgt-map" class="brs-rgt col-sm-8 pull-right">

                <div id="map">
                </div>        
            </div>
        </div>
    </div>
</section>

<script>

    var markers = [];
    function initMap()
    {
        // if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var infowindow = new google.maps.InfoWindow();
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            $("#lat").val(latitude);
            $("#lng").val(longitude);
            myLatlng = new google.maps.LatLng(latitude, longitude);
            mapOptions = {
                zoom: 10,
                center: myLatlng
            };
            image = '<?php echo asset('front/images/green_marker.png') ?>';
            map = new google.maps.Map(document.getElementById("map"), mapOptions);
            marker = new google.maps.Marker({
                position: myLatlng,
                animation: google.maps.Animation.DROP,
                map: map,
                icon: image,
                title: "Your Current Location"
            });
            google.maps.event.addListener(marker, 'click', function (e) {
                infowindow.setContent(this.title);
                infowindow.open(map, this);
            }.bind(marker));
            var tasks = getLoc();
            setMarkers(map, tasks);
        });
        // }
    }
    function getLoc() {
        var result = null;
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('get-locations'); ?>",
            type: "GET",
            cache: false,
            dataType: "JSON",
            data: formdata,
            success: function (data) {
                result = data;
                //console.log(result);
            }
        });
        return result;
    }
    function setMarkers(map, tasks) {
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        var infowindow = new google.maps.InfoWindow({
            maxWidth: 250
        });
        var bounds = new google.maps.LatLngBounds();
        for (var o in tasks) {
            if (tasks.hasOwnProperty(o)) {
                lat = tasks[o].latitude;
                lng = tasks[o].longitude;
                //console.log(lng);
                name = tasks[o].description;
                var key = tasks[o].key;
                image = '<?php echo asset('front/images/blue_marker.png') ?>';
                var position = new google.maps.LatLng(lat, lng);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    //label: labels[labelIndex++ % labels.length],
                    //icon: image,
                    title: name,
                    key: key
                });
                google.maps.event.addListener(marker, 'click', function (e) {
                    infowindow.setContent(this.title +
                            '<br><center><button onclick=myFunction("' + this.key + '") class="btn btn-info btn-sm">View Task</button></center>');
                    infowindow.open(map, this);
                }.bind(marker));
            }
        }
    }
    function myFunction(key) {
        window.location.href = '<?php echo url('task/') ?>/' + key;
    }
</script>
<script>
    $(document).ready(function () {
        page = 1;
        taskListing(page);
        // initMap();
        task('<?php echo $key; ?>');
    });

    $("form").submit(function (e) {
        var page = 1;
        taskListing(page);
        initMap();
        e.preventDefault();

    });

    $("form").change(function (e) {
        var page = 1;
        taskListing(page);
        initMap();
        e.preventDefault();
    });

    $("#filter_search").keyup(function (e) {
        var page = 1;
        taskListing(page);
        initMap();
    });
//    $(window).scroll(function () { //detect page scroll
//        if ($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled from top to bottom of the page
//            page++; //page number increment
//            taskListing(page); //load content   
//        }
//    });
    jQuery(function () {
        var page = 1;
        jQuery("#task_listing").bind("scroll", function () {
            if (jQuery(this).scrollTop() + jQuery(this).innerHeight() >= jQuery(this)[0].scrollHeight) {
                // alert('here');
                page++; //page number increment
                taskListing(page); //load content 
            }
        });
    });
    function taskListing(page) {
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('tasks/listing'); ?>?page=" + page,
            type: 'get',
            dataType: 'html',
            data: formdata,
            beforeSend: function ()
            {
                $('.ajax-loading').show();
            }})
                .done(function (response) {
                    //console.log(response.length);
                    if (response.length === 55) {
                        //notify user if nothing to load
                        $('.ajax-loading').html("");
                        return;
                    }
                    if (page === 1) {
                        $('#task_listing').html('');
                    }
                    $('#task_listing').append(response).show();
                    $('.ajax-loading').hide();
                })
                .fail(function (jqXHR, ajaxOptions, thrownError)
                {
                    alert('No response from server');
                });

    }

    function task(key)
    {
        $.ajax({
            url: "<?php echo url('tasks/view'); ?>/" + key,
            type: 'get',
            dataType: 'html',
            // data: {key: key},
            success: function (response) {
                $('#task').html(response).show();
                // window.history.pushState({"html": response}, "", "<?php echo url('task'); ?>/" + key);
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
<script>
    $(".brs-ctg-nav li").on("click", function () {
        $('.brs-ctg-nav li').not(this).removeClass("active");
        $(this).toggleClass("active");
    });

</script>

<script>
    $(document).ready(function () {
        $('.brs-ctg-nav li button.brs__ctg__srch').click(function () {
            $('.brs__srch__box').toggle(".brs__srch__box");
        });
    });

    $(document).ready(function () {
        $('.brs-ctg-nav li button.brs__ctg__filt').click(function () {
            $('.brs__filt__box').toggle(".brs__filt__box");
        });
    });

    $(document).ready(function () {
        $(".brs__rgt__close i").on('click', function () {
            $('.brs-rgt').hide();
        });
    });

    $(document).ready(function () {
        $(".rgt__map__icon a").on('click', function () {
            $('.brs__rgt__map').toggle();
        });
    });

</script>
@endsection
