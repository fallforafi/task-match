<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$commission = Config::get('params.commission');
?>
<style>
    #map1 {
        width: 100%;
        height: 250px;
    }
</style>
<div class="brs__rgt__inr col-sm-12 p0">
    <a href="#" class="close-panel fa fa-times"></a>


    <div class="brs__rgt__taskTitle">
        <!--hr-->
        <h2><?php echo $task->title; ?></h2>
    </div>

    <!-- close col-sm-9 noPadding -->
    <div class="brs__rgt__post">
        <div class="media">
            <a href="{{ url('user/'.$user->id)}}" class="pull-left">
                <div class="brs__postBy__img">
                    <?php
                    $row = $user;
                    ?>
                    @include('front/common/profile_picture')
                </div>
            </a>
            <div class="media-body ">
                <h4><small>Posted by</small><br/> <a href="{{ url('user/'.$user->key)}}">{{ $user->firstName.' '.$user->lastName}}</a></h4> 
                <h5>Date: <small> {{ date('M d, Y', strtotime($task->created_at))}} </small></h5>

            </div>
        </div>



    </div>

    <div class="brs__rgt__taskDesc">
        <div class="rgt__desc__box col-sm-8">
            <div class="progress">
                <?php
                if ($task->taskStatus == "open" || $task->taskStatus == "cancelled") {
                    ?>
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
                        OPEN
                    </div>
                    <?php
                } elseif ($task->taskStatus == "assigned") {
                    ?>
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
                        OPEN
                    </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" style="width:33.33%">
                        ASSIGNED
                    </div>
                    <?php
                } elseif ($task->taskStatus == "completed") {
                    ?>
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
                        OPEN
                    </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" style="width:33.33%">
                        ASSIGNED
                    </div>
                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:33.33%">
                        COMPLETED
                    </div>
                    <?php
                }
                ?>



            </div>


            <div class="rgt__task__txt rgt__task__abt">
                <h4><i class="fa fa-file-text"></i> Task description</h4>
                <p><?php echo $task->description; ?></p>
            </div>


            <div class="rgt__task__txt rgt__task__loc">
                <h4><i class="fa fa-map-marker"></i> Task Location</h4>
                <p><?php echo $task->location; ?></p>
            </div>

            <div class="clearfix"></div>
            <div class="brs__rgt__map">
                <!--                <div class="rgt__map__box col-sm-12" id="mapViewerMini">-->
                <div id="map12" style="width: 100%;height: 200px;position: relative;overflow: hidden;"></div>

                <div class="clearfix"></div>
                <!--</div>-->
            </div>
            <div class="rgt__task__info">
                <div class="task__tobe__compl">
                    <table>
                        <tr>
                            <td>
                                <small>To be completed: </small>
                            </td>
                            <td>
                                <?php
                                if ($task->toBeCompleted == 'inperson') {
                                    ?>
                                    <span><i class="fa fa-user"></i> In person task</span>
                                    <?php
                                } else {
                                    ?>
                                    <span><i class="fa fa-user"></i> Online task</span>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>



                        <tr>
                            <td>
                                <small>Categories: </small>
                            </td>
                            <td>
                                <?php
                                foreach ($categories as $category) {
                                    ?>
                                    <span><i class="<?php echo $category->class ?>"></i> <?php echo $category->name ?></span>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <small>Details: </small>
                            </td>
                            <td>
                                <span>Paid with Taskmatch Pay</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <small>Due date: </small>
                            </td>
                            <td>
                                <span><i class="fa fa-clock-o"></i> <?php echo date('l, F d, Y', strtotime($task->dueDate)) ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <small>Task Attachments: </small>
                            </td>
                            <td>
                                @if(count($taskFiles)>0)
                                @foreach($taskFiles as $row)
                                <a href="{{ url('uploads/task_files/'.$row->task_id.'/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Uploaded file or image"><i class="fa fa-download" aria-hidden="true"></i></a>

                                @endforeach
                                @else
                                No attachment found..
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="rgt__task__price col-sm-4">
            <div class="rgt__price__box">
                <div class="rgt__price__hdr hover-anchor">
                    <h5>Paid with Taskmatch Pay 
                        <a href="#" title="When an offer is accepted the job posters funds for the task are securely held within a TaskMatch trust account until the task is completed">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </h5>
                </div>
                <div class="rgt__price__cont">
                    <h2><?php echo $currency . ' ' . $task->price ?></h2>
                </div>
                <div class="rgt__price__offerBtn">
                    <?php
                    if (count($offer) == 0) {
                        if (!isset(Auth::user()->id) && $task->taskStatus == 'open' || $task->taskStatus == 'cancelled') {
                            ?>
                            <button data-toggle="modal" data-target="#signIn">Signin to make an offer</button>
                        <?php } elseif (!isset(Auth::user()->id) && $task->taskStatus == 'assigned') {
                            ?>
                            <a href="#offers" class="btn btn-block btn-default" data-toggle="tooltip" data-placement="left" title="You can no longer place an offer on this task" aria-hidden="false">This task has been assigned</a> 
                        <?php } elseif (!isset(Auth::user()->id) && $task->taskStatus == 'completed') { ?>
                            <a href="#" class="btn btn-block btn-default" data-toggle="tooltip" data-placement="left" title="You can no longer place an offer on this task" aria-hidden="false">This task has been completed</a>
                            <?php
                        } else {
                            if (Auth::user()->id != $task->user_id && $task->taskStatus != 'assigned' && $task->taskStatus != 'completed' && $task->taskStatus == 'open' || $task->taskStatus == 'cancelled') {
                                ?> 
                                <button data-toggle="modal" onclick="makeOffer('<?php echo $task->key; ?>')" data-target="#offerModal">Make me an offer</button>
                                <?php
                            }
                        }
                    } else {
                        if ($task->taskStatus == 'completed') {
                            ?>        
                            <a href="#" class="btn btn-block btn-default" data-toggle="tooltip" data-placement="left" title="You can no longer place an offer on this task" aria-hidden="false">This task has been completed</a>

                        <?php } elseif ($task->taskStatus == 'assigned') { ?>
                            <a href="#" class="btn btn-block btn-default" data-toggle="tooltip" data-placement="left" title="You can no longer place an offer on this task" aria-hidden="false">This task has been assigned</a>   
                        <?php } else { ?>
                            <a class="btn btn-success btn-block wOfferBtn" href="#withOffer">View Offers</a>
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
            <div class="rgt__price__moreOpt hover-anchor">
                <!--div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">More options
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="https://twitter.com/share" data-text="check out this task on Taskmatch.ie" data-related="Task_Match" data-hashtags="taskmatch">
                                <i class="fa fa-twitter-square" aria-hidden="true"></i> Share on Twitter
                            </a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </li>
                        <li>
                            <a data-href="<?php echo url('task/' . $task->key); ?>" 
                               data-layout="button_count"><i class="fa fa-facebook-square" aria-hidden="true"></i>Share on Facebook</a></li>
                    </ul>
                </div-->
                <div class="social-colors shareon text-center  clrlist">
                    <h4>Share on</h4>
                    <ul class="">
                        <li class="bg-twitter">
                            <a href="https://twitter.com/intent/tweet?url=<?php echo url('task/' . $task->key); ?>" data-text="check out this task on Taskmatch.ie" data-related="Task_Match" data-hashtags="taskmatch" target="_blank">
                                <i class="fa fa-twitter"></i>Twitter
                            </a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </li>
                        <li>
                            <div data-href="<?php echo url('task/' . $task->key); ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo url('task/' . $task->key); ?>"><i class="fa fa-facebook"></i>Facebook</a></div>
<!--                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.ecommercehouse.net/laravel/taskmatch/task/14904Hk55W70j7eE" target="_blank" ><i class="fa fa-facebook"></i>Facebook</a>-->
                        </li>
                    </ul>
                </div>
                <div class="social-colors shareon text-center  clrlist">
                    <i class="fa fa-flag"></i> <a data-toggle="modal" <?php if (isset(Auth::user()->id)) { ?> data-target="#myModal" <?php } else { ?> data-target="#signIn" <?php } ?>><span> Report this task</span> </a>
                </div>
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close refresh" onclick="pageReload()" style="width: 50px;padding: 0px 10px 10px 10px;">&times;</button>
                                <h3>What would you like to report?</h3>
                            </div>
                            <div class="modal-body">
                                <div id="errors1"></div>
                                <form class="reportForm" name="form" id="myForm">
                                    <div class="form-group" style="width: auto; overflow: hidden;">
                                        <p>Please give us more information regarding this report</p>
                                        <select class="form-control" name="category" id="category">
                                            <option value="">Please select a category</option>
                                            <option value="Spam">Spam</option>
                                            <option value="Rude or Offensive">Rude or Offensive</option>
                                            <option value="Breach of Community Guidelines">Breach of Community Guidelines</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <br>
                                        <textarea name="reason" class="form-control" id="reason" placeholder="Comment (required)" spellcheck="true" style="z-index: auto; position: relative; line-height: normal; font-size: 12px; transition: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255); border: 1px solid rgba(191, 212, 219, 0.95);"></textarea>
                                    </div>
                                    <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="task_id" id="task_id" value="<?php echo $task->id; ?>">
                                </form>      
                            </div>
                            <div class="modal-footer">

                                <button type="button" id="closemodal" class="btn btn-success btn-lg pull-right" onclick="saveTaskReport();">Send Report</button>
                                <button class="btn btn-default btn-lg pull-left" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <div class="rgt__map__icon hover-anchor">
                <a href="#mapViewerMini" title="Click this button to toggle view of the task map.">
                    <img src="{{asset('')}}/front/images/mapmarker.png" />
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="rgt__task__loc rgt__task__offers" id="withOffer">
        <div class="noPadding" id="offers">
        </div>
    </div>
    <?php if ($task->taskStatus == 'completed') { ?>
        <div class="alert now-closed">Comments are now closed</div>
    <?php } ?>  
    <div class="noPadding">
    </div>
    <div class="noPadding" id="comments">

    </div>
    <!-- close well -->
</div>

<div id="offerModal" class="modal fade makeOfferModal" role="dialog"></div>
<?php
$lat = $task->latitude;
$lng = $task->longitude;
?>
<script>
    jQuery(".brs__rgt__inr .close-panel").click(function () {

        jQuery(".brs-rgt").removeClass("is-active");
    });
    $(document).ready(function () {
        //taskLocationMap();
    });
    function taskLocationMap() {
        var myLatlng = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
        var mapOptions = {
            zoom: 14,
            center: myLatlng
        };
        var image = '<?php echo asset('front/images/blue_marker.png') ?>';
        var map = new google.maps.Map(document.getElementById("map12"), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            animation: google.maps.Animation.DROP,
            map: map,
            icon: image,
            title: "Task Location"
        });
    }

</script>
<script>
    function pageReload() {
        location.reload();
    }
    function saveTaskReport() {
        var formdata = $(".reportForm").serialize();
        $('#errors1').html("").hide();
        $.ajax({
            url: "<?php echo url('task/report'); ?>",
            type: 'POST',
            dataType: 'json',
            async: false,
            data: formdata,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#errors1').html(errorsHtml).show();
                } else
                {
                    $('#errors1').html('<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Successfully Submitted!</div>').show();

                }
            },
            error: function (xhr, status, response) {
            }
        });
    }
    function makeOffer(key)
    {
        $.ajax({
            url: "<?php echo url('makeoffer'); ?>",
            type: 'get',
            dataType: 'html',
            async: false,
            data: {key: key},
            success: function (response) {
                $('#offerModal').html(response);
            },
            error: function (xhr, status, response) {
            }
        });
    }
    function getComments()
    {
        var task_id =<?php echo $task->id ?>;
        $.ajax({url: "<?php echo url(""); ?>/comments/getcomments/" + task_id,
            type: 'get',
            dataType: 'html',
            async: false,
            //                                            data: {
            //                                                task_id: task_id
            //                                            },
            success: function (result) {
                $("#comments").html(result);
            }
        });
    }
    function getOffers()
    {

        var task_id =<?php echo $task->id ?>;
        $.ajax({url: "<?php echo url(""); ?>/offers/getoffers",
            type: 'get',
            dataType: 'html',
            async: false,
            data: {
                task_id: task_id
            },
            success: function (result) {
                $("#offers").html(result);
            }
        });
    }
    $(document).ready(function (e) {

        getComments();
        getOffers();
        taskLocationMap();
        $('[data-toggle="tooltip"]').tooltip();
        $(".rgt__map__icon a").on('click', function () {
            $('.brs__rgt__map').toggle();
        });
    });
</script>
