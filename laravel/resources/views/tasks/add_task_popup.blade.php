<!-- Post a Task Modal -->
<div id="postTask" class="modal fade" role="dialog">

</div>

<script>
    function viewForm()
    {
        $.ajax({
            url: "<?php echo url('tasks/form'); ?>",
            type: 'get',
            dataType: 'html',
            success: function (response) {

                $('#postTask').html(response).show();
            },
            error: function (xhr, status, response) {
            }
        });
    }

    function saveTask() {

        var formdata = $(".postTaskForm").serialize();
        $('#postErrors').html("").hide();
        $.ajax({
            url: "<?php echo url('task/store'); ?>",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });

                    errorsHtml += '</ul></div>';
                    $('#postErrors').html(errorsHtml).show();
                } else
                {
                    if (response.login === 0) {
                        window.location = "<?php echo url('signup'); ?>";
                    } else {
                        $('#postErrors').html('<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Successfully Submitted!</div>').show();
                        window.location = "<?php echo url('task'); ?>/" + response.key;
                    }

                }


                // window.location = "<?php echo url('task/create'); ?>";
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>

<!--Post Task Modal End-->
