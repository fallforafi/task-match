<?php
if (empty($errors)) {
    $currencies = Config::get('params.currencies');
    $currency = $currencies[Config::get('params.currency_default')]['symbol'];
    $commission = Config::get('params.commission');
    $minus = ($task->price / 100) * $commission;
    $actualPrice = $task->price - $minus;
    ?>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="offerBody">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Make an Offer</h4>
            </div>
            <div class="modal-body">
                <div class="make-offer-tip">
                    <p><i class="fa fa-check-circle-o"></i> &nbsp; Tip: The more information you give in your offer the higher chance you have of being successful. i.e relevant experience, availability, etc</p>
                </div>
                <div class="make__offer__inputs hover-anchor">
                    <div id="errors"></div>

                    <form id="offerForm" name="offerForm">
                        <div class="form-group col-sm-4">
                            <label class="" for="offerPrice"><a href="#"><i class="fa fa-question-circle"></i></a>Offer price</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                <input class="form-control" id="offerPrice" name="offerPrice" value="<?php echo $task->price; ?>" placeholder="<?php echo $task->price; ?>" type="number">
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="" for="getPaid">You get Paid</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                <input disabled="disabled" class="form-control" id="getPaid" name="getPaid" placeholder="<?php echo $actualPrice; ?>" value="<?php echo $actualPrice; ?>" aria-disabled="true">
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="" for="servFee">Service Fee</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                                <input disabled="disabled" class="form-control yourPrice" id="serviceCharge" name="serviceCharge" placeholder="<?php echo $minus; ?>" value="<?php echo $minus; ?>" aria-disabled="true">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group col-sm-12 mt20">
                            <label class="" for="offerDtl">Offer Details</label>
                            <textarea class="form-control" cols="45" id="description" name="description" placeholder="Explain why you are the right person for the job in min 5 chars" required="" rows="3" aria-multiline="true" tabindex="0" required min="20" max="500" aria-required="true" aria-invalid="true"></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group button-btn pul-rgt col-sm-12 mt20">
                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                            <input type="hidden" name="task_id" id="task_id" value="<?php echo $task->id; ?>">
                            <input type="button" id="test1" class="form-control" value="Make an offer">
                            <div class="loading text-center" style="display: none;">
                                <p><img src="{{ asset('front/images/loader.gif') }}">Loading..</p>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <!--        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            </div>
        </div>
        <div class="modal-content" id='offerSuccess' style="display:none;">
            <div class="modal-header">
                <button type="button" class="close refresh" onclick="pageReload()">&times;</button>
                <h3 class="modal-title text-center">Success!</h3>

            </div >
            <div class='modal-body text-center'>
                <div class="img"><img src="{{asset('')}}/front/images/email-sent.png" alt="logo" /></div>
                <h5><strong>Congratulations!</strong> your Offer has been made.</h5></div>
        </div>

    </div>

    <script>
        $('#test1').click(function () {
            $('.loading').show();
            $('#test1').hide();
            setTimeout(function () {
                saveOffer();
                $('.loading').hide();
                $('#test1').show();             
            }, 500);
        });
        function pageReload() {
            location.reload();
        }
        $("#offerPrice").keyup(function (e) {
            setCommission();
        });
        $("#offerPrice").change(function (e) {

            setCommission();
        });

        function setCommission()
        {
            var offerPrice = $("#offerPrice").val();
            var commission =<?php echo $commission; ?>;
            var serviceCharge;
            var getPaid;
            serviceCharge = (offerPrice / 100) * commission;
            getPaid = offerPrice - serviceCharge;
            $("#getPaid").val(getPaid);
            $("#serviceCharge").val(serviceCharge);

        }

        function saveOffer() {
            var formdata = $("#offerForm").serialize();
            $('#errors').html("").hide();
            $.ajax({
                url: "<?php echo url('offers/store'); ?>",
                type: 'POST',
                dataType: 'json',
                async: true,
                data: formdata,
                success: function (response) {
                    if (response.error === 1) {
                        var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                        $.each(response.errors, function (key, value) {
                            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                        });

                        errorsHtml += '</ul></div>';
                        $('#errors').html(errorsHtml).show();
                    } else
                    {
                        $('#offerBody').html("").hide();
                        $('#offerSuccess').show();
                    }
                    // window.location = "<?php //echo url('task/create');                               ?>";
                },
                error: function (xhr, status, response) {
                }
            });
        }
    </script>
    <?php
} else {
    ?>

    <div class="modal-dialog">
        <?php //d($errors,1);  ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">   
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title text-center">To Confirm your Offer</h3>
            </div>
            <div class="modal-body">
                <div class="form-group col-sm-12">
                    <label class="" for="offerDtl">Upload a Profile Picture</label><br>
                    <?php if (isset($errors['image'])) { ?>
                        <button type="button" class="btn btn-sm" onclick="window.open('<?php echo url('picture') ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1024,height=600');"><i class="fa fa-remove red"></i> Upload a Profile pic</button>                       
                    <?php } else { ?>
                        <i class="fa fa-check-circle green"></i> Profile photo set  
                    <?php } ?>
                </div>
                <div class="form-group col-sm-12">
                    <label class="" for="offerDtl">Choose How to Receive Payment</label><br>
                    <?php if (isset($errors['payment'])) { ?>
                        <button type="button" class="btn btn-sm" onclick="window.open('<?php echo url('payments?tabName=receivePay') ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1024,height=600');"><i class="fa fa-remove red"></i> Add your How to Receive Payment</button>               
                    <?php } else { ?>
                        <i class="fa fa-check-circle green"></i> Payment Added
                    <?php } ?>
                </div>
<!--                <div class="form-group col-sm-12">
                    <label class="" for="offerDtl">Provide a Billing Address</label><br>
                    <?php if (isset($errors['payment'])) { ?>
                        <button type="button" class="btn btn-sm" onclick="window.open('<?php echo url('payments?tabName=receivePay') ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1024,height=600');"><i class="fa fa-remove red"></i> Add your Billing Address </button>          
                    <?php } else { ?>
                        <i class="fa fa-check-circle green"></i> Payment Billing Address
                    <?php } ?>
                </div>-->
                <div class="form-group col-sm-12">
                    <label class="" for="offerDtl">Provide a Date of Birth</label><br>
                    <?php if (isset($errors['dob'])) { ?>
                        <button type="button" class="btn btn-sm" onclick="window.open('<?php echo url('profile') ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1024,height=600');"><i class="fa fa-remove red"></i> Add your Date of birth </button>                        
                    <?php } else { ?>
                        <i class="fa fa-check-circle green"></i> Date of Birth is added   
                    <?php } ?>
                </div>
                <div class="form-group col-sm-12">
                    <label class="" for="offerDtl">Provide a Mobile Number</label><br>
                    <?php if (isset($errors['phone'])) { ?>
                        <button type="button" class="btn btn-sm" onclick="window.open('<?php echo url('mobile') ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1024,height=600');"><i class="fa fa-remove red"></i> Add your Mobile Number or verify </button>                   
                    <?php } else { ?>
                        <i class="fa fa-check-circle green"></i> Mobile Number is added or verified
                    <?php } ?>
                </div>
                <br>
                <button onclick="makeOffer('<?php echo $task->key; ?>')" type="button" class="btn btn-success text-center">Continue</button>
            </div>

        </div>
    </div>
    <?php
}


