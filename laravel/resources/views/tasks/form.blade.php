<?php

use App\Categories;
use App\Functions\Functions;

$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$categoriesModel = Categories::get();
$categories = Functions::getChildCategories($categoriesModel);
$required = 'required';
?>
<style>
    ul#tree1 >li {
        border: 0px solid #ccc;
        padding: 5px;
    }
    ul#tree1 .product-title[href*="delete"] {
        color: #d00;
    }

    span.actions >a {
        background-color: #eee;
        display: inline-block;
        padding: 1px 8px;
        border-radius: 3px;
        margin: 0 0 10px 10px;
        font-size: 12px;
        border: 1px solid #ccc;
        line-height: 18px;
    }
    .pac-container {
        background-color: #FFF;
        z-index: 1055 !important;
        display: inline-block;
        float: left;
    }
</style>
<div id="postErrors"></div>

<form class="postTaskForm" name="postTaskForm" id="postTaskForm">
    <div class="tab-content">
        <div id="details" class="tab-pane fade in active">
            <div class="dtl-box pt10">
                <div class="form-group task-title-input hover-anchor">
                    <label for="taskTitle">
                        Task title
                        <a href="#" title="The Task Title is a short heading for your task, Eg, Cleaner needed for my home.">
                            <i class="fa fa-question-circle grey-icon"></i>
                            <i class="fa fa-check-circle green-icon"></i>
                        </a>
                    </label>
                    {!! Form::text('title', 'Testing testing Testing testing', array('class' => 'form-control','id'=>'title','placeholder'=>' Add Title',$required) ) !!}<span id="titleError"></span>

                </div>
                <div class="form-group task-desc hover-anchor">
                    <label for="taskDesc">
                        Task Description
                        <a href="#" title="Please enter a Task Description, give as much detail as you can about your task. Eg, Clean my four bedroom apartment for two hours">
                            <i class="fa fa-question-circle grey-icon"></i>
                            <i class="fa fa-check-circle green-icon"></i>
                        </a>
                    </label>
                    {!! Form::textarea('description','Testing testingTesting testingTesting testingTesting testingTesting testingTesting testingTesting testingTesting testing', ['class' => 'form-control', 'rows' => '5','id'=>'description', 'placeholder' => 'Task Description']) !!}<span id="descriptionError"></span>


                </div>
                <div class="need-help hover-anchor--2 hover-anchor">
                    <a href="faqs">Need more help &nbsp;</a><a href="{{ url('faqs') }}" title="Tasks with detailed descriptions receive 40% more engagements and lead to higher quality bids"><i class="fa fa-question-circle"></i></a>
                </div>
            </div>
            <div class="form-group col-md-12 oveload">
                <hr>
                <a data-toggle="tab" class="post-task-next btn btn-success pull-right" id="next1">Next</a>
                <input type="button" class="post-task-next btn btn-success" id="test2" disabled="disabled" value="Save as Draft">
                <div class="draftloading" style="display: none;">
                    <p><img src="{{ asset('front/images/loader.gif') }}">Loading..</p>
                </div>
                <button type="button" class="post-task-next btn btn-success pull-right" onclick="pageReload();">Cancel</button>
            </div>
        </div>
        <div id="location" class="tab-pane fade">
            <div class="post-location pt10">
                <div class="form-group w60">
                    <label for="taskLocation">
                        Task Location

                    </label>
                    <div id="test2">
                        <input type="text" class="form-control autocomplete location" name="location" id="locality" onfocus="geolocate();" value=""><span id="locationError"></span>
<!--                        <input type="hidden" name="location" id="locality" value="">-->
                        <input type="hidden" name="longitude"  id="longitude1" value="">
                        <input type="hidden" name="latitude"  id="latitude1" value="">
                    </div>
                </div>
                <div class="form-group w70">
                    <label for="taskLocation">
                        Due Date
                    </label>
                    <div class="task__hours">
                        <label>
                            <input type="radio" name="chooseDate" id="chooseDate" class="today" >Today
                        </label>
                        <label>
                            <input type="radio" name="chooseDate" id="chooseDate" class="certainDay">By a certain day
                        </label>
                        <label>
                            <input type="radio" name="chooseDate" id="chooseDate" class="withinWeek" checked="checked">Within 1 week
                        </label>
                    </div>
                    <input type="text"  class="form-control todaydueDate" required="required" placeholder="Due Date" readonly="readonly" value="<?php echo date('d-m-Y'); ?>" style="display:none;">

                    <input type="text" class="form-control certainDaydueDate" data-provide="datepicker" required="required" placeholder="Due Date" value="<?php echo date('d-m-Y', strtotime("+1 week")); ?>" style="display:none;">

                    <input type="text" name="dueDate" class="form-control withinWeekdueDate" required="required" placeholder="Due Date" readonly="readonly" value="<?php echo date('d-m-Y', strtotime("+6 days")); ?>" >
                </div>
                <div class="form-group task-tobe-comp btn-effect--ripple col-sm-6" data-toggle="buttons">
                    <label for="transport">To be completed
                    </label><br />
                    <label class="btn btn-primary">
                        <input type="radio" name="toBeCompleted" id="toBeCompleted" value="online"><i class="fa fa-laptop"></i>&nbsp; Online Task
                    </label>
                    <label class="btn btn-primary active">
                        <input type="radio" name="toBeCompleted" id="toBeCompleted" value="inperson" checked="checked"><i class="fa fa-user"></i>&nbsp; In person task
                    </label>
                </div>             
                <div class="form-group w50">
                    <label for="taskLocation">
                        Categories
                    </label>
                    <div class="dropdown post-task-ctg">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Choose Category
                            <span class="caret"></span></button><span id="categoriesError"></span>
                        <ul class="dropdown-menu btn-effect--ripple alist" id="ctgAdding" data-toggle="buttons">
                            <?php foreach ($categoriesModel as $cat) { ?>
                                <?php
                                if (!$cat->parent_id == 0) {
                                    ?>
                                    <li class="btn btn-primary" id="{{ $cat->id }}"><i class="{{ $cat->class }}"></i><input type="checkbox" id="ctgChecked" value="{{ $cat->id }}" id="check_1" name="categories[]"/>{{ $cat->name }}</li>                                       
                                <?php } ?>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="form-group w50 pul-rgt">
                    <div class="post-task-sum">
                        <label for="taskSummary">
                            Summary
                        </label>
                        <div class="clrlist blist">
                            <ul>
                                <?php foreach ($categoriesModel as $cat) { ?>
                                    <?php
                                    if (!$cat->parent_id == 0) {
                                        ?>
                                        <li class="btn btn-primary {{ $cat->id }}" data-id="{{ $cat->id }}"><i class="{{ $cat->class }}"></i><input type="checkbox" id="ctgChecked" value="Home & Cleaning" id="check_1"/> {{ $cat->name }}</li>                                       
                                    <?php } ?>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <hr>
                <table class="pull-right">
                    <tr>
                        <td><a data-toggle="tab" class="post-task-back btn btn-default right" href="#details" id="back" style="margin-right: 4px;">Back</a></td>
                        <td><a data-toggle="tab" class="post-task-next btn btn-success right" href="#budget" id="next2">Next</a></td>
                    </tr>
                </table>
                <input type="button" class="post-task-next btn btn-success" id="test2" value="Save as Draft">
            </div>
        </div>
        <div id="budget" class="tab-pane fade">
            <div class="budget-box">
                <div class="bud__lft w50">
                    <div class="form-group w75 hover-anchor">

                        <div class="task__hours">
                            <label>
                                <input type="radio" name="priceType" id="priceType" class="total" value="total" checked="checked">Total
                            </label>
                            <label>
                                <input type="radio" name="priceType" id="priceType" class="hours" value="hourly">Per Hour
                            </label>
                        </div>


                        <label for="budgetPrice" id="total">
                            Total Price
                            <a href="#" title="Tip - Always offer a fair price as you will receive higher quality interest quicker">
                                <i class="fa fa-question-circle grey-icon"></i>
                            </a>
                        </label>
                        <label for="budgetPrice" id="hourly" style="display:none;">
                            Per Hour Price
                            <a href="#" title="Tip - Always offer a fair price as you will receive higher quality interest quicker">
                                <i class="fa fa-question-circle grey-icon"></i>
                            </a>
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                            <?php //$rand = rand(50, 1000); ?> 
                            <input type="number" name="price" class="form-control" id="price" placeholder="Budget Price">
                            <input type="number" name="hourlyRate" class="form-control" id="hourlyRate" value="0" placeholder="Hourly Price">
                        </div>

                        <br>
                        <div class="input-group add" style="display:none;">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-clock-o">
                                </i></span>
                            <input type="number" name="hours" class="form-control" id="hours" value="0" placeholder="Add Hours">
                        </div>
                        <br>
                        <label class="getPaidLabel">
                            Estimated Budget
                        </label>
                        <div class="input-group getPaid">
                            <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
                            <input type="number" class="form-control" id="getPaid" value="0" placeholder="" disabled="disabled">
                        </div>
                        <?php ?>

                    </div>
                    <div class="need-help hover-anchor--2">
                        <a class="brs-ctg-nav1">Price Guide <i class="fa fa-question-circle"></i></a>
                    </div>
                    <div class="brs__filt__box1 sel " style="display:none;" data-toggle="buttons">
                        <div class="clearfix"></div>
                        <div class="brs__task__sts well col-sm-12">
                            <select class="form-control" name="cat" id="cat">
                                <option value="0">Choose Category</option>
                                <?php foreach ($categoriesModel as $cat) { ?>
                                    <?php
                                    if (!$cat->parent_id == 0) {
                                        ?>
                                        <option value="{{ $cat->id }}"> {{ $cat->name }}</option>                                        
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                            </select> 
                            <div class="text-left" id="data">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <hr>
                <table class="pull-right">
                    <tr>
                        <td><a data-toggle="tab" class="post-task-back btn btn-default right" href="#location" id="backLoc" style="margin-right: 4px;">Back</a></td>
                        <td><button type="button" class="post-task-next btn btn-success" onclick="saveTask();" id="submit">Get quotes now</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
</form>

<script type="text/javascript">
	    $(document).ready(function () {
        //$("li .brs__ctg__filt").on('click', function () {

            
           // geolocate1();
        //});

    });
    Errorimg = '<img src="{{ asset('front/images/error.png') }}">';
    $('#next1').click(function () {

        if ($("#title").val().length < '1') {
            jQuery('#titleError').html(Errorimg).show();
        }
        if ($("#description").val().length < '1') {
            jQuery('#descriptionError').html(Errorimg).show();
        }
        if ($("#title").val().length < '10') {
            jQuery('#errors1').append('<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Please enter a title for your task between 10 and 50 characters</div>');
        }
        if ($("#title").val().length > '50') {
            jQuery('#errors1').append('<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Title is too long!<br>The title may not be greater than 50 characters.</div>');
        }
        if ($("#description").val().length < '25') {
            jQuery('#errors1').append('<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>Please enter a description for your task that is at least 25 characters</div>');
        }
    });
    $('#next2').click(function () {
        if ($(".location").val().length < '1') {
            jQuery('#locationError').html(Errorimg).show();
            jQuery('#next2').removeAttr('href');
            jQuery('li#bud').fadeIn("fast").css("pointer-events", "none");
            jQuery('li#bud').removeClass('active');

        } else {
            jQuery('a#budg').fadeIn("fast").attr("href", "#budget");
            jQuery('#next2').attr('href', '#budget');
            jQuery('li#bud').addClass('active');
            jQuery('li#lo').removeClass('active');
            jQuery('#locationError').hide();
        }
//        if (document.form.categories.value.checked) {
//            alert('checked');
////            jQuery('#categoriesError').html(Errorimg).show();
////            jQuery('#next2').removeAttr('href');
////            jQuery('li#bud').fadeIn("fast").css("pointer-events", "none");
////            jQuery('li#bud').removeClass('active');
//        } else {
//             alert('Notchecked');
//        }
    });
</script>
<script type="text/javascript">
    function pageReload() {
        location.reload();
    }
    $(document).on("click", ".alist li", function () {
        $(this).toggleClass("is-select");
        var nValue = $(this).attr("id");

        if ($(".blist li").hasClass(nValue)) {
            $(".blist li" + "." + nValue).addClass("showMe");
        }
    });

    $(document).on("click", ".alist li.is-select", function () {
        //$('.alist li.is-select').click(function(){
        var rValue = $(this).attr("id");
        //alert(rValue);
        $(".blist li" + "." + rValue).removeClass("showMe");
    });
    $("#hourlyRate, #hours").keyup(function (e) {
        setTotalPerhour();
    });
    $("#hourlyRate, #hours").change(function (e) {
        setTotalPerhour();
    });
    $("#price").keyup(function (e) {
        setTotalPrice();
    });
    $("#price").change(function (e) {
        setTotalPrice();
    });
    $('.today').on('change', function () {
        jQuery('.todaydueDate').fadeIn("fast").css("display", "");
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', 'dueDate');
        jQuery('.certainDaydueDate').attr('name', '');
        jQuery('.withinWeekdueDate').attr('name', '');
    });
    $('.certainDay').on('change', function () {
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "");
        jQuery('.todaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', '');
        jQuery('.certainDaydueDate').attr('name', 'dueDate');
        jQuery('.withinWeekdueDate').attr('name', '');
    });
    $('.withinWeek').on('change', function () {
        jQuery('.withinWeekdueDate').fadeIn("fast").css("display", "");
        jQuery('.certainDaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').fadeIn("fast").css("display", "none");
        jQuery('.todaydueDate').attr('name', '');
        jQuery('.certainDaydueDate').attr('name', '');
        jQuery('.withinWeekdueDate').attr('name', 'dueDate');
    });

    function setTotalPerhour()
    {
        var hourlyRate = $("#hourlyRate").val();
        var hours = $("#hours").val();
        var getPaid;
        getPaid = hourlyRate * hours;
        $("#getPaid").val(getPaid);
    }
    function setTotalPrice()
    {
        var price = $("#price").val();
        $("#getPaid").val(price);
    }
</script>
<script>
    $('.need-help a.brs-ctg-nav1').click(function () {
        $('.sel').toggle(".sel");
    });
    $('#test2').click(function () {
        $('.draftloading').show();
        $('#test2').hide();
        saveDraft();
        setTimeout(function () {
            $('.draftloading').hide();
            $('#test2').show();
        }, 500);
    });

    function saveDraft() {
        var formdata = $(".postTaskForm").serialize();
        $('#errors1').html("").hide();
        $.ajax({
            url: "<?php echo url('task/draft/save'); ?>",
            type: 'POST',
            dataType: 'json',
            async: true,
            data: formdata,
            success: function (response) {

                if (response.error === 1) {
                    var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                    $.each(response.errors, function (key, value) {
                        errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                    });
                    errorsHtml += '</ul></div>';
                    $('#errors1').html(errorsHtml).show();
                } else
                {
                    if (response.login === 0) {
                        window.location = "<?php echo url('signup'); ?>";
                    } else {
                        $('#postDraft').html("").hide();
                        $('#draftSuccess').show();
                    }

                }
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
<script>
    $('#data').html('<p>No data Found...</p>');
    $('#cat').on('change', function () {
        $.ajax({
            type: "GET",
            url: "<?php echo url('/cat/get/'); ?>/" + this.value,
            data: "",
            async: true
        }).success(function (val) {
            var response = JSON.parse(val);
            if (response === null) {
                var html = "<p>No data Found...</p>";
                $('#data').html(html);
            } else {
                if (response.description === '') {
                    var html = "<p>No data Found...</p>";
                    $('#data').html(html);
                } else {
                    var html = response.description;
                    $('#data').html(html);
                }
            }

        });
    });</script>  
<script>
    var placeSearch;
    var autocomplete = {};
    var autocompletesWraps = ['test', 'test2'];
    var test_form = {locality: 'long_name'};
    var test2_form = {locality: 'long_name'};
    function initAutocomplete() {
        $.each(autocompletesWraps, function (index, name) {

            if ($('#' + name).length === 0) {
                return;
            }
            autocomplete[name] = new google.maps.places.Autocomplete($('#' + name + ' .autocomplete')[0], {types: ['geocode']});
            autocomplete[name].setComponentRestrictions(
                    {'country': ['ie']});
            google.maps.event.addListener(autocomplete[name], 'place_changed', function () {
                var place = autocomplete[name].getPlace();
                var form = eval(name + '_form');
                // Get the place details from the autocomplete object.
                //var place = autocomplete.getPlace();

                document.getElementById("latitude1").value = place.geometry.location.lat();
                document.getElementById("longitude1").value = place.geometry.location.lng();
                //                                        document.getElementById("latitude2").value = place.geometry.location.lat();
                //                                        document.getElementById("longitude2").value = place.geometry.location.lng();

                for (var component in form) {
                    $('#' + name + ' #' + component).val('');
                    $('#' + name + ' #' + component).attr('disabled', false);
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (typeof form[addressType] !== 'undefined') {
                        var val = place.address_components[i][form[addressType]];
                        $('#' + name + ' #' + addressType).val(val);
                    }
                }


            });
        });
    }
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
		initAutocomplete();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                document.getElementById("latitude1").value = latitude;
                document.getElementById("longitude1").value = longitude;
                //                                        document.getElementById("latitude2").value = latitude;
                //                                        document.getElementById("longitude2").value = longitude;

            });
            
        }
    }


</script>

<script>
    jQuery('#postTaskForm .form-control').change(function () {
        if ($("#title").val().length > '10' && $("#description").val().length > '25') {
            // alert('Hi! Amoosss');
            jQuery('li#lo').removeAttr('style');
            jQuery('li#bud').removeAttr('style');
            jQuery('a#loc').fadeIn("fast").attr("href", "#location");
            jQuery('a#budg').fadeIn("fast").attr("href", "#budget");
            jQuery('a#next1').fadeIn("fast").attr("href", "#location");
            $('#next1').click(function () {
                if ($("#title").val().length > '10' && $("#description").val().length > '25') {
                    jQuery('li#de').removeClass('active');
                    jQuery('li#lo').addClass('active');
                }
            });
            jQuery('#titleError').hide();
            jQuery('#descriptionError').hide();
        } else {
            jQuery('li#lo').fadeIn("fast").css("pointer-events", "none");
            jQuery('li#bud').fadeIn("fast").css("pointer-events", "none");
            jQuery('a#budg').fadeIn("fast").attr("href", "");
            jQuery('a#next1').fadeIn("fast").attr("href", "");
            jQuery('a#loc').fadeIn("fast").attr("href", "");

            jQuery('li#de').fadeIn("fast").addClass('active');
            jQuery('li#lo').fadeIn("fast").removeClass('active');
        }
        if ($(".location").val().length < '1') {
            jQuery('#next2').removeAttr('href');
            jQuery('li#bud').fadeIn("fast").css("pointer-events", "none");
            jQuery('li#bud').removeClass('active');
        } else {
            jQuery('a#budg').fadeIn("fast").attr("href", "#budget");
            jQuery('#next2').attr('href', '#budget');
            jQuery('li#bud').removeAttr('style');
            jQuery('#locationError').hide();
        }
    });
    jQuery("#back").click(function (e) {
        jQuery('li#de').addClass('active');
        jQuery('li#lo').removeClass('active');
    });
    jQuery("#backLoc").click(function (e) {
        jQuery('li#lo').addClass('active');
        jQuery('li#bud').removeClass('active');
    });

    jQuery('#title').keydown(function () {
        var mintitleTxtChar = 10;
        if ($("#title").val().length !== mintitleTxtChar)
        {
            $('#test2').attr('disabled', 'disabled');
            jQuery(this).parent("div").removeClass("title-valid");
            $("#title").focus();
        }
        if ($("#title").val().length > mintitleTxtChar)
        {
            $('#test2').removeAttr('disabled');
            jQuery(this).parent("div").addClass("title-valid");
        }
//        if (document.form.title.value.length > '50') {
//            jQuery('#titleError').html('Title is too long!<br>The title may not be greater than 50 characters.');
//        }
    });
    jQuery('#description').keydown(function () {
        var mindescriptionTxtChar = 25;
        if ($("#description").val().length !== mindescriptionTxtChar)
        {
            jQuery(this).parent("div").removeClass("title-valid");
            $("#description").focus();
        }
        if ($("#description").val().length > mindescriptionTxtChar)
        {
            jQuery(this).parent("div").addClass("title-valid");
        }
    });
    $(document).ready(function () {
		
        $('[data-toggle="tooltip"]').tooltip();
        jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
        $('.hours').on('change', function () {
            $("#getPaid").val(0);
            jQuery('.add').fadeIn("fast").css("display", "");
//            jQuery('.getPaid').fadeIn("fast").css("display", "");
//            jQuery('.getPaidLabel').fadeIn("fast").css("display", "");
            jQuery('#hourly').fadeIn("fast").css("display", "");
            jQuery('#hourlyRate').val('');
            jQuery('#hours').val('');
            jQuery('#total').fadeIn("fast").css("display", "none");
            jQuery('#price').fadeIn("fast").css("display", "none");
            jQuery('#price').val('0');
            jQuery('#hourlyRate').fadeIn("fast").css("display", "");
        });
        $('.total').on('change', function () {
            $("#getPaid").val(0);
            jQuery('.add').fadeIn("fast").css("display", "none");
//            jQuery('.getPaid').fadeIn("fast").css("display", "none");
//            jQuery('.getPaidLabel').fadeIn("fast").css("display", "none");
            jQuery('#hourly').fadeIn("fast").css("display", "none");
            jQuery('#total').fadeIn("fast").css("display", "");
            jQuery('#price').fadeIn("fast").css("display", "");
            jQuery('#price').val('');
            jQuery('#hourlyRate').val('0');
            jQuery('#hours').val('0');
            jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
        });
        $('.checkall').on('change', function () {
            $(".sub_cat_" + this.id).prop('checked', $(this).prop('checked'));
        });
    });
    $(document).on("click", ".task__ctg__listChk li ul li", function () {
        $(this).toggleClass("is-select");
    });

    jQuery('[data-provide="datepicker"]').datepicker({
        format: "dd-mm-yyyy"
    }).on('change', function () {
        jQuery('.datepicker').hide();
    });
</script>
