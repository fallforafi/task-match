@extends('layout')
<?php
$title = 'Create';
$description = "";
$keywords = "";
?>
@include('front/common/meta')
@section('content')
<section class="slider-area hover-ctrl fadeft" >
    <div class="container">
        <h1>Get free quotes now!</h1>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#details">Details</a></li>
            <li><a data-toggle="tab" href="#location">Location</a></li>
            <li><a data-toggle="tab" href="#budget">Budget</a></li>
        </ul>
        @include("front.tasks.form")
    </div>
</section>
@endsection