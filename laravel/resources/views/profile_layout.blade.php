<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
    <head>
        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <link rel="icon" type="image/png" href="{{asset('front/images/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/style.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/style-extra.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/messages.css')}}">
        <link rel="stylesheet" href="{{asset('front/dashboard.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/responsive.css')}}">
        <!--
        <script src="{{asset('front/extralized/modernizr-2.6.2.min.js')}}"></script>
         <script src="{{asset('/front/extralized/jquery.easing.1.3.js')}}"></script>
        -->

        <script src="{{asset('/front/js/jquery-2.2.4.min.js')}}"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
        <script src="{{asset('front/js/bootstrap.min.js')}}"></script>

    </head>

    <body class="transition nav-plusminus slide-navbar slide-navbar--right">

        @include('front/common/navigation')
        <main id="page-content">
            @yield('content')


            @include('front/common/footer')
            @include('front/tasks/add_task_popup')

        </main>

        <a href="" class="scrollToTop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        <div id="loading"></div>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo env('GOOGLE_API'); ?>&libraries=places" async defer></script>
        <script src="{{asset('/front/js/viewportchecker.js')}}"></script>
        <script src="{{asset('/front/js/kodeized.js')}}"></script>
        @include('front/common/js')
        <script>
$(document).ready(function () {
    unseenMessages();
    unseenNotifications();
});
var lastScrollizedTop = 0;
jQuery(window).scroll(function (event) {
    var scrollizedTop = jQuery(this).scrollTop();
    if (scrollizedTop > lastScrollizedTop) {
        jQuery("body").removeClass("scrollized--up");
        jQuery("body").addClass("scrollized--down");
    } else {
        jQuery("body").removeClass("scrollized--down");
        jQuery("body").addClass("scrollized--up");
    }
    lastScrollizedTop = scrollizedTop;
});
        </script>
        @include('front/common/chat')
        @include('front/common/google_analytics')
    </body>
</html>
