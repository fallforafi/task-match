<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$required = 'required';
?>

<div class="form-group">
    {!! Form::label('title') !!}
    {!! Form::text('title',$model->title , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" rows="5" name="description" id="description" placeholder="Description ">{{$model->description}}</textarea>
</div>
<div class="form-group">
    {!! Form::label('due date') !!}
    {!! Form::text('dueDate',date("Y-m-d",strtotime($model->dueDate)) , array('class' => 'form-control datepicker',$required) ) !!}
</div>
<div class="form-group">
    <label for="taskToBeCompleted">
        To be completed
    </label>
    <div class="form-group">
        <label>
        <input type="radio" name="toBeCompleted" id="toBeCompleted" value="online" {{ ($model->toBeCompleted == 'online')?'Checked':'' }}>Online task    
        </label>
        <label>
         <input type="radio" name="toBeCompleted" id="toBeCompleted" value="inperson" {{ ($model->toBeCompleted == 'inperson')?'Checked':'' }}>In person task   
        </label>
        
    </div>
</div>
<div class="form-group">
    <label for="budget">
        Budget
    </label><br>
    <label>
        <input type="radio" name="priceType" id="priceType" class="total" value="total" {{ ($model->priceType == 'total')?'Checked':'' }}>Total
    </label>
    <label>
        <input type="radio" name="priceType" id="priceType" class="hours" value="hourly" {{ ($model->priceType == 'hourly')?'Checked':'' }}>Per Hour
    </label><br><br>
    <label for="budgetPrice" id="total">
        Total Price
    </label>
    <label for="budgetPrice" id="hourly" style="display:none;">
        Per Hour Price
    </label>
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1"><?php echo $currency; ?></span>
        
        {!! Form::text('price',$model->price , array('class' => 'form-control','id'=>'price','placeholder'=>'Budget Price',$required) ) !!}
        {!! Form::text('hourlyRate',$model->hourlyRate, array('class' => 'form-control','id'=>'hourlyRate','placeholder'=>'Budget Price',$required) ) !!}
    </div>

    <br>

    <?php if ($model->hours == 0) { ?>
        <div class="input-group add" style="display:none;">
        <?php } else { ?>
            <div class="input-group add" style="">
            <?php } ?>
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-clock-o">
                </i></span>
            <input type="number" name="hours" class="form-control" id="hours" placeholder="Add Hours" value="{{($model->hours == '')?'':$model->hours}}">
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-6">
            <input type="hidden" name="id" value="{{ $task_id }}">
            <button type="submit" class="btn btn-primary btn-block">Save</button>
        </div>
        <div class="col-sm-6">
            <a href="{{ url('admin/tasks')}}" class="btn btn-warning btn-block">Cancel</a>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
            $('.hours').on('change', function () {
                jQuery('.add').fadeIn("fast").css("display", "");
                jQuery('#hourly').fadeIn("fast").css("display", "");
                jQuery('#total').fadeIn("fast").css("display", "none");
                jQuery('#price').fadeIn("fast").css("display", "none");
                jQuery('#hourlyRate').fadeIn("fast").css("display", "");
            });
            $('.total').on('change', function () {
                jQuery('.add').fadeIn("fast").css("display", "none");
                jQuery('#hourly').fadeIn("fast").css("display", "none");
                jQuery('#total').fadeIn("fast").css("display", "");
                jQuery('#price').fadeIn("fast").css("display", "");
                jQuery('#hourlyRate').fadeIn("fast").css("display", "none");
            });
        });
    </script>
