@extends('admin/admin_template')
@section('content')

<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        @include('admin/commons/errors')
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Form</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                    </div>
                </div>
                <div class="box-body">
                    <form class="form" role="form" id="filter">
                         <div class="form-group col-sm-12">
                        <label for="inlineFormInput">Title</label>
                        <input type="text" class="form-control" name="filter_search" id="filter_search" placeholder="Type your search here. . ." value="{{ (isset($filter_search))?$filter_search:'' }}"/>
                         </div>

                        
                        <div class="form-group col-sm-12">
                            <label for="filter">Filter by status</label>
                            <select class="form-control" name="task_status" id="task_status">
                                <option value="" selected>All </option>
                                <option value="open" {{ (isset($task_status) && $task_status == 'open')?'selected':'' }}>Open</option>
                                <option value="close" {{ (isset($task_status) && $task_status == 'cancelled')?'selected':'' }}>Cancelled</option>
                                <option value="assigned" {{ (isset($task_status) && $task_status == 'assigned')?'selected':'' }}>Assigned</option>
                                <option value="completed" {{ (isset($task_status) && $task_status == 'completed')?'selected':'' }}>Completed</option>
                            </select>
                        </div>
                        <input type="hidden" class="form-control" name="page" id="page" value="<?php echo $page; ?>"/>
                        <div class='clearfix'></div>
                        <div class="form-group col-sm-6">
                            <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-search"></i> Search</button>
                        </div>
                        <div class=" form-group col-sm-6">
                            <a href="{{ url('admin/tasks') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                        </div>





                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="box box-primary" id="task_listing">

            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        taskListing();
    });
    $("form").submit(function (e) {
        taskListing();
        e.preventDefault();

    });
    $("form").change(function (e) {
        taskListing();
        e.preventDefault();
    });
    $("#filter_search").keyup(function (e) {
        taskListing();
    });

    function taskListing() {
        var formdata = $("#filter").serialize();
        $.ajax({
            url: "<?php echo url('admin/tasks/listing'); ?>",
            type: 'get',
            dataType: 'html',
            data: formdata,
            success: function (response) {
                $('#task_listing').html(response);
            },
            error: function (xhr, status, response) {
            }
        });
    }
</script>
@endsection

