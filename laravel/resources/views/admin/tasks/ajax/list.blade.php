<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="box-header with-border">
    <h3 class="box-title">( Total Tasks : {{ count($model) }} )</h3>
    <div class="box-tools">
        <a href="{{ url('admin/cron-job') }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-star"></i> Reviews Awaiting</a>
    </div>
</div>
<div class="box-body">
    <?php if (count($model) > 0) { ?>

        <ul class="products-list product-list-in-box">
            <table class="table" id="order_table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Due Date</th>
                        <td></td> 

                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($model as $row)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><a href="{{ url('admin/task/'.$row->id) }}">{{ $row->title }}</a></td>
                        <td>{{ $row->description }}</td>
                        <td>{{ $row->taskStatus }}</td>
                        <td><?php echo date("d M Y", strtotime($row->dueDate)); ?></td>
                        <td><a href="{{ url('admin/task/edit/'.$row->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="task/delete/<?php echo $row->id ?>"><i class="fa fa-trash"></i> </button></td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                    @include('admin/commons/delete_modal')
                </tbody>

            </table>
            <?php echo $model->appends(Input::query())->render(); ?>
        </ul>


<?php } else {
    ?>
    <div class="">
        No Data found. . .
    </div>
        </div>
<?php }
?>
<script>
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>


