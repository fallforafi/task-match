@extends('admin/admin_template')

@section('content')
<?php
$currencies = Config::get('params.currencies');
$currency = $currencies[Config::get('params.currency_default')]['symbol'];
$orderPrefix = Config::get('params.order_prefix');
$per_month = Config::get('params.per_month');
$required = 'required';
?>

<div class="row">
    <div class="col-md-12">
        @include('admin/commons/errors')

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Task's Information </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>

                </div>
            </div>

            <?php
            $user = $model;
            ?>        
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Title :</td>
                                    <td>{{ $model->title }}</td>
                                </tr>
                                <tr>
                                    <td>Description :</td>
                                    <td>{{ $model->description }}</td>
                                </tr>
                                <tr>
                                    <td>Due Date :</td>
                                    <td><?php echo date("d M Y", strtotime($model->dueDate)); ?></td>
                                </tr>
                                <tr>
                                    <td>Price :</td>
                                    <td><?php echo $currency . ' ' . $model->price ?></td>
                                </tr>
                                <tr>
                                    <td>Key :</td>
                                    <td>{{ $model->key }}</td>
                                </tr>
                                <tr>
                                    <td>Location :</td>
                                    <td>{{ $model->location }}</td>
                                </tr>
                                <tr>
                                    <td>Task Status :</td>
                                    <td>{{ $model->taskStatus }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ url('admin/tasks') }}" class="btn btn-flat btn-default">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Task's Payment</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                </div>
            </div>       
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-group">
                            @if($model->taskStatus == 'completed')
                            @if(isset($check[0]) && $check[0]->a_id != '')
                            <li class="list-group-item bg-green text-center">
                                <p>Payment Done</p> 
                            </li>                         
                            @elseif(isset($check[0]) && $check[0]->a_id == '')
                            <li class="list-group-item bg-yellow text-center">
                                <p>Payment Pending</p>
                                <a class="btn btn-flat btn-success paynow">PayNow</a>

                            </li>
                            <div class="formPay" style="display:none;">
                                {!! Form::open(array( 'class' => 'form','url' => 'admin/task/payment/save', 'files' => true)) !!}
                                <div class="form-group col-sm-6">
                                    {!! Form::label('Reference') !!}
                                    {!! Form::text('reference',null , array('class' => 'form-control',$required) ) !!}
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('Amount') !!}
                                    {!! Form::text('amount',$check[0]->finalPrice , array('class' => 'form-control','readonly',$required) ) !!}
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 pull-right">
                                        <input type="hidden" name="gateway" value="stripe">
                                        <input type="hidden" name="type" value="receive">
                                        <input type="hidden" name="task_id" value="{{$task_id}}">
                                        <input type="hidden" name="assign_id" value="{{$check[0]->assign_id}}">
                                        <input type="hidden" name="user_id" value="{{$check[0]->user_id}}">
                                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                {!! Form::close() !!}
                            </div>
                            @endif
                            @else 
                            <li class="list-group-item bg-red text-center">
                                <p>Payment Required</p>
                            </li>                           
                            @endif

                        </ul>

                        <a href="{{ url('admin/tasks') }}" class="btn btn-flat btn-default">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Task's Offers</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                </div>
            </div>       
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-group">
                            @if(count($offers)>0)
                            <?php //d($offers,1); ?>
                            @foreach($offers as $row)
                            <li class="list-group-item">
                                <div class="pull-right">
                                    @if($row->offerStatus == 'pending')
                                    <a class="btn delete1" tabindex="0" data-toggle="modal" data-target="#myModal1" data-link="offer/delete/<?php echo $row->offer_id; ?>"><i class="fa fa-trash"></i></a>
                                    @endif
                                </div>
                                @include('front/common/profile_picture')
                                <small><a href="{{url('admin/user/')}}/{{$row->user_id}}">{{$row->firstName}} {{$row->lastName}}</a></small>
                                <div class="pull-right">
                                    <small>
                                        {{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>
                                </div>
                                <p>  <strong>Offer Message:</strong> {{ $row->message }}</p>
                            </li>
                            @endforeach
                            <div class="modal fade" id="myModal1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="mo                                                                                     dal-title" style="color: red">Alert</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3                                                                                         >Are you sure do you want to delete this?</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <a id="close                                                                                 modal1" class="btn btn-danger pull-left" href="">Yes</a>

                                            <button type="                                                                        button" class="btn btn-success pull                                                                        -left" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.moda                                                                    l -->
                            @else
                            <p>There are no offers yet on this task.</p>
                            @endif

                        </ul>

                        <a href="{{ url('admin/tasks') }}" class="btn btn-flat btn-default">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"> Task's Comments</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>

                </div>
            </div>       
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-group">
                            @if(count($comments)>0)
                            @foreach($comments as $row)
                            <li class="list-group-item">
                                <div class="pull-right">
                                    <a class="btn delete" tabindex="0" data-toggle="modal" data-target="#myModal" data-link="comment/delete/<?php echo $row->comment_id; ?>"><i class="fa fa-trash"></i></a>
                                </div>
                                @include('front/common/profile_picture')
                                <small><a href="{{url('admin/user/')}}/{{$row->user_id}}">{{$row->firstName}} {{$row->lastName}}</a></small>
                                <div class="pull-right">
                                    <small class="ng-binding">
                                        {{date('F d, Y h:i:s A',strtotime($row->created_at))}}</small>
                                </div>
                                <hr>
                                <p>{{$row->comment}}</p>
                            </li><br>
                            @endforeach
                            <div class="modal fade" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" style="color: red">Alert</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Are you sure do you want to delete this?</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <a id="closemodal" class="btn btn-danger pull-left" href="">Yes</a>

                                            <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            @else
                            <p>There are no comments yet on this task.</p>
                            @endif

                        </ul>
                        <a href="{{ url('admin/tasks') }}" class="btn btn-flat btn-default">Back</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        $(".paynow").on('click', function () {
            $('.formPay').toggle();
        });
    });
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
    jQuery('.delete1').click(function ()
    {
        $('#closemodal1').attr('href', $(this).data('link'));
    });
</script>
@endsection
