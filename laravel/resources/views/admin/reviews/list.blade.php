@extends('admin/admin_template')

@section('content')
<div class="row">
    <!-- Left col -->
<div class="col-sm-12">
    @include('admin/commons/errors')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">( Total Reviews : {{ count($model) }} )</h3>
            <div class="box-tools">
            </div>
        </div>
        <div class="box-body">
            <?php if (count($model) > 0) { ?>
                    <table class="table" id="order_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Comment</th>
                                <th>From</th>
                                <th>Task</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th></th> 

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($model as $row)
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>{{ $row->comment }}</td>
                                <td>{{ $row->firstName }} {{ $row->lastName }}</td>
                                <td>{{ $row->title }}</td>
                                <td>{{ ($row->status == 0)?'Deleted':'Active' }}</td>
                                <td><?php echo date("d M Y", strtotime($row->created_at)); ?></td>
                                <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-link="review/delete/<?php echo $row->id ?>" {{ ($row->status == 0)?'disabled':'' }}><i class="fa fa-trash"></i> </button></td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @include('admin/commons/delete_modal')
                        </tbody>

                    </table>
            <?php echo $model->render(); ?>
            
            </div>
            <?php } else { ?>

                <div class="">
                    No Data found. . .
                </div>
            <?php } 
?>
    </div>
            
</div>

</div>

<script>
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
</script>
@endsection


