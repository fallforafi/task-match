@extends('admin/admin_template')

@section('content')
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div> <!-- end .flash-message -->
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {{count($totalUsers)}}
                    </h3>
                    <p>
                        User Registrations
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{ url('admin/users') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{count($totalTasks)}}
                    </h3>
                    <p>
                        Total Tasks
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-plus"></i>
                </div>
                <a href="{{ url('admin/tasks') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        {{count($totalContacts)}}
                    </h3>
                    <p>
                        Total Contacts
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ url('admin/contactus') }}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
    <br><br>
    <div class="row">
        <div class="col-xs-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Recently Added Users</h3>
                    <div class="box-tools">
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    @if(count($recentTasks) > 0)
                    <table class="table table-hover">
                        <tbody><tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            @foreach($recentUsers as $users)
                            <tr>
                                <td><a href="<?php echo url('admin/user/' . $users->id); ?>">{{$users->id}}</a></td>
                                <td><a href="<?php echo url('admin/user/' . $users->id); ?>">{{$users->firstName}} {{$users->lastName}}</a></td>
                                <td>{{date('d/m/Y',strtotime($users->created_at))}}</td>
                                <?php
                                if ($users->status == 1) {
                                    $status = 'success';
                                    $text = 'Approved';
                                } else {
                                    $status = 'danger';
                                    $text = 'Disapproved';
                                }
                                ?>
                                <td><span class = "label label-{{$status}}">{{$text}}</span></td>
                            </tr>
                            @endforeach
                        </tbody></table>
                        @else
                <div class="col-sm-6">
                    <h3>No data found. . . . </h3>
                </div>
                @endif
                </div><!--/.box-body -->
                
            </div><!--/.box -->
        </div>
        <div class = "col-xs-6">
            <div class = "box">
                <div class = "box-header">
                    <h3 class = "box-title">Recently Added Tasks</h3>
                    <div class = "box-tools pull-right">
                    <!--<button type = "button" class = "btn btn-box-tool" data-widget = "collapse"><i class = "fa fa-minus"></i> </button> -->

                    </div>
                </div><!--/.box-header -->
                <div class = "box-body table-responsive no-padding">
                    @if(count($recentTasks) > 0)
                    <table class = "table table-hover">
                        <tbody><tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            @foreach($recentTasks as $tasks)
                            <tr>
                                <td><a href="<?php echo url('admin/task/' . $tasks->id); ?>">{{$tasks->id}}</a></td>
                                <td><a href="<?php echo url('admin/task/' . $tasks->id); ?>">{{$tasks->title}}</a></td>
                                <td>{{date('d/m/Y',strtotime($tasks->created_at))}}</td>
                                <?php
                                if ($tasks->taskStatus == 'open' || $tasks->taskStatus == 'cancelled') {
                                    $status = 'info';
                                    $text = 'Open';
                                } elseif ($tasks->taskStatus == 'assigned') {
                                    $status = 'warning';
                                    $text = 'Assigned';
                                } else {
                                    $status = 'danger';
                                    $text = 'Completed';
                                }
                                ?>
                                <td><span class = "label label-{{$status}}">{{$text}}</span></td>
                            </tr>
                            @endforeach
                        </tbody></table>
                        @else
                <div class="col-sm-6">
                    <h3>No data found. . . . </h3>
                </div>
                @endif
                </div><!--/.box-body -->
                
            </div><!--/.box -->
        </div>
    </div>
</section>
@endsection
